# Build this image using:
#   docker build -t registry.gitlab.com/emcrp/src .
#
# Run using:
#   docker run -it --rm registry.gitlab.com/emcrp/src

# start from custom tools image
FROM python:2.7.12

# install dependencies
RUN apt update && apt install -y swig3.0 libfftw3-dev
RUN cp /usr/bin/swig3.0 /usr/bin/swig

# copy and resolve dependencies
WORKDIR /usr/emc/src
COPY requirements.txt .
RUN pip install -r requirements.txt

# copy all
COPY . .

# compile C++ extension
RUN python setup.py

# remove cache
RUN rm -rf /var/lib/apt/lists/*

# set runner
CMD python algrun.py
