# EVOLUTIONARY MUSIC COMPOSITION #

## Description ##

In this project we present a music composition system that uses a corpus-based multi-objective evolutionary algorithm. We model the composition process using a Turing-complete virtual register machine with configurable architectures to render musical models. These are evaluated using a series of tests, which judge the statistical similarity of the model against a corpus of real music. Exploring the space of possible parameters, we demonstrate that the methodology succeeds in creating pieces of music that converge towards the properties of the chosen corpus. These pieces exhibit certain musical qualities (repetition and variation) not specifically targeted by our fitness tests;
they emerge solely based on the similarities.

[Project public page with info and resulting MIDIs](https://emcrp.gitlab.io)


## Current contributors ##

- *Csaba Sulyok* - [Babes-Bolyai University, Cluj-Napoca, RO](http://www.cs.ubbcluj.ro/); e-mail: [csaba.sulyok@gmail.com](mailto:csaba.sulyok@gmail.com)
- *Zalán Bodó* - [Babes-Bolyai University, Cluj-Napoca, RO](http://www.cs.ubbcluj.ro/); e-mail: [zbodo@cs.ubbcluj.ro](mailto:zbodo@cs.ubbcluj.ro)
- *Dr. Christopher Harte* - Melodient Ltd., London, UK; e-mail: [chris@melodient.com](mailto:chris@melodient.com)



## Running the application ##

1. Install dependencies

```bash
apt-get install python-dev python-pip swig3.0 libfftw3-dev python-tk
cp /usr/bin/swig3.0 /usr/bin/swig
pip install -r src/requirements.txt
```

2. Build C++ extension

```bash
cd src
python setup.py build_ext
```

3. Run application

```bash
cd src
python algrun.py -h # see command-line arguments
python algrun.py argName1=argValue1 ...
```

