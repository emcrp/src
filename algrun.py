#!/usr/bin/env python

'''
Main entry point of EMC algorithm.
May be configured from command-line using key=value arguments.

'''

from getopt import getopt
from sys import exit, argv

from emc.vm import VirtualMachine
from emc.fitness.ngram import NGramBasedFitnessTestContainer
from emc.model.modeldir import ModelDirectory
from emcpp.wrapper.modelbuilder import ModelBuilderWrapper
from emc.model.modelred import ReducedModelBuilder
from emcpp.wrapper.oci.musicdsloci import MusicDslOciWrapper
from emcpp.wrapper.selector import GenotypeSelectorWrapper
from emc.genetic import GeneticAlgorithm
from emc.population import PopulationBreeder
from emc.saving import GASavingMidiToFile
from helper.cmdparser import parseForEmcAlgRun, printHeader, printHelp
from helper.serializeutils import fromFile
from emc.integ.ceg import KEYPOINT


def example(corpusPath = 'http://www.cimbalom.nl/nepdalok.html',
            omitUrlCheck = True,
            forceRedownload = False,
            modelRepresentation = 'DIATONIC',
            modelKey = 64,
            modelMajor = True,
            fileNameFilter = None,
            preprocessingMethods = ['removeZeroVelocityNotes', 'makeMaxVelocity', 'resampleTo4Ticks',
                                    'forceNoOverlappingNotes', 'clipOverlyLongNotes', 'asuint16', 'reduceModels'],
            fitnessTestContainerClass = NGramBasedFitnessTestContainer,
            numClusters = 8,
            gramSizes = [1,2,3],
            weightDim='length',
            ociClass = MusicDslOciWrapper,
            memSizeInBytes = 65536,
            maxCommandsRatio = 8.0,
            expectedLength = 25.0,
            haltAllowed = False,
            numUnits = 500,
            crossoverRate = 0.002,
            mutationRate = 0.02,
            operatorDist = [0.08, 0.92, 0.0, 0.0],
            complementary = True,
            minProb = 0.15,
            ageFactor = 0.9,
            targetGeneration = 201,
            numberOfRuns = 1,
            loggingCycle = 5,
            cycleSize = 5,
            saveTopMidis = 8,
            saveGrades = True,
            saveAllGrades = False,
            outputDir = '../output',
            **kwargs):

    
    modelDir = ModelDirectory(corpusPath,
                              omitUrlCheck = omitUrlCheck,
                              forceRedownload = forceRedownload,
                              modelRepresentation = modelRepresentation,
                              fileNameFilter = fileNameFilter,
                              preprocessingMethods = preprocessingMethods).read()
    
    fitnessTestContainer = fitnessTestContainerClass(modelDir,
                                                     numClusters = numClusters,
                                                     expectedNumNotes = modelDir.expectedNumNotes(expectedLength),
                                                     expectedLength = expectedLength,
                                                     gramSizes = gramSizes,
                                                     weightDim = weightDim).setUp()
    
    modelBuilderClass = ReducedModelBuilder if modelDir.reduced else ModelBuilderWrapper
    modelBuilder = modelBuilderClass(numTracks = modelDir.numTracks,
                                     modelRepresentation = modelRepresentation,
                                     modelKey = modelKey,
                                     modelMajor = modelMajor)
    
    opCodeInterpreter = ociClass(memSizeInBytes = memSizeInBytes,
                                 expectedOutputs = modelDir.expectedNumNotes(expectedLength) * modelBuilder.bytesPerNote(),
                                 haltAllowed = haltAllowed,
                                 maxCommandsRatio = maxCommandsRatio,
                                 diatonic = modelRepresentation == 'DIATONIC',
                                 key = modelBuilder.modelKey,
                                 isMajor = modelMajor)
    
    phenotypeRenderer = VirtualMachine(opCodeInterpreter = opCodeInterpreter,
                                       modelBuilder = modelBuilder)
    
    genotypeSelector = GenotypeSelectorWrapper(numUnits = numUnits,
                                               numTests = fitnessTestContainer.numTests,
                                               weights = fitnessTestContainer.weights,
                                               complementary = complementary,
                                               minProb = minProb,
                                               ageFactor = ageFactor)
    
    populationBreeder = PopulationBreeder(numUnits = numUnits,
                                          numTests = fitnessTestContainer.numTests,
                                          geneticStringSize = opCodeInterpreter.geneticStringSize,
                                          genotypeSelector = genotypeSelector,
                                          operatorDist = operatorDist,
                                          crossoverRate = crossoverRate,
                                          mutationRate = mutationRate)
    
    saving = GASavingMidiToFile(cycleSize = cycleSize,
                                saveTopMidis = saveTopMidis,
                                outputDir = outputDir,
                                saveGrades = saveGrades,
                                saveAllGrades = saveAllGrades)
        
    algorithm = GeneticAlgorithm(populationBreeder = populationBreeder,
                                 phenotypeRenderer = phenotypeRenderer,
                                 fitnessTestContainer = fitnessTestContainer,
                                 saving = saving,
                                 targetGeneration = targetGeneration,
                                 numberOfRuns = numberOfRuns,
                                 loggingCycle = loggingCycle)
    
    return algorithm



def perform():
    # get command-line options as dict
    argsDict = parseForEmcAlgRun()
    
    if 'fileToResume' in argsDict:
        # previous file given, use that
        algorithm = fromFile(argsDict['fileToResume'])
    else:
        # build algorithm based on dict
        algorithm = example(**argsDict)
    
    algorithm.performFull()


if __name__ == "__main__":
    # MAIN ENTRY POINT OF EMC
    
    printHeader()
    optlist, args = getopt(argv[1:], "h") 
    for (option, value) in optlist: 
        if option == '-h': 
            printHelp()
            exit(0) 
                        
    perform()
