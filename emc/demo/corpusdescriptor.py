'''
Show average corpus descriptor.
'''

from matplotlib.pyplot import *
from numpy import *

from emc.model.modeldir import ModelDirectory
from emcpp.wrapper.descriptor import DescriptorDirectoryWrapper, numDescriptors
from emc.model.model import valueToPitch



if __name__ == '__main__':
    
    # read model
    mdUrl = 'http://www.cimbalom.nl/nepdalok.html'
    mdPpm = ['removeZeroVelocityNotes', 'makeMaxVelocity', 'resampleTo4Ticks',
             'forceNoOverlappingNotes', 'clipOverlyLongNotes', 'asuint16']
    
    #md = ModelDirectory(mdUrl, preprocessingMethods=mdPpm, modelRepresentation='CHROMATIC').read()
    md = ModelDirectory(mdUrl, preprocessingMethods=mdPpm, modelRepresentation='SHIFTED_CHROMATIC').read()
    #md = ModelDirectory(mdUrl, preprocessingMethods=mdPpm, modelRepresentation='DIATONIC').read()
    
    cd = DescriptorDirectoryWrapper(md, numClusters=1).centreDescriptors()
    cd.shape = (1, numDescriptors, 128)
    
    # fig 1 - centre pitch histogram
    figure()
    
    x = arange(128)
    xt = arange(45,94,4)
    y = cd[0, 8, :]
    plot(x, y, color='#105020')
    fill_between(x, y, where = y>=0, color=(0.2, 0.45, 0.25, 0.3))
    
    grid(True)
    xlim([42, 96])
    ylim([0, 12])
    xticks(xt)
    gca().set_xticklabels(valueToPitch(xt, representation=md.modelRepresentation))
    for tick in gca().xaxis.get_major_ticks():
        tick.label.set_fontsize(16)
    xlabel('Pitch value', fontsize=20)
    ylabel('Number of occurrences', fontsize=20)
    tight_layout()

    #savefig('pitchhistcorpus.pdf')
    show()
        
    
    '''
    # fig 2 - centre pitch differential histogram
    figure()
    
    x = arange(-64,64)
    y = cd[0, 9, :]
    plot(x, y, color='#502010')
    fill_between(x, y, where = y>=0, color=(0.45, 0.25, 0.2, 0.3))
    
    grid(True)
    xticks(arange(-16,16,4))
    xlim([-16, 16])
    xlabel('Pitch change rate', fontsize=20)
    ylabel('Number of occurrences', fontsize=20)
    tight_layout()
    
    #savefig('pitchdiffhistcorpus.pdf')
    #show()
    '''
    
    
    # fig 5 - inter-onset interval FFT of one
    '''
    for i in arange(0):
        ioi = md.models[i].interonsets.astype(uint16)
        pf = dd.diffFourier(ioi, 128, False)
        #pf = cd[0, 2, :]
        
        figure()
        plot(arange(pi/128,pi+pi/128,pi/128), pf, color='#502010')
        
        grid(True)
        
        T = array([8,7,6,5,4,3,2,1], dtype=uint8)
        omega = (2 * pi / T) % (2 * pi)
        xticks(omega)
        gca().set_xticklabels(['$\\frac{2\pi}{8}$',
                               '$\\frac{2\pi}{7}$',
                               '$\\frac{2\pi}{6}$',
                               '$\\frac{2\pi}{5}$',
                               '$\\frac{2\pi}{4}$',
                               '$\\frac{2\pi}{3}$',
                               '$\\frac{2\pi}{2}$',
                               '0'])
        for tick in gca().xaxis.get_major_ticks()[:-1]:
            tick.label.set_fontsize(20)
        
        xlabel('Inter-onset interval frequency', fontsize=20)
        ylabel('FFT value', fontsize=20)
        xlim([-0.1, pi+0.1])
        tight_layout()
        
        #savefig('ioifftcorpus.pdf')
    show()
    '''