'''
Demo of changing number of clusters.

Given a model directory (corpus), we test the scores the models themselves
would get when tested, given we change the number of clusters used.

1 cluster means we always compare to the mean of all descriptors.
n clusters (for n models) means we compare to each of the descriptors, and
the minimal distance wins each time.
'''

from matplotlib.pyplot import plot, legend, xlabel, ylabel, show, tight_layout, grid,\
    xlim, xticks, stackplot, gca, savefig
from numpy import array, zeros, arange, histogram, float32, min, max, mean

from emc.model.modeldir import ModelDirectory
from emc.fitness.ngram import NGramBasedFitnessTestContainer
from os.path import exists
from helper.serializeutils import fromFile, toFile
from tymed import lap
from matplotlib import rcParams


#allNumClusters = array([1,2,4,8,16,32])
allNumClusters = array([2,4,8,16])
gramSizes = [1,2,3]
weightDim = 'length'

b = 50

minf = 0.0
maxf = 1.0

markers = ['o', '<', 'v', '>', 'v', 'd']
colors = ['#903030',
          '#309030',
          '#303090',
          '#808040',
          '#804080',
          '#408080']
linestyles = [':', '-.', '--', '-']
linewidths = [4.0, 2.5, 2.5, 2]

if __name__ == '__main__':
    
    # calculation
    
    #set_printoptions(threshold='nan', precision=3, suppress=True)
    rcParams['pdf.fonttype'] = 42

    md = ModelDirectory('http://www.cimbalom.nl/nepdalok.html',
                        preprocessingMethods=['removeZeroVelocityNotes', 'makeMaxVelocity', 'resampleTo4Ticks',
                                              'forceNoOverlappingNotes', 'clipOverlyLongNotes', 'asuint16', 'reduceModels']).read()
    
    grades = zeros((md.numModels, 9, allNumClusters.size), dtype=float32)
    overallGrades = zeros((md.numModels, allNumClusters.size), dtype=float32)
    hists = zeros((b, allNumClusters.size), dtype=float32)
    bins = arange(minf, maxf+0.00001, (maxf-minf)/b)
    binCenters = (bins[1:]+bins[:-1])/2
    
    for numClusterIdx, numClusters in enumerate(allNumClusters):
        gradesCacheName = '.grades-k%02d-gramSizes%s-weightDim%s.bin' %(numClusters, str(gramSizes).replace(' ', ''), weightDim)
        if exists(gradesCacheName) and False:
            print 'Reading for %d clusters' %(numClusters)
            grades[:, : numClusterIdx], overallGrades[:, numClusterIdx] = fromFile(gradesCacheName)
        else:
            print 'Calculating for %d clusters' %(numClusters)
            ftc = NGramBasedFitnessTestContainer(md, numClusters=numClusters, gramSizes=gramSizes, weightDim=weightDim).setUp()
        
            for modelIdx, model in enumerate(md.models):
                grades[modelIdx, :, numClusterIdx], overallGrades[modelIdx, numClusterIdx] = ftc.runOnModel(model)
            
            print 'Calculated grades in %.3f seconds' %(lap(ftc.runOnModel))
            toFile((grades[:, : numClusterIdx], overallGrades[:, numClusterIdx]), gradesCacheName)
            
        hists[:, numClusterIdx] = histogram(overallGrades[:, numClusterIdx], bins)[0]
    
    
    #print min(grades), max(grades)
    print 'Grades by K:', mean(overallGrades, axis=0)
    print 'Grades by test type:', mean(mean(grades, axis=2), axis=0)
    
    # plotting
    
    labels=['K=%d' %(numClusters) for numClusters in allNumClusters]
    
    '''
    shists = sum(hists, axis=1)
    for i in arange(hists.shape[0]):
        if shists[i] == 0:
            hists[i,:] = 1.0 / hists.shape[1]
        else:
            hists[i,:] /= shists[i]
    '''
    #stackplot(binCenters, hists.T, labels=labels)
    #stackplot(binCenters, hists.T, labels=labels, baseline='wiggle')
    
    
    for numClusterIdx, numClusters in enumerate(allNumClusters):
        label = '%d cluster' %(numClusters)
        if numClusters > 1: label += 's'
        
        plot(binCenters,
             hists[:, numClusterIdx], 
             linestyle=linestyles[numClusterIdx],
             linewidth=linewidths[numClusterIdx], 
             #marker=markers[numClusterIdx], 
             color=colors[numClusterIdx],
             label=label)
    
    
    grid(True)
    legend(loc=2, fontsize=16)
    
    #xticks(bins)
    xticks(arange(0.0, 1.05, 0.1))
    for tick in gca().xaxis.get_major_ticks():
        tick.label.set_fontsize(14)
    for tick in gca().yaxis.get_major_ticks():
        tick.label.set_fontsize(14) 
    xlabel("Fitness scores", fontsize=20)
    ylabel("# corpus members", fontsize=20)
    xlim([minf, maxf])
    
    tight_layout()
    
    savefig('corpusgradesnepdalok.pdf')
    show()