from numpy import array, float32, histogram, arange, uint32, zeros, max, exp
from matplotlib.pyplot import bar, show, plot, xlabel, ylabel, xlim, xticks, gca,\
    grid, savefig, tight_layout

from emc.model.modeldir import nepdalokModelDirectory
from emc.fitness.entropy import corpusEntropy, entropyReducedModel


def renderEntropyNormal():
    md = nepdalokModelDirectory()
    
    es = array([entropyReducedModel(m) for m in md.models], dtype=float32)
    
    b = arange(0.0, 1.0, 0.04)
    bc = (b[1:] + b[:-1]) / 2.0
    w = 0.5 * (b[1] - b[0])
    
    h = histogram(es[:,2], bins=b)[0].astype(float32)
    h /= max(h)
    bar(bc, h, align='center', linewidth=2, edgecolor='#403020', width=w, color='#706860')
    
    mus, sigmas = corpusEntropy(md)
    mu = mus[2]
    sig = sigmas[2]
    
    nx = arange(0.0, 1.0, 0.005)
    ny = exp(-(nx - mu) ** 2 / (2 * sig ** 2))
    
    plot(nx, ny, 'k', linewidth=4, label='Deduced normal')
    
    xlabel('Entropy value', fontsize=18)
    ylabel('Resulting fitness', fontsize=18)
    xticks(arange(0.0, 1.0, 0.1))
    xlim([0.15, 0.75])
    grid(True)
    for tick in gca().xaxis.get_major_ticks():
        tick.label.set_fontsize(16)
    for tick in gca().yaxis.get_major_ticks():
        tick.label.set_fontsize(16)
    
    tight_layout()
    #savefig('corpusentropynormal.pdf')
    show()


if __name__ == '__main__':
    renderEntropyNormal()