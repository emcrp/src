from matplotlib.pyplot import plot, show
from numpy import mean, std, zeros, float32, arange, histogram

from emc.fitness.normal import GivenNumNotesFitnessTest
from emc.model.modeldir import bachModelDirectory
from emcpp.swig.emcpp import randomByteArray
from emcpp.wrapper.modelbuilder import ModelBuilderWrapper
from emcpp.wrapper.oci.immediateoci import ImmediateOciWrapper


if __name__ == '__main__':
        
    md = bachModelDirectory()
    gnnft = GivenNumNotesFitnessTest(mu  =  [mean(md.numNotes, axis=0)],
                                     sig =  [std (md.numNotes, axis=0)])
    print gnnft.mu
    
    oci = ImmediateOciWrapper(25000, 2600, True)
    mb = ModelBuilderWrapper(numTracks = md.numTracks)
    
    num = 1000
    
    grades = zeros(num, dtype=float32)
    
    for i in arange(num):
        data = randomByteArray(oci.geneticStringSize)
        
        vmResult = oci.geneticStringToOutput(data)
            
        model = mb.bytesToModel(vmResult)
        grades[i] = gnnft.normalValueOf([model.numNotes])
    
    
    hist = histogram(grades)
    plot(hist[1][:-1], hist[0])
    show()
