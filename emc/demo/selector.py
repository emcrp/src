from numpy import arange, float32, uint16, sort, histogram, float64, array
from matplotlib.pyplot import plot, show, bar, figure, grid, yticks

from emcpp.swig.emcpp import choose, chooseUnique, applyMinProb
from emcpp.wrapper.selector import GenotypeSelectorWrapper
from emc.population import Population


if __name__ == '__main__':
    
    # 1. aux methods from bytearray.cpp
    
    npoints = 10000
    nbars = 20
    minProb = 0.2
    
    grades = arange(0.0, 1.0, 1.0/npoints, dtype=float32)
    
    gradesMP = applyMinProb(grades, len(grades), minProb)
    ch = choose(gradesMP, npoints)
    '''
    h, _ = histogram(ch, bins=nbars)
    h = h.astype(float64) / sum(h) * nbars / 2
    plot([0, nbars-1], [minProb / 2, 1.0 - (minProb / 2)], 'r')
    bar(arange(nbars), h, width=0.5)
    #show()
    '''
    
    # 2. selector from selector.cpp
    ntests = 1
    weights = array([1.], dtype=float32)
    
    gs = GenotypeSelectorWrapper(npoints, ntests, weights, complementary=False, minProb=minProb)
    gs.setDebug(True)
    pop = Population(npoints, ntests)
    pop.grades[:] = grades
    
    _, ch, _, _ = gs.chooseUnits(pop, 0, npoints, 0, 0)
    
    figure()
    h, _ = histogram(ch, bins=nbars)
    h = h.astype(float64) / sum(h) * nbars / 2
    plot([0, nbars-1], [minProb / 2, 1.0 - (minProb / 2)], 'r')
    bar(arange(nbars), h, width=0.5)
    grid(True)
    yticks(arange(0., 1.1, .1))
    show()
    
    