'''
Fitness tests related to a MIDI corpus.
From MIDI files, a Model directory is built up,
and all properties and diffs are compared.

'''
from numpy import zeros, mean, sum, std, float32, frombuffer, array, copy
from tymed import tymedCls, tymed

from emc.fitness.normal import GivenLengthFitnessTest, GivenNumNotesFitnessTest, \
    EntropyFitnessTest
from emcpp.wrapper.descriptor import DescriptorDirectoryWrapper, numDescriptors



@tymedCls
class DescriptorBasedFitnessTestContainer(object):
    '''
    Fitness test container which contains tests based on a corpus and the histogram descriptor
    built from it.
    '''
    
    '''
    Number of tests is fixed.
    '''
    numTests = numDescriptors + 3
    
    '''
    Default value for weights
    '''
    defaultWeights = array([8,1,8,
                            8,2,3,1,
                            3,2,1,1,
                            15,6,6,1], dtype=float32)
    
    
    def __init__(self, modelDir, numBins = 128, fourierSize = 2048, numClusters = 1,
                 weights = None, expectedLength = None, **kwargs):
        '''
        Initialize by props necessary for descriptor directory.
        Also configure given length and numNotes tests from model directory.
        '''
        
        print 'Initializing descriptor-based fitness test container'
        self.dd = DescriptorDirectoryWrapper(modelDir, numBins, fourierSize, numClusters)
        if weights is None:
            self.weights = copy(self.defaultWeights)
        else:
            self.weights = copy(weights)
        self.normalize()
            
        if expectedLength is None:
            lengthRatio = 1.0
        else:
            # times 8 because there are 8 ticks per second and expectedLength is in seconds
            lengthRatio = 8.0 * expectedLength / float32(mean(modelDir.lengths))
        
        self.glft = GivenLengthFitnessTest   (mu  = [lengthRatio * mean(modelDir.lengths)],
                                              sig = [lengthRatio * std (modelDir.lengths)])
        self.gnnft = GivenNumNotesFitnessTest(mu  = [lengthRatio * mean(modelDir.numNotes, axis=0)],
                                              sig = [lengthRatio * std (modelDir.numNotes, axis=0)])
        self.eft = EntropyFitnessTest        (mu  =  mean(modelDir.entropies(), axis=0),
                                              sig =  std (modelDir.entropies(), axis=0))
    
    
    def setUp(self):
        return self
    
    def names(self):
        '''
        Get list of fitness test names.
        '''
        return [self.glft.name,  self.gnnft.name,     self.eft.name,
                'IOI hist',      'IOI diffHist',      'IOI Fft',          'IOI diffFft',
                'Duration hist', 'Duration diffHist', 'Duration Fourier', 'Duration diffFft',
                'Pitch hist',    'Pitch diffHist',    'Pitch Fourier',    'Pitch diffFft']
                #'Vel hist',      'Vel diffHist',      'Vel Fourier',      'Vel diffFft']
    
    
    @tymed
    def runOnModel(self, model, grades=None):
        if grades is None:
            grades = zeros(self.numTests, dtype=float32)
        grades[0] = self.glft.run(model)
        grades[1] = self.gnnft.run(model)
        grades[2] = self.eft.run(model)
        grades[3:] = mean(self.dd.modelFitness(model), axis=0)
        overallGrade = sum(grades * self.weights)
        return (grades, overallGrade)
    
    
    def normalize(self):
        '''
        Recalculates weights of fitness test container
        to be summed to 1
        '''
        if sum(self.weights) != 0:
            self.weights /= sum(self.weights)
        else:
            self.weights[:] = 1.0 / self.numTests


    def __getstate__(self):
        '''
        Do not cache weights as numpy array.
        '''
        weightsBA = bytearray(self.weights)
        return (self.dd, self.glft, self.gnnft, self.eft, weightsBA)
    
    
    def __setstate__(self, state):
        '''
        Do not cache weights as numpy array.
        '''
        self.dd, self.glft, self.gnnft, self.eft, weightsBA = state
        self.weights = frombuffer(weightsBA, dtype=float32)

