from numpy import array, uint8, unique, log2, sum, zeros, arange, float32, mean,\
    std, exp, histogram
from numpy.random import random
from emcpp.swig.emcpp import entropy
from tymed import tymed


def entropy2(data):
    '''
    Measure binary Shannon entropy of a 2-D data set per column.
    Returns value between 0 and 255 if casting needed to uint8.
    '''
    cols = data.shape[1]
    ret = zeros(data.shape[1], dtype=float32)
    
    for col in arange(cols):
        ret[col] = entropy(data[:, col])
    
    return ret


def entropyModel(model):
    '''
    Measure entropy of model.
    Only take IOI, duration and pitch.
    Values per track are flattened into a vector.
    '''
    ret = zeros([model.numTracks * 3], dtype=float32)
    for trackIdx, track in enumerate(model.tracks):
        ret[trackIdx*3] = entropy(track.notes[:, 0])
        ret[trackIdx*3 + 1] = entropy(track.notes[:, 1])
        ret[trackIdx*3 + 2] = entropy(track.notes[:, 4])
    return ret


def entropyReducedModel(model):
    return [entropy(model.durations, 3),
            entropy(model.pitches, 5),
            entropy(model.durationsAndPitches)]


def corpusEntropy(modelDir):
    de  = [entropy(m.durations, 3) for m in modelDir.models]
    pe  = [entropy(m.pitches, 5) for m in modelDir.models]
    dpe = [entropy(m.durationsAndPitches) for m in modelDir.models]
    
    mus    = [mean(de), mean(pe), mean(dpe)]
    sigmas = [std(de), std(pe), std(dpe)]
    return mus, sigmas


@tymed
def entropyFitness(model, mus, sigs):
    me = entropyReducedModel(model)
    # print me[0], mus[0], sigs[0]
    return [exp(-(me[i] - mus[i]) ** 2 / (2 * sigs[i] ** 2)) for i in range(3)]


if __name__ == '__main__':
    from emc.model.modeldir import nepdalokModelDirectory
    from time import time
    
    a1 = array([1,1,1,1,1,1,1,1], dtype=uint8)
    a2 = array([1,2,1,2,1,2,1,2,1,2,1,2], dtype=uint8)
    a3 = array([1,2,3,1,4,3,1,2,3], dtype=uint8)
    a4 = array(random(1000) * 256, dtype=uint8)
    
    for a in [a1, a2, a3, a4]:
        print entropy(a)
    
    print '3 bit entropy', entropy(array(random(10000) * 8, dtype=uint8), 3)
    print '5 bit entropy', entropy(array(random(10000) * 32, dtype=uint8), 5)
    print '8 bit entropy', entropy(array(random(10000) * 256, dtype=uint8))
    
    print 'Corpus entropy'
        
    md = nepdalokModelDirectory()
    mus, sigmas = corpusEntropy(md)
    print 'Mus:', mus, 'Sigmas:', sigmas
    
    st = time()
    ef = [entropyFitness(m, mus, sigmas) for m in md.models]
    print "Calculated %d entropies in %.3f seconds" %(md.numModels, time() - st)
    h, hb = histogram(ef, bins=arange(0.0, 1.01, 0.1))
    print 'Corpus entropy fitnesses:', h
