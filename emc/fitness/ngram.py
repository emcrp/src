from numpy import zeros, float32, array, dot, max, log2, round, int8, uint8,\
    uint16, uint32
from tymed import tymedCls, tymed
from collections import Counter
from math import sqrt
from scipy.sparse import csr_matrix

from sklearn.preprocessing import normalize
from sklearn.cluster import KMeans

from helper.serializeutils import fromFile, toFile
from os.path import exists
from time import time
from emcpp.swig.emcpp import NGramHelper
from emc.fitness.entropy import corpusEntropy, entropyFitness, entropyReducedModel


@tymed
def ngrams(data, gramSizes):
    '''
    Computes n-grams for data
    '''
    return Counter([gram for n in gramSizes for gram in zip(*[data[i:] for i in range(n)])])
 
def make_index_from_data(data):
    '''
    Indexes items occurring in data. 
    Used for converting dict/Counter representation to sparse matrix.
    '''
    index = set()
    for d in data:
        index = index.union(d.keys())
    lindex = list(index)
    return {lindex[i]:i for i in range(len(lindex))}

def csr_to_dict_with_iindex(csr, iindex):
    '''
    Converts csr_matrix to dictionary using the `iindex` inverted index.
    '''
    return {iindex[x]:csr[0, x] for x in csr.nonzero()[1]}

@tymed
def dot_counters(a, b):
    '''
    Dot product of two dicts.
    IMPORTANT: the first parameter should be the one with fewer expected nonzero elements!
    '''
    #return sum(v * b.get(k,0) for k, v in a.items())
    return sum([a[k]*b[k] for k in a if k in b])
    
def norm_counter(c):
    '''
    Euclidean norm of dict.
    '''
    return sqrt(sum([x**2 for x in c.values()]))

def dict_to_csr(data, shape):
    '''
    Converts dicts to csr_matrix.
    '''
    rows, cols, vals = [], [], []
    for i in range(len(data)):
        rows.extend([i] * len(data[i].keys()))
        cols.extend(data[i].keys())
        vals.extend(data[i].values())
    return csr_matrix((vals, (rows, cols)), shape=shape, dtype=float32)

def idf(data):
    '''
    Inverse document frequency (idf = specificity).
    (Borrowed from IR, see the tf-idf weighting.)
    '''
    count = Counter()
    for d in data:
        count.update(d.keys())
    return {x:float(len(data))/count[x] for x in count.keys()}

def weight_data_by_ngram_length(data):
    '''
    Reweights data only by the length of the indexing n-gram.
    Can modify the weight, e.g. use len(key)**2 instead of just len(key), i.e. some function of the n-gram length.
    '''
    return {key:len(key)*data[key] for key in data.keys()}

def weight_data_by_precomputed_weights(data, weights):
    '''
    Reweights data using the weights from `weights` obtained from idf 
    '''
    return {key:data[key]*weights.get(key, 1) for key in data.keys()}

def weight_data_by_precomputed_weights_and_ngram_length(data, weights):
    '''
    Reweights using n-gram length and idf 
    '''
    return {key:len(key)*weights.get(key, 1)*data[key] for key in data.keys()}

def tupleKeyToInt(k):
    if len(k) == 1:
        return (1 << 24) | k[0]
    elif len(k) == 2:
        return (2 << 24) | (k[0] << 8) | k[1]
    else:
        return (3 << 24) | (k[0] << 16) | (k[1] << 8) | k[2]


@tymedCls
class NGramBasedFitnessTestContainer(object):
    '''
    Activate this class with cmd arg "fitnessTestContainerClass=NGram"
    '''

    def __init__(self, modelDir, gramSizes=[1,2,3], expectedNumNotes=None,
                 numClusters=2, weightDim='none', **kwargs):
        '''
        Initialize test container.
        Any pre-processing/data-gathering may be done here.
        '''
        #print 'Initializing n-gram-based fitness test container'
        self.modelDir = modelDir
        self.gramSizes = gramSizes
        self.expectedNumNotes = expectedNumNotes
        self.numClusters = numClusters
        self.weightDim = weightDim
        
        if weightDim == 'none':
            self.weighting = lambda x, z: x
        elif weightDim == 'length':
            self.weighting = lambda x, z: weight_data_by_ngram_length(x)
        elif weightDim == 'idf':
            self.weighting = lambda x, z: weight_data_by_precomputed_weights(x, z)
        elif weightDim == 'lengthandidf':  # both
            self.weighting = lambda x, z: weight_data_by_precomputed_weights_and_ngram_length(x, z)
        else:
            print "WARNING: Unsupported weightDim=%s provided, behavior unpredictable" %(self.weightDim)

    
    def setUp(self):
        '''
        separate setting up from constructor
        '''
        # Clustering
        cacheFile = self.cacheFile()
        if exists(self.cacheFile()):
            print 'Cluster cache file %s exists, using it as starting point' %(cacheFile)
            self.w, self.matrices, self.cluster_centers = fromFile(cacheFile)
        else:
            print 'Cluster cache file %s missing, recalculating' %(cacheFile)
            self.performClustering()
        
        # load C++ library
        self.ngram_helper = NGramHelper(self.numNGramTests, self.numClusters,
                                        uni=1 in self.gramSizes,
                                        bi=2 in self.gramSizes,
                                        tri=3 in self.gramSizes)
        
        # set cluster centres in C++ lib
        for c in range(self.numClusters):
            for t in range(self.numNGramTests):
                keys = array([tupleKeyToInt(k) for k in self.cluster_centers[t][c].keys()], dtype=uint32)
                values = self.cluster_centers[t][c].values()
                self.ngram_helper.setClusterCenters(c, t, keys, values)
        
        # set up entropy mu and sigmas
        self.mus, self.sigmas = corpusEntropy(self.modelDir)
        
        return self
        

    
    def featuresFromModel(self, model):
        '''
        Extract features from model.
        Based on model representation.
        '''
        if self.modelDir.reduced:
            return [model.durations,     model.pitches,    model.durationsAndPitches,
                    model.durationDiffs, model.pitchDiffs, model.durationAndPitchDiffs]
        else:
            return [model.durations.astype(uint8),     model.pitches.astype(uint8),
                    model.durationDiffs.astype(uint8), model.pitchDiffs.astype(uint8)]
        
    
    def cacheFile(self):
        '''
        Cache file name for cluster centres.
        Based on cache file of inherent model directory.
        '''
        return '%s-ngramClusterCentres-numClusters%d-gramSizes%s-weightDim%s' %(
            self.modelDir.cacheFile(),self.numClusters, str(self.gramSizes).replace(' ', ''), self.weightDim)


    
    def performClustering(self):
        '''
        Perform preprcoessing and write to cache file.
        '''
        startTime = time()
        
        print("Calculating n-grams data")
        self.ngrams = [[] for i in range(self.numNGramTests)]
                
        # Calculating n-gram representations of data
        for i in range(self.modelDir.numModels):
            features = self.featuresFromModel(self.modelDir.models[i])
            for j in range(self.numNGramTests):
                self.ngrams[j].append(ngrams(features[j], self.gramSizes))
                
        self.indices = [make_index_from_data(self.ngrams[i]) for i in range(self.numNGramTests)]
        
        # Calculating IDF:
        self.w = [idf(self.ngrams[i]) for i in range(self.numNGramTests)]

        # Reweighting:
        for i in range(self.modelDir.numModels):
            for j in range(self.numNGramTests):
                # Reweighting:
                self.ngrams[j][i] = self.weighting(self.ngrams[j][i], self.w[j])
                # Convert keys using the indices in order to be able to convert then to csr_matrix
                self.ngrams[j][i] = {self.indices[j][x]:self.ngrams[j][i][x] for x in self.ngrams[j][i].keys()}

        
        # Building sparse matrices for the n-gram representation: 
        self.matrices = [0] * self.numNGramTests
        for i, data in enumerate(zip(self.ngrams, self.indices)):
            self.matrices[i] = dict_to_csr(data[0], shape=(len(data[0]), len(data[1])))
            
            
        print("Clustering, K=%d, this may take a while..." %(self.numClusters))
        
        self.cluster_centers = [0] * self.numNGramTests  # sklearn clustering objects
         
        for k, M in enumerate(self.matrices):
            clustering = KMeans(n_clusters=self.numClusters, n_jobs=-1)
            clustering.fit(M)
#             clustering.fit(M[0:2,:])
            # Convert back to dict/Counter (cluster_centers_ will be of type ndarray):
#             csr = csr_matrix(clustering.cluster_centers_, dtype=float32)
            csr = csr_matrix(normalize(clustering.cluster_centers_), dtype=float32)
            # Convert back to original keys:
            self.cluster_centers[k] = [csr_to_dict_with_iindex(csr[i,:], {self.indices[k][x]:x for x in self.indices[k].keys()}) for i in range(csr.shape[0])]

        cacheFile = self.cacheFile()
        print("n-gram calc & clustering done in %.3fs, writing to cache file %s" %(time() - startTime, cacheFile))
        
        cacheFileContent = (self.w, self.matrices, self.cluster_centers)
        toFile(cacheFileContent, cacheFile)


    
    @tymed
    def runOnModelOld(self, model, grades=None):
        '''
        Runs fitness test for one model.
        '''
        if grades is None:
            grades = zeros(self.numTests, dtype=float32)
        if self.expectedNumNotes != None and model.numNotes < self.expectedNumNotes:
            return (grades, 0.)
            
        # n-gram representation of `model`:
        data = [ngrams(feature, self.gramSizes) for feature in self.featuresFromModel(model)]
        d = [self.weighting(data[i], self.w[i]) for i in range(self.numNGramTests)]
        nd = [norm_counter(d[i]) for i in range(self.numNGramTests)]
        
        # Calculating the grades (= dot product):
        for i in range(self.numNGramTests):
            grades[i] = max([dot_counters(d[i], self.cluster_centers[i][j]) / nd[i] for j in range(len(self.cluster_centers[i]))])
#             print grades[i]
        
        # entropy tests
        grades[self.numNGramTests:] = entropyFitness(model, self.mus, self.sigmas)
        # Maximum Entropy approach:
        # grades[self.numNGramTests:] = entropyReducedModel(model)
        
        overallGrade = dot(grades, self.weights)
#         print overallGrade
        
        return (grades, overallGrade)
    
    
    @tymed
    def runOnModel(self, model, grades=None):
        '''
        Runs fitness test for one model.
        '''
        if grades is None:
            grades = zeros(self.numTests, dtype=float32)
        
        if self.expectedNumNotes != None and model.numNotes < self.expectedNumNotes:
            return (grades, 0.)
            
        # n-gram representation of `model`:
        features = self.featuresFromModel(model)
        if self.weightDim == 'length':
            for i, f in enumerate(features):
                self.ngram_helper.setModelFeatureWeighted(i, f)
        else:
            for i, f in enumerate(features):
                self.ngram_helper.setModelFeature(i, f)
                
        grades[:self.numNGramTests] = self.ngram_helper.grades(self.numNGramTests)
        grades[self.numNGramTests:] = entropyFitness(model, self.mus, self.sigmas)
        # Maximum Entropy approach:
        # grades[self.numNGramTests:] = entropyReducedModel(model)
                
        overallGrade = dot(grades, self.weights)
        return (grades, overallGrade)
    
    
    @property
    def numTests(self):
        '''
        Number of tests this container uses for multi-objectivity.
        '''
        return self.numNGramTests + 3
    
    
    @property
    def numNGramTests(self):
        '''
        Number of tests conducted with n-grams
        '''
        return 6 if self.modelDir.reduced else 4
    
    
    @property
    def weights(self):
        '''
        Returns weights used for different tests.
        Must be array of size self.numTests
        '''
        return array([1.0/self.numTests] * self.numTests, dtype=float32)
    
    
    def __getstate__(self):
        '''
        What to save in the state of this object. Should return tuple.
        All numpy numeric arrays should be made into bytearrays, otherwise the pickled version is large AF.
        '''
        return (self.modelDir, self.expectedNumNotes, self.gramSizes, self.numClusters, self.weightDim)
    
    
    def __setstate__(self, state):
        '''
        Restore state of serialized object.
        Numpy arrays serialized as bytearrays are reconstructed using frombuffer(<ba>, dtype=<datatype>)
        Warning: __init__ not called if reading serialized version. 
        '''
        modelDir, expectedNumNotes, gramSizes, numClusters, weightDim = state
        self.__init__(modelDir=modelDir,
                      expectedNumNotes=expectedNumNotes,
                      gramSizes=gramSizes,
                      numClusters=numClusters,
                      weightDim=weightDim)


if __name__ == '__main__':
    #from timeit import default_timer as timer
    from emc.model.modeldir import ModelDirectory
    from emc.integ.midi import readFromMidi
    
    modelDir = ModelDirectory('http://www.cimbalom.nl/nepdalok.html',
                              preprocessingMethods = ['removeZeroVelocityNotes', 'makeMaxVelocity', 'resampleTo4Ticks', 
                                                      'forceNoOverlappingNotes', 'clipOverlyLongNotes', 'asuint16', 'reduceModels']).read()
    
    ftc = NGramBasedFitnessTestContainer(modelDir=modelDir, 
                                         gramSizes=[1,2,3], 
                                         numClusters=8,
                                         weightDim='length').setUp()
    #ftc.performClustering()
    for i in range(5):
        grades, overallGrades = ftc.runOnModel(modelDir.models[i])
        print grades, overallGrades
        grades, overallGrades = ftc.runOnModelOld(modelDir.models[i])
        print grades, overallGrades
        print ''
    
    start = time()
    for i in range(1024):
        ftc.runOnModelOld(modelDir.models[i])
    end = time()
    print("Old time: {}".format(end - start))
    
    start = time()
    for i in range(1024):
        ftc.runOnModel(modelDir.models[i])
    end = time()
    print("New time: {}".format(end - start))
    
    
    