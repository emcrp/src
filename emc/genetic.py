'''
Container for genetic algorithm runner.
Configurable instances of all configurations, population number and generation number
may come from a config file configuring the instance for the class below.
'''
from time import time
from numpy import arange, min, max, mean
from helper.git import gitRevision, verifyGitRevision
from helper.serializeutils import toFile
from emc.population import Population
from tymed import lap
from emcpp.swig.emcpp import randomByteArrayInpl


class GeneticAlgorithm(object):
    '''
    Class for running a genetic algorithm based on configurable breeders, renderers,
    genotypes, phenotypes etc.
    '''
    
    def __init__(self, populationBreeder = None,
                       phenotypeRenderer = None,
                       fitnessTestContainer = None,
                       targetGeneration = 20000,
                       numberOfRuns = 5,
                       loggingCycle = 10,
                       saving = None):
        '''
        Build algorithm before first run.
        For building from previous runs, see __getstate__ and __setstate__
        '''
        self.populationBreeder = populationBreeder
        self.phenotypeRenderer = phenotypeRenderer
        self.fitnessTestContainer = fitnessTestContainer
        self.targetGeneration = targetGeneration
        self.numberOfRuns = numberOfRuns
        self.loggingCycle = loggingCycle
        self.saving = saving
        
        self.population = None
        self.generation = 0
        self.running = False
        
        self.usedGitRevision = gitRevision()
    
    
    def __getstate__(self):
        '''
        Serialization
        '''
        return (self.generation,
                self.populationBreeder,
                self.phenotypeRenderer,
                self.fitnessTestContainer,
                self.targetGeneration,
                self.numberOfRuns,
                self.loggingCycle,
                self.saving,
                self.population,
                self.usedGitRevision)
    
    
    def __setstate__(self, state):
        '''
        Serialization
        '''
        self.generation, self.populationBreeder, self.phenotypeRenderer, self.fitnessTestContainer,\
            self.targetGeneration, self.numberOfRuns, self.loggingCycle, self.saving,\
            self.population, self.usedGitRevision = state
        
        # verify git revision is the same as current
        verifyGitRevision(self.usedGitRevision)
    
    
    def save(self, fileName):
        '''
        Save current state of algorithm to file
        '''
        toFile(self, fileName)
        
    
    def performFull(self):
        '''
        Perform full algorithm, using set target generation
        and number of runs.
        '''
        self.running = True
        
        for _ in arange(self.numberOfRuns):
            self._performForGenerations(self.targetGeneration)
            self.reset()
        
        
    def performUntilGeneration(self, generationCount):
        '''
        Main method for performing the genetic algorithm for until a given number of generations is reached.
        '''
        while self.generation < generationCount:
            self._performForNextGeneration()
        self.saving.finishRun()
    
    
    def _performForGenerations(self, generationCount):
        '''
        Main method for performing the genetic algorithm for a given number of generations.
        '''
        target = self.generation + generationCount
        while self.generation < target:
            self._performForNextGeneration()
        self.saving.finishRun()
        
        
    def _performForNextGeneration(self):
        '''
        Perform next generation. If first, do generation zero,
        if not, do normal generation.
        '''
        if self.generation == 0:
            self._performForGenerationZero()
        else:
            self._performForOneGeneration()
  
    
    
    def _performForGenerationZero(self):
        '''
        Method for creating and evaluating generation zero
        '''
        startTime = time()
        
        self.saving.init(self, self.usedGitRevision, self.populationBreeder.numUnits, self.fitnessTestContainer.numTests)
        self.populationBreeder.numTests = self.fitnessTestContainer.numTests
        self.populationBreeder.weights = self.fitnessTestContainer.weights
        
        self.population = Population(self.populationBreeder.numUnits, self.populationBreeder.numTests,
                                     self.populationBreeder.geneticStringSize)
        
        for unitIdx in range(self.population.numUnits):
            randomByteArrayInpl(self.population.genotypes[unitIdx])
            self.population.phenotypes[unitIdx] = self.phenotypeRenderer.run(self.population.genotypes[unitIdx])
            _, self.population.grades[unitIdx] = self.fitnessTestContainer.runOnModel(self.population.phenotypes[unitIdx], self.population.gradesPerTest[unitIdx])
        
        self.printInfo(time()-startTime)
        self.saving.afterGeneration()
        
        self.generation += 1
        return self.population
        
    
    
    def _performForOneGeneration(self):
        '''
        Method for breeding and evaluating a generation
        '''
        
        if self.saving.algorithm is None:
            self.saving.init(self, self.usedGitRevision, self.populationBreeder.numUnits, self.fitnessTestContainer.numTests)
            
            
        startTime = time()
        
        self.population = self.populationBreeder.breedNewGeneration(self.population)
        for unitIdx in range(self.population.numUnits):
            self.population.phenotypes[unitIdx] = self.phenotypeRenderer.run(self.population.genotypes[unitIdx])
            _, self.population.grades[unitIdx] = self.fitnessTestContainer.runOnModel(self.population.phenotypes[unitIdx], self.population.gradesPerTest[unitIdx])
        
        if self.generation % self.loggingCycle == 0:
            self.printInfo(time() - startTime)
            
        self.saving.afterGeneration()
        
        self.generation += 1
        return self.population
    

    def printInfo(self, fullTime):
        print "Generation %d" %self.generation
        print "Output: %s" %self.saving.outputDir
        print "Grades: Min/Mean/Max: %.3f, %.3f, %.3f" %(min(self.population.grades),
                         mean(self.population.grades), max(self.population.grades))
        print "Time:   G/Ph/F/Total: %.3f, %.3f, %.3f, %.3f" %(lap(self.populationBreeder.breedNewGeneration) / self.loggingCycle,
                                                               lap(self.phenotypeRenderer.run) / self.loggingCycle,
                                                               lap(self.fitnessTestContainer.runOnModel) / self.loggingCycle,
                                                               fullTime)
    
    
    def reset(self):
        '''
        Reset state of algorithm.
        Retains all builders and renderers, but resets generation to 0, and flushes any remaining data.
        '''
        self.saving.finishRun()
        self.generation = 0
        self.saving.reset()