'''
Recreating the Spiral Array model and the center of effects generator
from Chew (2001) for key detection in EMC models.
'''
from numpy import array, float32, uint8, sqrt, zeros, arange, repeat,\
    sum, argsort, mean, round, argmin, int16
from numpy.core.numeric import matmul
from copy import deepcopy


# setup

KEYPOINT = 64 # midpoint in MIDI scale where 0 is mapped in any other representation than 

h = sqrt(2.0/15.0)
w = array([0.516, 0.315, 0.169], dtype=float32)

H = array([0,0,h], dtype=float32)
R = array([[0,1,0], [-1,0,0], [0,0,1]], dtype=float32)

# build up 1 octave of the spiral + extra space for CM,TM,etc. calculations below
# central pitch = 10, central pitch + 7 = 11, etc.
P = zeros((21,3), dtype=float32)
P[0,:] = [0,1,0]
for i in arange(1,21):
    P[i,:] = matmul(R, P[i-1,:]) + H

# build triad representations
# line 0 = major, line 1 = minor
triads = zeros((2,14,3), dtype=float32)
for i in arange(14):
    triads[0,i,:] = w[0] * P[i+3,:] + w[1] * P[i+4,:] + w[2] * P[i+7,:]
    triads[1,i,:] = w[0] * P[i+3,:] + w[1] * P[i+4,:] + w[2] * P[i,:]

# build chord representations
chords = zeros((2,12,3), dtype=float32)
for i in arange(12):
    chords[0,i,:] = w[0] * triads[0,i+1,:] + w[1] * triads[0,i+2,:] + w[2] * triads[0,i,:]
    chords[1,i,:] = w[0] * triads[1,i+1,:] + w[1] * (0.5*triads[0,i+2,:] + 0.5*triads[1,i+2,:]) + w[2] * (0.5*triads[0,i,:] + 0.5*triads[1,i,:])
# reshape centers - first 12 are major, last 12 are minor
chords.shape = (24,3)
# final result will be difference to center pitch
chordDiffs = array([-6, 1, -4, 3, -2, 5, 0, -5, 2, -3, 4, -1])
# chord names based on (MIDI note % 12)
chordNames = array(['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B'])  



def findKeysFromModel(model, numKeys=1, returnDistances=False):
    '''
    Use CEG algorithm to deduce key(s) of model.
    '''
    # extract durations and pitches
    durations = model.durations
    durations.shape = (model.numNotes, 1)
    durationsExtended = repeat(durations, 3, axis=1)
    
    pitches = model.pitches
    # mean pitch goes to the middle of the spiral
    centralPitch = int(round(mean(pitches)))
    
    # map MIDI notes to spiral location, always the middle octave of the 3
    # centralPitch   +- 12n => 10
    # centralPitch+7 +- 12n => 11 etc.
    start = 6 - 7*centralPitch
    end = 6 + 7*(128-centralPitch)
    m2P = (arange(start, end, 7) % 12 + 4).astype(uint8)
    pitches = P[m2P[pitches]]
    
    # calculate centers of effect and distances
    center = sum(durationsExtended * pitches, axis=0) / sum(durations)
    distances = zeros(24, dtype=float32)
    for i in arange(24):
        distances[i] = sqrt(sum((center - chords[i,:]) ** 2))
    
    
    # choose winners
    if numKeys is 1:
        bestResultArg = argmin(distances)
        bestDistance = distances[bestResultArg]
        key = centralPitch + chordDiffs[bestResultArg % 12]
        major = (bestResultArg < 12)
        if major:
            keyName = chordNames[key % 12]
        else:
            keyName = chordNames[key % 12] + 'm'
    else:
        bestResultArgs = argsort(distances)[:numKeys]
        bestDistance = distances[bestResultArgs]
        key = centralPitch + chordDiffs[bestResultArgs % 12]
        major = (bestResultArgs < 12)
        keyName = []
        for i in arange(numKeys):
            if major[i]:
                keyName.append(chordNames[key[i] % 12])
            else:
                keyName.append(chordNames[key[i] % 12] + 'm')
        
    if returnDistances:
        return (key, major, keyName, bestDistance)
    else:
        return (key, major, keyName)
    


# CONVERTING MODELS

# map chromatic scale to diatonic major scale degrees
c2dM = array([0, 0, 1, 1, 2, 3, 3, 4, 4, 5, 5, 6], dtype=int16)
# map chromatic scale to diatonic minor scale degrees
c2dm = array([0, 0, 1, 2, 2, 3, 3, 4, 5, 5, 6, 6], dtype=int16)
# map diatonic major scale to chromatic scale
d2cM = array([0, 2, 4, 5, 7, 9, 11], dtype=int16)
# map diatonic minor scale to chromatic scale
d2cm = array([0, 2, 3, 5, 7, 8, 10], dtype=int16)


def chromaticToShiftedChromatic(model, makeCopy=True,
                                assumedKey=None, assumedMajor=None):
    '''
    Transpose a model's pitches so the key becomes the center (64).
    Performs key detection and transposes notes accordingly.
    '''
    if makeCopy:
        newModel = deepcopy(model)
    else:
        newModel = model
    
    if assumedKey is None:
        newModel.key, newModel.major, _ = findKeysFromModel(newModel)
    else:
        newModel.key = assumedKey
        newModel.major = assumedMajor
        
    for trackIdx in arange(newModel.numTracks):
        pitches = newModel.tracks[trackIdx].notes[:,4].astype(int16)
        newPitches = pitches + (KEYPOINT - newModel.key)
        
        # do not allow overflow
        newPitches[newPitches < 0] = 0
        newPitches[newPitches > 127] = 127
        
        newModel.tracks[trackIdx].notes[:,4] = newPitches
    
    newModel.representation = 'SHIFTED_CHROMATIC'
    return newModel


def shiftedChromaticToChromatic(model, makeCopy=True):
    '''
    Transpose a model's pitches from shifted chromatic representation to default chromatic.
    '''
    if makeCopy:
        newModel = deepcopy(model)
    else:
        newModel = model
    
    for trackIdx in arange(newModel.numTracks):
        pitches = newModel.tracks[trackIdx].notes[:,4].astype(int16)
        newPitches = pitches + (newModel.key - KEYPOINT)
        
        # do not allow overflow
        newPitches[newPitches < 0] = 0
        newPitches[newPitches > 127] = 127
        
        newModel.tracks[trackIdx].notes[:,4] = newPitches
    
    newModel.representation = 'CHROMATIC'
    return newModel


def chromaticToDiatonic(model, makeCopy=True,
                        assumedKey=None, assumedMajor=None):
    '''
    Given a model with MIDI-like chromatic pitch representation, convert to one
    using a diatonic scale. Performs key detection and transposes notes accordingly.
    '''
    if makeCopy:
        newModel = deepcopy(model)
    else:
        newModel = model
    
    if assumedKey is None:
        newModel.key, newModel.major, _ = findKeysFromModel(newModel)
    else:
        newModel.key = assumedKey
        newModel.major = assumedMajor
        
    for trackIdx in arange(newModel.numTracks):
        pitches = newModel.tracks[trackIdx].notes[:,4].astype(int16)
        if newModel.major:
            newPitches = KEYPOINT + (pitches - newModel.key) / 12 * 7 + c2dM[(pitches - newModel.key) % 12]
        else:
            newPitches = KEYPOINT + (pitches - newModel.key) / 12 * 7 + c2dm[(pitches - newModel.key) % 12]
        
        # do not allow overflow
        newPitches[newPitches < 0] = 0
        newPitches[newPitches > 127] = 127
        
        newModel.tracks[trackIdx].notes[:,4] = newPitches
    
    newModel.representation = 'DIATONIC'
    return newModel


def shiftedChromaticToDiatonic(model, makeCopy=True):
    '''
    Given a model with shifted MIDI-like chromatic pitch representation, convert to one
    using a diatonic scale. Transposes notes accordingly.
    '''
    if makeCopy:
        newModel = deepcopy(model)
    else:
        newModel = model
    
    for trackIdx in arange(newModel.numTracks):
        pitches = newModel.tracks[trackIdx].notes[:,4].astype(int16)
        if newModel.major:
            newPitches = KEYPOINT + (pitches - KEYPOINT) / 12 * 7 + c2dM[(pitches - KEYPOINT) % 12]
        else:
            newPitches = KEYPOINT + (pitches - KEYPOINT) / 12 * 7 + c2dm[(pitches - KEYPOINT) % 12]
        newModel.tracks[trackIdx].notes[:,4] = newPitches
    
    newModel.representation = 'DIATONIC'
    return newModel


def diatonicToChromatic(model, makeCopy=True):
    '''
    Given a model with diatonic pitch representation + key information, convert to
    chromatic MIDI-like scale.
    '''
    if makeCopy:
        newModel = deepcopy(model)
    else:
        newModel = model
        
    for trackIdx in arange(newModel.numTracks):
        pitches = newModel.tracks[trackIdx].notes[:,4].astype(int16)
        if newModel.major:
            newPitches = newModel.key + (pitches - KEYPOINT) / 7 * 12 + d2cM[(pitches - KEYPOINT) % 7]
        else:
            newPitches = newModel.key + (pitches - KEYPOINT) / 7 * 12 + d2cm[(pitches - KEYPOINT) % 7]
        
        # do not allow overflow
        newPitches[newPitches < 0] = 0
        newPitches[newPitches > 127] = 127
        
        newModel.tracks[trackIdx].notes[:,4] = newPitches
    
    newModel.representation = 'CHROMATIC'
    return newModel


def diatonicToShiftedChromatic(model, makeCopy=True):
    '''
    Given a model with diatonic pitch representation, convert to
    shifted chromatic MIDI-like scale.
    '''
    if makeCopy:
        newModel = deepcopy(model)
    else:
        newModel = model
        
    for trackIdx in arange(newModel.numTracks):
        pitches = newModel.tracks[trackIdx].notes[:,4].astype(int16)
        if newModel.major:
            newPitches = KEYPOINT + (pitches - KEYPOINT) / 7 * 12 + d2cM[(pitches - KEYPOINT) % 7]
        else:
            newPitches = KEYPOINT + (pitches - KEYPOINT) / 7 * 12 + d2cm[(pitches - KEYPOINT) % 7]
        
        # do not allow overflow
        newPitches[newPitches < 0] = 0
        newPitches[newPitches > 127] = 127
        
        newModel.tracks[trackIdx].notes[:,4] = newPitches
    
    newModel.representation = 'SHIFTED_CHROMATIC'
    return newModel


def toChromatic(model, makeCopy=True):
    '''
    Convert model to chromatic
    '''
    if model.representation == 'CHROMATIC':
        if makeCopy:
            return deepcopy(model)
        else:
            return model
    
    if model.representation == 'SHIFTED_CHROMATIC':
        return shiftedChromaticToChromatic(model, makeCopy=makeCopy)
    elif model.representation == 'DIATONIC':
        return diatonicToChromatic(model, makeCopy=makeCopy)
    else:
        print 'Model representation "%s" not known' %(model.representation)
        return model


def toShiftedChromatic(model, makeCopy=True,
                       assumedKey=None, assumedMajor=None):
    '''
    Convert model to shifted chromatic
    '''
    if model.representation == 'SHIFTED_CHROMATIC':
        if makeCopy:
            return deepcopy(model)
        else:
            return model
    
    if model.representation == 'CHROMATIC':
        return chromaticToShiftedChromatic(model, makeCopy=makeCopy,
                                           assumedKey=assumedKey, assumedMajor=assumedMajor)
    elif model.representation == 'DIATONIC':
        return diatonicToShiftedChromatic(model, makeCopy=makeCopy)
    else:
        print 'Model representation "%s" not known' %(model.representation)
        return model


def toDiatonic(model, makeCopy=True,
               assumedKey=None, assumedMajor=None):
    '''
    Convert model to diatonic
    '''
    if model.representation == 'DIATONIC':
        if makeCopy:
            return deepcopy(model)
        else:
            return model
    
    if model.representation == 'CHROMATIC':
        return chromaticToDiatonic(model, makeCopy=makeCopy,
                                   assumedKey=assumedKey, assumedMajor=assumedMajor)
    elif model.representation == 'SHIFTED_CHROMATIC':
        return shiftedChromaticToDiatonic(model, makeCopy=makeCopy)
    else:
        print 'Model representation "%s" not known' %(model.representation)
        return model


def toRepresentation(model, modelRepresentation, makeCopy=True,
                     assumedKey=None, assumedMajor=None):
    '''
    Convert model to given representation.
    '''
    if model.representation == modelRepresentation:
        if makeCopy:
            return deepcopy(model)
        else:
            return model
    
    if modelRepresentation == 'CHROMATIC':
        return toChromatic(model, makeCopy=makeCopy)
    elif modelRepresentation == 'SHIFTED_CHROMATIC':
        return toShiftedChromatic(model, makeCopy=makeCopy,
                                  assumedKey=assumedKey, assumedMajor=assumedMajor)
    elif modelRepresentation == 'DIATONIC':
        return toDiatonic(model, makeCopy=makeCopy,
                          assumedKey=assumedKey, assumedMajor=assumedMajor)
    else:
        print 'Model representation "%s" not known' %(modelRepresentation)
        return model


if __name__ == '__main__':
    
    from emcpp.wrapper.modelbuilder import ModelBuilderWrapper
    #from emc.integ.midi import writeToMidi

    # set up input
    b = array([0, 0, 4, 48,
               0, 4, 4, 53, 0, 4, 2, 53, 0, 2, 2, 55, 0, 2, 2, 57, 0, 2, 2, 53, 0, 2, 2, 57, 0, 2, 2, 58,
               0, 2, 4, 60, 0, 4, 2, 60, 0, 2, 2, 58, 0, 2, 4, 57, 0, 4, 2, 55, 0, 2, 2, 53,
               0, 2, 4, 55, 0, 4, 4, 48, 0, 4, 4, 55, 0, 4, 4, 48,
               0, 4, 2, 55, 0, 2, 2, 57, 0, 2, 2, 55, 0, 2, 2, 52, 0, 2, 4, 48, 0, 4, 4, 48,
               0, 4, 4, 53, 0, 4, 2, 53, 0, 2, 2, 55, 0, 2, 2, 57, 0, 2, 2, 53, 0, 2, 2, 57, 0, 2, 2, 58,
               0, 2, 4, 60, 0, 4, 2, 60, 0, 2, 2, 58, 0, 2, 4, 57, 0, 4, 2, 55, 0, 2, 2, 53,
               0, 2, 4, 55, 0, 4, 4, 48, 0, 4, 4, 57, 0, 4, 4, 55,
               0, 4, 4, 53, 0, 4, 4, 53, 0, 4, 8, 53], dtype=uint8)
    #b[3::4] -= 5
    mbw = ModelBuilderWrapper(numTracks=1)
    model = mbw.bytesToModel(b)
    #writeToMidi(model, "simplegifts.mid")
    
    # test 1: find key of simple gifts
    numKeys = 4
    key, major, keyNames, distances = findKeysFromModel(model, numKeys=numKeys, returnDistances=True)
    print 'Test model (simple gifts)'
    for i in arange(numKeys):
        print '%d. %d - %s - %.3f' %(i, key[i], keyNames[i], distances[i])
    print ''
    
    # test 2: transpose simple gifts
    print 'transpose'
    print model.tracks[0].notes[:,4]
    scmodel = chromaticToShiftedChromatic(model, makeCopy=True)
    print scmodel.tracks[0].notes[:,4]
    scmodel.key = 57
    shiftedChromaticToChromatic(scmodel, makeCopy=False)
    print scmodel.tracks[0].notes[:,4]
    #writeToMidi(scmodel, "simplegifts_transposed.mid")
    print ''
    
    # test 3: make simple gifts minor
    print 'diatonicize'
    print model.tracks[0].notes[:,4]
    chromaticToDiatonic(model, makeCopy=False)
    print model.tracks[0].notes[:,4]
    model.major = False
    diatonicToChromatic(model, makeCopy=False)
    print model.tracks[0].notes[:,4]
    #writeToMidi(model, "simplegifts_minor.mid")
    print ''
    
    # test 4: diatonicToChromatic of any pitch
    b = zeros((128,4), dtype=uint8)
    for i in arange(128):
        b[i,:] = [0, 2, 2, i]
    b.shape = (128*4)
    
    mbw = ModelBuilderWrapper(numTracks=1, modelRepresentation='DIATONIC', modelKey=53, modelMajor=False)
    model = mbw.bytesToModel(b)
    
    print model.tracks[0].notes[:, 4]
    diatonicToChromatic(model, makeCopy=False)
    print model.tracks[0].notes[:, 4]
