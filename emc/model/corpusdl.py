'''
Download corpus MIDI files
'''

from multiprocessing import Pool
from numpy import array, arange, int32
from os import makedirs, remove
from os.path import exists, join, dirname, abspath, isfile
from re import findall, match
from time import time, sleep
from urllib import urlretrieve
import urllib2
from emc.integ.midi import readFromMidi
from urlparse import urljoin


'''
Default directory to store new downloaded corpora.
A new subdirectory is created here for each download, deduced from the download URL.
'''
defaultCorpusDownloadDir = abspath(join(dirname(__file__), '../../../midi/'))



def downloadAndCheckMidi(arg):
    '''
    Download MIDI file and check if a model may be built from it.
    Single-argument so we can use it in multiprocessing pool.
    Returns filename only if download failed.
    '''
    sourceUrl = arg[0]
    midiFileName = arg[1]
    midiPath = arg[2]
    
    downloaded = False
    numAttemptsLeft = 10

    while not downloaded and numAttemptsLeft > 0:
        try:
            urlretrieve(sourceUrl, midiPath)
            downloaded = True
        except:
            print "Something went wrong downloading %s, trying again" %(midiFileName)
            sleep(1)
            numAttemptsLeft -= 1
    
    if not downloaded:
        print "Couldn't download %s in 10 tries, you might be having network issues. Behavior unpredictable" %(midiFileName)
        return
        
    try:
        readFromMidi(midiPath)
    except:
        print "%s not MIDI, removing" %midiPath
        remove(midiPath)
        return midiFileName
            


def downloadCorpus(listPageUrl, targetDir = None, fileNameFilter = None,
                   omitUrlCheck = True, forceRedownload = False, 
                   numMessages = 10, poolSize = 32):
    '''
    Download all MIDI files listed on listPageUrl into targetDir.
    Files may be filtered using a regexp in fileNameFilter
    '''
    listPageBaseUrl = listPageUrl[:listPageUrl.rfind("/")]
    listPageFileName = listPageUrl[listPageUrl.rfind("/")+1:]
    if targetDir is None:
        baseName = listPageFileName[:listPageFileName.rfind(".")]
        targetDir = join(defaultCorpusDownloadDir, baseName)
    
    if not exists(targetDir):
        makedirs(targetDir)
    else:
        if omitUrlCheck:
            print "Corpus directory exists and URL check ommitted"
            return targetDir

    print 'Downloading corpus from %s into %s' %(listPageUrl, targetDir)
    
    # read contents of page
    url = urllib2.urlopen(listPageUrl)
    contents = url.read()
    
    # find duplicates among midiFileNames
    midiUrls = findall(r'<a href="([^"]+\.mid)"', contents)
    print 'Found corpus of size %d' %(len(midiUrls))
    duplicates = sum([midiUrls.count(midiUrl)-1 for midiUrl in set(midiUrls) if midiUrls.count(midiUrl) > 1])
    if duplicates > 0:
        print "%d duplicates found on URL, ommitting" %(duplicates)
    
    # extract MIDI file names and URLs from page
    midiUrls = [urljoin(listPageBaseUrl, midiUrl) for midiUrl in midiUrls]
    midiFileNames = [midiUrl[midiUrl.rfind('/')+1:] for midiUrl in midiUrls]
    midiPaths = ['%s/%s' %(targetDir, midiFileName) for midiFileName in midiFileNames]
    
    # check if we cached invalid midi files
    invalidMidiCacheFileName = join(targetDir, '.invalidMidis')
    if isfile(invalidMidiCacheFileName):
        invalidMidiCacheFile = open(invalidMidiCacheFileName)
        invalidMidis = invalidMidiCacheFile.readlines()
        invalidMidis = [x.strip() for x in invalidMidis]
        invalidMidiCacheFile.close()
    else:
        invalidMidis = []
    
    # filter included downloads based on regexp and/or file existence and/or MIDI invalidity
    included = [i for i in arange(len(midiUrls)) if midiFileNames[i] not in invalidMidis]
    if fileNameFilter is not None:
        included = [i for i in included if match(fileNameFilter, midiFileNames[i])]
    if not forceRedownload:
        included = [i for i in included if not exists(midiPaths[i])]
    
    # check if anything left to do
    if len(included) == 0:
        print 'All corpus files up-to-date'
        return targetDir
    
    # apply filters
    midiUrls = [midiUrls[i] for i in included]
    midiFileNames = [midiFileNames[i] for i in included]
    midiPaths = [midiPaths[i] for i in included]
    
    # build source and target URL/filenames    
    numFiles = len(included)
    midiUrlsToPaths = array([midiUrls, midiFileNames, midiPaths]).T
    
    # create process pool and run download of slices on each
    numMessages = min(numMessages, numFiles)
    delta = float(numFiles)/float(numMessages)
    poolSize = min(poolSize, int(delta))
    pool = Pool(poolSize)
    indices = (arange(numMessages+1) * delta).round().astype(int32)
    
    st = time()
    print 'Downloading corpus of %d files' %numFiles
    for k in arange(numMessages):
        newInvalidMidis = pool.map(downloadAndCheckMidi, midiUrlsToPaths[indices[k]:indices[k+1], :])
        if len(newInvalidMidis) > 0:
            for newInvalidMidi in newInvalidMidis:
                if newInvalidMidi is not None:
                    invalidMidis.append(newInvalidMidi)
        print '  %d files completed' %(indices[k+1])
    
    pool.close()
    pool.join()
    t = time() - st
    print 'Elapsed time:', t, 'seconds'

    # save invalid MIDIs' names
    if len(invalidMidis) > 0:
        invalidMidiCacheFile = open(invalidMidiCacheFileName, 'w')
        for invalidMidi in invalidMidis:
            invalidMidiCacheFile.write(invalidMidi + '\n')
        invalidMidiCacheFile.close()
    
    return targetDir


if __name__ == '__main__':
    #downloadCorpus(listPageUrl="http://ujmagyarevezred.nl/umek-135.html")
    downloadCorpus(listPageUrl="http://midiworld.com/bach.htm", fileNameFilter='.*bwv(7|800|801).*', forceRedownload=True)
    #downloadCorpus(listPageUrl="http://www.cimbalom.nl/nepdalok.html")
