'''
Model-related classes in EMC.
'''

from numpy import frombuffer, zeros, array, uint16, append, max, ndarray


def valueToPitch(i, representation='DIATONIC'):
    if type(i) is ndarray:
        return array([valueToPitch(s, representation) for s in i])
    else: 
        if representation == 'DIATONIC':
            # 62 is C4
            baseNotes = ['C', 'D', 'E', 'F', 'G', 'A', 'B']
            baseNote = baseNotes[(i - 62) % 7]
            octave = (i - 62) / 7 + 4
            return '%s%d' %(baseNote, octave)
        else:
            # 60 is C4
            baseNotes = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B']
            baseNote = baseNotes[(i - 60) % 12]
            octave = (i - 60) / 12 + 4
            return '%s%d' %(baseNote, octave)



class Track(object):
    '''
    Class representation of a track, i.e. a collection of notes related to a virtual
    instrument/voice.
    '''
    
    propsPerNote = 6
    
    def __init__(self, trackNum, dtype):
        self.trackNum = trackNum
        self.dtype = dtype
        
        self.notes = array([[]], dtype=self.dtype)
        self.notes.shape = (0, self.propsPerNote)
    
    
    '''
    Getters for one property of all notes
    '''
    @property
    def interonsets(self): return self.notes[:, 0]
    @property
    def durations(self):   return self.notes[:, 1]
    @property    
    def onsets(self):      return self.notes[:, 2]
    @property    
    def offsets(self):     return self.notes[:, 3]
    @property    
    def pitches(self):     return self.notes[:, 4]
    @property
    def velocities(self):  return self.notes[:, 5]
    
    
    @property
    def durationDiffs(self): return (64 + self.durations[1:] - self.durations[:-1])
    @property
    def pitchDiffs(self):    return (64 + self.pitches[1:] - self.pitches[:-1])
    
    
    @property
    def numNotes(self):
        '''
        Number of notes
        '''
        if self.notes is None or len(self.notes.shape) == 0 or self.notes.shape[0] == 0:
            return 0
        elif len(self.notes.shape) > 1:
            return self.notes.shape[0]
        else:
            return 1
    
    
    @property
    def length(self):
        '''
        Length
        '''
        if self.notes is None or len(self.notes.shape) == 0 or self.notes.shape[0] == 0:
            return 0
        else:
            # length is furthest offset
            return max(self.notes[:, 3])
        
    
    def __repr__(self):
        ret = "[trackNum=%d, numNotes=%d, length=%d, notes:\n" %(self.trackNum, self.numNotes, self.length)
        for i in range(self.numNotes):
            ret += "        %d:[interonset=%d, duration=%d, onset=%d, offset=%d, pitch=%d, velocity=%d]\n" %(
                i, self.notes[i,0], self.notes[i,1], self.notes[i,2], self.notes[i,3], self.notes[i,4], self.notes[i,5])
        ret += "    ]"
        return ret
    
    
    def __eq__(self, other):
        if not isinstance(other, Track):
            return False
        if self.__getstate__() != other.__getstate__():
            return False
        return True
    
    
    def __getstate__(self):
        '''
        Overwrite serialization because cPickle saves extra information making the output files very large.
        '''
        notesBA = bytearray(self.notes)
        
        return (self.trackNum,
                self.dtype,
                notesBA)


    def __setstate__(self, state):
        '''
        Overwrite serialization because cPickle saves extra information making the output files very large.
        '''
        self.trackNum, self.dtype, notesBA = state
        self.notes = frombuffer(notesBA, dtype=self.dtype)
        self.notes.shape = (len(self.notes) / self.propsPerNote, self.propsPerNote)
            
    


class Model(object):
    '''
    Phenotype of the EMC genetic algorithm.
    Represents a musical piece as a collection of tracks.
    '''
    
    def __init__(self, ticksPerQuarterNote=4,
                       quarterNotesPerMinute=120,
                       numTracks=1,
                       representation='CHROMATIC',
                       key=64,major=True,
                       dtype=uint16):
        self.ticksPerQuarterNote = ticksPerQuarterNote
        self.quarterNotesPerMinute = quarterNotesPerMinute
        self.numTracks = numTracks
        self.representation = representation
        self.key = key
        self.major = major
        self.dtype = dtype
        
        self.tracks = zeros(self.numTracks, dtype=Track)
        for i in range(self.tracks.size):
            self.tracks[i] = Track(i, self.dtype)
        
    
    @property
    def interonsets(self):
        '''
        All interonsets.
        TODO if this type of function needed without reallocation with multiple tracks,
        use one array with appended notes for full model.
        '''
        if self.numTracks == 1:
            return self.tracks[0].interonsets
        iois = array([], dtype=self.dtype)
        for track in self.tracks:
            iois = append(iois, track.interonsets)
        return iois
    
    
    @property
    def durations(self):
        '''
        All durations.
        TODO if this type of function needed without reallocation with multiple tracks,
        use one array with appended notes for full model.
        '''
        if self.numTracks == 1:
            return self.tracks[0].durations
        iois = array([], dtype=self.dtype)
        for track in self.tracks:
            iois = append(iois, track.durations)
        return iois
    
    
    @property
    def durationDiffs(self):
        '''
        All duration diffs.
        TODO if this type of function needed without reallocation with multiple tracks,
        use one array with appended notes for full model.
        '''
        if self.numTracks == 1:
            return self.tracks[0].durationDiffs
        iois = array([], dtype=self.dtype)
        for track in self.tracks:
            iois = append(iois, track.durationDiffs)
        return iois
    
    @property
    def pitches(self):
        '''
        All pitches.
        TODO if this type of function needed without reallocation with multiple tracks,
        use one array with appended notes for full model.
        '''
        if self.numTracks == 1:
            return self.tracks[0].pitches
        iois = array([], dtype=self.dtype)
        for track in self.tracks:
            iois = append(iois, track.pitches)
        return iois
    
    
    @property
    def pitchDiffs(self):
        '''
        All pitches.
        TODO if this type of function needed without reallocation with multiple tracks,
        use one array with appended notes for full model.
        '''
        if self.numTracks == 1:
            return self.tracks[0].pitchDiffs
        iois = array([], dtype=self.dtype)
        for track in self.tracks:
            iois = append(iois, track.pitchDiffs)
        return iois
    
    
    @property
    def velocities(self):
        '''
        All velocities.
        TODO if this type of function needed without reallocation with multiple tracks,
        use one array with appended notes for full model.
        '''
        if self.numTracks == 1:
            return self.tracks[0].velocities
        iois = array([], dtype=self.dtype)
        for track in self.tracks:
            iois = append(iois, track.velocities)
        return iois
    
    
    @property
    def numNotes(self):
        '''
        Number of notes per track.
        '''
        if self.numTracks == 1:
            return self.tracks[0].numNotes
        else:
            return [track.numNotes for track in self.tracks]
    
    
    @property
    def length(self):
        '''
        Length of model = maximum of track lengths.
        '''
        return max([track.length for track in self.tracks])
    
    
    @property
    def lengthInSecs(self):
        '''
        Length in seconds.
        '''
        return float(self.length) / self.ticksPerQuarterNote / self.quarterNotesPerMinute * 60.0
            
    
    def __repr__(self):
        ret = "[numTracks=%d, ticksPerQuarterNote=%d, quarterNotesPerMinute=%d, representation=%s, key=%d, major=%s, length=%d, tracks:\n" %(
            self.numTracks, self.ticksPerQuarterNote, self.quarterNotesPerMinute, self.representation, self.key, self.major, self.length)
        for i in range(self.tracks.size):
            ret += "    %d:%s\n" %(i,str(self.tracks[i]))
        ret += "]"
        return ret


    def __eq__(self, other):
        if not isinstance(other, Model):
            return False
    
        selfdata = [self.ticksPerQuarterNote, self.quarterNotesPerMinute, self.representation, self.key, self.major, self.numTracks]
        otherdata = [other.ticksPerQuarterNote, other.quarterNotesPerMinute, other.representation, other.key, other.major, other.numTracks]
        if selfdata != otherdata:
            return False
        
        for trackInd in range(self.numTracks):
            if not self.tracks[trackInd].__eq__(other.tracks[trackInd]):
                return False

        return True