'''
Model directory

'''
import os

from numpy import array, zeros, size, unique, uint16, float32, mean, uint32

from emc.model.model import Model
from emc.model.corpusdl import downloadCorpus
from emc.integ.midi import readFromMidi, writeToMidi
from emc.fitness.entropy import entropyModel
from emc.integ.ceg import toChromatic
from emc.scripts.preprocessing import preprocessModels
from helper.serializeutils import toFile, fromFile
from emc.model.modelred import ReducedModel


class ModelDirectory(object):
    '''
    Collection of model instances to be used in analysis/transformation.
    Built with a folder name containing MIDI files which will all be read and transformed into Model instances.
    Transformations can be performed and resulting models will be written back to same files
    '''
    
    def __init__(self, midiFolder = '.', omitUrlCheck = True, 
                 modelRepresentation = 'DIATONIC', 
                 fileNameFilter = None, preprocessingMethods = [],
                 forceRedownload = False, forceReread = False):
        '''
        Initialize by foldername.
        midiFolder can also be a URL, in which case all MIDI files are downloaded
        into a default directory.
        '''
        self.omitUrlCheck = omitUrlCheck
        self.modelRepresentation = modelRepresentation
        self.fileNameFilter = fileNameFilter
        self.preprocessingStatus = 0
        self.preprocessingMethods = preprocessingMethods
        
        if not os.path.isdir(midiFolder):
            print 'Provided corpus dir %s is not a directory' %midiFolder
            print 'Assuming URL to download from'
            self.folder = downloadCorpus(midiFolder,
                                         omitUrlCheck=omitUrlCheck,
                                         fileNameFilter=fileNameFilter,
                                         forceRedownload=forceRedownload)
        else:
            self.folder = os.path.abspath(midiFolder)
    
    
    def cacheFile(self, preprocessingStatus = None):
        '''
        A model directory's content is cached into a file with this name under its root directory.
        preprocessingStatus - number of preprocessing steps performed on all models at this point.
        '''
        fileName = '.emcmodeldir-%s' %(self.modelRepresentation)
        if preprocessingStatus is None:
            preprocessingStatus = self.preprocessingStatus
        if len(self.preprocessingMethods) != 0 and preprocessingStatus != 0:
            fileName += '-' + '-'.join(self.preprocessingMethods[:preprocessingStatus])
        
        return os.path.abspath(os.path.join(self.folder, fileName))
    
    
    def read(self, forceReread = False):
        '''
        Read model directory, either from cache or by reading MIDI files one by one.
        '''
        if forceReread:
            self.readFromMidis()
        else:
            foundCache = False
            preprocessingStatusToCheck = len(self.preprocessingMethods)
            while not foundCache and preprocessingStatusToCheck >= 0:
                cacheFile = self.cacheFile(preprocessingStatusToCheck)
                if os.path.isfile(cacheFile):
                    foundCache = True
                else:
                    preprocessingStatusToCheck -= 1
            
            if foundCache:
                print "Cache file %s exists, using it as starting point" %(cacheFile)
                self.preprocessingStatus = preprocessingStatusToCheck
                self.readFromCache()
            else:
                self.readFromMidis()
        
        
        print "Initialized model directory with size %d" %(self.numModels)
        self.preprocess()
        # check number of tracks same in each model, give error otherwise
        if size(unique(array([m.numTracks for m in self.models]))) != 1:
            print "ERROR: Model directory '%s' contains MIDIs with different number of tracks. Behavior unpredictable" %self.folder
        
        return self
    
    
    def readFromCache(self):
        '''
        Reads model directory from already converted cache.
        '''
        cacheFile = self.cacheFile()
        self.numTracks, self.numModels, self.filenames, self.models = fromFile(cacheFile)
    
    
    def readFromMidis(self):
        '''
        Reads model directory by reading all MIDI files from given folder and transforming them to instances of Model.
        '''
        self.preprocessingStatus = 0
        
        self.filenames = array([fname for fname in sorted(os.listdir(self.folder)) if fname.endswith(".mid")])
        self.numModels = len(self.filenames)
        self.models = zeros(self.numModels, dtype=Model)
        
        for i, fname in enumerate(self.filenames):
            self.models[i] = readFromMidi("%s/%s" %(self.folder, fname),
                                          representation=self.modelRepresentation)
        
        self.numTracks = self.models[0].numTracks
        self.writeToCache()
    
    
    def preprocess(self):
        '''
        Perform preprocessing.
        '''
        for methodIdx in range(self.preprocessingStatus, len(self.preprocessingMethods)):
            method = self.preprocessingMethods[methodIdx]
            print 'Performing preprocessing step %s' %(method)
            self.models = preprocessModels(method, self.models)
            self.preprocessingStatus = methodIdx + 1
            self.numTracks = self.models[0].numTracks
            self.writeToCache()
        
    
    def writeToCache(self):
        '''
        Caches converted model directory to single file.
        '''
        cacheFile = self.cacheFile()
        state = (self.numTracks, self.numModels, self.filenames, self.models)
        toFile(state, cacheFile)
        print 'Writing %s' %(cacheFile)
    
    
    def write(self, newMidiFolder = None, includeEmptyTracks = True):
        '''
        Writes model directory as MIDI to new output folder.
        This resets all preprocessing methods and their status.
        '''
        if newMidiFolder is None:
            newMidiFolder = self.folder
        
        self.preprocessingStatus = 0
        self.preprocessingMethods = []
        
        self.folder = os.path.abspath(newMidiFolder)
        if not os.path.exists(self.folder):
            os.makedirs(self.folder)
        self.writeToCache()
        
        for i, fname in enumerate(self.filenames):
            model = toChromatic(self.models[i], makeCopy=True)
            writeToMidi(model, "%s/%s" %(self.folder, fname), includeEmptyTracks)


    @property
    def reduced(self):
        '''
        Shows if current model directory uses reduced models or not.
        '''
        return isinstance(self.models[0], ReducedModel)
        
        
    @property
    def lengths(self):
        '''
        List of all lengths in models.
        '''
        return [model.length for model in self.models]
    
    
    @property
    def lengthsInSecs(self):
        '''
        List of all lengths in models, given in seconds.
        '''
        return [model.lengthInSecs for model in self.models]
    
    
    @property
    def numNotes(self):
        '''
        List of number of notes in models.
        '''
        if self.numTracks == 1:
            ret = [model.numNotes for model in self.models]
        else:
            ret = zeros((self.numModels, self.numTracks), dtype=uint16)
            for modelIdx, model in enumerate(self.models):
                ret[modelIdx, :] = model.numNotes
        return ret
    
    
    @property
    def meanNumNotes(self):
        '''
        Average number of notes in corpus.
        '''
        if self.numTracks == 1:
            return mean([model.numNotes for model in self.models])
        else:
            return mean([sum(model.numNotes) for model in self.models])
    
    
    def expectedNumNotes(self, expectedLength = None):
        '''
        Given an expected length in seconds (if none given, model's mean is taken),
        gives the expected number of notes any VM should produce to concur with the length of this corpus.
        '''
        if expectedLength is None:
            return uint32(mean(self.numNotes))
            
        # times 8 because there are 8 ticks per second and expectedLength is in seconds
        expectedLength *= 8.0
        # rule of 3 :)
        return uint32(expectedLength / mean(self.lengths) * mean(self.numNotes))
    
    
    def entropies(self):
        '''
        Entropy of all models in dir.
        '''
        ret = zeros((self.numModels, self.numTracks * 3), dtype=float32)
        for modelIdx, model in enumerate(self.models):
            ret[modelIdx, :] = entropyModel(model)
        return ret
    
    
    def __getstate__(self):
        '''
        Do not serialize cached values.
        '''
        return (os.path.relpath(self.folder),
                self.omitUrlCheck,
                self.modelRepresentation,
                self.fileNameFilter,
                self.preprocessingMethods)


    def __setstate__(self, state):
        '''
        Rebuild cached values when deserializing.
        '''
        folder, omitUrlCheck, modelRepresentation, fileNameFilter, preprocessingMethods = state
        self.__init__(midiFolder=os.path.abspath(folder),
                      omitUrlCheck=omitUrlCheck,
                      modelRepresentation=modelRepresentation,
                      fileNameFilter=fileNameFilter,
                      preprocessingMethods=preprocessingMethods)



def bachModelDirectory(modelRepresentation='DIATONIC'):
    return ModelDirectory('http://midiworld.com/bach.htm',
        modelRepresentation=modelRepresentation,
        fileNameFilter='.*bwv(7|800|801).*',
        preprocessingMethods=['removeZeroVelocityNotes', 'makeMaxVelocity', 'resampleTo4Ticks',
                              'forceNoOverlappingNotes', 'clipOverlyLongNotes', 'asuint16']).read()


def bach2TrackModelDirectory(modelRepresentation='DIATONIC'):
    return ModelDirectory('http://midiworld.com/bach.htm',
        modelRepresentation=modelRepresentation,
        fileNameFilter='.*bwv(7|800|801).*',
        preprocessingMethods=['removeZeroVelocityNotes', 'makeMaxVelocity', 'resampleTo4Ticks',
                              'separateTo2Tracks', 'forceNoOverlappingNotes', 'clipOverlyLongNotes', 'asuint16']).read()


def nepdalokModelDirectory(modelRepresentation='DIATONIC'):
    return ModelDirectory('http://www.cimbalom.nl/nepdalok.html',
        modelRepresentation=modelRepresentation,
        preprocessingMethods=['removeZeroVelocityNotes', 'makeMaxVelocity', 'resampleTo4Ticks',
                              'forceNoOverlappingNotes', 'clipOverlyLongNotes', 'asuint16', 'reduceModels']).read()


if __name__ == '__main__':
    md = bach2TrackModelDirectory()
    md.write('../../../midi/bach_preprocessed')
