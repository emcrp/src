'''
Reduced model
'''

from numpy import frombuffer, zeros, uint16, uint8, array, arange, sum
from tymed import tymedCls, tymed
from emc.model.model import Model, valueToPitch


CENTER_PITCH = 69 # MIDI pitch value mapped to middle point in reduced repr. (16)
MIN_PITCH = CENTER_PITCH - 16
MAX_PITCH = CENTER_PITCH + 16


durNames = ['16th', '8th', '*8th', 'qrtr', '*qrtr', 'half', '*half', 'whole']

def reducedValueToPitch(i, representation='DIATONIC'):
    return valueToPitch(i + MIN_PITCH, representation=representation)
    
    
# CONVERSION ARRAYS - reduced to original and back

# duration
dr2o = array([1,2,3,4,6,8,12,16], dtype=uint16)
do2r = array([0,0,1,2,3,3,4,4,5,5,5,5,6,6,6,6,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7], dtype=uint8)

# PITCH: 69 +- 16 = 53..85 => 0..32
# chromatic pitch
cpo2r = arange(128, dtype=uint8)
cpo2r[:MIN_PITCH] = (cpo2r[:MIN_PITCH]+(-MIN_PITCH%12)) % 12
cpo2r[MIN_PITCH:MAX_PITCH] = (cpo2r[MIN_PITCH:MAX_PITCH] - MIN_PITCH)
cpo2r[MAX_PITCH:] = (cpo2r[MAX_PITCH:]-MAX_PITCH) % 12 + 20

# diatonic pitch
dpo2r = arange(128, dtype=uint8)
dpo2r[:MIN_PITCH] = (dpo2r[:MIN_PITCH]+(-MIN_PITCH%7)) % 7
dpo2r[MIN_PITCH:MAX_PITCH] = (dpo2r[MIN_PITCH:MAX_PITCH] - MIN_PITCH)
dpo2r[MAX_PITCH:] = (dpo2r[MAX_PITCH:]-MAX_PITCH) % 7 + 25



class ReducedModel(object):
    '''
    Simplified musical model
    '''
    
    def __init__(self, representation='CHROMATIC', key=64, major=True, numNotes=0):
        self.representation = representation
        self.key = key
        self.major = major
        self.notes = zeros((numNotes, 3), dtype=uint8)
        
    
    @property
    def numNotes(self):              return self.notes.shape[0]
    @property
    def durations(self):             return self.notes[:,0]
    @property
    def pitches(self):               return self.notes[:,1]
    @property
    def durationsAndPitches(self):   return self.notes[:,2]
    
    @property
    def durationDiffs(self):         return (64 + self.durations[1:] - self.durations[:-1])
    @property
    def pitchDiffs(self):            return (64 + self.pitches[1:] - self.pitches[:-1])
    @property
    def durationAndPitchDiffs(self): return (64 + self.durationsAndPitches[1:] - self.durationsAndPitches[:-1])

    # legacy methods
    @property
    def durationsOrig(self):         return dr2o[self.durations]
    @property
    def pitchesOrig(self):           return self.pitches + MIN_PITCH
    @property
    def length(self):                return sum(self.durationsOrig)
    @property
    def lengthInSecs(self):          return float(self.length) / 8.0
    @property
    def numTracks(self):             return 1
    @property
    def ticksPerQuarterNote(self):   return 4
    @property
    def quarterNotesPerMinute(self): return 120
        

    def __repr__(self):
        ret = "ReducedModel[representation=%s, key=%d, major=%s, numNotes=%d, length=%d, notes:\n" %(
            self.representation, self.key, self.major, self.numNotes, self.length)
        for i in range(self.numNotes):
            ret += "    %03d:[duration=%05s(%d), pitch=%s(%02d)]\n" %(
                i, durNames[self.notes[i,0]], self.notes[i,0], reducedValueToPitch(self.notes[i,1], self.representation), self.notes[i,1])
        ret += "]"
        return ret

    
    def __getstate__(self):
        '''
        Overwrite serialization because cPickle saves extra information making the output files very large.
        '''
        notesBA = bytearray(self.notes)
        return (self.representation, self.key, self.major, notesBA)


    def __setstate__(self, state):
        '''
        Overwrite serialization because cPickle saves extra information making the output files very large.
        '''
        self.representation, self.key, self.major, notesBA = state
        self.notes = frombuffer(notesBA, dtype=uint8)
        self.notes.shape = (len(self.notes) / 3, 3)



@tymedCls
class ReducedModelBuilder(object):
    '''
    '''
    
    def __init__(self, modelRepresentation = 'DIATONIC', modelKey = 64, modelMajor = True, **kwargs):
        print 'Initializing reduced model builder'
        self.modelRepresentation = modelRepresentation
        self.modelKey = int(cpo2r[modelKey])
        self.modelMajor = modelMajor
    
    #@property
    def bytesPerNote(self): return 1
        
    @tymed
    def bytesToModel(self, mbytes):
        numNotes = len(mbytes)
        
        model = ReducedModel(representation=self.modelRepresentation,
                             key=self.modelKey, major=self.modelMajor,
                             numNotes=numNotes)
        
        model.notes[:,0] = mbytes >> 5   # duration is most significant 3 bits
        model.notes[:,1] = mbytes & 0x1F # pitch is last 5 bits
        model.notes[:,2] = mbytes        # combined number
            
        return model
    
    
    def __getstate__(self): return (self.modelRepresentation, self.modelKey, self.modelMajor)
    def __setstate__(self, state):  self.modelRepresentation, self.modelKey, self.modelMajor = state



def toReduced(model):
    '''
    Convert original model to reduced model.
    '''
    ret = ReducedModel(representation=model.representation,
                       key=cpo2r[model.key],
                       major=model.major,
                       numNotes=model.tracks[0].numNotes)
    
    ret.notes[:,0] = do2r[model.tracks[0].durations]    # duration
    
    if model.representation == 'DIATONIC':
        ret.notes[:,1] = dpo2r[model.tracks[0].pitches] # pitch
    else:
        ret.notes[:,1] = cpo2r[model.tracks[0].pitches]
    
    ret.notes[:,2] = (ret.notes[:,0] << 5) + ret.notes[:,1] # combined
    
    return ret
    

def toOriginal(redmodel):
    '''
    Convert reduced model to original.
    '''
    ret = Model(numTracks=1,
                representation=redmodel.representation,
                key=redmodel.key+MIN_PITCH,
                major=redmodel.major)
    
    notes = zeros((redmodel.numNotes, ret.tracks[0].propsPerNote), dtype=ret.dtype)
    
    notes[:,1] = dr2o[redmodel.durations] # durations
    notes[0,0] = 0
    notes[1:,0] = notes[:-1,1]             # iois
    
    notes[0,2] = 0
    notes[0,3] = notes[0,1]
    for i in range(1, redmodel.numNotes):
        notes[i,2] = notes[i-1,2] + notes[i,0] # onset
        notes[i,3] = notes[i,2]   + notes[i,1] # offset
    
    notes[:,4] = redmodel.pitches + MIN_PITCH # pitch
    notes[:,5] = 127
    
    ret.tracks[0].notes = notes
    return ret
    


if __name__ == '__main__':
    
    from emc.model.modeldir import ModelDirectory
    md = ModelDirectory('http://www.cimbalom.nl/nepdalok.html',
                    modelRepresentation='DIATONIC',
                    preprocessingMethods = ['removeZeroVelocityNotes', 'makeMaxVelocity', 'resampleTo4Ticks', 
                                            'forceNoOverlappingNotes', 'clipOverlyLongNotes', 'asuint16']).read()
    i = [i for i in range(md.numModels) if md.filenames[i] == 'fene-ette-vona.mid'][0]
    print i
    
    m = md.models[i]
    print m
    rm = toReduced(m)
    print rm
    m2 = toOriginal(rm)
    print m2
    
    
