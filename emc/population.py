'''
Model objects for genetic algorithms.

'''
from numpy import float32, array, append, delete, zeros, arange, mean,\
    frombuffer, int32, round
from numpy import uint8
from tymed import tymedCls, tymed
from emcpp.swig.emcpp import combineByteArray, mutateBytesInByteArray


class Population(object):
    '''
    Denotes a general population object.
    Contains lists of genotypes, phenotypes and grades
    '''
    
    numUnits = 0
    numTests = 0
    
    def __init__(self, numUnits, numTests, geneticStringSize):
        self.numUnits = numUnits
        self.numTests = numTests
        self.geneticStringSize = geneticStringSize
        
        self.genotypes = zeros((numUnits, geneticStringSize), dtype=uint8)
        self.phenotypes = zeros(numUnits, dtype=object)
        self.grades = zeros(numUnits, dtype=float32)
        self.gradesPerTest = zeros((numUnits, numTests), dtype=float32)
        self.ages = zeros(numUnits, dtype=uint8)
    
    
    
    def __getstate__(self):
        genotypesBA = bytearray(self.genotypes)
        phenotypesList = self.phenotypes.tolist()
        gradesBA = bytearray(self.grades)
        gradesPerTestBA = bytearray(self.gradesPerTest)
        agesBA = bytearray(self.ages)
        
        return (self.numUnits, self.numTests, self.geneticStringSize,
                genotypesBA, phenotypesList, gradesBA, gradesPerTestBA, agesBA)
    
    
    
    def __setstate__(self, state):
        self.numUnits, self.numTests, self.geneticStringSize,\
            genotypesBA, phenotypesList, gradesBA, gradesPerTestBA, agesBA = state
        
        self.genotypes = frombuffer(genotypesBA, dtype=uint8)
        self.genotypes.shape = (self.numUnits, self.geneticStringSize)
        self.phenotypes = array(phenotypesList, dtype=object)
        self.grades = frombuffer(gradesBA, dtype=float32)
        self.gradesPerTest = frombuffer(gradesPerTestBA, dtype=float32)
        self.gradesPerTest.shape = (self.numUnits, self.numTests)
        self.ages = frombuffer(agesBA, dtype=uint8)
        


@tymedCls
class PopulationBreeder(object):
    '''
    Class which performs the procedures of the genotype breeder
    on entire populations.
    '''
    
    def __init__(self, numUnits, numTests, geneticStringSize, genotypeSelector,
                 operatorDist = [0.02, 0.98, 0.0, 0.0],
                 crossoverRate = 0.001, mutationRate = 0.02):
        self.numUnits = numUnits
        self.numTests = numTests
        self.geneticStringSize = geneticStringSize
        self.genotypeSelector = genotypeSelector
        
        self.operatorDist = 2 * round(array(operatorDist) * numUnits / 2.0).astype(int32)
        self.operatorDist[3] = numUnits - self.operatorDist[0] - self.operatorDist[1] - self.operatorDist[2]
        print "Operator distribution for pop size %d: %s" %(numUnits, self.operatorDist)
        self.crossoverRate = int(crossoverRate * geneticStringSize)
        self.mutationRate = int(mutationRate * geneticStringSize)
        print "Operator rates: %d, %d" %(self.crossoverRate, self.mutationRate)
        
    
    @tymed
    def breedNewGeneration(self, oldPopulation):
        '''
        Breeds a new generation from a previous one. Chooses parents and survivors
        using the genotype selector, and performs breeding/mutation using the genotype breeder.
        '''
        newPopulation = Population(self.numUnits, self.numTests, self.geneticStringSize)
        
        # selection
        (numSurvivors, numCrossMutated, numCrossover, numMutated) = self.operatorDist
        (survivors, parentsCM, parentsC, mutated) = self.genotypeSelector.chooseUnits(
            oldPopulation, numSurvivors, numCrossMutated, numCrossover, numMutated)
        
        #print (numSurvivors, numCrossMutated, numCrossover, numMutated)
        
        # 1. reproduction
        # survive chosen units to new population
        for idx in range(numSurvivors):
            newPopulation.genotypes[idx] = oldPopulation.genotypes[survivors[idx]]
            newPopulation.ages[idx] = oldPopulation.ages[survivors[idx]] + 1
        #print 'mean', mean(oldPopulation.grades), mean(oldPopulation.grades[survivors]), 'max', max(oldPopulation.grades), max(oldPopulation.grades[survivors])
        #for unitIdx in arange(numSurvivors):
            #print 'S', survivors[unitIdx]
        
        # 2. cross-mutation
        for unitIndex in arange(numCrossMutated / 2):
            # select parent genotypes
            mother = oldPopulation.genotypes[parentsCM[0, unitIndex]]
            father = oldPopulation.genotypes[parentsCM[1, unitIndex]]
            offspring1 = newPopulation.genotypes[numSurvivors + 2 * unitIndex]
            offspring2 = newPopulation.genotypes[numSurvivors + 2 * unitIndex + 1]
            # perform crossover
            combineByteArray(mother, father, offspring1, offspring2, self.crossoverRate)
            # mutate both offspring
            mutateBytesInByteArray(offspring1, self.mutationRate)
            mutateBytesInByteArray(offspring2, self.mutationRate)
            #print 'CM', parents[0, unitIndex], parents[1, unitIndex], crossoverRate, mutationRate1, mutationRate2
            
        # 3. crossover
        for unitIndex in arange(numCrossover / 2):
            # select parent genotypes
            mother = oldPopulation.genotypes[parentsC[0, unitIndex]]
            father = oldPopulation.genotypes[parentsC[1, unitIndex]]
            offspring1 = newPopulation.genotypes[numSurvivors + numCrossMutated + 2 * unitIndex]
            offspring2 = newPopulation.genotypes[numSurvivors + numCrossMutated + 2 * unitIndex + 1]
            # perform crossover
            combineByteArray(mother, father, offspring1, offspring2, self.crossoverRate)
            #print 'C', parents[0, unitIndex], parents[1, unitIndex], crossoverRate
        
        
        # 4. mutation
        newSurvivorIdxs = range(numMutated) + numSurvivors + numCrossMutated + numCrossover
        for idx in range(len(mutated)):
            newPopulation.genotypes[newSurvivorIdxs[idx]] = oldPopulation.genotypes[mutated[idx]].copy()
            newPopulation.ages[newSurvivorIdxs[idx]] = oldPopulation.ages[mutated[idx]]
        for unitIdx in range(numMutated) + numSurvivors + numCrossMutated + numCrossover:
            mutateBytesInByteArray(newPopulation.genotypes[unitIdx], self.mutationRate)
            #print 'M', mutated, mutationRate
        
        #print ''
        return newPopulation
    

