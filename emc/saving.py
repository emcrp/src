'''
Provides saving features for a genetic algorithm run.
Will be called after each generation.
Necessary since we do not wish to store all generations in memory. 

Provides an empty implementation (nothing is done to save state in-between generations).
Also provides an implementation where the individual fitness test results and the algorithm state are
saved after a certain generation count.
'''

import datetime
from numpy import max, mean, std, argpartition
from os import makedirs
from os.path import exists
from time import time

from emc.integ.midi import writeToMidi
from helper.numpyba import NumpyArrayBA
from helper.serializeutils import toFile


class GASavingMidiToFile(object):
    '''
    File serializer for algorithm state.
    Saves algorithm and fitness test results after a given number of iterations.
    This way they do not need to stay in memory.
    '''

    def __init__(self, cycleSize = 3, saveTopMidis = 1, outputDir = 'output',
                 saveGrades = True, saveAllGrades = False):
        '''
        Settings setup
        '''
        self.algorithm = None
        self.baseName = None
        
        self.cycleSize = cycleSize
        self.saveTopMidis = saveTopMidis
        self.outputDir = outputDir
        self.saveGrades = saveGrades
        self.saveAllGrades = saveAllGrades
        
    
    def init(self, algorithm, gitRevision, numUnits, numTests):
        '''
        Initialize caching fitness test array.
        Will get flushed when full and restarted from the beginning.
        
        D1: generation index, D2: unit index, D3: fitness test index.
        '''
        print 'Initializing saving mechanism'
        self.algorithm = algorithm
        self.ptr = 0
        
        if self.baseName is None:
            self.gitRevision = gitRevision
            self.timeStamp = datetime.datetime.fromtimestamp(time()).strftime('%Y%m%d-%H%M%S')
            
            if self.gitRevision:
                self.baseName = 'emc_time%s_rev%s' %(self.timeStamp, self.gitRevision)
            else:
                self.baseName = 'emc_time%s' %(self.timeStamp)
        
        if self.saveGrades:
            self.grades = NumpyArrayBA((self.cycleSize, 3))
        if self.saveAllGrades:
            self.allGrades = NumpyArrayBA((self.cycleSize, numUnits, numTests))
        
    
    def afterGeneration(self):
        '''
        Saves fitness test results to its array.
        If array full, will flush it.
        '''
        if self.saveGrades:
            self.grades[self.ptr, 0] = max(self.algorithm.population.grades)
            self.grades[self.ptr, 1] = mean(self.algorithm.population.grades)
            self.grades[self.ptr, 2] = std(self.algorithm.population.grades)
        if self.saveAllGrades:
            self.allGrades[self.ptr, :, :] = self.algorithm.population.gradesPerTest

        self.ptr = self.ptr + 1
        
        if self.algorithm.generation == 0 or self.ptr == self.cycleSize:
            self.flush(self.algorithm.generation)
        
    
    
    def finishRun(self):
        '''
        Flushes fitness test result array even if not full.
        Call with gen - 1, because this will be called after generation index already incremented.
        '''
        self.flush(self.algorithm.generation - 1)
    
    
    
    def flush(self, generation):
        '''
        Flushes fitness test array elements to file. Serializes algorithm state into another file.
        Goes up until write pointer of cache. Ignored if write pointer is at 0.
        Also exports best MIDI files from current generation.
        '''
        if self.ptr is 0:
            return
        
        self.ensureSubDirExists('')
        
        if self.saveGrades:
            gradesFileName = "%s/%s/emc_grades%05d-%05d.bin" %(self.outputDir, self.baseName,
                                                                  generation - self.ptr + 1, generation)
            toFile(self.grades[:self.ptr, :], gradesFileName)
        if self.saveAllGrades:
            allGradesFileName = "%s/%s/emc_allGrades%05d-%05d.bin" %(self.outputDir, self.baseName,
                                                                     generation - self.ptr + 1, generation)
            toFile(self.allGrades[:self.ptr, :, :], allGradesFileName)
        
        algFileName = "%s/%s/emc_gen%05d.emc" %(self.outputDir, self.baseName, generation)
        toFile(self.algorithm, algFileName)
        
        self.ptr = 0
        
        bestModelIdxs = argpartition(self.algorithm.population.grades, -self.saveTopMidis)[-self.saveTopMidis:]
        for bestModelIdx in bestModelIdxs:
            bestModel = self.algorithm.population.phenotypes[bestModelIdx]
            bestModelGrade = self.algorithm.population.grades[bestModelIdx]
            
            midiFileName = "%s/%s/emc_score%.3f_gen%05d_ind%04d.mid" %(self.outputDir, self.baseName, 
                                                                       bestModelGrade, generation, bestModelIdx)
            #print "WRITING THE BEST"
            #print bestModel
            #print self.algorithm.population.grades[bestModelIdx], self.algorithm.population.gradesPerTest[bestModelIdx]
            #print midiFileName
            writeToMidi(bestModel, midiFileName)
        
        self.ptr = 0
    
    
    
    def ensureSubDirExists(self, subdir):
        if not exists('%s/%s/%s' %(self.outputDir, self.baseName, subdir)):
            makedirs('%s/%s/%s' %(self.outputDir, self.baseName, subdir))
    
    
            
    def reset(self):
        '''
        Reset state of saving mechanism.
        Resets timestamp so it becomes a new run.
        '''
        self.timeStamp = datetime.datetime.fromtimestamp(time()).strftime('%Y%m%d-%H%M%S')
        
        if self.gitRevision:
            self.baseName = 'emc_time%s_rev%s' %(self.timeStamp, self.gitRevision)
        else:
            self.baseName = 'emc_time%s' %(self.timeStamp)
    
    
    
    def __getstate__(self):
        '''
        Do not serialize cache array.
        '''
        return (self.cycleSize, self.saveTopMidis, self.outputDir, self.baseName,
                self.saveGrades, self.saveAllGrades)
    
    
    
    def __setstate__(self, state):
        '''
        Recall only helper objects.
        '''
        self.algorithm = None
        self.cycleSize, self.saveTopMidis, self.outputDir, self.baseName,\
            self.saveGrades, self.saveAllGrades = state

