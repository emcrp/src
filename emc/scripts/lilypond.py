'''
Creating MIDI Lilypond graphic out of model:
'''

from subprocess import call
from os import remove, listdir
from os.path import exists, join
from emc.scripts.midicrop import cropModel
from emc.integ.midi import readFromMidi

def midi2ly(midiPath, lyPath=None):
    if lyPath is None:
        lyPath = midiPath.replace('.mid', '.ly')
    call(['midi2ly', '--output=%s' %(lyPath), midiPath])
    return lyPath

def ly2pdf(lyPath, pdfPath=None):
    if pdfPath is None:
        pdfPath = lyPath.replace('.ly', '.pdf')
    extraMidiPath = lyPath.replace('.ly', '.midi')
    call(['lilypond', '--output=%s' %(pdfPath.replace('.pdf', '')), lyPath])
    remove(extraMidiPath)
    return pdfPath

def midi2pdf(midiPath, pdfPath=None, removeLy=True):
    lyPath = midi2ly(midiPath)
    pdfPath = ly2pdf(lyPath, pdfPath)
    
    if removeLy:
        remove(lyPath)
    return pdfPath
    
if __name__ == '__main__':
    '''
    mname = 'emc_score0.790_gen04500_ind0473.mid'
    startTicks = 66
    endTicks = 128
    
    midiPath = join('/home/csaba/src/emc/halloffame', mname)
    #print readFromMidi(mpath, assumedKey=64, assumedMajor=True)
    midiPath = cropModel(filename=midiPath, startTicks=startTicks, endTicks=endTicks)
    
    lyPath = midiPath.replace('.mid', '.ly')
    pdfPath = lyPath.replace('.ly', '.pdf')
    
    if not exists(lyPath):
        midi2ly(midiPath, lyPath)
        
        print ''Ly file created, please modify. Pointers:
        \\time 11/16
        \key d \major
        \set Staff.instrumentName = ""
        ''
    
    else:
        print "LY file exists"
        ly2pdf(lyPath, pdfPath)
    '''
    
    midiDir = '/home/csaba/src/emc/phd-preliminary/midi/random'
    for midiFn in listdir(midiDir):
        if midiFn.endswith('.mid'):
            midiPath = join(midiDir, midiFn)
            midi2pdf(midiPath)
    