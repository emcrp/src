from re import search

from os import listdir
from os.path import join

if __name__ == '__main__':
    confName = 'gecco2019'
    outputDir = '/home/csaba/src/emc/emcrp.gitlab.io/public/midi/%s' %(confName)
    htmlFile = '/home/csaba/src/emc/emcrp.gitlab.io/public/html/midi/%s.html' %(confName)
    
    spacing = 0
    tab = ' ' * spacing
    
    htmlHeader = tab + '''<table style="width:100%" class="sortable">
      <thead>
        <tr>
          <th><a href="#a">Time stamp</a></th>
          <th><a href="#a">VM architecture</a></th>
          <th><a href="#a">Pitch representation</a></th>
          <th><a href="#a">Generation</a></th>
          <th><a href="#a">Grade</a></th>
          <th><a href="#a">Download link</a></th>
        </tr>
      </thead>
      <tbody>
    '''.replace("\n", "\n" + tab)
    
    htmlFooter = tab + '''  </tbody>
      <tfoot>
      </tfoot>
    </table>
    '''.replace("\n", "\n" + tab)
    
    
    htmlStr = htmlHeader
    
    archs = {'musicdsl':'Music DSL',
             'immediate':'Immediate' }
    pitchs = {'diatonic':'Diatonic',
              'chromatic':'Chromatic',
              'shiftedchromatic':'Shifted chromatic' }
    
    for a in archs.keys():
        for p in pitchs.keys():
            testDir = join(outputDir, 'emc-%s-%s-%s' %(confName, a, p))
            
            timestamps = []
            runDirs = []
            
            print "Searching for test runs in %s" %(testDir)
            
            for subDir in sorted(listdir(testDir)):
                match = search('emc_time([0-9\-]*)_rev.*', subDir)
                if match:
                    timestamps.append(match.group(1))
                    runDirs.append(subDir)
            
            for run, runDir in enumerate(runDirs):
                for mf in listdir(join(testDir, runDir)):
                    match = search('emc_score([0-9\.]+)_gen([0-9]+)_ind.*\.mid', mf)
                    if match:
                        grade = match.group(1)
                        generation = int(match.group(2))
                        relPath = "midi/%s/emc-%s-%s-%s/%s/%s" %(confName, confName, a, p, runDir, mf)
                        htmlStr += tab + '    <tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td><a href="%s">Download</a></td></tr>\n' %(
                                   timestamps[run], archs[a], pitchs[p], generation, grade, relPath)
            
    htmlStr += htmlFooter
    print htmlStr
    f = open(htmlFile, 'w')
    f.write(htmlStr)
    f.close()
    