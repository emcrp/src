'''
Model preprocessing methods.
Can be called in chain for model directory.
'''
from numpy import append, diff, arange, histogram, argmax, log2, uint16
from emc.integ.midi import readFromMidi
from emc.scripts.pitchcluster import clusterModelByPitchToTwoTracks
from emc.model.model import Model
from emc.integ.ceg import findKeysFromModel, toRepresentation, toDiatonic
from emc.model.modelred import toReduced


def makeMaxVelocity(model):
    '''
    make all notes' velocities max (127)
    '''
    for track in model.tracks:
        track.notes[:, 5] = 127
    return model        
        
        
def removeZeroVelocityNotes(model):
    '''
    Remove all notes with 0 velocity from model.
    If makeOtherNotesMax is set, all other notes are set to velocity 127.
    '''
    for track in model.tracks:
        # find all notes with non-zero velocity
        nonZeroIndices = [i for i in arange(track.numNotes) if track.notes[i, 5] != 0]
        # only keep those notes
        track.notes = track.notes[nonZeroIndices,:]
        # recalculate onset/duration from start and end time
        track.notes[:,1] = track.notes[:,3] - track.notes[:, 2]
        track.notes[:,0] = diff(append([0], track.notes[:,2]))
    
    return model



def _findOptimalTicksPerQuarterNote(model):
    '''
    Given a model, finds optimal value for ticksPerQuarterNote, so that minimal quantization error occurs.
    Needed since many MIDIs are encoded with 384 ticks per quarter note, where the actual quarter notes are different sized.
    '''
    iois = model.interonsets
    # find histogram of inter-onset intervals
    h = histogram(iois, bins=arange(max(iois)+1))[0]
    # discard notes occuring at the same time
    h[0] = 0
    # most occuring inter-onset interval
    maxioi = argmax(h)
    # find which note this should correspond to: quarter, eighth etc.
    # i.e. find n where (2^n * maxioi) closest to original ticksPerQuarterNote
    n = log2(float(model.ticksPerQuarterNote) / maxioi)
    # new ticksPerQuarterNote should be |2^n| * maxioi
    return round(2 ** n) * maxioi


    
def resampleTo4Ticks(model):
    '''
    Assign new ticksPerQuarterNote, quantize data in a model.
    '''
    optimalTicks = _findOptimalTicksPerQuarterNote(model)
    model.ticksPerQuarterNote = optimalTicks
    
    # calculate scaling factor
    # target is 4 ticks per quarter note
    q = 4.0 / float(model.ticksPerQuarterNote)
    
    model.ticksPerQuarterNote = 4
    
    for track in model.tracks:
        # scale down start and end times
        track.notes[:,2] = track.notes[:,2] * q
        track.notes[:,3] = track.notes[:,3] * q
        # deduce onset/duration from start and end time
        track.notes[:,1] = track.notes[:,3] - track.notes[:, 2]
        track.notes[:,0] = diff(append([0], track.notes[:,2]))
    
    return model



def forceNoOverlappingNotes(model):
    '''
    Force the notes of the model to have no overlap,
    i.e. if a note resounds until after the onset of the next, it is cut short.
    Exception: if two notes start at the same time.
    '''
    for track in model.tracks:
        for i in arange(track.numNotes - 1):
            # if current note offset after next note's onset and they did not start together
            if track.notes[i,3] > track.notes[i+1, 2] and track.notes[i,2] < track.notes[i+1, 2]:
                # then clip note
                track.notes[i,3] = track.notes[i+1, 2]
                # adjust duration accordingly
                track.notes[i,1] = track.notes[i,3] - track.notes[i,2]
    return model


def clipOverlyLongNotes(model):
    '''
    Clip all notes whose IOI/durations are too long (over 15).
    '''
    for track in model.tracks:
        for i in arange(track.numNotes):
            # if current IOI too long
            if track.notes[i,0] > 16:
                # register how much we are shifting onsets/offsets
                delta = track.notes[i,0] - 16
                # limit IOI accordingly
                track.notes[i,0] = 16
                # adjust all onsets/offsets from here
                track.notes[i:,2] -= delta
                track.notes[i:,3] -= delta
                
            # if current duration too long
            if track.notes[i,1] > 16:
                # limit duration accordingly
                track.notes[i,1] = 16 
                # adjust new offset
                track.notes[i,3] = track.notes[i,2] + track.notes[i,1]
    return model


def asuint16(model):
    '''
    Cast all notes to uint16.
    Should be performed after resampling.
    '''
    for track in model.tracks:
        track.notes = track.notes.astype(uint16)
        track.dtype = uint16
    model.dtype = uint16
    return model


def reduceModels(model):
    '''
    Convert full model into reduced one.
    '''
    return toReduced(model)

    
def separateTo2Tracks(model):
    '''
    Separate one-track model into 2 tracks.
    '''
    notes = model.tracks[0].notes
    
    cl = clusterModelByPitchToTwoTracks(model)
    
    # extract notes based on cluster
    # update IOI using onsets, since that depends on previous note
    # warning: will not work if onsets overflowed
    notes1 = notes[cl == 0, :]
    notes1[0, 0] = notes1[0, 2]
    notes1[1:, 0] = notes1[1:, 2] - notes1[:-1, 2]
    notes2 = notes[cl == 1, :]
    notes2[0, 0] = notes2[0, 2]
    notes2[1:, 0] = notes2[1:, 2] - notes2[:-1, 2]
    
    # build new 2-track model
    newmodel = Model(ticksPerQuarterNote=model.ticksPerQuarterNote,
                     quarterNotesPerMinute=model.quarterNotesPerMinute,
                     numTracks=2,
                     representation=model.representation,
                     key=model.key,major=model.major,
                     dtype=model.dtype)
    newmodel.tracks[0].notes = notes1
    newmodel.tracks[1].notes = notes2

    return newmodel


def onlyMajor(model):
    '''
    Filter out models whose detected key is not major.
    '''
    if model.representation == 'CHROMATIC':
        model.key, model.major, _ = findKeysFromModel(model)
        
    if model.major: 
        return model
    else: 
        return None


def onlyMinor(model):
    '''
    Filter out models whose detected key is not minor.
    '''
    if model.representation == 'CHROMATIC':
        model.key, model.major, _ = findKeysFromModel(model)
        
    if not model.major:
        return model
    else:
        return None
    

def ensureMajor(model):
    '''
    Make sure the model is in major. If it isn't, convert it.
    '''
    if model.representation == 'CHROMATIC':
        model.key, model.major, _ = findKeysFromModel(model)
    if model.major:
        return model
    
    representation = model.representation
    toDiatonic(model, makeCopy=False)
    model.major = True
    toRepresentation(model, representation, makeCopy=False)
    return model


def ensureMinor(model):
    '''
    Make sure the model is in minor. If it isn't, convert it.
    '''
    if model.representation == 'CHROMATIC':
        model.key, model.major, _ = findKeysFromModel(model)
    if not model.major:
        return model
    
    representation = model.representation
    toDiatonic(model, makeCopy=False)
    model.major = False
    toRepresentation(model, representation, makeCopy=False)
    return model


# ===================
# overarching methods
# ===================

def preprocessSingleModel(methods, model):
    '''
    Call preprocessing method from current module for single model.
    '''
    for method in methods:
        model = globals()[method](model)
    return model


def preprocessModels(method, models):
    '''
    Call preprocessing method from current module for single model.
    '''
    for modelIdx in range(len(models)):
        models[modelIdx] = globals()[method](models[modelIdx])
    return models


if __name__ == '__main__':
    m = readFromMidi('/home/csaba/src/emc/midi/nepdalok/a-romanoke.mid')
    preprocessSingleModel(['removeZeroVelocityNotes', 'makeMaxVelocity', 'resampleTo4Ticks', 'forceNoOverlappingNotes'], m)
    print m