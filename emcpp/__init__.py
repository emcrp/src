'''
Check if C++ module is compiled and importable.
If not, run build manually.
'''

try:
    print 'Importing EMC C++ module for the first time'
    import emcpp.swig.emcpp
except:
    print 'Could not import EMC C++ module, compiling it'
    from setup import main
    main()