#include "bytearray.h"


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>


unsigned int seed()
{
  unsigned int ret = time(NULL);
  srand(ret);
  return ret;
}


void seed(unsigned int seed)
{
  srand(seed);
}


void randomByteArray(unsigned char* outp, int outp_size)
{
  for (int i = 0; i < outp_size; i++)
  {
    outp[i] = rand();
  }
}

void randomByteArrayInpl(unsigned char* outp, int outp_size)
{
  for (int i = 0; i < outp_size; i++)
  {
    outp[i] = rand();
  }
}


void combineByteArray(unsigned char* inp1, int inp1_size, unsigned char* inp2, int inp2_size,
                      unsigned char* outp1, int outp1_size, unsigned char* outp2, int outp2_size,
                      unsigned int num_cut_points)
{
  if (num_cut_points == 0)
  {
  	// no crossover, just copy parents to children
  	memcpy(outp1, inp1, inp1_size);
  	memcpy(outp2, inp2, inp2_size);
  }
  
  unsigned int* cut_points = new unsigned int[num_cut_points + 1];
  double cut_points_scale = 0.0;

  for (unsigned int i = 0; i < num_cut_points + 1; i++)
  {
    cut_points[i] = rand();
    cut_points_scale += cut_points[i];
  }

  cut_points_scale = inp1_size / cut_points_scale;
  double cut_point_sum = 0.5; // use 0.5 because msvc has no round function - simulate by using floor(x+0.5)

  for (unsigned int i = 0; i < num_cut_points + 1; i++)
  {
    cut_point_sum += cut_points[i] * cut_points_scale;
    cut_points[i] = (unsigned int) floor(cut_point_sum);
  }

  unsigned char** inputs = new unsigned char*[2];
  inputs[0] = inp1; inputs[1] = inp2;
  char current_inp = 0;

  unsigned int next_cut_point = 0;

  for (int i = 0; i < inp1_size; i++)
  {
    outp1[i] = inputs[current_inp][i];
    outp2[i] = inputs[1 - current_inp][i];

    if (i == cut_points[next_cut_point])
    {
      current_inp = 1 - current_inp;
      next_cut_point++;
    }
  }

  delete [] inputs;
  delete [] cut_points;
}


void mutateBytesInByteArray(unsigned char* ba, int ba_size, unsigned int num_mutated_bytes)
{
  if (num_mutated_bytes == 0) {
    // no mutation necessary
  	return;
  }
  
  int rand_byte_idx;

  for (unsigned int i = 0; i < num_mutated_bytes; ++i)
  {
    rand_byte_idx = rand() % ba_size;
    ba[rand_byte_idx] = rand();
  }
}


float randf(float maximum)
{
  return (float) rand() / (float) RAND_MAX * maximum;
}


float sum(float* values, int values_size)
{
  float sum = 0.0;

  for (int i = 0; i < values_size; ++i)
  {
    sum += values[i];
  }
  
  return sum;
}


bool contains(unsigned short* inp, int inp_size, unsigned short value)
{
  for (int i = 0; i < inp_size; ++i)
  {
    if (inp[i] == value)
    {
      return true;
    }
  }
  return false;
}


void probability(float* values, int values_size, float minProb)
{
  float s = sum(values, values_size);

  if (s != 0)
  {
    for (int i = 0; i < values_size; ++i)
    {
      values[i] /= s;
    }
  }
  else
  {
    values[0] = 1.0f / values_size;

    for (int i = 1; i < values_size; ++i)
    {
      values[i] = values[i-1];
    }
  }
  
  if (minProb != 0.0f)
  {
    float scaledMinProb = minProb / values_size;

    for (int i = 0; i < values_size; ++i)
    {
      values[i] = (1.0f - minProb) * values[i] + scaledMinProb;
    }
  }
}


void probability(float* values, int values_size, float* probs, int probs_size, float minProb)
{
  float s = sum(values, values_size);

  if (s != 0)
  {
    for (int i = 0; i < values_size; ++i)
    {
      probs[i] = values[i] / s;
    }
  }
  else
  {
    probs[0] = 1.0f / values_size;

    for (int i = 1; i < values_size; ++i)
    {
      probs[i] = probs[i-1];
    }
  }
  
  if (minProb != 0.0f)
  {
    float scaledMinProb = minProb / values_size;

    for (int i = 0; i < values_size; ++i)
    {
      probs[i] = (1.0f - minProb) * probs[i] + scaledMinProb;
    }
  }
}


void cumulative(float* probs, int probs_size)
{
  for (int i = 1; i < probs_size; ++i)
  {
    probs[i] += probs[i-1];
  }
}


void cumulative(float* inp, int inp_size, float* outp, int outp_size)
{
  outp[0] = inp[0];
  
  for (int i = 1; i < outp_size; ++i)
  {
    outp[i] = outp[i] + inp[i-1];
  }
}


unsigned short chooseOne(float* probs, int probs_size)
{
  float s = sum(probs, probs_size);
  float partialSum = 0.0f;
  
  short idx = 0;
  float r = randf(s);
  while (idx <= probs_size - 1)
  {
    if (r >= partialSum && r <= partialSum + probs[idx])
    {
      break;
    }
    partialSum += probs[idx];
    idx++;
  }
  return idx;
}



void choose(float* probs, int probs_size, unsigned short* units, int units_size)
{
  float s = sum(probs, probs_size);
  //printf("choose = {");
  
  for (int i = 0; i < units_size; ++i)
  {
    units[i] = 0;
    float partialSum = 0.0f;
    float r = randf(s);
    while (units[i] <= probs_size - 1)
    {
      if (r >= partialSum && r <= partialSum + probs[units[i]])
      {
        break;
      }
      partialSum += probs[units[i]];
      units[i]++;
    }
    //printf("%d, ", units[i]);
  }
  //printf("}\n");
}


void chooseUnique(float* probs, int probs_size, unsigned short* units, int units_size)
{
  if (units_size > probs_size)
  {
    printf("Error: More values expected from chooseUnique than there are in the array: %d > %d\n", units_size, probs_size);
    return;  	
  }
  
  float s = sum(probs, probs_size);
  //printf("chooseUnique = {");
  
  for (int i = 0; i < units_size; ++i)
  {
    units[i] = 0;
    float partialSum = 0.0f;
    float r = randf(s);
    //printf("r=%.9f\n", r);
    
    while (units[i] <= probs_size - 1)
    {
      if (!contains(units, i, units[i]))
      {
        //printf("  %d: %.9f - %.9f\n", units[i], partialSum, partialSum + probs[units[i]]);
        if (r >= partialSum && r <= partialSum + probs[units[i]])
        {
          break;
        }
      	partialSum += probs[units[i]];
      }
      /*else
      {
      	printf("  %d: x\n", units[i]);
      }*/
      units[i]++;
    }
    
    if (units[i] >= probs_size)
    {
      // if none was found, float sum correction failed; take first available value
      units[i] = 0;
      
      while (contains(units, i, units[i]) && units[i] <= probs_size - 1)
      {
        units[i]++;
      }
      
      if (units[i] >= probs_size)
      {
        // if for some strange reason it is still not good.
      	printf("Error in chooseUnique. Behavior unpredictable.\n");
      }
    }
    
    s -= probs[units[i]];
    
    /*printf("%d, ", units[i]);
    printf("idx[%d]=%d\n", i, units[i]);
    printf("s=%.9f  ", s);*/
  }
  //printf("}\n");
}


void applyMinProb(float* inp, int inp_size, float* outp, int outp_size, float minProb)
{
  float defaultProb = sum(inp, inp_size) * minProb / (float) inp_size;
  
  for (int i = 0; i < inp_size; ++i)
  {
  	outp[i] = (1.0f - minProb) * inp[i] + defaultProb;
  }
}
