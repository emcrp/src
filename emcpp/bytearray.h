/**
 * C++ library for EMC genetic string manipulation.
 *
 */


#pragma once


/**
 * Seed random generator with time and return seed
 * for later reproducibility.
 */
unsigned int seed();


/**
 * Reseed random generator with given seed.
 * Used to reproduce previous results.
 */
void seed(unsigned int seed);


/**
 * Create random byte array of given size.
 * Uses NumPy array, therefore allocation not necessary, but size must be in parameters.
 */
void randomByteArray(unsigned char* outp, int outp_size);

/**
 * Create random byte in the in-place array.
 * Uses NumPy array, therefore allocation not necessary, but size must be in parameters.
 */
void randomByteArrayInpl(unsigned char* inpl, int inpl_size);


/**
 * Splice together two byte arrays at multiple cut points. All 3 given sizes must be equal.
 * Randomly generates a set number of cut points, and at those points, changes which parent it takes values from.
 * Uses NumPy array, therefore allocation not necessary, but size must be in parameters.
 */
void combineByteArray(unsigned char* inp1, int inp1_size, unsigned char* inp2, int inp2_size,
                      unsigned char* inpl1, int inpl1_size, unsigned char* inpl2, int inpl2_size,
                      unsigned int num_cut_points);


/**
 * Mutate some bytes in a byte array.
 * Effectively give new random value to some random bytes in a byte array.
 * Uses NumPy array, therefore allocation not necessary, but size must be in parameters.
 */
void mutateBytesInByteArray(unsigned char* inpl, int inpl_size, unsigned int num_mutated_bytes);


//=================
// PROB OPERATIONS
//=================

float randf(float maximum = 1.0f);

float sum(float* inp, int inp_size);
bool contains(unsigned short* inp, int inp_size, unsigned short value);

void applyMinProb(float* inp, int inp_size, float* outp, int outp_size, float minProb);

void probability(float* inpl, int inpl_size, float minProb);
void probability(float* inp, int inp_size, float* outp, int outp_size, float minProb);
void cumulative(float* inpl, int inpl_size);
void cumulative(float* inp, int inp_size, float* outp, int outp_size);

unsigned short chooseOne(float* inp, int inp_size);
void choose(float* inp, int inp_size, unsigned short* outp, int outp_size);
void chooseUnique(float* inp, int inp_size, unsigned short* outp, int outp_size);
