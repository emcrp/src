#include "entropy.h"

#include <stdio.h>
#include <string.h>
#include <math.h>


int _occurrences[256];

float entropy(unsigned char* inp, int inp_size, int num_bits)
{
  // set counters to 0
  memset(_occurrences, 0, 256 * sizeof(int));

  for (unsigned int i = 0; i < inp_size; ++i)
  {
    _occurrences[inp[i]]++;
  }

  float ret = 0;

  for (unsigned int i = 0; i < 256; ++i)
  {
  	//printf("i=%d, occurence[i]=%d\n", i, _occurrences[i]);
    if (_occurrences[i] > 0)
    {
      float p_i = _occurrences[i] / (float) inp_size;
      ret += -p_i * log2(p_i);
    }
  }

  return ret / num_bits;
}
