/**
 * C++ library for data entropy calculation
 *
 */


#pragma once


/**
 * Measure binary Shannon entropy of data set.
 * Returns value between 0 and 1.
 */
float entropy(unsigned char* inp, int inp_size, int num_bits = 8);

