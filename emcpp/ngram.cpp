#include "ngram.h"
#include <string.h>
#include <stdio.h>
#include <math.h>


NGramHelper::NGramHelper(unsigned int nt, unsigned int nc,
                         bool uni, bool bi, bool tri) :
    _nt(nt), _nc(nc), _uni(uni), _bi(bi), _tri(tri) {
  //printf("uni=%d,bi=%d,tri=%d\n", _uni, _bi, _tri);
  _m = new map_s[_nt];
  _norm = new float[_nt];

  _c = new map_f*[_nc];
  for (int c = 0; c < _nc; ++c) {
    _c[c] = new map_f[_nt];
  }
}

NGramHelper::~NGramHelper() {
  delete [] _m;
  delete [] _norm;

  for (int c = 0; c < _nc; ++c) {
    delete [] _c[c];
  }
  delete [] _c;
}

void NGramHelper::setClusterCenters(int c, int t, unsigned int* keys, int keys_size,
                                                  float* values, int values_size) {
  for (int i = 0; i < keys_size; ++i) {
    //if (t == 0) printf("Setup: c=%d, t=%d, key=%d, value=%.3f\n", c, t, keys[i], values[i]);
    _c[c][t][keys[i]] = values[i];
  }
}

void NGramHelper::setModelFeature(int t, unsigned char* f, int f_size) {
  _m[t].clear();

  // unigram
  if (_uni)
    for (int i = 0; i < f_size; ++i) {
      _m[t][(1 << 24) | f[i]]++;
    }
  // bigram
  if (_bi)
    for (int i = 0; i < f_size-1; ++i) {
      _m[t][(2 << 24) | (f[i] << 8) | f[i+1]]++;
    }
  // trigram
  if (_tri)
    for (int i = 0; i < f_size-2; ++i) {
      _m[t][(3 << 24) | (f[i] << 16) | (f[i+1] << 8) | f[i+2]]++;
    }

  // calculate norm
  _norm[t] = 0.0f;
  for (iter_s itr = _m[t].begin(); itr != _m[t].end(); ++itr) {
    /*if (t == 0) {
      printf("k=%d, v=%d\n", itr->first, itr->second);
    }*/
    _norm[t] += itr->second * itr->second;
  }
  _norm[t] = sqrt(_norm[t]);
  //printf("t=%d,norm=%.3f\n", t,_norm[t]);
}


void NGramHelper::setModelFeatureWeighted(int t, unsigned char* f, int f_size) {
  _m[t].clear();

  // unigram
  if (_uni)
    for (int i = 0; i < f_size; ++i) {
      _m[t][(1 << 24) | f[i]]++;
    }
  // bigram
  if (_bi)
    for (int i = 0; i < f_size-1; ++i) {
      _m[t][(2 << 24) | (f[i] << 8) | f[i+1]] += 2;
    }
  // trigram
  if (_tri)
    for (int i = 0; i < f_size-2; ++i) {
      _m[t][(3 << 24) | (f[i] << 16) | (f[i+1] << 8) | f[i+2]] += 3;
    }

  // calculate norm
  _norm[t] = 0.0f;
  for (iter_s itr = _m[t].begin(); itr != _m[t].end(); ++itr) {
    /*if (t == 0) {
      printf("k=%d, v=%d\n", itr->first, itr->second);
    }*/
    _norm[t] += itr->second * itr->second;
  }
  _norm[t] = sqrt(_norm[t]);
  //printf("t=%d,norm=%.3f\n", t,_norm[t]);
}


void NGramHelper::grades(float* g, int g_size) {
  for (int t = 0; t < _nt; ++t) {
    g[t] = 0.0f;
    
    //int sizec = 0;
    for (int c = 0; c < _nc; ++c) {
      //sizec += _c[c][t].size();

      float gc = 0.0f;

      for (iter_s itm = _m[t].begin(); itm != _m[t].end(); ++itm) {
        if (_c[c][t].count(itm->first)) {
          gc += itm->second * _c[c][t][itm->first];
        }
      }

      //printf("t=%d,c=%d,g=%.3f\n", t,c,gc);
      gc /= _norm[t];
      g[t] = (gc > g[t]) ? gc : g[t];
    }

    //printf("t=%d, avg_size_c=%d, size_m=%d\n", t, sizec / _nc, _m[t].size());
  }
}