#include <map>

typedef std::map<unsigned int, unsigned short> map_s;
typedef map_s::const_iterator iter_s;
typedef std::map<unsigned int, float> map_f;
typedef map_f::const_iterator iter_f;

class NGramHelper {

public:
  NGramHelper(unsigned int nt, unsigned int nc,
              bool uni=true, bool bi=true, bool tri=true);
  ~NGramHelper();

  void setClusterCenters(int c, int t, unsigned int* inp1, int inp1_size, float* inp2, int inp2_size);
  void setModelFeature(int t, unsigned char* inp, int inp_size);
  void setModelFeatureWeighted(int t, unsigned char* inp, int inp_size);
  void grades(float* outp, int outp_size);

private:

  map_s* _m;
  float* _norm;
  map_f** _c;

  unsigned int _nt, _nc;

  bool _uni, _bi, _tri;

};