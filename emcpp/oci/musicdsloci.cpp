#include "musicdsloci.h"

#include <iostream>
using namespace std;

#define RAM(i) _touch(i)

#define DEFAULT_DURATION 0x20 // default dur is 1 and 1 << 5 = 000100000 = 0x20
//#define DEFAULT_DIATONIC_PITCH 64
//#define DEFAULT_CHROMATIC_PITCH 57
//#define DEFAULT_PITCH _diatonic ? DEFAULT_DIATONIC_PITCH : DEFAULT_CHROMATIC_PITCH

// (pitch - DEFAULT_CHROMATIC_PITCH) % 12 - gives delta index

// differences for seconds if piece in major
const int M2UpDelta[]   = {2,1,2,1,1,2,1,2,1,2,1,1};
const int M2DownDelta[] = {1,1,2,1,2,1,1,2,1,2,1,2};
// differences for seconds if piece in minor
const int m2UpDelta[]   = {2,1,1,2,1,2,1,1,2,1,2,1};
const int m2DownDelta[] = {2,1,2,1,1,2,1,2,1,1,2,1};

#define M2_UP_DELTA(i)    _isMajor ? M2UpDelta[i]   : m2UpDelta[i]
#define M2_DOWN_DELTA(i)  _isMajor ? M2DownDelta[i] : m2DownDelta[i]

// differences for thirds if piece in major
const int M3UpDelta[]   = {4,3,3,4,3,4,3,4,3,3,4,3};
const int M3DownDelta[] = {3,4,3,4,4,3,4,3,4,4,3,4};
// differences for thirds if piece in minor
const int m3UpDelta[]   = {3,4,3,4,3,3,4,3,4,3,4,3};
const int m3DownDelta[] = {4,3,4,3,4,3,4,4,3,4,3,4};

#define M3_UP_DELTA(i)    _isMajor ? M3UpDelta[i]   : m3UpDelta[i]
#define M3_DOWN_DELTA(i)  _isMajor ? M3DownDelta[i] : m3DownDelta[i]

// differences for fourths if piece in major
const int M4UpDelta[]   = {5,6,5,6,5,6,5,5,6,5,6,5};
const int M4DownDelta[] = {5,6,5,6,5,5,6,5,6,5,5,6};
// differences for fourths if piece in minor
const int m4UpDelta[]   = {5,6,5,5,6,5,6,5,6,5,5,6};
const int m4DownDelta[] = {5,5,6,5,6,5,6,5,5,6,5,6};

#define M4_UP_DELTA(i)    _isMajor ? M4UpDelta[i]   : m4UpDelta[i]
#define M4_DOWN_DELTA(i)  _isMajor ? M4DownDelta[i] : m4DownDelta[i]

// differences for fifths if piece in major
const int M5UpDelta[]   = {7,6,7,6,7,7,6,7,6,7,7,6};
const int M5DownDelta[] = {7,6,7,6,7,6,7,7,6,7,6,7};
// differences for fifths if piece in minor
const int m5UpDelta[]   = {7,7,6,7,6,7,6,7,7,6,7,6};
const int m5DownDelta[] = {7,6,7,7,6,7,6,7,6,7,7,6};

#define M5_UP_DELTA(i)    _isMajor ? M5UpDelta[i]   : m5UpDelta[i]
#define M5_DOWN_DELTA(i)  _isMajor ? M5DownDelta[i] : m5DownDelta[i]


// mapping a duration to another when doubling
const unsigned char DurDouble[] = {1,3,4,5,6,7,7,7};
// mapping a duration to another when halving
const unsigned char DurHalve[]  = {0,0,0,1,2,3,4,5};
// mapping a duration to another when dotting
const unsigned char DurDot[]    = {0,2,1,4,3,6,5,7};


// ========
// Transfer
// ========

void MusicDslOci::movNote_Next(unsigned char n) {
    _registers[n] = RAM(_counter++);
}

void MusicDslOci::movNote_Next_b(unsigned char n) {
    _registers[n+4] = RAM(_counter++);
}

void MusicDslOci::movNoteDur_Next(unsigned char n) {
    _registers[n] = (_registers[n] & 0x1F) | (RAM(_counter++) & 0xE0);
}

void MusicDslOci::movNoteDur_Next_b(unsigned char n) {
    _registers[n+4] = (_registers[n+4] & 0x1F) | (RAM(_counter++) & 0xE0);
}

void MusicDslOci::movNotePth_Next(unsigned char n) {
    _registers[n] = (_registers[n] & 0xE0) | (RAM(_counter++) & 0x1F);
}

void MusicDslOci::movNotePth_Next_b(unsigned char n) {
    _registers[n+4] = (_registers[n+4] & 0xE0) | (RAM(_counter++) & 0x1F);
}

void MusicDslOci::movNote_DPtr(unsigned char n) {
    _registers[n] = RAM(_dataPtr++);
}

void MusicDslOci::movNote_DPtr_b(unsigned char n) {
    _registers[n+4] = RAM(_dataPtr++);
}

void MusicDslOci::movNoteDur_DPtr(unsigned char n) {
    _registers[n] = (_registers[n] & 0x1F) | (RAM(_dataPtr++) & 0xE0);
}

void MusicDslOci::movNoteDur_DPtr_b(unsigned char n) {
    _registers[n+4] = (_registers[n+4] & 0x1F) | (RAM(_dataPtr++) & 0xE0);
}

void MusicDslOci::movNotePth_DPtr(unsigned char n) {
    _registers[n] = (_registers[n] & 0xE0) | (RAM(_dataPtr++) & 0x1F);
}

void MusicDslOci::movNotePth_DPtr_b(unsigned char n) {
    _registers[n+4] = (_registers[n+4] & 0xE0) | (RAM(_dataPtr++) & 0x1F);
}

void MusicDslOci::movDPtr_Note(unsigned char n) {
    RAM(_dataPtr) = _registers[n];
}

void MusicDslOci::movDPtr_Note_b(unsigned char n) {
    RAM(_dataPtr) = _registers[n+4];
}

void MusicDslOci::movDPtr_NoteDur(unsigned char n) {
    RAM(_dataPtr) = (RAM(_dataPtr) & 0x1F) | (_registers[n] & 0xE0);
}

void MusicDslOci::movDPtr_NoteDur_b(unsigned char n) {
    RAM(_dataPtr) = (RAM(_dataPtr) & 0x1F) | (_registers[n+4] & 0xE0);
}

void MusicDslOci::movDPtr_NotePth(unsigned char n) {
    RAM(_dataPtr) = (RAM(_dataPtr) & 0xE0) | (_registers[n] & 0x1F);
}

void MusicDslOci::movDPtr_NotePth_b(unsigned char n) {
    RAM(_dataPtr) = (RAM(_dataPtr) & 0xE0) | (_registers[n+4] & 0x1F);
}


void MusicDslOci::movD_Next() {
    ((unsigned char*) &_dataPtr)[0] = RAM(_counter++);
    ((unsigned char*) &_dataPtr)[1] = RAM(_counter++);
}

void MusicDslOci::movDH_Next() { ((unsigned char*) &_dataPtr)[1] = RAM(_counter++); }
void MusicDslOci::movDL_Next() { ((unsigned char*) &_dataPtr)[0] = RAM(_counter++); }
void MusicDslOci::movDPtr_Next() { RAM(_dataPtr) = RAM(_counter++); }
void MusicDslOci::movFlag_Next() { _flag = RAM(_counter++) & 1; }
void MusicDslOci::movFlag_DPtr() { _flag = RAM(_dataPtr) & 1; }

// ==================
// Arithmetic & Logic
// ==================

void MusicDslOci::reset_Dur(unsigned char n) {
    _registers[n] = (_registers[n] & 0x1F) | DEFAULT_DURATION;
}

void MusicDslOci::reset_Dur_b(unsigned char n) {
    _registers[n+4] = (_registers[n+4] & 0x1F) | DEFAULT_DURATION;
}

void MusicDslOci::reset_Pitch(unsigned char n) {
    _registers[n] = (_registers[n] & 0xE0) | _key;
}

void MusicDslOci::reset_Pitch_b(unsigned char n) {
    _registers[n+4] = (_registers[n+4] & 0xE0) | _key;
}

void MusicDslOci::inc_DurDbl(unsigned char o, unsigned char n) {
    _registers[n] = (DurDouble[_registers[n] >> 5] << 5) | (_registers[n] & 0x1F);
    if (o) outNote(n);
}

void MusicDslOci::inc_DurDbl_b(unsigned char o, unsigned char n) {
    _registers[n+4] = (DurDouble[_registers[n+4] >> 5] << 5) | (_registers[n+4] & 0x1F);
    if (o) outNote(n);
}

void MusicDslOci::inc_DurDot(unsigned char o, unsigned char n) {
    _registers[n] = (DurDot[_registers[n] >> 5] << 5) | (_registers[n] & 0x1F);
    if (o) outNote(n);
}

void MusicDslOci::inc_DurDot_b(unsigned char o, unsigned char n) {
    _registers[n+4] = (DurDot[_registers[n+4] >> 5] << 5) | (_registers[n+4] & 0x1F);
    if (o) outNote(n);
}

void MusicDslOci::dec_DurHalve(unsigned char o, unsigned char n) {
    _registers[n] = (DurHalve[_registers[n] >> 5] << 5) | (_registers[n] & 0x1F);
    if (o) outNote(n);
}

void MusicDslOci::dec_DurHalve_b(unsigned char o, unsigned char n) {
    _registers[n+4] = (DurHalve[_registers[n+4] >> 5] << 5) | (_registers[n+4] & 0x1F);
    if (o) outNote(n);
}


void MusicDslOci::inc_Pitch2(unsigned char o, unsigned char n) {
    unsigned char p = _registers[n] & 0x1F;

    if (_diatonic) {
        if (p <= 30) p += 1;
        else p -= 6;
    } else {
        int delta = M2_UP_DELTA((p + 12 - (_key % 12)) % 12);
        if (p <= 31 - delta) p += delta;
        else p -= 12 - delta;
    }

    _registers[n] = (p & 0x1F) | (_registers[n] & 0xE0);
    if (o) outNote(n);
}

void MusicDslOci::inc_Pitch3(unsigned char o, unsigned char n) {
    unsigned char p = _registers[n] & 0x1F;

    if (_diatonic) {
        if (p <= 29) p += 2;
        else p -= 5;
    } else {
        int delta = M3_UP_DELTA((p + 12 - (_key % 12)) % 12);
        if (p <= 31 - delta) p += delta;
        else p -= 12 - delta;
    }

    _registers[n] = (p & 0x1F) | (_registers[n] & 0xE0);
    if (o) outNote(n);
}

void MusicDslOci::inc_Pitch4(unsigned char o, unsigned char n) {
    unsigned char p = _registers[n] & 0x1F;

    if (_diatonic) {
        if (p <= 28) p += 3;
        else p -= 4;
    } else {
        int delta = M4_UP_DELTA((p + 12 - (_key % 12)) % 12);
        if (p <= 31 - delta) p += delta;
        else p -= 12 - delta;
    }

    _registers[n] = (p & 0x1F) | (_registers[n] & 0xE0);
    if (o) outNote(n);
}

void MusicDslOci::inc_Pitch5(unsigned char o, unsigned char n) { 
    unsigned char p = _registers[n] & 0x1F;

    if (_diatonic) {
        if (p <= 27) p += 4;
        else p -= 3;
    } else {
        int delta = M5_UP_DELTA((p + 12 - (_key % 12)) % 12);
        if (p <= 31 - delta) p += delta;
        else p -= 12 - delta;
    }

    _registers[n] = (p & 0x1F) | (_registers[n] & 0xE0);
    if (o) outNote(n);
}

void MusicDslOci::inc_Pitch8(unsigned char o, unsigned char n) {
    unsigned char p = _registers[n] & 0x1F;

    if (_diatonic) {
        if (p <= 24) p += 7;
    } else {
        if (p <= 19) p += 12;
    }

    _registers[n] = (p & 0x1F) | (_registers[n] & 0xE0);
    if (o) outNote(n);
}

void MusicDslOci::dec_Pitch2(unsigned char o, unsigned char n) {
    unsigned char p = _registers[n] & 0x1F;

    if (_diatonic)
        if (p >= 1) p -= 1;
        else p += 6;
    else {
        int delta = M2_DOWN_DELTA((p + 12 - (_key % 12)) % 12);
        if (p >= delta) p -= delta;
        else p += 12 - delta;
    }

    _registers[n] = (p & 0x1F) | (_registers[n] & 0xE0);
    if (o) outNote(n);
}

void MusicDslOci::dec_Pitch3(unsigned char o, unsigned char n) {
    unsigned char p = _registers[n] & 0x1F;

    if (_diatonic)
        if (p >= 2) p -= 2;
        else p += 5;
    else {
        int delta = M3_DOWN_DELTA((p + 12 - (_key % 12)) % 12);
        if (p >= delta) p -= delta;
        else p += 12 - delta;
    }

    _registers[n] = (p & 0x1F) | (_registers[n] & 0xE0);
    if (o) outNote(n);
}

void MusicDslOci::dec_Pitch4(unsigned char o, unsigned char n) {
    unsigned char p = _registers[n] & 0x1F;

    if (_diatonic)
        if (p >= 3) p -= 3;
        else p += 4;
    else {
        int delta = M4_DOWN_DELTA((p + 12 - (_key % 12)) % 12);
        if (p >= delta) p -= delta;
        else p += 12 - delta;
    }

    _registers[n] = (p & 0x1F) | (_registers[n] & 0xE0);
    if (o) outNote(n);
}

void MusicDslOci::dec_Pitch5(unsigned char o, unsigned char n) {
    unsigned char p = _registers[n] & 0x1F;

    if (_diatonic) {
        if (p >= 4) p -= 4;
        else p += 3;
    } else {
        int delta = M5_DOWN_DELTA((p + 12 - (_key % 12)) % 12);
        if (p >= delta) p -= delta;
        else p += 12 - delta;
    }

    _registers[n] = (p & 0x1F) | (_registers[n] & 0xE0);
    if (o) outNote(n);
}

void MusicDslOci::dec_Pitch8(unsigned char o, unsigned char n) { 
    unsigned char p = _registers[n] & 0x1F;

    if (_diatonic) {
        if (p >= 7) p -= 7;
    } else {
        if (p >= 12) p -= 12;
    }

    _registers[n] = (p & 0x1F) | (_registers[n] & 0xE0);
    if (o) outNote(n);
}

void MusicDslOci::inc_Pitch2_b(unsigned char o, unsigned char n) { inc_Pitch2(o, n+4); }
void MusicDslOci::inc_Pitch3_b(unsigned char o, unsigned char n) { inc_Pitch3(o, n+4); }
void MusicDslOci::inc_Pitch4_b(unsigned char o, unsigned char n) { inc_Pitch4(o, n+4); }
void MusicDslOci::inc_Pitch5_b(unsigned char o, unsigned char n) { inc_Pitch5(o, n+4); }
void MusicDslOci::inc_Pitch8_b(unsigned char o, unsigned char n) { inc_Pitch8(o, n+4); }
void MusicDslOci::dec_Pitch2_b(unsigned char o, unsigned char n) { dec_Pitch2(o, n+4); }
void MusicDslOci::dec_Pitch3_b(unsigned char o, unsigned char n) { dec_Pitch3(o, n+4); }
void MusicDslOci::dec_Pitch4_b(unsigned char o, unsigned char n) { dec_Pitch4(o, n+4); }
void MusicDslOci::dec_Pitch5_b(unsigned char o, unsigned char n) { dec_Pitch5(o, n+4); }
void MusicDslOci::dec_Pitch8_b(unsigned char o, unsigned char n) { dec_Pitch8(o, n+4); }


void MusicDslOci::setFlag() { _flag = 1; }
void MusicDslOci::unsetFlag() { _flag = 0; }
void MusicDslOci::invertFlag() { _flag = 1 - (_flag & 1); }
void MusicDslOci::inc_D(unsigned char d) { _dataPtr += d+1; }
void MusicDslOci::dec_D(unsigned char d) { _dataPtr -= d+1; }

// =========
// Branching
// =========

void MusicDslOci::jmpD()     { _counter = _dataPtr; }
void MusicDslOci::sjmpNext() { _counter += (char) RAM(_counter); }
void MusicDslOci::jmpNext()  { _counter = ((unsigned short*)(&RAM(_counter)))[0]; }
void MusicDslOci::sjmpDH()   { _counter += ((char*) &_dataPtr)[1]; }
void MusicDslOci::sjmpDL()   { _counter += ((char*) &_dataPtr)[0]; }

void MusicDslOci::jf_D()      { if (_flag)  jmpD(); }
void MusicDslOci::jnf_D()     { if (!_flag) jmpD(); }
void MusicDslOci::sjf_Next()  { if (_flag)  sjmpNext(); }
void MusicDslOci::sjnf_Next() { if (!_flag) sjmpNext(); }
void MusicDslOci::jf_Next()   { if (_flag)  jmpNext(); }
void MusicDslOci::jnf_Next()  { if (!_flag) jmpNext(); }
void MusicDslOci::sjf_DH()    { if (_flag)  sjmpDH(); }
void MusicDslOci::sjnf_DH()   { if (!_flag) sjmpDH();  }
void MusicDslOci::sjf_DL()    { if (_flag)  sjmpDL(); }
void MusicDslOci::sjnf_DL()   { if (!_flag) sjmpDL(); }

// ======
// Output
// ======

void MusicDslOci::outNote(unsigned char n) 
{
    // output note
    _out(_registers, n, 1);
}

void MusicDslOci::outNote_b(unsigned char n) 
{
    // output note
    _out(_registers, n+4, 1);
}

