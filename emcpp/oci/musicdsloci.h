/**
 * WARNING
 * =======
 * This is a generated file that can be overwritten with regeneration.
 * It is an op-code interpreter for the op code state machine MusicDsl.
 */


#pragma once

#include <stdio.h>
#include <string.h>

/**
 * Class for op-code interpreter MusicDsl
 * Description: DSL interpreter for music generation
 */
class MusicDslOci
{
public:

  MusicDslOci(unsigned int ramSize, bool isMajor, bool diatonic, unsigned char key, 
                unsigned int maxCommands, unsigned int maxOutputs, bool haltAllowed);
  ~MusicDslOci();


  // ===========================
  // externally callable methods
  // ===========================

  /**
   * Sets condition of VM using a genetic string (stored in inp).
   */
  void setFromGeneticString(unsigned char* inp, int inp_size);

  /**
   * Interpret RAM commands.
   * Reads bytes from where program counter points, and executes the mapped instructions.
   * Stops if halt flag set or if a maximum number of commands/outputs reached.
   */
  void interpret();

  /**
   * Interpret upcoming command.
   * Reads a byte from memory and bit-parses it to find the correct instruction to call.
   */
  void interpretNext();
  
  /**
   * Interpret n RAM commands.
   * Doesn't stop in either case the interpret() method would.
   * Use for performance testing.
   */
  void interpretN(unsigned int n);

  
  // =========
  // constants
  // =========
  
  static const unsigned int counterSize = 2;
  static const unsigned int numRegisters = 6;
  static const unsigned int numCategories = 4;
  
  
  // ===============
  // getters-setters
  // ===============

  const unsigned int geneticStringSize();
  
  unsigned short counter();
  void setCounter(unsigned short counter);
  bool isMajor();
  void setIsMajor(bool isMajor);
  bool diatonic();
  void setDiatonic(bool diatonic);
  unsigned char key();
  void setKey(unsigned char key);
  
  void registers(unsigned char* outp, int outp_size);
  unsigned char registerAt(unsigned short index);
  void setRegisters(unsigned char* inp, int inp_size);
  void setRegister(unsigned short index, unsigned char value);
  
  unsigned short dataPtr();
  void setDataPtr(unsigned short dataPtr);
  unsigned char flag();
  void setFlag(unsigned char flag);

  void ram(unsigned char* outp, int outp_size);
  unsigned char ramAt(unsigned short index);
  void setRam(unsigned char* inp, int inp_size);
  void setRamAt(unsigned short index, unsigned char value);

  void output(unsigned char* outp, int outp_size);
  unsigned char outputAt(unsigned short index);
  void setOutput(unsigned char* inp, int inp_size);
  void setOutputAt(unsigned short index, unsigned char value);
  unsigned int outputPtr();
  void setOutputPtr(unsigned int outputPtr);
  
  bool debug();
  void setDebug(bool debug);
  bool countOccurrences();
  void setCountOccurrences(bool countOccurrences);
  bool countTouched();
  void setCountTouched(bool countTouched);
  int numTouched();
  void setNumTouched(int numTouched);
  
  void occurrences(unsigned int* outp, int outp_size);
  unsigned int occurrencesAt(unsigned short index);
  void setOccurrences(unsigned int* inp, int inp_size);
  void setOccurrencesAt(unsigned short index, unsigned int value);
  
  void touched(unsigned short* outp, int outp_size);
  unsigned short touchedAt(unsigned short index);
  void setTouched(unsigned short* inp, int inp_size);
  void setTouchedAt(unsigned short index, unsigned short value);

  
  // ===================================
  // assigned states - main instructions
  // ===================================
  
  void movNote_Next(unsigned char n); // 000000 - Copies next byte into nth note
  void movNote_Next_b(unsigned char n); // 0000010 - Copies next byte into nth note
  void movNoteDur_Next_b(unsigned char n); // 0000011 - Copies next byte as duration of nth note
  void movNoteDur_Next(unsigned char n); // 000010 - Copies next byte as duration of nth note
  void movNotePth_Next(unsigned char n); // 000011 - Copies next byte as pitch of nth note
  void movNotePth_Next_b(unsigned char n); // 0001000 - Copies next byte as pitch of nth note
  void movNote_DPtr_b(unsigned char n); // 0001001 - Copies 1 byte from where dataPtr points into nth note
  void movNote_DPtr(unsigned char n); // 000101 - Copies 1 byte from where dataPtr points into nth note
  void movNoteDur_DPtr(unsigned char n); // 000110 - Copies 1 byte from where dataPtr points as duration of nth note
  void movNoteDur_DPtr_b(unsigned char n); // 0001110 - Copies 1 byte from where dataPtr points as duration of nth note
  void movNotePth_DPtr_b(unsigned char n); // 0001111 - Copies 1 byte from where dataPtr points as pitch of nth note
  void movNotePth_DPtr(unsigned char n); // 001000 - Copies 1 byte from where dataPtr points as pitch of nth note
  void movDPtr_Note(unsigned char n); // 001001 - Copies 1 byte from nth note to where dataPtr points
  void movDPtr_Note_b(unsigned char n); // 0010100 - Copies 1 byte from nth note to where dataPtr points
  void movDPtr_NoteDur_b(unsigned char n); // 0010101 - Copies 1 byte from duration of nth note to where dataPtr points
  void movDPtr_NoteDur(unsigned char n); // 001011 - Copies 1 byte from duration of nth note to where dataPtr points
  void movDPtr_NotePth(unsigned char n); // 001100 - Copies 1 byte from pitch of nth note to where dataPtr points
  void movDPtr_NotePth_b(unsigned char n); // 0011010 - Copies 1 byte from pitch of nth note to where dataPtr points
  void movD_Next(); // 00110110 - Copies next 2 bytes to `dataPtr
  void movDH_Next(); // 00110111 - Copies next 1 byte into high byte of dataPtr
  void movDL_Next(); // 00111000 - Copies next 1 byte into low byte of dataPtr
  void movDPtr_Next(); // 00111001 - Copies next 1 byte to where dataPtr points
  void movFlag_Next(); // 00111010 - Set flag based on last bit of next byte
  void movFlag_DPtr(); // 00111011 - Set flag based on last bit of where dataPtr points
  void reset_Dur(unsigned char n); // 001111 - Resets duration of given note to default value
  void reset_Dur_b(unsigned char n); // 0100000 - Resets duration of given note to default value
  void reset_Pitch_b(unsigned char n); // 0100001 - Resets pitch of given note to default value
  void reset_Pitch(unsigned char n); // 010001 - Resets pitch of given note to default value
  void inc_DurDbl(unsigned char o, unsigned char n); // 01001 - Doubles duration of note (unless max)
  void inc_DurDbl_b(unsigned char o, unsigned char n); // 010100 - Doubles duration of note (unless max)
  void inc_DurDot_b(unsigned char o, unsigned char n); // 010101 - Adds/removes dotted duration of note
  void inc_DurDot(unsigned char o, unsigned char n); // 01011 - Adds/removes dotted duration of note
  void dec_DurHalve(unsigned char o, unsigned char n); // 01100 - Halves duration of note (unless 1)
  void dec_DurHalve_b(unsigned char o, unsigned char n); // 011010 - Halves duration of note (unless 1)
  void inc_Pitch2_b(unsigned char o, unsigned char n); // 011011 - Increments pitch of note by second (major or minor depending on key)
  void inc_Pitch2(unsigned char o, unsigned char n); // 01110 - Increments pitch of note by second (major or minor depending on key)
  void inc_Pitch3(unsigned char o, unsigned char n); // 01111 - Increments pitch of note by third (major or minor depending on key)
  void inc_Pitch3_b(unsigned char o, unsigned char n); // 100000 - Increments pitch of note by third (major or minor depending on key)
  void inc_Pitch4_b(unsigned char o, unsigned char n); // 100001 - Increments pitch of note by perfect fourth
  void inc_Pitch4(unsigned char o, unsigned char n); // 10001 - Increments pitch of note by perfect fourth
  void inc_Pitch5(unsigned char o, unsigned char n); // 10010 - Increments pitch of note by perfect fifth
  void inc_Pitch5_b(unsigned char o, unsigned char n); // 100110 - Increments pitch of note by perfect fifth
  void inc_Pitch8_b(unsigned char o, unsigned char n); // 100111 - Increments pitch of note by octave
  void inc_Pitch8(unsigned char o, unsigned char n); // 10100 - Increments pitch of note by octave
  void dec_Pitch2(unsigned char o, unsigned char n); // 10101 - Decrements pitch of note by second (major or minor depending on key)
  void dec_Pitch2_b(unsigned char o, unsigned char n); // 101100 - Decrements pitch of note by second (major or minor depending on key)
  void dec_Pitch3_b(unsigned char o, unsigned char n); // 101101 - Decrements pitch of note by third (major or minor depending on key)
  void dec_Pitch3(unsigned char o, unsigned char n); // 10111 - Decrements pitch of note by third (major or minor depending on key)
  void dec_Pitch4(unsigned char o, unsigned char n); // 11000 - Decrements pitch of note by perfect fourth
  void dec_Pitch4_b(unsigned char o, unsigned char n); // 110010 - Decrements pitch of note by perfect fourth
  void dec_Pitch5_b(unsigned char o, unsigned char n); // 110011 - Decrements pitch of note by perfect fifth
  void dec_Pitch5(unsigned char o, unsigned char n); // 11010 - Decrements pitch of note by perfect fifth
  void dec_Pitch8(unsigned char o, unsigned char n); // 11011 - Decrements pitch of note by octave
  void dec_Pitch8_b(unsigned char o, unsigned char n); // 111000 - Decrements pitch of note by octave
  void setFlag(); // 11100100 - Set flag to true
  void unsetFlag(); // 11100101 - Set flag to false
  void invertFlag(); // 11100110 - Invert value of flag
  void jmpD(); // 11100111 - Absolute jumps to value of dataPtr
  void inc_D(unsigned char d); // 1110100 - Increments value of dataPtr
  void dec_D(unsigned char d); // 1110101 - Decrements value of dataPtr
  void sjmpNext(); // 11101100 - Short jumps program counter with value of next byte
  void jmpNext(); // 11101101 - Absolute jumps to memory address given by next 2 bytes
  void sjmpDH(); // 11101110 - Short jumps program counter with value of high byte of dataPtr
  void sjmpDL(); // 11101111 - Short jumps program counter with value of low byte of dataPtr
  void jnf_D(); // 11110000 - jmpD if flag is not set
  void jf_D(); // 11110001 - jmpD if flag is set
  void sjnf_Next(); // 11110010 - sjmpNext if flag is not set
  void sjf_Next(); // 11110011 - sjmpNext if flag is set
  void jnf_Next(); // 11110100 - jmpNext if flag is not set
  void jf_Next(); // 11110101 - jmpNext if flag is set
  void sjnf_DH(); // 11110110 - sjmpDH if flag is not set
  void sjf_DH(); // 11110111 - sjmpDH if flag is set
  void sjnf_DL(); // 11111000 - sjmpDL if flag is not set
  void sjf_DL(); // 11111001 - sjmpDL if flag is set
  void outNote_b(unsigned char n); // 1111101 - Outputs the value of a note
  void outNote(unsigned char n); // 111111 - Outputs the value of a note


private:

  // =======
  // members
  // =======

  unsigned int _geneticStringSize;
    
  bool _haltFlag;
  unsigned int _maxCommands;
  unsigned int _maxOutputs;
  bool _haltAllowed;

  unsigned short _counter; // program counter
  bool _isMajor;
  bool _diatonic;
  unsigned char _key;
  // registers
  unsigned char* _registers; // general-purpose registers
  // additional registers
  unsigned short _dataPtr;
  unsigned char _flag;
  
  // flexible memory segments
  unsigned char* _ram;
  unsigned int _ramSize;
  unsigned int _ramMask;
  
  // fixed memory segments
  
  unsigned char* _output; // output memory block
  unsigned int _outputPtr; // output write pointer
  unsigned int _outputAllocated;

  bool _debug;
  bool _countOccurrences;
  unsigned int* _occurrences; // count occurrences of command types in debug mode
  bool _countTouched;
  int _numTouched;
  unsigned short* _touched; // mark touched bytes in the ram


  // ===========================
  // private interpreter methods
  // ===========================

  // =================================================
  // further interpret pending states in state machine
  // =================================================
  
  void _interpret_00000(unsigned char cmd);
  void _interpret_000001(unsigned char cmd);
  void _interpret_00001(unsigned char cmd);
  void _interpret_00010(unsigned char cmd);
  void _interpret_000100(unsigned char cmd);
  void _interpret_00011(unsigned char cmd);
  void _interpret_000111(unsigned char cmd);
  void _interpret_00100(unsigned char cmd);
  void _interpret_00101(unsigned char cmd);
  void _interpret_001010(unsigned char cmd);
  void _interpret_00110(unsigned char cmd);
  void _interpret_001101(unsigned char cmd);
  void _interpret_0011011(unsigned char cmd);
  void _interpret_00111(unsigned char cmd);
  void _interpret_001110(unsigned char cmd);
  void _interpret_01000(unsigned char cmd);
  void _interpret_010000(unsigned char cmd);
  void _interpret_01010(unsigned char cmd);
  void _interpret_01101(unsigned char cmd);
  void _interpret_10000(unsigned char cmd);
  void _interpret_10011(unsigned char cmd);
  void _interpret_10110(unsigned char cmd);
  void _interpret_11001(unsigned char cmd);
  void _interpret_11100(unsigned char cmd);
  void _interpret_111001(unsigned char cmd);
  void _interpret_11101(unsigned char cmd);
  void _interpret_1110110(unsigned char cmd);
  void _interpret_1110111(unsigned char cmd);
  void _interpret_11110(unsigned char cmd);
  void _interpret_11111(unsigned char cmd);
  void _interpret_111110(unsigned char cmd);
  void _interpret_1111100(unsigned char cmd);


  // =====================
  // extra private methods
  // =====================

  /**
   * Sets halt flag to true.
   * Only stops execution if it was called with interpret().
   */
  void _halt();

  /**
   * Outputs byte to output array and increases output write pointer.
   */
  void _out(unsigned char byte);
  void _out(unsigned char* bytes, unsigned short pos, unsigned char num);
  void _out(unsigned char* bytes, unsigned short pos, unsigned char num, unsigned int mask);
  

  /**
   * If allocated output size has been reached, extends and copies output array.
   * TODO: optimize using paging?
   */
  void _checkAndExtendOutput(unsigned char num = 1);
  
  /**
   * "Touch" byte in RAM (increase its heat) and return reference to it.
   */
  unsigned char& _touch(unsigned int pos);
  
  /**
   * Reset counters for occurrences.
   */
  void _resetOccurrences();
  
  /**
   * Reset counters for how many bytes are touched.
   */
  void _resetTouched();
};
