/**
 * WARNING
 * =======
 * This is a generated file that can be overwritten with regeneration.
 * It is an op-code interpreter for the op code state machine MusicDsl.
 */


#include "musicdsloci.h"

#define ALLOCATION_UNIT 1024


MusicDslOci::MusicDslOci(unsigned int ramSize, bool isMajor, bool diatonic, unsigned char key, unsigned int maxCommands, unsigned int maxOutputs, bool haltAllowed) :
	_ramSize(ramSize), _ramMask(ramSize-1), _isMajor(isMajor), _diatonic(diatonic), _key(key), 
    _haltFlag(false), _maxCommands(maxCommands), _maxOutputs(maxOutputs), _haltAllowed(haltAllowed),
    _counter(0), _outputPtr(0), _outputAllocated(ALLOCATION_UNIT), _debug(false),
    _countOccurrences(false), _countTouched(false), _numTouched(0)
{
  _registers = new unsigned char[numRegisters];
  
  _ram = new unsigned char[_ramSize];
  
  _touched = new unsigned short[_ramSize];
  
  _output = new unsigned char[_outputAllocated];

  _occurrences = new unsigned int[numCategories];
  
  _geneticStringSize = 11 + _ramSize;
}


MusicDslOci::~MusicDslOci()
{
  delete [] _registers;
  delete [] _ram;
  delete [] _touched;
  delete [] _output;
  delete [] _occurrences;
}


void MusicDslOci::_checkAndExtendOutput(unsigned char num)
{
  if (_outputPtr + num > _outputAllocated)
  {
    // TODO implement paging for efficiency
    _outputAllocated += ALLOCATION_UNIT;
    unsigned char* newOutput = new unsigned char[_outputAllocated];

    output(newOutput, _outputPtr);

    delete [] _output;
    _output = newOutput;
  }
}


void MusicDslOci::setFromGeneticString(unsigned char* geneticString, int geneticString_size)
{
  _haltFlag = false;
  _outputPtr = 0;
  if (_countOccurrences)
  {
    _resetOccurrences();
  }
  if (_countTouched)
  {
    _resetTouched();
  }

  unsigned int genStrReadPtr = 0;
  unsigned int i;

  // set program counter
  _counter = ((unsigned short *) &(geneticString[genStrReadPtr]))[0];
  _counter &= _ramMask;
  genStrReadPtr += 2;

  // set main registers
  for (i = 0; i < numRegisters; ++i)
  {
    _registers[i] = geneticString[genStrReadPtr++];
  }

  // set register dataPtr
  _dataPtr = ((unsigned short *) &(geneticString[genStrReadPtr]))[0];
  genStrReadPtr += 2;

  // set register flag
  _flag = ((unsigned char *) &(geneticString[genStrReadPtr]))[0];
  genStrReadPtr += 1;

  // set memory ram
  for (i = 0; i < _ramSize; ++i)
  {
    _ram[i] = ((unsigned char *) &(geneticString[genStrReadPtr]))[i];
  }
  genStrReadPtr += _ramSize;
}


void MusicDslOci::interpret()
{
  _haltFlag = false;
  unsigned int numCommands = 0;
  //_counter = 0;
  _outputPtr = 0;

  while ((!_haltFlag || !_haltAllowed)
       && numCommands < _maxCommands
       && _outputPtr < _maxOutputs)
  {
    interpretNext();
    numCommands++;
  }
}


void MusicDslOci::interpretN(unsigned int n)
{
  for (unsigned int i = 0; i < n; ++i)
  {
    interpretNext();
  }
}


void MusicDslOci::interpretNext()
{
  _touch(_counter);
  unsigned char cmd = _ram[_counter & _ramMask];
  if (_debug) printf("_ram[%d]=%d => ", _counter & _ramMask, cmd);
  _counter = (_counter + 1) & _ramMask;
  
  unsigned char i = cmd >> 3;
  cmd = cmd << 5;
  
  switch (i)
  {
  case 0:
    {
      _interpret_00000(cmd);
      break;
    }
  case 1:
    {
      _interpret_00001(cmd);
      break;
    }
  case 2:
    {
      _interpret_00010(cmd);
      break;
    }
  case 3:
    {
      _interpret_00011(cmd);
      break;
    }
  case 4:
    {
      _interpret_00100(cmd);
      break;
    }
  case 5:
    {
      _interpret_00101(cmd);
      break;
    }
  case 6:
    {
      _interpret_00110(cmd);
      break;
    }
  case 7:
    {
      _interpret_00111(cmd);
      break;
    }
  case 8:
    {
      _interpret_01000(cmd);
      break;
    }
  case 9:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("inc_DurDbl(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      inc_DurDbl(o, n);
      break;
    }
  case 10:
    {
      _interpret_01010(cmd);
      break;
    }
  case 11:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("inc_DurDot(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      inc_DurDot(o, n);
      break;
    }
  case 12:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("dec_DurHalve(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      dec_DurHalve(o, n);
      break;
    }
  case 13:
    {
      _interpret_01101(cmd);
      break;
    }
  case 14:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("inc_Pitch2(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      inc_Pitch2(o, n);
      break;
    }
  case 15:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("inc_Pitch3(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      inc_Pitch3(o, n);
      break;
    }
  case 16:
    {
      _interpret_10000(cmd);
      break;
    }
  case 17:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("inc_Pitch4(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      inc_Pitch4(o, n);
      break;
    }
  case 18:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("inc_Pitch5(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      inc_Pitch5(o, n);
      break;
    }
  case 19:
    {
      _interpret_10011(cmd);
      break;
    }
  case 20:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("inc_Pitch8(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      inc_Pitch8(o, n);
      break;
    }
  case 21:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("dec_Pitch2(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      dec_Pitch2(o, n);
      break;
    }
  case 22:
    {
      _interpret_10110(cmd);
      break;
    }
  case 23:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("dec_Pitch3(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      dec_Pitch3(o, n);
      break;
    }
  case 24:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("dec_Pitch4(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      dec_Pitch4(o, n);
      break;
    }
  case 25:
    {
      _interpret_11001(cmd);
      break;
    }
  case 26:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("dec_Pitch5(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      dec_Pitch5(o, n);
      break;
    }
  case 27:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("dec_Pitch8(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      dec_Pitch8(o, n);
      break;
    }
  case 28:
    {
      _interpret_11100(cmd);
      break;
    }
  case 29:
    {
      _interpret_11101(cmd);
      break;
    }
  case 30:
    {
      _interpret_11110(cmd);
      break;
    }
  case 31:
    {
      _interpret_11111(cmd);
      break;
    }
  }
}


void MusicDslOci::_interpret_00000(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("movNote_Next(%d)\n", n);
      if (_countOccurrences) _occurrences[0]++;
      movNote_Next(n);
      break;
    }
  case 1:
    {
      _interpret_000001(cmd);
      break;
    }
  }
}


void MusicDslOci::_interpret_000001(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("movNote_Next_b(%d)\n", n);
      if (_countOccurrences) _occurrences[0]++;
      movNote_Next_b(n);
      break;
    }
  case 1:
    {
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("movNoteDur_Next_b(%d)\n", n);
      if (_countOccurrences) _occurrences[0]++;
      movNoteDur_Next_b(n);
      break;
    }
  }
}


void MusicDslOci::_interpret_00001(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("movNoteDur_Next(%d)\n", n);
      if (_countOccurrences) _occurrences[0]++;
      movNoteDur_Next(n);
      break;
    }
  case 1:
    {
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("movNotePth_Next(%d)\n", n);
      if (_countOccurrences) _occurrences[0]++;
      movNotePth_Next(n);
      break;
    }
  }
}


void MusicDslOci::_interpret_00010(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      _interpret_000100(cmd);
      break;
    }
  case 1:
    {
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("movNote_DPtr(%d)\n", n);
      if (_countOccurrences) _occurrences[0]++;
      movNote_DPtr(n);
      break;
    }
  }
}


void MusicDslOci::_interpret_000100(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("movNotePth_Next_b(%d)\n", n);
      if (_countOccurrences) _occurrences[0]++;
      movNotePth_Next_b(n);
      break;
    }
  case 1:
    {
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("movNote_DPtr_b(%d)\n", n);
      if (_countOccurrences) _occurrences[0]++;
      movNote_DPtr_b(n);
      break;
    }
  }
}


void MusicDslOci::_interpret_00011(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("movNoteDur_DPtr(%d)\n", n);
      if (_countOccurrences) _occurrences[0]++;
      movNoteDur_DPtr(n);
      break;
    }
  case 1:
    {
      _interpret_000111(cmd);
      break;
    }
  }
}


void MusicDslOci::_interpret_000111(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("movNoteDur_DPtr_b(%d)\n", n);
      if (_countOccurrences) _occurrences[0]++;
      movNoteDur_DPtr_b(n);
      break;
    }
  case 1:
    {
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("movNotePth_DPtr_b(%d)\n", n);
      if (_countOccurrences) _occurrences[0]++;
      movNotePth_DPtr_b(n);
      break;
    }
  }
}


void MusicDslOci::_interpret_00100(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("movNotePth_DPtr(%d)\n", n);
      if (_countOccurrences) _occurrences[0]++;
      movNotePth_DPtr(n);
      break;
    }
  case 1:
    {
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("movDPtr_Note(%d)\n", n);
      if (_countOccurrences) _occurrences[0]++;
      movDPtr_Note(n);
      break;
    }
  }
}


void MusicDslOci::_interpret_00101(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      _interpret_001010(cmd);
      break;
    }
  case 1:
    {
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("movDPtr_NoteDur(%d)\n", n);
      if (_countOccurrences) _occurrences[0]++;
      movDPtr_NoteDur(n);
      break;
    }
  }
}


void MusicDslOci::_interpret_001010(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("movDPtr_Note_b(%d)\n", n);
      if (_countOccurrences) _occurrences[0]++;
      movDPtr_Note_b(n);
      break;
    }
  case 1:
    {
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("movDPtr_NoteDur_b(%d)\n", n);
      if (_countOccurrences) _occurrences[0]++;
      movDPtr_NoteDur_b(n);
      break;
    }
  }
}


void MusicDslOci::_interpret_00110(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("movDPtr_NotePth(%d)\n", n);
      if (_countOccurrences) _occurrences[0]++;
      movDPtr_NotePth(n);
      break;
    }
  case 1:
    {
      _interpret_001101(cmd);
      break;
    }
  }
}


void MusicDslOci::_interpret_001101(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("movDPtr_NotePth_b(%d)\n", n);
      if (_countOccurrences) _occurrences[0]++;
      movDPtr_NotePth_b(n);
      break;
    }
  case 1:
    {
      _interpret_0011011(cmd);
      break;
    }
  }
}


void MusicDslOci::_interpret_0011011(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      if (_debug) printf("movD_Next()\n");
      if (_countOccurrences) _occurrences[0]++;
      movD_Next();
      break;
    }
  case 1:
    {
      if (_debug) printf("movDH_Next()\n");
      if (_countOccurrences) _occurrences[0]++;
      movDH_Next();
      break;
    }
  }
}


void MusicDslOci::_interpret_00111(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      _interpret_001110(cmd);
      break;
    }
  case 1:
    {
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("reset_Dur(%d)\n", n);
      if (_countOccurrences) _occurrences[1]++;
      reset_Dur(n);
      break;
    }
  }
}


void MusicDslOci::_interpret_001110(unsigned char cmd)
{
  unsigned char i = cmd >> 6;
  cmd = cmd << 2;
  
  switch (i)
  {
  case 0:
    {
      if (_debug) printf("movDL_Next()\n");
      if (_countOccurrences) _occurrences[0]++;
      movDL_Next();
      break;
    }
  case 1:
    {
      if (_debug) printf("movDPtr_Next()\n");
      if (_countOccurrences) _occurrences[0]++;
      movDPtr_Next();
      break;
    }
  case 2:
    {
      if (_debug) printf("movFlag_Next()\n");
      if (_countOccurrences) _occurrences[0]++;
      movFlag_Next();
      break;
    }
  case 3:
    {
      if (_debug) printf("movFlag_DPtr()\n");
      if (_countOccurrences) _occurrences[0]++;
      movFlag_DPtr();
      break;
    }
  }
}


void MusicDslOci::_interpret_01000(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      _interpret_010000(cmd);
      break;
    }
  case 1:
    {
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("reset_Pitch(%d)\n", n);
      if (_countOccurrences) _occurrences[1]++;
      reset_Pitch(n);
      break;
    }
  }
}


void MusicDslOci::_interpret_010000(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("reset_Dur_b(%d)\n", n);
      if (_countOccurrences) _occurrences[1]++;
      reset_Dur_b(n);
      break;
    }
  case 1:
    {
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("reset_Pitch_b(%d)\n", n);
      if (_countOccurrences) _occurrences[1]++;
      reset_Pitch_b(n);
      break;
    }
  }
}


void MusicDslOci::_interpret_01010(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("inc_DurDbl_b(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      inc_DurDbl_b(o, n);
      break;
    }
  case 1:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("inc_DurDot_b(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      inc_DurDot_b(o, n);
      break;
    }
  }
}


void MusicDslOci::_interpret_01101(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("dec_DurHalve_b(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      dec_DurHalve_b(o, n);
      break;
    }
  case 1:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("inc_Pitch2_b(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      inc_Pitch2_b(o, n);
      break;
    }
  }
}


void MusicDslOci::_interpret_10000(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("inc_Pitch3_b(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      inc_Pitch3_b(o, n);
      break;
    }
  case 1:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("inc_Pitch4_b(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      inc_Pitch4_b(o, n);
      break;
    }
  }
}


void MusicDslOci::_interpret_10011(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("inc_Pitch5_b(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      inc_Pitch5_b(o, n);
      break;
    }
  case 1:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("inc_Pitch8_b(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      inc_Pitch8_b(o, n);
      break;
    }
  }
}


void MusicDslOci::_interpret_10110(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("dec_Pitch2_b(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      dec_Pitch2_b(o, n);
      break;
    }
  case 1:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("dec_Pitch3_b(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      dec_Pitch3_b(o, n);
      break;
    }
  }
}


void MusicDslOci::_interpret_11001(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("dec_Pitch4_b(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      dec_Pitch4_b(o, n);
      break;
    }
  case 1:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("dec_Pitch5_b(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      dec_Pitch5_b(o, n);
      break;
    }
  }
}


void MusicDslOci::_interpret_11100(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      unsigned char o = cmd >> 7; cmd = cmd << 1;
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("dec_Pitch8_b(%d,%d)\n", o, n);
      if (_countOccurrences) _occurrences[1]++;
      dec_Pitch8_b(o, n);
      break;
    }
  case 1:
    {
      _interpret_111001(cmd);
      break;
    }
  }
}


void MusicDslOci::_interpret_111001(unsigned char cmd)
{
  unsigned char i = cmd >> 6;
  cmd = cmd << 2;
  
  switch (i)
  {
  case 0:
    {
      if (_debug) printf("setFlag()\n");
      if (_countOccurrences) _occurrences[1]++;
      setFlag();
      break;
    }
  case 1:
    {
      if (_debug) printf("unsetFlag()\n");
      if (_countOccurrences) _occurrences[1]++;
      unsetFlag();
      break;
    }
  case 2:
    {
      if (_debug) printf("invertFlag()\n");
      if (_countOccurrences) _occurrences[1]++;
      invertFlag();
      break;
    }
  case 3:
    {
      if (_debug) printf("jmpD()\n");
      if (_countOccurrences) _occurrences[2]++;
      jmpD();
      break;
    }
  }
}


void MusicDslOci::_interpret_11101(unsigned char cmd)
{
  unsigned char i = cmd >> 6;
  cmd = cmd << 2;
  
  switch (i)
  {
  case 0:
    {
      unsigned char d = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("inc_D(%d)\n", d);
      if (_countOccurrences) _occurrences[1]++;
      inc_D(d);
      break;
    }
  case 1:
    {
      unsigned char d = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("dec_D(%d)\n", d);
      if (_countOccurrences) _occurrences[1]++;
      dec_D(d);
      break;
    }
  case 2:
    {
      _interpret_1110110(cmd);
      break;
    }
  case 3:
    {
      _interpret_1110111(cmd);
      break;
    }
  }
}


void MusicDslOci::_interpret_1110110(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      if (_debug) printf("sjmpNext()\n");
      if (_countOccurrences) _occurrences[2]++;
      sjmpNext();
      break;
    }
  case 1:
    {
      if (_debug) printf("jmpNext()\n");
      if (_countOccurrences) _occurrences[2]++;
      jmpNext();
      break;
    }
  }
}


void MusicDslOci::_interpret_1110111(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      if (_debug) printf("sjmpDH()\n");
      if (_countOccurrences) _occurrences[2]++;
      sjmpDH();
      break;
    }
  case 1:
    {
      if (_debug) printf("sjmpDL()\n");
      if (_countOccurrences) _occurrences[2]++;
      sjmpDL();
      break;
    }
  }
}


void MusicDslOci::_interpret_11110(unsigned char cmd)
{
  unsigned char i = cmd >> 5;
  cmd = cmd << 3;
  
  switch (i)
  {
  case 0:
    {
      if (_debug) printf("jnf_D()\n");
      if (_countOccurrences) _occurrences[2]++;
      jnf_D();
      break;
    }
  case 1:
    {
      if (_debug) printf("jf_D()\n");
      if (_countOccurrences) _occurrences[2]++;
      jf_D();
      break;
    }
  case 2:
    {
      if (_debug) printf("sjnf_Next()\n");
      if (_countOccurrences) _occurrences[2]++;
      sjnf_Next();
      break;
    }
  case 3:
    {
      if (_debug) printf("sjf_Next()\n");
      if (_countOccurrences) _occurrences[2]++;
      sjf_Next();
      break;
    }
  case 4:
    {
      if (_debug) printf("jnf_Next()\n");
      if (_countOccurrences) _occurrences[2]++;
      jnf_Next();
      break;
    }
  case 5:
    {
      if (_debug) printf("jf_Next()\n");
      if (_countOccurrences) _occurrences[2]++;
      jf_Next();
      break;
    }
  case 6:
    {
      if (_debug) printf("sjnf_DH()\n");
      if (_countOccurrences) _occurrences[2]++;
      sjnf_DH();
      break;
    }
  case 7:
    {
      if (_debug) printf("sjf_DH()\n");
      if (_countOccurrences) _occurrences[2]++;
      sjf_DH();
      break;
    }
  }
}


void MusicDslOci::_interpret_11111(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      _interpret_111110(cmd);
      break;
    }
  case 1:
    {
      unsigned char n = cmd >> 6; cmd = cmd << 2;
      if (_debug) printf("outNote(%d)\n", n);
      if (_countOccurrences) _occurrences[3]++;
      outNote(n);
      break;
    }
  }
}


void MusicDslOci::_interpret_111110(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      _interpret_1111100(cmd);
      break;
    }
  case 1:
    {
      unsigned char n = cmd >> 7; cmd = cmd << 1;
      if (_debug) printf("outNote_b(%d)\n", n);
      if (_countOccurrences) _occurrences[3]++;
      outNote_b(n);
      break;
    }
  }
}


void MusicDslOci::_interpret_1111100(unsigned char cmd)
{
  unsigned char i = cmd >> 7;
  cmd = cmd << 1;
  
  switch (i)
  {
  case 0:
    {
      if (_debug) printf("sjnf_DL()\n");
      if (_countOccurrences) _occurrences[2]++;
      sjnf_DL();
      break;
    }
  case 1:
    {
      if (_debug) printf("sjf_DL()\n");
      if (_countOccurrences) _occurrences[2]++;
      sjf_DL();
      break;
    }
  }
}


void MusicDslOci::_halt()
{
  _haltFlag = true;
}


void MusicDslOci::_out(unsigned char byte)
{
  _checkAndExtendOutput();
  _output[_outputPtr++] = byte;
}


void MusicDslOci::_out(unsigned char* bytes, unsigned short pos, unsigned char num)
{
  _out(bytes, pos, num, _ramMask);
}


void MusicDslOci::_out(unsigned char* bytes, unsigned short pos, unsigned char num, unsigned int mask)
{
  _checkAndExtendOutput(num);
  for (int i = pos; i < pos + num; ++i)
  {
    _output[_outputPtr++] = bytes[i & mask];
  }
}


unsigned char& MusicDslOci::_touch(unsigned int pos)
{
  if (_countTouched)
  {
  	if (_debug) printf("touch(%d)\n", pos);
    if (!_touched[pos & _ramMask]) 
    {
      _numTouched++;
    }
    _touched[pos & _ramMask]++;
  }
  return _ram[pos & _ramMask];
}


void MusicDslOci::_resetOccurrences()
{
  memset(_occurrences, 0, numCategories * sizeof(unsigned int));
}

void MusicDslOci::_resetTouched()
{
  _numTouched = 0;
  memset(_touched, 0, _ramSize * sizeof(unsigned short));
}

// ===============
// getters-setters
// ===============

const unsigned int MusicDslOci::geneticStringSize() { return _geneticStringSize; }

unsigned short MusicDslOci::counter() { return _counter; }
void MusicDslOci::setCounter(unsigned short counter) { _counter = counter; }
unsigned int MusicDslOci::outputPtr() { return _outputPtr; }
void MusicDslOci::setOutputPtr(unsigned int outputPtr) { _outputPtr = outputPtr; }
bool MusicDslOci::isMajor() { return _isMajor; }
void MusicDslOci::setIsMajor(bool isMajor) { _isMajor = isMajor; }
bool MusicDslOci::diatonic() { return _diatonic; }
void MusicDslOci::setDiatonic(bool diatonic) { _diatonic = diatonic; }
unsigned char MusicDslOci::key() { return _key; }
void MusicDslOci::setKey(unsigned char key) { _key = key; }
unsigned short MusicDslOci::dataPtr() { return _dataPtr; }
void MusicDslOci::setDataPtr(unsigned short dataPtr) { _dataPtr = dataPtr; }
unsigned char MusicDslOci::flag() { return _flag; }
void MusicDslOci::setFlag(unsigned char flag) { _flag = flag; }

void MusicDslOci::output(unsigned char* out_output, int out_output_size) { memcpy(out_output, _output, out_output_size); }
unsigned char MusicDslOci::outputAt(unsigned short index) { return _output[index]; }
void MusicDslOci::setOutput(unsigned char* in_output, int in_output_size) { memcpy(_output, in_output, in_output_size); }
void MusicDslOci::setOutputAt(unsigned short index, unsigned char value) { _output[index] = value; }

void MusicDslOci::registers(unsigned char* out_registers, int out_registers_size) { memcpy(out_registers, _registers, out_registers_size); }
unsigned char MusicDslOci::registerAt(unsigned short index) { return _registers[index]; }
void MusicDslOci::setRegisters(unsigned char* in_registers, int in_registers_size) { memcpy(_registers, in_registers, in_registers_size); }
void MusicDslOci::setRegister(unsigned short index, unsigned char value) { _registers[index] = value; }

void MusicDslOci::ram(unsigned char* out_ram, int out_ram_size) { memcpy(out_ram, _ram, out_ram_size * sizeof(unsigned char)); }
unsigned char MusicDslOci::ramAt(unsigned short index) { return _ram[index & _ramMask]; }
void MusicDslOci::setRam(unsigned char* in_ram, int in_ram_size) { memcpy(_ram, in_ram, in_ram_size * sizeof(unsigned char)); }
void MusicDslOci::setRamAt(unsigned short index, unsigned char value) { _ram[index & _ramMask] = value; }

bool MusicDslOci::debug() { return _debug; }
void MusicDslOci::setDebug(bool debug) { _debug = debug; }
bool MusicDslOci::countOccurrences() { return _countOccurrences; }
void MusicDslOci::setCountOccurrences(bool countOccurrences) { _countOccurrences = countOccurrences; }
bool MusicDslOci::countTouched() { return _countTouched; }
void MusicDslOci::setCountTouched(bool countTouched) { _countTouched = countTouched; }
int  MusicDslOci::numTouched() { return _numTouched; }
void MusicDslOci::setNumTouched(int numTouched) { _numTouched = numTouched; }

void MusicDslOci::occurrences(unsigned int* out_occurrences, int out_occurrences_size) { memcpy(out_occurrences, _occurrences, out_occurrences_size * sizeof(unsigned int)); }
unsigned int MusicDslOci::occurrencesAt(unsigned short index) { return _occurrences[index]; }
void MusicDslOci::setOccurrences(unsigned int* in_occurrences, int in_occurrences_size) { memcpy(_occurrences, in_occurrences, in_occurrences_size * sizeof(unsigned int)); }
void MusicDslOci::setOccurrencesAt(unsigned short index, unsigned int value) { _occurrences[index] = value; }

void MusicDslOci::touched(unsigned short* out_touched, int out_touched_size) { memcpy(out_touched, _touched, out_touched_size * sizeof(unsigned short)); }
unsigned short MusicDslOci::touchedAt(unsigned short index) { return _touched[index]; }
void MusicDslOci::setTouched(unsigned short* in_touched, int in_touched_size) { memcpy(_touched, in_touched, in_touched_size * sizeof(unsigned short)); }
void MusicDslOci::setTouchedAt(unsigned short index, unsigned short value) { _touched[index] = value; }
