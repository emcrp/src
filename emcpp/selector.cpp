#include "selector.h"
#include "bytearray.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


#define PRINT(s, ...)     if (_debug) printf(s, ##__VA_ARGS__)
#define GRADES(i,j)       grades[i * _numTests + j]



GenotypeSelector::GenotypeSelector(unsigned short numUnits, unsigned short numTests,
                                   float* weights, int weights_size,
                                   bool complementary, float minProb, float ageFactor) :
    _numUnits(numUnits), _numTests(numTests), _complementary(complementary),
    _minProb(minProb), _ageFactor(ageFactor), _debug(false)
{
  _childOverallGrades    = new float[_numUnits - 1];
  _survivalProb          = new float[_numUnits];
  _adjustedOverallGrades = new float[_numUnits];

  _weights               = new float[_numTests];
  memcpy(_weights, weights, weights_size * sizeof(float));
}


GenotypeSelector::~GenotypeSelector()
{
  delete [] _childOverallGrades;
  delete [] _survivalProb;
  delete [] _adjustedOverallGrades;
  delete [] _weights;
}


//================
// public methods
//================

void GenotypeSelector::chooseUnits(float* grades,         int grades_size_1, int grades_size_2,
                                   float* overall_grades, int overall_grades_size,
                                   unsigned short* ages,  int ages_size,
                                   unsigned short* units, int units_size,
                                   int numSurvivors, int numCrossMutated, int numCrossover, int numMutated)
{
  unsigned short* mothersCM = &(units[numSurvivors]);
  unsigned short* fathersCM = &(units[numSurvivors + numCrossMutated / 2]);
  unsigned short* mothersC  = &(units[numSurvivors + numCrossMutated]);
  unsigned short* fathersC  = &(units[numSurvivors + numCrossMutated + numCrossover / 2]);
  unsigned short* mutated   = &(units[numSurvivors + numCrossMutated + numCrossover]);
  
  PRINT("\nreproduction:\n");

  // apply minimal probability
  applyMinProb(overall_grades, overall_grades_size, _adjustedOverallGrades, _numUnits, _minProb);
  PRINT("\ngrades   = {"); if (_debug) for (int i = 0; i < _numUnits; ++i) printf("%.3f, ", overall_grades[i]); PRINT("}\n");
  
  // 1. reproduction
  _calculateSurvivalProbability(overall_grades, ages);
  chooseUnique(_survivalProb, overall_grades_size, units, numSurvivors);
  PRINT("survivors = {"); if (_debug) for (int i = 0; i < numSurvivors; ++i) printf("%d, ", units[i]); PRINT("}\n");
  
  // 2. cross-mutation
  _chooseParents(grades, mothersCM, fathersCM, numCrossMutated);

  // 3. crossover
  _chooseParents(grades, mothersC, fathersC, numCrossover);
  
  // 4. mutation
  PRINT("\nmutation:\n");
  chooseUnique(_adjustedOverallGrades, overall_grades_size, mutated, numMutated);
  PRINT("mutations = {"); if (_debug) for (int i = 0; i < numMutated; ++i) printf("%d, ", mutated[i]); PRINT("}\n");
  
  PRINT("\nfinal = {"); if (_debug) for (int i = 0; i < units_size; ++i) printf("%d, ", units[i]); PRINT("}\n"); 
}


//===================
// getters & setters
//===================

bool GenotypeSelector::complementary() { return _complementary; }
void GenotypeSelector::setComplementary(bool complementary) { _complementary = complementary; }
bool GenotypeSelector::debug() { return _debug; }
void GenotypeSelector::setDebug(bool debug) { _debug = debug; }


//===================
// auxiliary methods
//===================

void GenotypeSelector::_calculateSurvivalProbability(float* overall_grades, unsigned short* ages)
{
  PRINT("survProb = {");
  for (int i = 0; i < _numUnits; ++i)
  {
  	_survivalProb[i] = overall_grades[i] * pow(_ageFactor, ages[i]);
  	PRINT("%.3f, ", _survivalProb[i]);
  }
  PRINT("}\n");
}


void GenotypeSelector::_chooseParents(float* grades, unsigned short* mothers, unsigned short* fathers, int numToChoose)
{
  // choose all mothers
  PRINT("\ncrossover:\n");
  choose(_adjustedOverallGrades, _numUnits, mothers, numToChoose / 2);
  PRINT("mothers = {"); if (_debug) for (int i = 0; i < numToChoose / 2; ++i) printf("%d, ", mothers[i]); PRINT("}\n");

  if (_complementary) {
    // for each mother, find father
    PRINT("fathers = {"); 
    for (int i = 0; i < numToChoose / 2; ++i) {
      fathers[i] = _chooseFather(grades, mothers[i]);
      PRINT("%d, ", fathers[i]);
    }
    PRINT("}\n");
  } else {
    // choose all fathers the same way
    choose(_adjustedOverallGrades, _numUnits, fathers, numToChoose / 2);
    PRINT("fathers = {"); if (_debug) for (int i = 0; i < numToChoose / 2; ++i) printf("%d, ", fathers[i]); PRINT("}\n");
  }
}


unsigned short GenotypeSelector::_chooseFather(float* grades, unsigned short motherIdx)
{
  int unitIdx;

  for (int childIdx = 0; childIdx < _numUnits - 1; ++childIdx)
  {
    unitIdx = childIdx + (childIdx >= motherIdx);

    _childOverallGrades[childIdx] = 0.0;
    for (int testIdx = 0; testIdx < _numTests; ++testIdx)
    {
      _childOverallGrades[childIdx] += _weights[testIdx] *
                                      (GRADES(unitIdx, testIdx) > GRADES(motherIdx, testIdx) ?
                                       GRADES(unitIdx, testIdx) : GRADES(motherIdx, testIdx));
    }
  }
  
  // apply minimal probability
  applyMinProb(_childOverallGrades, _numUnits-1, _childOverallGrades, _numUnits-1, _minProb);

  // choose father
  unsigned short ret = chooseOne(_childOverallGrades, _numUnits-1);

  if (ret >= motherIdx)
  {
    ret++;
  }

  return ret;
}

