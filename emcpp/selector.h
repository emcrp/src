/**
 * Genotype selector.
 * An extension to the roulette wheel choosing algorithm, which builds
 * hypothetical best-case children and chooses every father based on those.
 */

#pragma once


/**
 * Genotype selector.
 * Allows simple roulette-wheel selection OR complementary.
 * The complementary method uses roulette-wheel selection for each mother, and builds hypothetical best-case
 * children with each potential father, then chooses father based on a roulette-wheel of children.
 */
class GenotypeSelector
{
public:

  GenotypeSelector(unsigned short numUnits, unsigned short numTests,
                   float* inp, int inp_size,
                   bool complementary = true, float minProb = 0.1f, float ageFactor = 0.9f);
  ~GenotypeSelector();


  /**
   * Select candidates for genetic operators based on multi-objective fitness test grades.
   * Survivors/mutators are chosen randomly based on their overall grades.
   * Mothers are chosen based on roulette wheel,
   * and fathers are chosen based on roulette wheel of the hypothetical best-case
   * children with each mother.
   */
  void chooseUnits(float* inp2d,         int inp_size_1, int inp_size_2,
                   float* inp,           int inp_size,
                   unsigned short* inp1, int inp1_size,
                   unsigned short* outp, int outp_size,
                   int numSurvivors, int numCrossMutated, int numCrossover, int numMutated);

  // getters-setters
  bool complementary();
  void setComplementary(bool complementary);
  bool debug();
  void setDebug(bool debug);
  
private:
  /**
   * Number of units and tests in a population.
   */
  unsigned short _numUnits;
  unsigned short _numTests;

  /**
   * Flag to determine if complementary father selection is used.
   */
  bool _complementary;

  /**
   * Turn on debug messages.
   */
  bool _debug;

  /**
   * Minimal probability each unit has to be chosen.
   * All probabilities are scaled to be between minProb and 1-minProb
   */
  float _minProb;

  /**
   * Probability of survival.
   * This is taken together with overall grades, so every unit has this chance of survival
   * is their overall grades are 100%.
   */
  float _ageFactor;

  /**
   * Helper array to store hypothetical children's overall grades.
   * This will be used to choose fathers.
   */
  float*  _childOverallGrades;

  /**
   * Helper array to store survival probabilities (overall grade * ageFactor ^ age).
   */
  float*  _survivalProb;
  
  /**
   * Helper array to store scaled overall grades, so probability is given
   * even to 0 entities
   */
  float*  _adjustedOverallGrades;

  /**
   * Weights attributed to each fitness test.
   */
  float*  _weights;

  /**
   * Given a set of grades, choose pairs of mothers and fathers for crossover.
   * If _complementary flag set, uses complementary selection of fathers.
   */
  void _chooseParents(float* grades, unsigned short* mothers, unsigned short* fathers, int numToChoose);

  /**
   * Choose a father, given a selected mother.
   * Builds hypothetical best-case children based on multi-objective fitness test results,
   * builds their CDF and chooses father based on it.
   */
  unsigned short _chooseFather(float* grades, unsigned short motherIdx);
  
  /**
   * Calculate and store survival probabilities (overall grade * ageFactor ^ age).
   */
  void _calculateSurvivalProbability(float* overall_grades, unsigned short* ages);
};
