'''
Wrapper for C++ descriptor & descriptor distancing class.
'''

from numpy import size, float32, zeros, frombuffer, uint16

from emcpp.swig import emcpp
from os.path import isfile
from helper.serializeutils import toFile, fromFile


numDescriptors = emcpp.NUM_DESCRIPTORS


class DescriptorDirectoryWrapper(object):
    '''
    Wrapper for C++ descriptor & descriptor distancing class.
    '''
    
    def __init__(self, modelDir, numBins = 128, fourierSize = 2048, numClusters = 1):
        '''
        Initialize by model directory and descriptor directory properties.
        This will use each model from directory as corpus object.
        Will add to descriptor correlationr and compute cluster center descriptors.
        '''
        self.modelDir = modelDir
        self.numModels = modelDir.numModels
        self.numTracks = modelDir.models[0].numTracks
        self.numBins = numBins
        self.fourierSize = fourierSize
        self.numClusters = numClusters
        
        if self.numModels < self.numClusters:
            print "WARNING: More clusters than models. Behavior unpredictable."
        
        self.native = emcpp.DescriptorDirectory(self.numModels, self.modelDir.numTracks, self.numBins, self.fourierSize, self.numClusters)
        self.checkDescriptorCache()
        
    
    def readFromCache(self, cacheFileName):
        content = fromFile(cacheFileName)
        descriptors = frombuffer(content['descriptors'], dtype=float32)
        descriptors.shape = (self.numModels, self.numTracks, numDescriptors * self.numBins)
        centreDescriptors = frombuffer(content['centreDescriptors'], dtype=float32)
        centreDescriptors.shape = (self.numClusters, self.numTracks, numDescriptors * self.numBins)
        clusters = frombuffer(content['clusters'], dtype=uint16)
        clusters.shape = (self.numModels, self.numTracks, numDescriptors)
        
        self.native.setDescriptors(descriptors)
        self.native.setCentreDescriptors(centreDescriptors)
        self.native.setClusters(clusters)
    
    
    def calculateCentresLocally(self):
        print 'Calculating cluster centres locally. This may take a while...'
        # calculate corpus descriptors
        for modelIdx, model in enumerate(self.modelDir.models):
            for trackIdx, track in enumerate(model.tracks):
                self.native.addDescriptor(track.notes.T, modelIdx, trackIdx)
        # perform clustering of these
        self.native.calculateCentreDescriptors()
    
    
    def writeToCache(self, cacheFileName):
        # write descriptors
        content = {
            'descriptors' : bytearray(self.descriptors()),
            'centreDescriptors' : bytearray(self.centreDescriptors()),
            'clusters' : bytearray(self.clusters())
        }
        toFile(content, cacheFileName)
        
        
    def checkDescriptorCache(self, cacheFileName = None):
        '''
        Calculate and cache centre descriptors.
        '''
        if cacheFileName is None:
            cacheFileName = self.modelDir.cacheFile()
            cacheFileName += '-%scentres' %(self.numClusters)
        
        if isfile(cacheFileName):
            print 'Cluster centres read from cache file %s' %cacheFileName
            # read descriptors
            self.readFromCache(cacheFileName)
            
        else:
            print 'Cache file %s not found. Calculating and caching' %cacheFileName
            self.calculateCentresLocally()
            self.writeToCache(cacheFileName)
            
        
    def modelFitness(self, model):
        '''
        Calculates fitness values based on correlations between model's and centre descriptors.
        '''
        ret = zeros((self.numTracks, numDescriptors), dtype=float32)
        for trackIdx, track in enumerate(model.tracks):
            ret[trackIdx, :] = self.native.trackFitness(track.notes.T, trackIdx, numDescriptors)
        return ret
        
        
    def modelCorrelationsWithCentres(self, model):
        '''
        Calculates a model's descriptor and the correlations between it
        and the centre descriptors from the corpus.
        '''
        ret = zeros((self.numTracks, self.numClusters * numDescriptors), dtype=float32)
        for trackIdx, track in enumerate(model.tracks):
            ret[trackIdx, :] = self.native.trackCorrelationsWithCentres(track.notes.T, trackIdx, self.numClusters * numDescriptors)
        ret.shape = (self.numTracks, self.numClusters, numDescriptors)
        return ret
    
    
    def correlationsWithCentres(self, descriptor, trackIdx):
        '''
        Calculate correlations between a descriptor
        and the centre descriptors from the corpus.
        '''
        ret = self.native.correlationsWithCentres(descriptor.ravel(), trackIdx, self.numClusters * numDescriptors)
        ret.shape = (self.numClusters, numDescriptors)
        return ret
    
    
    def descriptor(self, model):
        '''
        Returns descriptor of model, i.e. an array of histograms/FFTs. 
        '''
        ret = zeros((self.numTracks, numDescriptors * self.numBins), dtype=float32)
        for trackIdx, track in enumerate(model.tracks):
            ret[trackIdx, :] = self.native.trackDescriptor(track.notes.T, numDescriptors * self.numBins)
        ret.shape = (self.numTracks, numDescriptors, self.numBins)
        return ret
    
    
    def descriptors(self):
        '''
        Returns all descriptors used in directory.
        '''
        ret = self.native.descriptors(self.numModels * self.numTracks * numDescriptors * self.numBins)
        ret.shape = (self.numModels, self.numTracks, numDescriptors, self.numBins)
        return ret
    
    
    def centreDescriptors(self):
        '''
        Returns centre descriptors of corpus.
        '''
        ret = self.native.centreDescriptors(self.numClusters * self.numTracks * numDescriptors * self.numBins)
        ret.shape = (self.numClusters, self.numTracks, numDescriptors, self.numBins)
        return ret
    
    
    def clusters(self):
        '''
        Returns clusters - indices of clusters each reference descriptor's been classified into.
        '''
        ret = self.native.clusters(self.numModels * self.numTracks * numDescriptors)
        ret.shape = (self.numModels, self.numTracks, numDescriptors)
        return ret
    
    
    def fourier(self, data, size = None, normalize = True):
        '''
        Returns FFT of data.
        '''
        if size is None:
            size = self.numBins
            
        return self.native.fourier(data, size, normalize)
    
    
    def diffFourier(self, data, size = None, normalize = True):
        '''
        Returns FFT of data.
        '''
        if size is None:
            size = self.numBins
            
        return self.native.diffFourier(data, size, normalize)
    
    
    def __getstate__(self):
        '''
        Do not serialize native.
        '''
        return (self.modelDir, self.numBins, self.fourierSize, self.numClusters)
    
    
    def __setstate__(self, state):
        '''
        Do not serialize native.
        '''
        modelDir, numBins, fourierSize, numClusters = state
        self.__init__(modelDir=modelDir,
                      numBins=numBins,
                      fourierSize=fourierSize,
                      numClusters=numClusters)




def normalize(data):
    '''
    Wrapper for normalization.
    '''
    emcpp.normalize(data)
    
    
    
def histogram(data, size = 128, normalize = True):
    '''
    Wrapper for histogram.
    '''
    return emcpp.histogram(data, size, normalize)
    
    
    
def diffHistogram(data, size = 128, normalize = True):
    '''
    Wrapper for histogram of first-order derivative.
    '''
    return emcpp.diffHistogram(data, size, normalize)



def correlation(descriptor1, descriptor2):
    '''
    Calculate correlations between 2 descriptors.
    '''
    if descriptor1.ndim == 1:
        length = 1
        numBins = size(descriptor1)
    else:
        length, numBins = descriptor1.shape
    
    return emcpp.correlation(descriptor1.ravel(), descriptor2.ravel(), int(length), int(numBins))



def fitness(data):
    '''
    Calculates fitness values based on correlations. Assumes input array are correlations between descriptors.
    '''
    emcpp.fitness(data)
    