from emcpp.swig.emcpp import GenotypeSelector
from numpy import float32, frombuffer


class GenotypeSelectorWrapper(object):

    def __init__(self, numUnits, numTests, weights, 
                 complementary=True, minProb=0.1, ageFactor=0.9):
        self.numUnits = numUnits
        self.numTests = numTests
        self.weights = weights
        self.complementary = complementary
        self.minProb = minProb
        self.ageFactor = ageFactor
        
        self.native = GenotypeSelector(self.numUnits, self.numTests, self.weights, self.complementary, self.minProb, self.ageFactor)
    
    
    def setNumUnits(self, numUnits):
        self.numUnits = numUnits
        self.native = GenotypeSelector(self.numUnits, self.numTests, self.weights, self.complementary, self.minProb, self.ageFactor)
        
        
    def chooseUnits(self, population, numSurvivors, numCrossMutated, numCrossover, numMutated):
        units = self.native.chooseUnits(population.gradesPerTest,
                                        population.grades,
                                        population.ages,
                                        self.numUnits,
                                        int(numSurvivors), int(numCrossMutated), int(numCrossover), int(numMutated))
        survivors = units[:numSurvivors]
        parentsCM = units[numSurvivors:numSurvivors+numCrossMutated]
        parentsCM.shape = (2, numCrossMutated/2)
        parentsC = units[numSurvivors+numCrossMutated:numSurvivors+numCrossMutated+numCrossover]
        parentsC.shape = (2, numCrossover/2)
        mutated = units[numSurvivors+numCrossMutated+numCrossover:]
        return (survivors, parentsCM, parentsC, mutated)
    
    
    def complementary(self):
        return self.native.complementary()
    
    def setComplementary(self, complementary):
        self.native.setComplementary(complementary)

    def debug(self):
        return self.native.debug()
    
    def setDebug(self, debug):
        self.native.setDebug(debug)
        
    def __getstate__(self):
        weightsBA = bytearray(self.weights)
        return (self.numUnits, self.numTests, weightsBA, self.complementary, self.minProb, self.ageFactor)


    def __setstate__(self, state):
        (self.numUnits, self.numTests, weightsBA, self.complementary, self.minProb, self.ageFactor) = state
        self.weights = frombuffer(weightsBA, dtype=float32)
        self.native = GenotypeSelector(self.numUnits, self.numTests, self.weights, self.complementary, self.minProb, self.ageFactor)
