#!/bin/bash
# this file contains the launcher for the experiments for the ICCP 2017 paper.

if [ $# -eq 0 ]; then
  echo "Usage: `basename $0` selection [numRuns]"
  exit 1
fi


allCmdArgs=(
    "ociClass=MusicDsl  modelRepresentation=CHROMATIC         outputDir=../output/emc-gecco2019-musicdsl-chromatic"
    "ociClass=MusicDsl  modelRepresentation=SHIFTED_CHROMATIC outputDir=../output/emc-gecco2019-musicdsl-shiftedchromatic"
    "ociClass=MusicDsl  modelRepresentation=DIATONIC          outputDir=../output/emc-gecco2019-musicdsl-diatonic"
    "ociClass=Immediate modelRepresentation=CHROMATIC         outputDir=../output/emc-gecco2019-immediate-chromatic"
    "ociClass=Immediate modelRepresentation=SHIFTED_CHROMATIC outputDir=../output/emc-gecco2019-immediate-shiftedchromatic"
    "ociClass=Immediate modelRepresentation=DIATONIC          outputDir=../output/emc-gecco2019-immediate-diatonic"
)

datetime=`date "+%Y%m%d-%H%M%S"`
allLogFiles=(
    "../output/$datetime-musicdsl-chromatic.log"
    "../output/$datetime-musicdsl-shiftedchromatic.log"
    "../output/$datetime-musicdsl-diatonic.log"
    "../output/$datetime-immediate-chromatic.log"
    "../output/$datetime-immediate-shiftedchromatic.log"
    "../output/$datetime-immediate-diatonic.log"
)


# deduce selection and map to oci and mem size
selection=$1
cmdArgs=${allCmdArgs[$selection]}
logFile=${allLogFiles[$selection]}

numRuns=20
if [ $# -eq 2 ]; then
    numRuns=$2
fi

commonArgs="corpusPath=http://www.cimbalom.nl/nepdalok.html \
            preprocessingMethods=removeZeroVelocityNotes;makeMaxVelocity;resampleTo4Ticks;forceNoOverlappingNotes;clipOverlyLongNotes;asuint16;reduceModels \
            fitnessTestContainerClass=NGram \
            modelKey=64 \
            modelMajor=True \
            expectedLength=25 \
            numClusters=8 \
            gramSizes=[1,2,3] \
            weightDim=length \
            operatorDist=[0.08,0.92,0.0,0.0] \
            crossoverRate=0.002 \
            mutationRate=0.02 \
            minProb=0.15 \
            ageFactor=0.8 \
            memSizeInBytes=65536 \
            numUnits=500 \
            targetGeneration=5001 \
            cycleSize=500 \
            loggingCycle=10 \
            saveTopMidis=2 \
            numberOfRuns=$numRuns"


args="$commonArgs $cmdArgs" 
echo Deduced args: $args
echo ''


cmd="screen -d -L -Logfile $logFile -m python algrun.py $args"
echo Executing cmd: $cmd
echo ''
$cmd
