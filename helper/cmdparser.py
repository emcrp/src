'''
Parse and limit command line arguments and return a dictionary based on it.
'''

from re import search
from sys import stderr, argv

from numpy import float32, int16, int32

separator = ';'


# Transformer methods for command line arguments

def _leaveAsIs(value):     return value
def _castToInt16(value):   return int16(value)
def _castToInt32(value):   return int32(value)
def _castToInt(value):     return int(value)
def _castToFloat32(value): return float32(value)
def _castToFloat(value):   return float(value)
def _castToBool(value):    return bool(value)
def _castByEval(value):    return eval(value)
def _castToStrList(value): return value.split(separator)

def _castToClass(value):
    '''
    Resolve a class based on its full name (module name included).
    '''
    # find last dot - module is before it
    lastDotIdx = value.rfind('.')
    
    # distinguish between local and external classes
    if lastDotIdx != -1:
        moduleName = value[:lastDotIdx]
    else:
        moduleName = '__main__'

    className = value[lastDotIdx+1:]
    
    #import module and class
    module = __import__(moduleName, globals(), locals(), className)
    clazz = getattr(module, className)
    return clazz


def _castToFitnessTestContainerClass(value):
    return _castToClass("emc.fitness.%s.%sBasedFitnessTestContainer" %(value.lower(), value))


def _castToOciClass(value):
    return _castToClass("emcpp.wrapper.oci.%soci.%sOciWrapper" %(value.lower(), value))


def parseCmdLine(argTransformMethods = {}):
    '''
    Parse system arguments to set properties of an algorithm
    Parse command-line options and return map and props to use in initializing a script.
    '''
    argsDict = {}
    
    for arg in argv[1:]:
        # search for pattern opt=value
        match = search('(.*)=(.*)', arg)
        
        if not match:
            stderr.write('Unrecognized program argument: %s\n' %arg)
        else:
            argKey = match.group(1)
            argValue = match.group(2)
            
            if argKey not in argTransformMethods:
                stderr.write('Unrecognized configuration key: %s\n' %argKey)
            else:
                argTransformMethod = argTransformMethods[argKey]
                print 'Configuring using argument key=%s, value=%s, method=%s' %(
                                     argKey, argValue, argTransformMethod.__name__)
                argValue = argTransformMethod(argValue)
                argsDict[argKey] = argValue
    
    print 'Final args dictionary', argsDict
    print ''
    return argsDict
        


def parseForEmcAlgRun():   
    return parseCmdLine({
                  
        # corpus
        'corpusPath' : _leaveAsIs,
        'omitUrlCheck' : _castToBool,
        'modelRepresentation' : _leaveAsIs,
        'modelKey' : _castToInt,
        'modelMajor' : _castToBool,
        'forceRedownload' : _castToBool,
        'preprocessingMethods' : _castToStrList,
        
        # fitness test container
        'fitnessTestContainerClass' : _castToFitnessTestContainerClass,
        'numClusters' : _castToInt,
        'gramSizes' : _castByEval,
        'expectedLength' : _castToFloat32,
        'weightDim' : _leaveAsIs,
                
        # OCI
        'ociClass' : _castToOciClass,
        'memSizeInBytes' : _castToInt,
        'maxCommandsRatio' : _castToFloat32,
        'haltAllowed' : _castToBool,
        
        # population breeder
        'numUnits' : _castToInt,
        'survivalRate' : _castToFloat32,
        'crossoverRate' : _castToFloat32,
        'mutationRate' : _castToFloat32,
        'operatorDist' : _castByEval,
        'complementary' : _castToBool,
        'minProb' : _castToFloat,
        'ageFactor' : _castToFloat,
                
        # general algorithm props
        'targetGeneration' : _castToInt16,
        'numberOfRuns' : _castToInt16,
        'loggingCycle' : _castToInt16,
        
        # saving
        'fileToResume' : _leaveAsIs,
        'savePeriodically' : _castToBool,
        'outputDir' : _leaveAsIs,
        'cycleSize' : _castToInt16,
        'saveTopMidis' : _castToInt,
        'saveGrades' : _castToBool,
        'saveAllGrades' : _castToBool,
        
    })


def printHeader():
    print '''EMC - Evolutionary Music Composition
------------------------------------
    '''
    

def printHelp():
    print '''A program that uses genetic programming to create pieces of music. Call as:

    emc [-h] [<optionKey>=<optionValue>]

Options:
    KEY                        DESCRIPTION
    
    corpusPath                 Folder or URL containing corpus MIDI files
    omitUrlCheck               If corpusPath is URL and download directory exists, omit re-check if all files available
    forceRedownload            If corpusPath is URL, force redownloading even if download directory exists
    modelRepresentation        Representation of models. Valid values={'CHROMATIC', 'SHIFTED_CHROMATIC', 'DIATONIC'}
    modelKey                   Key to use for any created MIDI file. Only taken into account if modelRepresentation is not CHROMATIC.
    modelMajor                 True/False value for major to use for any created MIDI file. Only taken into account if modelRepresentation is not CHROMATIC.
    fitnessTestContainerClass  Python class name prefix of fitness test container. Valid values={'Descriptor', 'NGram'}
    numClusters                Number of corpus clusters
    expectedLength             Expected length (in seconds) of output musical pieces
    gramSizes                  N-gram sizes to use in N-gram based fitness test container
    ociClass                   Python class name of OCI interpreter.
    memSizeInBytes             Size of flexible memory (RAM/ROM) used in opcode interpretation.
    maxCommandsRatio           Maximum ratio of VM instructions to be executed relative to expected number for output instructions 
    numUnits                   Population size 
    crossoverRate              Represents number of cut points used in crossover in ratio with the genetic string size.
    mutationRate               Percentage of genetic string that is mutated in each offspring.
    operatorDist               Distribution of reproduction/crossover/survival ratios. Accepts 3-value array, e.g "[0.1,0.8,0.1]"
    complementary              Flag to set if complementary genotype selection is applied over regular roulette-wheel selection
    minProb                    Minimal probability for genotype choice even if fitness is 0
    ageFactor                  How much age impacts chances of survival (default: 0.8)
    targetGeneration           The algorithm will run until it reaches this target generation.
    numberOfRuns               How many times the algorithm shall be run
    fileToResume               Resume previous run stored in given file. Everything else ignored in this case
    savePeriodically           Flag signalling whether to store models/MIDI files or not
    outputDir                  Folder to store output models/MIDI files (savePeriodically must be set)
    cycleSize                  Save status/best MIDI file every cycleSize generations (savePeriodically must be set)
    saveTopMidis               Number of highest scoring MIDI files to save
    saveGrades                 Also save max/mean/std grades per generation.
    saveAllGrades              Also save each grade (per unit and test) per generation.
    loggingCycle               Print status information every loggingCycle generations
    '''

