from numpy.random.mtrand import randint, choice
from numpy import zeros, float32, arange, set_printoptions, array, identity


def transprob(n, s = 8):
    '''
    Generates a Markovian state transition probability
    with uniform transition probability to all OTHER states.
    n = number of states
    s = how many times more likely it is to stay in current state than it is to transition.
    ''' 
    return (identity(n) * (s-1.0) + 1.0) / (s+n-1.0)
    

    
class HillClimberValue(object):
    
    def __init__(self, x0,
                       deltas = array([-1, 0, 1]),
                       p_delta_pos = transprob(3, 8.0),
                       p_delta_neg = transprob(3, 1.0/8.0),
                       minValue = 0,
                       maxValue = 256):
        self.x = x0
        self.deltas = deltas
        self.p_delta_pos = p_delta_pos
        self.p_delta_neg = p_delta_neg
        self.minValue = minValue
        self.maxValue = maxValue
        
        # delta is -1 (step left), 0 (stay) or 1 (step right)
        self.deltaIdx = randint(len(self.deltas))
        self.fx = None
        self.singleValue = len(self.deltas.shape) == 1
        
    
    def copy(self):
        ret = HillClimberValue(x0 = self.x,
                               deltas = self.deltas,
                               p_delta_pos = self.p_delta_pos, p_delta_neg = self.p_delta_neg,
                               minValue = self.minValue, maxValue = self.maxValue)
        ret.deltaIdx = self.deltaIdx
        ret.fx = self.fx
        ret.singleValue = self.singleValue
        return ret
    
    def step(self, newFx):
        if self.fx is not None:
            if newFx <= self.fx:
                self.deltaIdx = choice(len(self.deltas), p=self.p_delta_neg[self.deltaIdx])
            else:
                self.deltaIdx = choice(len(self.deltas), p=self.p_delta_pos[self.deltaIdx])
                
        self.fx = newFx
        newX = self.x + self.deltas[self.deltaIdx]
        
        valid = (self.singleValue and newX >= self.minValue and newX <= self.maxValue
                 ) or (not self.singleValue and (newX >= self.minValue).all() and (newX <= self.maxValue).all())
        if valid: self.x = newX
        
        return self.x


if __name__ == '__main__':
    
    def f(x):
        fx = zeros(4, dtype=float32)
        fx[0] = - x[0] ** 2 + 1 # 0
        fx[1] = - 6*x[1] ** 2 + 18*x[1] - 1 # 1.5
        fx[2] = -x[2]**4 + x[2]**3 + x[2]**2 + x[2] # 1.25
        fx[3] = -(x[3]-4) ** 2 # 4
        return fx
    
    N = 2000
    set_printoptions(precision=2)
    x = zeros((4, N), dtype=float32) - 10
    x2 = zeros((4, N), dtype=float32) + ((1.25+1.5+4.0)/4.0)
    fx = f(x[:, 0])
    fx2 = f(x2[:, 0])
    
    hc0 = HillClimberValue(x0=x[0, 0], deltas = array([-.05, 0, .05]), minValue=-30, maxValue=55)
    hc1 = HillClimberValue(x0=x[1, 0], deltas = array([-.05, 0, .05]), minValue=-30, maxValue=55)
    hc2 = HillClimberValue(x0=x[2, 0], deltas = array([-.05, 0, .05]), minValue=-30, maxValue=55)
    hc3 = HillClimberValue(x0=x[3, 0], deltas = array([-.05, 0, .05]), minValue=-30, maxValue=55)
    
    deltas = array([[0,0,0,0],
                    [1,-1,0,0],
                    [-1,1,0,0],
                    [1,0,-1,0],
                    [-1,0,1,0],
                    [1,0,0,-1],
                    [-1,0,0,1],
                    [0,1,-1,0],
                    [0,-1,1,0],
                    [0,1,0,-1],
                    [0,-1,0,1],
                    [0,0,1,-1],
                    [0,0,-1,1]]) * .05
    hc = HillClimberValue(x0=x2[:, 0], deltas = deltas,
                          p_delta_pos=transprob(13, 8.0),
                          p_delta_neg=transprob(13, 1.0/8.0),
                          minValue=array([-30, -20, -10, -5]),
                          maxValue=array([50, 52, 54, 55]))
    
    from time import time
    from matplotlib.pyplot import figure, plot, show
    
    st = time()
    for i in arange(N):
        #print x[:, i], fx
        x[0, i] = hc0.step(fx[0])
        x[1, i] = hc1.step(fx[1])
        x[2, i] = hc2.step(fx[2])
        x[3, i] = hc3.step(fx[3])
        fx = f(x[:, i])
        #print x2[:, i], fx
        x2[:, i] = hc.step(sum(fx2))
        fx2 = f(x2[:, i])
    
    print x[:, N-1], fx
    print x2[:, N-1], fx
    print time() - st
    
    figure()
    plot(x.T)
    figure()
    plot(x2.T)
    show()