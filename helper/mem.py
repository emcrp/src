import os
import psutil


process = psutil.Process(os.getpid())

def printMem(msg = ''):
    print "%s: Using %.3f MB memory" %(msg, process.memory_info().rss / 1048576.0)