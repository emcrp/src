'''
Wrapper for cPickle,
Contains utility methods to serialize/deserialize arbitrary objects
to/from binary format.
'''

import cPickle
import sys
from os.path import dirname, exists
from os import makedirs


def toString(obj):
    '''
    Serialize object.
    '''
    return cPickle.dumps(obj)


def fromString(string):
    '''
    Deserializes object.
    '''
    return cPickle.loads(string)


def toFile(obj, filename, mode = 'wb'):
    '''
    Serializes object and writes to file.
    '''
    try:
        dirName = dirname(filename)
        if dirName != "" and not exists(dirName):
            makedirs(dirName)
        
        _file = open(filename, mode)
        cPickle.dump(obj, _file)
    except SystemError as e:
        print "Weird system error", e, "when trying to save following to", filename
        print obj
    except:
        print sys.exc_info()
        print "Reeeally unexpected behavior:", sys.exc_info()[0], "when trying to save following to", filename
        print obj
    

def fromFile(filename, mode = 'rb'):
    '''
    Deserializes object from file.
    Reads and decodes.
    '''
    _file = open(filename, mode)
    return cPickle.load(_file)

