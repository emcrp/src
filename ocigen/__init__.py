from django.conf import settings
import os

if not settings.configured:
    print 'Configuring Django'

    TEMPLATES = [
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': ['templates'],
        }
    ]
    settings.configure(TEMPLATES=TEMPLATES)
    
    from django import setup
    setup()