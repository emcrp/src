from os.path import abspath, dirname

from ocigen.generator import OpCodeInterpreterModel


def renderExampleOci():
    oci = OpCodeInterpreterModel(name = 'Example',
                             counterSize = 1, # 8-bit program counter => 256 byte RAM
                             description = 'Example OCI')

    oci.mem(name = 'extraMem', size = 256)        # additional memory segment
    oci.reg(name = 'extraMemPtr', size = 1)       # pointer which can fully access extraMem
    
    # Assign instructions
    oci.assign(opCodePrefix = '0',  instructionName = 'a')
    oci.assign(opCodePrefix = '10', instructionName = 'b', parameters = {'x':1})
    oci.assign(opCodePrefix = '11', instructionName = 'c')
    
    print 'Rendering OCI', oci.name
    oci.compress()
    print oci
    
    # C++ OCI
    
    templates = abspath('%s/templates' %dirname(abspath(__file__)))
    
    oci.render(templateFileName = '%s/cpp_oci.ht' %(templates))
    

if __name__ == "__main__":
    renderExampleOci()