'''
Model of the opcode interpreter binary radix tree generator.
Contains the different nodes the interpreter goes through while reading opcodes,
and the logic for rendering the low-level output.

Uses radix-tree of opcode prefixes.
'''

from collections import OrderedDict
from django.template.base import Template
from django.template.context import Context
from os import makedirs
from os.path import abspath, dirname, join, isfile, exists

from ocigen.integ import filters
from ocigen.integ.autolist import AutoExpandingList
from ocigen.integ.smartrender import smartRender


class OpCodeInterpreterModel(object):
    '''
    Opcode interpreter DSL model.
    Contains root node.
    '''
    
    def __init__(self, name, counterSize = 2, numRegisters = 8, 
                 outputDir = None, customInterpret = False, description = '', pOutput = None):
        self.name = name
        self.shortName = self.name.lower() + 'oci'
        self.counterSize = counterSize
        self.customInterpret = customInterpret
        
        if outputDir:
            self.outputDir = abspath(outputDir)
        else:
            self.outputDir = abspath('%s/../emcpp/oci' %dirname(abspath(__file__)))
        
        self.description = description
        self.root = PendingNode()
        self.single = False
        self.pOutput = pOutput
        self.regs = {}
        self.mainMem = None
        self.flexibleMems = {}
        self.fixedMems = {}
        self.props = {}
        self.numRegisters = numRegisters
        self.rawGeneticStringSize = self.numRegisters + self.counterSize
        self.ops = AutoExpandingList()
        self.coveredOpCodes = 0
        self.numCategories = 0
        self.categories = []
        self.categoryCommands = OrderedDict()
    
        
    def assign(self, opCodePrefix, instructionName, parameters = {}, category = 'default', description = ''):
        '''
        Assign a new opcode prefix to be interpretable by the generated code.
        The given prefixed opCode will be attributed to an instruction name,
        and to and optional number of parameters.
        
        The root node will then create children binarily until this prefix is
        reached, and an assigned node is added there.
        '''
        
        if category is not None:
            if not category in self.categories:
                self.categories.append(category)
                self.categoryCommands[category] = 0
                self.numCategories += 1
            self.categoryCommands[category] += 2 ** (8 - len(opCodePrefix))
            categoryIdx = self.categories.index(category)
        else:
            categoryIdx = -1
        
        self.ops[len(self.ops)] = { 'opCodePrefix':opCodePrefix,
                                    'instructionName':instructionName,
                                    'parameters':parameters,
                                    'category':category,
                                    'categoryIdx':categoryIdx,
                                    'description':description }
        self.coveredOpCodes += 2 ** (8 - len(opCodePrefix))
        
        if opCodePrefix is '':
            # one instruction instruction set
            if len(self.ops) > 1:
                raise NodeAlreadyAssignedException(self.ops[0].opCodePrefix)
            self.single = True
            self.root = AssignedNode(opCodePrefix, instructionName, parameters, categoryIdx, category, description)
        else:
            self.root.assign(opCodePrefix, instructionName, parameters, categoryIdx, category, description)
    
    
    def assignBlind(self, instructionName, parameters = {}, category = 'default', description = ''):
        opCodePrefixLength = 8 - sum(parameters.values())
        opCodePrefixes = self.root.findOpenOpCodePrefix(opCodePrefixLength)
        if opCodePrefixes == []:
            print self
            raise OpCodeInterpreterModelFullException(instructionName)
        else:
            opCodePrefix = opCodePrefixes[0]
            
        self.assign(opCodePrefix, instructionName, parameters, category, description)
    
    
    def fillWithEmpties(self):
        for opCodePrefixLength in range(1,9):
            opCodePrefixes = self.root.findOpenOpCodePrefix(opCodePrefixLength)
            for opCodePrefix in opCodePrefixes:
                print opCodePrefix
                self.assign(opCodePrefix=opCodePrefix,
                            instructionName='doNothing',
                            category=None)
    
    
    def reg(self, name, size = 1, description = ''):
        self.regs[name] = {'name':name, 'size':size, 'description':description}
        self.rawGeneticStringSize += size
    
    
    def prop(self, name, size = 1, description = ''):
        '''
        Add property to virtual machine. Can be globally set/get.
        '''
        self.props[name] = { 'name':name, 'size':size, 'description':description}
        
        
    def mem(self, name, size = 256, unitSize = 1, flexible = False, hasInstructions = False, description = ''):
        '''
        Add a memory segment to the current virtual machine.
        @param size: Size of memory segment in bytes.
        @param flexible: If set, size is not necessary, it will be included in generated constructor
        @param hasInstructions: If set, the instruction interpreter will take its instructions from this piece of memory.
        '''
        
        mem = { 'name': name, 'unitSize':unitSize, 'main':hasInstructions, 'size':size, 'description':description }
        
        if flexible:
            self.flexibleMems[name] = mem
        else:
            self.fixedMems[name] = mem
            self.rawGeneticStringSize += size * unitSize
        
        if hasInstructions:
            self.mainMem = mem
        
        
    
    def verifyCompleteness(self):
        '''
        Verifies all possible outcomes are covered by the tree (all leaves are assigned).
        Otherwise, exception is raised.
        Must be done before compression.
        '''
        self.root.verifyCompleteness()
        
        
    def compress(self):
        '''
        Compresses the OCI by cutting out mid-layers which only contain
        pending nodes, and assigns all the children to a given node.
        '''
        self.verifyCompleteness()
        for node in self.pendingNodes():
            node.compress()
    
    
    def render(self, templateFileName, output = None, overwrite = True, smart = False, extraArgs = {}):
        '''
        Render Django template to an output file, and also return it.
        Uses current model as the context of the Django template call.
        '''
        print 'Rendering template %s' %(templateFileName)
        templateFile = open(templateFileName, 'rb')
        template = Template(templateFile.read())
        
        selfInfo = {'name': self.name, 'shortName': self.shortName,
                    'root': self.root, 'single': self.single, 'pOutput': self.pOutput,
                    'rawGeneticStringSize': self.rawGeneticStringSize, 'counterSize': self.counterSize,
                    'description': self.description, 'customInterpret': self.customInterpret,
                    'numRegisters': self.numRegisters, 'regs': self.regs, 'props': self.props,
                    'fixedMems':self.fixedMems, 'flexibleMems': self.flexibleMems, 'mainMem': self.mainMem,
                    'pendingNodes':self.pendingNodes(), 'assignedNodes':self.assignedNodes(),
                    'numCategories':self.numCategories, 'categoryCommands':self.categoryCommands}
        selfInfo.update(extraArgs)
        
        ret = template.render(Context(selfInfo))
        
        if output != None:
            output = join(self.outputDir, output)
            outputExists = isfile(output)
            
            if not outputExists or (overwrite and not smart):
                outputDir = dirname(output)
                if outputDir == '': outputDir = '.'
                if not exists(outputDir):
                    makedirs(outputDir)
                outputFile = open(output, 'wb')
                print 'Rendering template %s to file %s' %(templateFileName, output)
                outputFile.write(ret)
            elif not overwrite:
                print "File %s already exists, not overwriting. Set flag to true to overwrite" %(output)
            else:
                smartRender(output, selfInfo)
            
        else:
            print 'Rendering template %s' %(templateFileName)
            print ret
            
        return ret
    
    
    def renderDefault(self):
        print 'Rendering OCI', self.name
        print self
    
        templates = abspath('%s/templates' %dirname(abspath(__file__)))
    
        self.render(templateFileName = '%s/cpp_oci.ht' %(templates),
                    output = '%s.h' %(self.shortName))
        
        self.render(templateFileName = '%s/cpp_oci_stub.cppt' %(templates),
                    output = '%sstub.cpp' %(self.shortName),
                    extraArgs = {'allocationUnit': 1024})
        
        self.render(templateFileName = '%s/cpp_oci_wrapper.pyt' %(templates),
                    output = '../wrapper/oci/%s.py' %(self.shortName))
        
        self.render(templateFileName = '%s/cpp_oci.cppt' %(templates),
                    output = '%s.cpp' %(self.shortName),
                    smart = True)
        
        self.render(templateFileName = '%s/cpp_oci_test.pyt' %(templates),
                    output = '../../test/cpp/oci/%s.py' %(self.shortName),
                    smart = True)
        
        if exists('/home/csaba/src/emc/emcrp.gitlab.io/public/html/oci'):
            self.render(templateFileName = '%s/html_oci_doc.htmlt' %(templates),
                        output = '/home/csaba/src/emc/emcrp.gitlab.io/public/html/oci/%s.html' %(self.shortName))
        
        print 'Rendered. Please add header files to emcpp.i if not present already'
        print ''
    
    
    def allNodes(self):
        ret = AutoExpandingList([self.root])
        ret.extend(self.root.allChildren())
        return ret


    def pendingNodes(self):
        return self.allNodes().findAll(lambda x: isinstance(x, PendingNode))
    
    
    def assignedNodes(self):
        return self.allNodes().findAll(lambda x: isinstance(x, AssignedNode))
    
    
    def __repr__(self):
        ret = 'OpCodeInterpreterModel[%s, counterSize=%s, numRegisters=%d, rawGeneticStringSize=%d, coveredOpCodes=%d, ops=[\n' % (
                            self.name, self.counterSize, self.numRegisters, self.rawGeneticStringSize, self.coveredOpCodes)
        for op in self.ops:
            ret += '    %s -> instructionName=%s, parameters=%s, category=%s, description="%s"\n' %(
                op['opCodePrefix'], op['instructionName'], str(op['parameters']), op['category'], op['description'])
        ret += '],regs=[\n'
        for regName, reg in self.regs.iteritems():
            ret += '  name=%s, size=%d, description=%s\n' %(regName, reg['size'], reg['description'])
        ret += '],flexibleMems=[\n'
        for memName, mem in self.flexibleMems.iteritems():
            ret += '  name=%s, unitSize=%d\n' %(memName, mem['unitSize'])
        ret += '],fixedMems=[\n'
        for memName, mem in self.fixedMems.iteritems():
            ret += '  name=%s, size=%d, unitSize=%d, description=%s\n' %(memName, mem['size'], mem['unitSize'], mem['description'])
        ret +=']]'
        
        return ret



class Node(object):
    '''
    General node within the interpreter DSL model.
    '''
    
    def __init__(self, opCodePrefix = ''):
        self.opCodePrefix = opCodePrefix
    
    


class PendingNode(Node):
    '''
    The pending node within the interpreter DSL model.
    Pending means the opcode has not returned a full match at this point, and we must go further
    to get an interpretation (i.e. until we reach an assigned node.
    
    The pending node always has two children nodes, based on the next bit read from the input, a node for 0,
    and a node for 1.
    '''
    
    def __init__(self, opCodePrefix = ''):
        super(PendingNode, self).__init__(opCodePrefix)
        self.children = AutoExpandingList()
        self.pending = True
        self.bitDifferenceToChildren = 1
        
    
    def findOpenOpCodePrefix(self, opCodePrefixLength):
        if opCodePrefixLength == len(self.opCodePrefix):
            return []
        
        ret = []
        for i in range(2):
            if self.children[i] is not None:
                ret += self.children[i].findOpenOpCodePrefix(opCodePrefixLength)
            else:
                remainingBits = opCodePrefixLength - len(self.opCodePrefix) - 1
                if remainingBits == 0:
                    ret += [self.opCodePrefix + '%d'%i]
                else:
                    ret += [self.opCodePrefix + '%d'%i + bin(j)[2:].zfill(remainingBits) for j in range(2 ** remainingBits)]
        
        return ret
    
        
    def assign(self, opCodePrefix, instructionName, parameters = {}, categoryIdx = 0, category = '', description = ''):
        '''
        Assign instruction name to an opCodePrefix.
        Done by creating children nodes and protruding them until the opcode prefix is satisfied,
        at which point a leaf in the tree will be created, an AssignedNode.
        '''
        
        newBit = int(opCodePrefix[0])
        opCodePrefix = opCodePrefix[1:]
        newOpCodePrefix = self.opCodePrefix + str(newBit)
        
        if opCodePrefix is '':
            if self.children[newBit] is not None:
                raise NodeAlreadyAssignedException(self.children[newBit].opCodePrefix)
            newNode = AssignedNode(newOpCodePrefix, instructionName, parameters, categoryIdx, category, description)
            self.children[newBit] = newNode
        else:
            if self.children[newBit] is None:
                newNode = PendingNode(newOpCodePrefix)
                self.children[newBit] = newNode
            elif isinstance(self.children[newBit], AssignedNode):
                raise NodeAlreadyAssignedException(self.children[newBit].opCodePrefix)
            self.children[newBit].assign(opCodePrefix, instructionName, parameters, categoryIdx, category, description)
    
    
    
    def allChildren(self):
        ret = []
        for value in self.children:
            ret.append(value)
            ret.extend(value.allChildren())
        return ret
    
    
    
    def verifyCompleteness(self):
        '''
        Verifies integrity of current node and recursively calls for children.
        Must be called before compression.
        '''
        for i in range(2):
            if self.children[i] is None:
                missingBranch = self.opCodePrefix + str(i)
                raise OpCodeInterpreterModelIncompleteException(missingBranch)
            elif isinstance(self.children[i], PendingNode):
                self.children[i].verifyCompleteness() 
    
        
        
    def compress(self):
        '''
        Compresses current node by cutting out layers with only pending nodes,
        and going directly to the closest assigned one.
        '''
        assignedChild = self.children.find(lambda x: isinstance(x, AssignedNode))
        while assignedChild == None:
            childrenOfChildren = self.children.collect(lambda x: x.children)
            childrenOfChildren = AutoExpandingList([x for sublist in childrenOfChildren for x in sublist if x is not None])
            self.bitDifferenceToChildren += 1
            self.children = childrenOfChildren
            assignedChild = self.children.find(lambda x: isinstance(x, AssignedNode))
            



class AssignedNode(Node):
    '''
    A node with no more children, which has an actual instruction
    assigned to it.
    
    Will be placed into the appropriate place in the radix tree.
    Contains the instruction name this node should generate, and a map of parameter names
    to parameter bit sizes.
    '''
    
    def __init__(self, opCodePrefix, instructionName, parameters, categoryIdx, category, description):
        super(AssignedNode, self).__init__(opCodePrefix)
        self.instructionName = instructionName
        self.parameters = parameters
        self.__calcBitsToDiscard__()
        self.categoryIdx = categoryIdx 
        self.category = category
        self.description = description
        self.pending = False
    
    
    def findOpenOpCodePrefix(self, opCodePrefixLength):
        return []
    
         
    def __calcBitsToDiscard__(self):
        '''
        Some last bits in an instruction may be chosen to be left discarded.
        Since the instructions should be of the same size, we jump through some bits.
        The number of those is calculated and stored here.
        '''
        self.bitsToDiscard = 8 - len(self.opCodePrefix)
        for _, paramBits in self.parameters.iteritems():
            self.bitsToDiscard -= paramBits


    def rangeBitsToDiscard(self):
        return range(self.bitsToDiscard)
    
    
    def paramTestValues(self):
        ret = {}
        for paramName, paramSize in self.parameters.iteritems():
            ret[paramName] = '0b' + '1' * paramSize
        return ret
     
    
    def allChildren(self):
        return []
    



class OpCodeInterpreterModelIncompleteException(Exception):
    '''
    Exception to throw if any branch of the binary tree is not covered by an op code.
    '''
    def __init__(self, prefix):
        self.prefix = prefix
    
    def __str__(self):
        return "OCI model not complete! Following branch has not been covered: %s..." % self.prefix




class NodeAlreadyAssignedException(Exception):
    '''
    Exception to throw if an opcode already taken is trying to be overriden.
    '''
    def __init__(self, prefix):
        self.prefix = prefix
    
    def __str__(self):
        return "OCI model inconsistent! Following branch has already been covered: %s..." % self.prefix



class OpCodeInterpreterModelFullException(Exception):
    '''
    Exception to throw when assigning a new instruction blindly, but the tree has no capacity for it.
    '''
    def __init__(self, instructionName):
        self.instructionName = instructionName
    
    def __str__(self):
        return "Could not assign instruction '%s'. There is not enough space in the OCI model" % self.instructionName