'''
Generator for an indirect op-code interpreter of EMC.
Contains the op-code/instruction mappings.

'''

from os.path import abspath, dirname

from ocigen.generator import OpCodeInterpreterModel


def buildIndirectOci():
    oci = OpCodeInterpreterModel("Indirect", 
                                 numRegisters = 8,
                                 description = 'OCI with no immediate addressing, every instruction is exactly 8 bits')
    
    # registers
    oci.reg('dataPtr', size = 2, description = 'Data pointer which can address entire memory')
    oci.reg('acc',               description = 'Accumulator used in arithmetic/logic instructions')
    oci.reg('stackPtr',          description = 'Stack pointer which can address entire stack')
    oci.reg('flags',             description = 'Set of 8 flags; only used for carry as of yet')
    
    # memory segments
    oci.mem('ram', flexible = True, hasInstructions = True, description = 'Read/writable memory for both instructions and data')
    oci.mem('stack', size = 256, description = 'Stack used for pushing/popping register values')
    
    
    # MOVs for general-purpose registers
    oci.assignBlind('movR_DH',          { 'r':3 },  category = 'transfer',     description = 'Copies high byte of `dataPtr` into given register')
    oci.assignBlind('movDH_R',          { 'r':3 },  category = 'transfer',     description = 'Copies value of given register into high byte of `dataPtr`')
    oci.assignBlind('movR_DL',          { 'r':3 },  category = 'transfer',     description = 'Copies low byte of `dataPtr` into given register')
    oci.assignBlind('movDL_R',          { 'r':3 },  category = 'transfer',     description = 'Copies value of given register into low byte of `dataPtr`')
    
    oci.assignBlind('movR_DPtr',        { 'r':3 },  category = 'transfer',     description = 'Copies 1 byte from memory where `dataPtr` points, into given register')
    oci.assignBlind('movDPtr_R',        { 'r':3 },  category = 'transfer',     description = 'Copies value of given register to the memory where `dataPtr` points')
    
    oci.assignBlind('movR_A',           { 'r':3 },  category = 'transfer',     description = 'Copies `acc` into given register')
    oci.assignBlind('movA_R',           { 'r':3 },  category = 'transfer',     description = 'Copies given register into `acc`')
    oci.assignBlind('xchA_R',           { 'r':3 },  category = 'transfer',     description = 'Exchanges values of `acc` and given register')
    
    
    # MOVs between A, D, DPtr
    oci.assignBlind('movA_DH',                      category = 'transfer',     description = 'Copies high byte of `dataPtr` into `acc`')
    oci.assignBlind('movA_DL',                      category = 'transfer',     description = 'Copies value of `acc` into high byte of `dataPtr`')
    oci.assignBlind('movDH_A',                      category = 'transfer',     description = 'Copies low byte of `dataPtr` into `acc`')
    oci.assignBlind('movDL_A',                      category = 'transfer',     description = 'Copies value of `acc` into low byte of `dataPtr`')
    
    oci.assignBlind('movA_DPtr',                    category = 'transfer',     description = 'Copies 1 byte from memory where `dataPtr` points, into `acc`')
    oci.assignBlind('movDPtr_A',                    category = 'transfer',     description = 'Copies value of `acc` to the memory where `dataPtr` points')


    # Carry flag
    oci.assignBlind('cplC',                         category = 'arithmetic',   description = 'Inverts value of carry flag')
    
    
    # Arithmetic & Logic
    oci.assignBlind('inc_A',                        category = 'arithmetic',   description = 'Increments value of `acc`')
    oci.assignBlind('dec_A',                        category = 'arithmetic',   description = 'Decrements value of `acc`')
    oci.assignBlind('inc_D',                        category = 'arithmetic',   description = 'Increments value of `dataPtr`')
    oci.assignBlind('dec_D',                        category = 'arithmetic',   description = 'Decrements value of `dataPtr`')
    
    oci.assignBlind('anlA_R',           { 'r':3 },  category = 'arithmetic',   description = 'Performs bitwise and between `acc` and given register')
    oci.assignBlind('orlA_R',           { 'r':3 },  category = 'arithmetic',   description = 'Performs bitwise or between `acc` and given register')
    
    oci.assignBlind('addA_R',           { 'r':3 },  category = 'arithmetic',   description = 'Adds value of given register to `acc`')
    oci.assignBlind('addcA_R',          { 'r':3 },  category = 'arithmetic',   description = 'Adds value of given register to `acc`, with carry')
    oci.assignBlind('subA_R',           { 'r':3 },  category = 'arithmetic',   description = 'Subtracts value of given register from `acc`')
    oci.assignBlind('subbA_R',          { 'r':3 },  category = 'arithmetic',   description = 'Subtracts value of given register from `acc`, with borrow')
    
    oci.assignBlind('cplA',                         category = 'arithmetic',   description = 'Inverts value of `acc` bitwise')
    oci.assignBlind('anlA_RPtr',        { 'rp':2 }, category = 'arithmetic',   description = 'Performs bitwise and between `acc` and memory memory where given register points')
    oci.assignBlind('orlA_RPtr',        { 'rp':2 }, category = 'arithmetic',   description = 'Performs bitwise or between `acc` and memory memory where given register points')
    oci.assignBlind('anlA_DPtr',                    category = 'arithmetic',   description = 'Performs bitwise and between `acc` and memory where `dataPtr` points')
    oci.assignBlind('orlA_DPtr',                    category = 'arithmetic',   description = 'Performs bitwise or between `acc` and memory where `dataPtr` points')
    
    oci.assignBlind('addA_RPtr',        { 'rp':2 }, category = 'arithmetic',   description = 'Adds value of memory where given register points to `acc`')
    oci.assignBlind('addcA_RPtr',       { 'rp':2 }, category = 'arithmetic',   description = 'Adds value of memory where given register points to `acc`, with carry')
    oci.assignBlind('subA_RPtr',        { 'rp':2 }, category = 'arithmetic',   description = 'Subtracts value of memory where given register points from `acc`')
    oci.assignBlind('subbA_RPtr',       { 'rp':2 }, category = 'arithmetic',   description = 'Subtracts value of memory where given register points from `acc`, with borrow')
    oci.assignBlind('addA_DPtr',                    category = 'arithmetic',   description = 'Adds value of memory where `dataPtr` points to `acc`')
    oci.assignBlind('addcA_DPtr',                   category = 'arithmetic',   description = 'Adds value of memory where `dataPtr` points to `acc`, with carry')
    oci.assignBlind('subA_DPtr',                    category = 'arithmetic',   description = 'Subtracts value of memory where `dataPtr` points from `acc`')
    oci.assignBlind('subbA_DPtr',                   category = 'arithmetic',   description = 'Subtracts value of memory where `dataPtr` points from `acc`, with borrow')

    oci.assignBlind('rlA',                          category = 'arithmetic',   description = 'Rotates `acc` left with 1 bit')
    oci.assignBlind('rrA',                          category = 'arithmetic',   description = 'Rotates `acc` right with 1 bit')
    oci.assignBlind('rlcA',                         category = 'arithmetic',   description = 'Rotates `acc` left with 1 bit, using carry flag')
    oci.assignBlind('rrcA',                         category = 'arithmetic',   description = 'Rotates `acc` right with 1 bit, using carry flag')
    
    
    # Stack
    oci.assignBlind('pushDH',                       category = 'machine',      description = 'Pushes high byte of `dataPtr` to stack')
    oci.assignBlind('popDH',                        category = 'machine',      description = 'Pops high byte of `dataPtr` from stack')
    oci.assignBlind('pushDL',                       category = 'machine',      description = 'Pushes low byte of `dataPtr` to stack')
    oci.assignBlind('popDL',                        category = 'machine',      description = 'Pops low byte of `dataPtr` from stack')
    oci.assignBlind('pushA',                        category = 'machine',      description = 'Pushes value of `acc` to stack')
    oci.assignBlind('popA',                         category = 'machine',      description = 'Pops value of `acc` from stack')
    oci.assignBlind('pushR',            { 'r':2 },  category = 'machine',      description = 'Pushes value of a register to stack')
    oci.assignBlind('popR',             { 'r':2 },  category = 'machine',      description = 'Pops value of a register from stack')
    
    
    
    # Jump
    oci.assignBlind('sjmpR',            { 'r':2 },  category = 'branching',    description = 'Short jumps program counter with value of register (R0-R3)')
    oci.assignBlind('jmpR',             { 'rp':2 }, category = 'branching',    description = 'Absolute jumps to memory address given by pair of registers')
    oci.assignBlind('jmpD',                         category = 'branching',    description = 'Absolute jumps to value of `dataPtr`')
    
    
    # Call/return
    oci.assignBlind('scallR',           { 'r':2 },  category = 'branching',    description = 'Pushes program counter, and then `sjmpR`')
    oci.assignBlind('callR',            { 'rp':2 }, category = 'branching',    description = 'Pushes program counter, and then `jmpR`')
    oci.assignBlind('callD',                        category = 'branching',    description = 'Pushes program counter, and then `jmpD`')
    oci.assignBlind('ret',                          category = 'branching',    description = 'Pops program counter from stack')
    
    
    # Conditional Jump
    oci.assignBlind('jnc_D',                        category = 'branching',    description = '`jmpD` if carry bit is not set')
    oci.assignBlind('jc_D',                         category = 'branching',    description = '`jmpD` if carry bit is set')
    oci.assignBlind('sjnc_R',           { 'r':2 },  category = 'branching',    description = '`sjmpR` if carry bit is not set')
    oci.assignBlind('sjc_R',            { 'r':2 },  category = 'branching',    description = '`sjmpR` if carry bit is set')
    oci.assignBlind('jnc_R',            { 'rp':2 }, category = 'branching',    description = '`jmpR` if carry bit is not set')
    oci.assignBlind('jc_R',             { 'rp':2 }, category = 'branching',    description = '`jmpR` if carry bit is set')
    
    oci.assignBlind('jnzA_D',                       category = 'branching',    description = '`jmpD` if `acc` is not zero')
    oci.assignBlind('jzA_D',                        category = 'branching',    description = '`jmpD` if `acc` is zero')
    oci.assignBlind('sjnzA_R',          { 'r':2 },  category = 'branching',    description = '`sjmpR` if `acc` is not zero')
    oci.assignBlind('sjzA_R',           { 'r':2 },  category = 'branching',    description = '`sjmpR` if `acc` is zero')
    oci.assignBlind('jnzA_R',           { 'rp':2 }, category = 'branching',    description = '`jmpR` if `acc` is not zero')
    oci.assignBlind('jzA_R',            { 'rp':2 }, category = 'branching',    description = '`jmpR` if `acc` is zero')
    
    # Output & Halt
    oci.assignBlind('halt',                         category = 'machine',      description = 'Signals the VM to halt')
    oci.assignBlind('outR',             { 'r':3 },  category = 'output',       description = 'Outputs the value of a given register')
    oci.assignBlind('outRPtr',          { 'rp':2 }, category = 'output',       description = 'Outputs byte from where a pair of registers is pointing')
    oci.assignBlind('outDPtr',          { 'n':3 },  category = 'output',       description = 'Outputs next *n* bytes from where `dataPtr` points')
    
    oci.compress()
    return oci


def renderIndirectOci():
    oci = buildIndirectOci()
    oci.renderDefault()
    

if __name__ == "__main__":
    renderIndirectOci()
