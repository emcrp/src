'''
Helper class to add extra functionality to a list.

'''

class AutoExpandingList(list):
    '''
    Extended helper class of lists.
    Provides functionality to automatically allocate new space if index too high, filling all
    intermediate state with None.
    '''
    
    def __getitem__(self, index):
        if index >= len(self):
            return None
        return super(AutoExpandingList, self).__getitem__(index)
    
    
    def __setitem__(self, index, value):
        if isinstance(index, int):
            self.expandfor(index)
            return super(AutoExpandingList, self).__setitem__(index, value)

        elif isinstance(index, slice):
            if index.stop < index.start:
                return super(AutoExpandingList, self).__setitem__(index, value)
            else:
                self.expandfor(index.stop if abs(index.stop) > abs(index.start) else index.start)
                
            return super(AutoExpandingList, self).__setitem__(index, value)


    def expandfor(self, index):
        rng = []
        if abs(index) > len(self)-1:
            if index < 0:
                rng = xrange(abs(index)-len(self))
                for _ in rng:
                    self.insert(0, None)
            else:
                rng = xrange(abs(index)-len(self)+1)
                for _ in rng:
                    self.append(None)


    def find(self, lambdaExpr):
        for x in self:
            if lambdaExpr(x):
                return x
        return None
    
    
    def findAll(self, lambdaExpr):
        ret = AutoExpandingList()
        for x in self:
            if lambdaExpr(x):
                ret.append(x)
        return ret

    
    def collect(self, lambdaExpr):
        ret = AutoExpandingList()
        for i, x in enumerate(self):
            if x is not None:
                ret[i] = lambdaExpr(x)
        return ret