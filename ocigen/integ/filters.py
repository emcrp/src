'''
Custom Django filters
'''

from django.template.base import Template
from django.template.context import Context
from django.template.defaultfilters import register
from re import sub


@register.filter
def compl(value):
    '''
    Custom Django template filter to quickly access complementary - 8 minus a value.
    '''
    return 8 - value


@register.filter
def cppType(value):
    '''
    Custom Django template filter to determine data type based on size in bytes.
    '''
    if value == 'flag':
        return "bool"
    elif value == 1:
        return "unsigned char"
    elif value == 2:
        return "unsigned short"
    else:
        return "unsigned int"


@register.filter
def cppDefault(value):
    if value == 'flag':
        return "false"
    else:
        return "0"


@register.filter
def pyDefault(value):
    if value == 'flag':
        return "False"
    else:
        return "None"


@register.filter
def capFirst(value):
    '''
    Django filter to capitalize first letter of a string.
    '''
    return value[0].upper() + value[1:]


@register.filter
def printfTimes(value):
    '''
    Django filter to use in printfs for %d,%d,%d...
    '''
    l = len(value)
    if l == 0: return ''
    ret = '%d'
    for _ in range(l-1): ret += ',%d'
    return ret


@register.filter
def joinUC(value):
    params = ["unsigned char %s" %(paramName) for paramName in value.iterkeys()]
    return ('(' + ', '.join(params) + ')').ljust(18)


@register.filter
def macro(value):
    '''
    Creater C++ type macro name from a camel-case string.
    '''
    ret = value[0].upper()
    for ch in value[1:]:
        if ch == ch.upper():
            ret += '_'
        ret += ch.upper()
    return ret


@register.filter
def hexmask(value):
    '''
    Creates a mask that you can "&" with in C++
    '''
    return hex(value-1).upper()


@register.filter
def clean(value):
    '''
    Convert markup text to clean text - strip tags
    '''
    value = sub('\*\*([^\*]+)\*\*', '\\1', value)
    value = sub('\*([^\*]+)\*',     '\\1', value)
    value = sub('`([^`]+)`',        '\\1', value)
    return value


@register.filter
def html(value):
    '''
    Convert markup text to HTML text - use tags where necessary
    '''
    value = sub('\*\*([^\*]+)\*\*', '<b>\\1</b>', value)
    value = sub('\*([^\*]+)\*',     '<i>\\1</i>', value)
    value = sub('`([^`]+)`',        '<code>\\1</code>', value)
    return value


@register.filter
def tex(value):
    '''
    Convert markup text to LaTeX text - use tags where necessary
    '''
    value = sub('\*\*([^\*]+)\*\*', '\\textbf{\\1}', value)
    value = sub('\*([^\*]+)\*',     '\\emph{\\1}</i>', value)
    value = sub('`([^`]+)`',        '\\texttt{\\1}</code>', value)
    return value


@register.filter
def cppMethod(state, name):
    cppMethodTemplateText = """void {{ name }}Oci::{{ state.instructionName }}{{ state.parameters | joinUC }} { cout << "{{ state.instructionName }} " {% for paramName in state.parameters.keys %}<< (int) {{ paramName }} << " " {% endfor %}<< endl; }"""
    cppMethodTemplate = Template(cppMethodTemplateText)
    context = Context({'name' : name, 'state':state})
    return cppMethodTemplate.render(context)


@register.filter
def pyTestMethod(state):
    pyTestMethodTemplateText = """    def test_{{ state.instructionName }}(self):
        #given
        #when
        self.oci.native.{{ state.instructionName }}({% for paramName in state.parameters.keys %}{% ifnotequal forloop.counter0 0 %}, {% endifnotequal %}0{% endfor %})
        #then
        self.fail("test not implemented yet")
    """
    pyTestMethodTemplate = Template(pyTestMethodTemplateText)
    context = Context({'state':state})
    return pyTestMethodTemplate.render(context)