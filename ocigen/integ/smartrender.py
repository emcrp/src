from ocigen.integ.filters import cppMethod, pyTestMethod
from re import search, sub

def smartRender(output, model):
    if output.endswith(".cpp"):
        smartRenderCpp(output, model)
    elif output.endswith(".py"):
        smartRenderPyTest(output, model)
    else:
        print "Can't smart render. Output name not supported"


def smartRenderCpp(output, model):
    print "Smart rendering C++ %s" %output
    
    content = open(output, 'rb').read()
    changed = False
    
    oldIns = []
    for line in content.splitlines():
        match = search("void %sOci::([a-zA-Z0-9_]+)\(.*" %(model['name']), line)
        if match:
            oldIns.append(match.group(1))
    
    newIns = [state.instructionName for state in model['assignedNodes'] if state.instructionName != "doNothing"]
    toRemove = [ins for ins in oldIns if ins not in newIns]
    toAdd = [ins for ins in newIns if ins not in oldIns]
    
    for state in model['assignedNodes']:
        if state.instructionName in toAdd:
            print "  %s not present, adding it" %state.instructionName
            content += "\n" + cppMethod(state, model['name']) + " // added by smart render"
            changed = True
    
    for instructionName in toRemove:
        print "  %s obsolete, removing it" %instructionName
        pattern = "void %sOci::(%s\(.*)" %(model['name'], instructionName)
        subs = r'// removed by smart render: \1'
        content = sub(pattern, subs, content)
        changed = True
    
    if changed:
        open(output, 'wb').write(content)
        
        

def smartRenderPyTest(output, model):
    print "Smart rendering py test %s" %output
    
    content = open(output, 'rb').read()
    changed = False
    
    oldIns = []
    for line in content.splitlines():
        match = search(" {4}def test_([a-zA-Z0-9_]+)\(self\):.*", line)
        if match:
            oldIns.append(match.group(1))
    
    newIns = [state.instructionName for state in model['assignedNodes'] if state.instructionName != "doNothing"]
    toRemove = [ins for ins in oldIns if ins not in newIns]
    toAdd = [ins for ins in newIns if ins not in oldIns]
    
    for state in model['assignedNodes']:
        if state.instructionName in toAdd:
            print "  %s not present, adding it" %state.instructionName
            contentLines = content.splitlines()
            contentLines.insert(-4, '    # added by smart render\n' + pyTestMethod(state))
            content = "\n".join(contentLines)
            changed = True
    
    for instructionName in toRemove:
        print "  %s obsolete, removing it" %instructionName
        pattern = "def (test_%s\(self\):\n( {8}.*\n)* {4})" %(instructionName)
        subs = r"'''removed by smart render: \1'''"
        content = sub(pattern, subs, content)
        changed = True
    
    if changed:
        open(output, 'wb').write(content)