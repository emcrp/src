from ocigen.generator import OpCodeInterpreterModel

def renderLoadStoreOci():
    '''
    QUESTIONS:
    - MOV A->AddrL, A->AddrH   - should there also be one for B?
    - MOV A->B, B->A, A<->B?
    - OUT B?
    - SUBB - if the usage of C correct there?
    '''
    
    #simple Load/Store Instruction set
    oci = OpCodeInterpreterModel("LoadStore", numRegisters = 0)
    
    oci.mem('ram', flexible = True, hasInstructions = True)
    oci.reg('flags')
    
    #No stack, Minimal register set
    #A - accumulator and 1st input to ALU
    oci.reg('a')
    #B - "spare" register and second input to ALU
    oci.reg('b')
    #Addr - double width address register
    oci.reg('addr', size = 2)
    
    
    #data operations
    #    MOV reg, Addr         mem[Addr] -> reg A or B  (load)
    oci.assign('0000', 'mov_a_addr')
    oci.assign('0001', 'mov_b_addr')
    #    MOV Addr, reg         reg A or B -> mem[Addr]  (store)
    oci.assign('0010', 'mov_addr_a')
    oci.assign('0011', 'mov_addr_b')
    #    MOV AddrL, A          A->AddrL
    #    MOV AddrH, A          A->AddrH
    oci.assign('0100', 'mov_addrl_a')
    oci.assign('0101', 'mov_addrh_a')
    
    #Branching
    #    JNC Addr              if !C then Addr -> PC   Jump to Addr if carry bit not set
    oci.assign('011', 'jnc_addr')
    
    #Arithmetic
    #    SUBB                  A - B - C -> A      Subtract with borrow
    #    CPL A                 -A -> A             2's complement A
    #    CLR C                 0 -> C              zero the carry bit
    #    CPL C                 ~C -> C             complement the carry bit
    #    AND                   A AND B -> A        Bitwise AND A with B
    #    NOR                   A NOR B -> A        Bitwise NOR A with B
    oci.assign('1000', 'subb')
    oci.assign('1001', 'cpl_a')
    oci.assign('1010', 'clr_c')
    oci.assign('1011', 'cpl_c')
    oci.assign('1100', 'andl')
    oci.assign('1101', 'norl')
    
    
    #Machine control
    #    OUT                   A -> output         write contents of A to output
    oci.assign('111', 'out_a', category='output')
    
    
    oci.renderDefault()


if __name__ == '__main__':
    renderLoadStoreOci()