from ocigen.generator import OpCodeInterpreterModel


def buildDslOci():
    name = "MusicDsl"
        
    oci = OpCodeInterpreterModel(name, 
                                 numRegisters = 6,
                                 description = 'DSL interpreter for music generation')
    
    # prop
    oci.prop('diatonic', size = 'flag', description = 'Flag saying the pitch output of the VM is read as diatonic')
    oci.prop('key',      size = 1,      description = 'Represents the key the output pieces will be interpreted as')
    oci.prop('isMajor',  size = 'flag', description = 'Represents if output pieces should be in major or minor')
    
    # memory segments
    oci.mem('ram', flexible = True, hasInstructions = True)
    
    # registers
    oci.reg('dataPtr', size = 2, description = 'Data pointer which can address entire memory')
    oci.reg('flag',              description = 'Flag used for branching instructions')
    
    
    # MOVs for general-purpose registers
    oci.assignBlind('movNote_Next',     { 'n':2 },  category = 'transfer',     description = 'Copies next byte into nth note')
    oci.assignBlind('movNote_Next_b',   { 'n':1 },  category = 'transfer',     description = 'Copies next byte into nth note')
    oci.assignBlind('movNoteDur_Next',  { 'n':2 },  category = 'transfer',     description = 'Copies next byte as duration of nth note')
    oci.assignBlind('movNoteDur_Next_b',{ 'n':1 },  category = 'transfer',     description = 'Copies next byte as duration of nth note')
    oci.assignBlind('movNotePth_Next',  { 'n':2 },  category = 'transfer',     description = 'Copies next byte as pitch of nth note')
    oci.assignBlind('movNotePth_Next_b',{ 'n':1 },  category = 'transfer',     description = 'Copies next byte as pitch of nth note')
    
    oci.assignBlind('movNote_DPtr',     { 'n':2 },  category = 'transfer',     description = 'Copies 1 byte from where `dataPtr` points into nth note')
    oci.assignBlind('movNote_DPtr_b',   { 'n':1 },  category = 'transfer',     description = 'Copies 1 byte from where `dataPtr` points into nth note')
    oci.assignBlind('movNoteDur_DPtr',  { 'n':2 },  category = 'transfer',     description = 'Copies 1 byte from where `dataPtr` points as duration of nth note')
    oci.assignBlind('movNoteDur_DPtr_b',{ 'n':1 },  category = 'transfer',     description = 'Copies 1 byte from where `dataPtr` points as duration of nth note')
    oci.assignBlind('movNotePth_DPtr',  { 'n':2 },  category = 'transfer',     description = 'Copies 1 byte from where `dataPtr` points as pitch of nth note')
    oci.assignBlind('movNotePth_DPtr_b',{ 'n':1 },  category = 'transfer',     description = 'Copies 1 byte from where `dataPtr` points as pitch of nth note')
    
    oci.assignBlind('movDPtr_Note',     { 'n':2 },  category = 'transfer',     description = 'Copies 1 byte from nth note to where `dataPtr` points')
    oci.assignBlind('movDPtr_Note_b',   { 'n':1 },  category = 'transfer',     description = 'Copies 1 byte from nth note to where `dataPtr` points')
    oci.assignBlind('movDPtr_NoteDur',  { 'n':2 },  category = 'transfer',     description = 'Copies 1 byte from duration of nth note to where `dataPtr` points')
    oci.assignBlind('movDPtr_NoteDur_b',{ 'n':1 },  category = 'transfer',     description = 'Copies 1 byte from duration of nth note to where `dataPtr` points')
    oci.assignBlind('movDPtr_NotePth',  { 'n':2 },  category = 'transfer',     description = 'Copies 1 byte from pitch of nth note to where `dataPtr` points')
    oci.assignBlind('movDPtr_NotePth_b',{ 'n':1 },  category = 'transfer',     description = 'Copies 1 byte from pitch of nth note to where `dataPtr` points')
    
    oci.assignBlind('movD_Next',                    category = 'transfer',     description = 'Copies next 2 bytes to `dataPtr')
    oci.assignBlind('movDH_Next',                   category = 'transfer',     description = 'Copies next 1 byte into high byte of `dataPtr`')
    oci.assignBlind('movDL_Next',                   category = 'transfer',     description = 'Copies next 1 byte into low byte of `dataPtr`')
    oci.assignBlind('movDPtr_Next',                 category = 'transfer',     description = 'Copies next 1 byte to where `dataPtr` points')
    
    oci.assignBlind('movFlag_Next',                 category = 'transfer',     description = 'Set flag based on last bit of next byte')
    oci.assignBlind('movFlag_DPtr',                 category = 'transfer',     description = 'Set flag based on last bit of where `dataPtr` points')
    
    
    # Arithmetic & Logic
    
    oci.assignBlind('reset_Dur',        { 'n':2 },  category = 'arithmetic',   description = 'Resets duration of given note to default value')
    oci.assignBlind('reset_Dur_b',      { 'n':1 },  category = 'arithmetic',   description = 'Resets duration of given note to default value')
    oci.assignBlind('reset_Pitch',      { 'n':2 },  category = 'arithmetic',   description = 'Resets pitch of given note to default value')
    oci.assignBlind('reset_Pitch_b',    { 'n':1 },  category = 'arithmetic',   description = 'Resets pitch of given note to default value')
    
    oci.assignBlind('inc_DurDbl',       { 'o':1, 'n':2 },  category = 'arithmetic',   description = 'Doubles duration of note (unless max)')
    oci.assignBlind('inc_DurDbl_b',     { 'o':1, 'n':1 },  category = 'arithmetic',   description = 'Doubles duration of note (unless max)')
    oci.assignBlind('inc_DurDot',       { 'o':1, 'n':2 },  category = 'arithmetic',   description = 'Adds/removes dotted duration of note')
    oci.assignBlind('inc_DurDot_b',     { 'o':1, 'n':1 },  category = 'arithmetic',   description = 'Adds/removes dotted duration of note')
    oci.assignBlind('dec_DurHalve',     { 'o':1, 'n':2 },  category = 'arithmetic',   description = 'Halves duration of note (unless 1)')
    oci.assignBlind('dec_DurHalve_b',   { 'o':1, 'n':1 },  category = 'arithmetic',   description = 'Halves duration of note (unless 1)')
    
    oci.assignBlind('inc_Pitch2',       { 'o':1, 'n':2 },  category = 'arithmetic',   description = 'Increments pitch of note by second (major or minor depending on key)')
    oci.assignBlind('inc_Pitch2_b',     { 'o':1, 'n':1 },  category = 'arithmetic',   description = 'Increments pitch of note by second (major or minor depending on key)')
    oci.assignBlind('inc_Pitch3',       { 'o':1, 'n':2 },  category = 'arithmetic',   description = 'Increments pitch of note by third (major or minor depending on key)')
    oci.assignBlind('inc_Pitch3_b',     { 'o':1, 'n':1 },  category = 'arithmetic',   description = 'Increments pitch of note by third (major or minor depending on key)')
    oci.assignBlind('inc_Pitch4',       { 'o':1, 'n':2 },  category = 'arithmetic',   description = 'Increments pitch of note by perfect fourth')
    oci.assignBlind('inc_Pitch4_b',     { 'o':1, 'n':1 },  category = 'arithmetic',   description = 'Increments pitch of note by perfect fourth')
    oci.assignBlind('inc_Pitch5',       { 'o':1, 'n':2 },  category = 'arithmetic',   description = 'Increments pitch of note by perfect fifth')
    oci.assignBlind('inc_Pitch5_b',     { 'o':1, 'n':1 },  category = 'arithmetic',   description = 'Increments pitch of note by perfect fifth')
    oci.assignBlind('inc_Pitch8',       { 'o':1, 'n':2 },  category = 'arithmetic',   description = 'Increments pitch of note by octave')
    oci.assignBlind('inc_Pitch8_b',     { 'o':1, 'n':1 },  category = 'arithmetic',   description = 'Increments pitch of note by octave')

    oci.assignBlind('dec_Pitch2',       { 'o':1, 'n':2 },  category = 'arithmetic',   description = 'Decrements pitch of note by second (major or minor depending on key)')
    oci.assignBlind('dec_Pitch2_b',     { 'o':1, 'n':1 },  category = 'arithmetic',   description = 'Decrements pitch of note by second (major or minor depending on key)')
    oci.assignBlind('dec_Pitch3',       { 'o':1, 'n':2 },  category = 'arithmetic',   description = 'Decrements pitch of note by third (major or minor depending on key)')
    oci.assignBlind('dec_Pitch3_b',     { 'o':1, 'n':1 },  category = 'arithmetic',   description = 'Decrements pitch of note by third (major or minor depending on key)')
    oci.assignBlind('dec_Pitch4',       { 'o':1, 'n':2 },  category = 'arithmetic',   description = 'Decrements pitch of note by perfect fourth')
    oci.assignBlind('dec_Pitch4_b',     { 'o':1, 'n':1 },  category = 'arithmetic',   description = 'Decrements pitch of note by perfect fourth')
    oci.assignBlind('dec_Pitch5',       { 'o':1, 'n':2 },  category = 'arithmetic',   description = 'Decrements pitch of note by perfect fifth')
    oci.assignBlind('dec_Pitch5_b',     { 'o':1, 'n':1 },  category = 'arithmetic',   description = 'Decrements pitch of note by perfect fifth')
    oci.assignBlind('dec_Pitch8',       { 'o':1, 'n':2 },  category = 'arithmetic',   description = 'Decrements pitch of note by octave')
    oci.assignBlind('dec_Pitch8_b',     { 'o':1, 'n':1 },  category = 'arithmetic',   description = 'Decrements pitch of note by octave')
    
    oci.assignBlind('setFlag',                      category = 'arithmetic',   description = 'Set flag to true')
    oci.assignBlind('unsetFlag',                    category = 'arithmetic',   description = 'Set flag to false')
    oci.assignBlind('invertFlag',                   category = 'arithmetic',   description = 'Invert value of flag')
    
    oci.assignBlind('inc_D',            { 'd':1 },  category = 'arithmetic',   description = 'Increments value of `dataPtr`')
    oci.assignBlind('dec_D',            { 'd':1 },  category = 'arithmetic',   description = 'Decrements value of `dataPtr`')
    
    
    # Branching & jumping
    
    oci.assignBlind('jmpD',                         category = 'branching',    description = 'Absolute jumps to value of `dataPtr`')
    oci.assignBlind('sjmpNext',                     category = 'branching',    description = 'Short jumps program counter with value of next byte')
    oci.assignBlind('jmpNext',                      category = 'branching',    description = 'Absolute jumps to memory address given by next 2 bytes')
    oci.assignBlind('sjmpDH',                       category = 'branching',    description = 'Short jumps program counter with value of high byte of `dataPtr`')
    oci.assignBlind('sjmpDL',                       category = 'branching',    description = 'Short jumps program counter with value of low byte of `dataPtr`')
    
    # Conditional Jump
    
    oci.assignBlind('jnf_D',                        category = 'branching',    description = '`jmpD` if flag is not set')
    oci.assignBlind('jf_D',                         category = 'branching',    description = '`jmpD` if flag is set')
    oci.assignBlind('sjnf_Next',                    category = 'branching',    description = '`sjmpNext` if flag is not set')
    oci.assignBlind('sjf_Next',                     category = 'branching',    description = '`sjmpNext` if flag is set')
    oci.assignBlind('jnf_Next',                     category = 'branching',    description = '`jmpNext` if flag is not set')
    oci.assignBlind('jf_Next',                      category = 'branching',    description = '`jmpNext` if flag is set')
    oci.assignBlind('sjnf_DH',                      category = 'branching',    description = '`sjmpDH` if flag is not set')
    oci.assignBlind('sjf_DH',                       category = 'branching',    description = '`sjmpDH` if flag is set')
    oci.assignBlind('sjnf_DL',                      category = 'branching',    description = '`sjmpDL` if flag is not set')
    oci.assignBlind('sjf_DL',                       category = 'branching',    description = '`sjmpDL` if flag is set')
    
    # Output & Halt
    
    oci.assignBlind('outNote',          { 'n':2 },   category = 'output',       description = 'Outputs the value of a note')
    oci.assignBlind('outNote_b',        { 'n':1 },   category = 'output',       description = 'Outputs the value of a note')
    
    print oci
    oci.fillWithEmpties()
    oci.compress()
    return oci
    
def renderDslOci():
    oci = buildDslOci()
    oci.renderDefault()
    

if __name__ == "__main__":
    renderDslOci()
