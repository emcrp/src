'''
The ultimate RISC would be the one instruction computer which just has
one "Subtract and Branch if Not Zero" instruction:

SBNZ AddrA, AddrB, AddrC, AddrD

Subtract value at mem[AddrB] from value at mem[AddrA], store result
into mem[AddrC] then, if result wasn't zero, branch to AddrD.

We would have to add an OUT instruction too unless we were to memory
map an address as the output.
'''
from ocigen.generator import OpCodeInterpreterModel


def buildSbnzOiscOci(harvard):
    if harvard:
        name = "SbnzOiscHarvard"
        description = 'One-instruction set computer (OISC) using SBNZ and the Harvard architecture' 
    else:
        name = "SbnzOisc"
        description = 'One-instruction set computer (OISC) using SBNZ and the von Neumann architecture'
    
    oci = OpCodeInterpreterModel(name = name, 
                                 numRegisters = 0,
                                 pOutput = 0.5,
                                 description = description)
    
    if harvard:
        oci.mem('rom', unitSize = 2, flexible = True, hasInstructions = True, description = 'Read-only memory for instructions')
        oci.mem('ram', unitSize = 2, flexible = True,                         description = 'Read/writable memory for data')
    else:
        oci.mem('ram', unitSize = 2, flexible = True, hasInstructions = True, description = 'Read/writable memory for both instructions and data')
    
    
    oci.assign('', 'sbnz', description = '`SBNZ AddrA, AddrB, AddrC, AddrD`: Subtract value at `ram[AddrB]` from value at `ram[AddrA]`; store result in `ram[AddrC]`; ' +
                                         'if result was not zero, branch to `AddrD`; if underflow occurred, output `ram[addrA]+ram[addrB]`.')
    
    return oci
    
        
def renderSbnzOiscOci():
    oci = buildSbnzOiscOci(True)
    oci.renderDefault()
    oci = buildSbnzOiscOci(False)
    oci.renderDefault()
    

if __name__ == '__main__':
    renderSbnzOiscOci()