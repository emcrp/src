#Simple stack based set
from ocigen.generator import OpCodeInterpreterModel


def buildStackOci(harvard):
    if harvard:
        name = "StackHarvard"
    else:
        name = "Stack"
    
    
    oci = OpCodeInterpreterModel(name, numRegisters = 0)
    
    if harvard:
        oci.mem('rom', unitSize = 2, flexible = True, hasInstructions = True,   description = 'Read-only memory for instructions')
        oci.mem('stack', unitSize = 2, flexible = True,                         description = 'Read/writable memory for stackdata')
    else:
        oci.mem('stack', unitSize = 2, flexible = True, hasInstructions = True, description = 'Read/writable memory for both instructions and stack data')
        
    oci.reg('sp', size = 2, description = 'Stack pointer which can address entire stack')
    
    #DUP: Duplicate the stack top. This is the only way to allocate stack space. 
    #mem[SP]->mem[SP+1]; SP+1->SP
    oci.assign('000', 'dup', description = 'Duplicate the stack top. This is the only way to allocate stack space. ')
    
    #ONE: Shift the stack top left one bit, shifting one into the least significant bit. 
    #mem[SP]<<1
    oci.assign('001', 'one', description = 'Shift the stack top left one bit, shifting one into the least significant bit. ')
    
    #ZERO: Shift the stack top left one bit, shifting zero into the least significant bit. 
    #mem[SP]<<0
    oci.assign('010', 'zero', description = 'Shift the stack top left one bit, shifting zero into the least significant bit.')
    
    #LOAD: Use the value on the stack top as a memory address; replace it with the contents of the referenced location. 
    #mem[mem[SP]]->mem[SP]
    oci.assign('011', 'load', description = 'Use the value on the stack top as a memory address; replace it with the contents of the referenced location. ')
    
    #POP: Store the value from the top of the stack in the memory location referenced by the second word on the stack; pop both. 
    #mem[SP]->mem[mem[SP-1]]; SP-2 -> SP
    oci.assign('100', 'pop', description = 'Store the value from the top of the stack in the memory location referenced by the second word on the stack; pop both. ')
    
    #SUB: Subtract the top value on the stack from the value below it, pop both and push the result. 
    #mem[SP-1] - mem[SP] -> mem[SP-1]; SP-1 -> SP
    oci.assign('101', 'sub', description = 'Subtract the top value on the stack from the value below it, pop both and push the result. ')
    
    #JPOS: If the word below the stack top is positive, jump to the word pointed to by the stack top. In any case, pop both. 
    #if mem[SP-1]>-1 then mem[SP]->PC; SP-2 -> SP
    oci.assign('110', 'jpos', description = 'If the word below the stack top is positive, jump to the word pointed to by the stack top. In any case, pop both. ')
    
    #OUT: write the value of the stack top to the output
    #mem[SP]->output
    oci.assign('111', 'out', category = 'output', description = 'Write the value of the stack top to the output')
    
    return oci


def renderStackOci():
    oci = buildStackOci(True)
    oci.renderDefault()
    oci = buildStackOci(False)
    oci.renderDefault()
    
    
if __name__ == '__main__':
    renderStackOci()