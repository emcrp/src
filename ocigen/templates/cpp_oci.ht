/**
 * WARNING
 * =======
 * This is a generated file that can be overwritten with regeneration.
 * It is an op-code interpreter for the op code state machine {{ name }}.
 */


#pragma once

#include <stdio.h>
#include <string.h>

/**
 * Class for op-code interpreter {{ name }}
 * Description: {{ description | clean }}
 */
class {{ name }}Oci
{
public:

  {{ name }}Oci({% for memName in flexibleMems.keys %}unsigned int {{ memName }}Size, {% endfor %}{% for propName, prop in props.items %}{{ prop.size | cppType }} {{ propName }}, {% endfor %}
                unsigned int maxCommands, unsigned int maxOutputs, bool haltAllowed);
  ~{{ name }}Oci();


  // ===========================
  // externally callable methods
  // ===========================

  /**
   * Sets condition of VM using a genetic string (stored in inp).
   */
  void setFromGeneticString(unsigned char* inp, int inp_size);

  /**
   * Interpret RAM commands.
   * Reads bytes from where program counter points, and executes the mapped instructions.
   * Stops if halt flag set or if a maximum number of commands/outputs reached.
   */
  void interpret();{% if not single %}

  /**
   * Interpret upcoming command.
   * Reads a byte from memory and bit-parses it to find the correct instruction to call.
   */
  void interpretNext();{% endif %}
  
  /**
   * Interpret n RAM commands.
   * Doesn't stop in either case the interpret() method would.
   * Use for performance testing.
   */
  void interpretN(unsigned int n);

  
  // =========
  // constants
  // =========
  
  static const unsigned int counterSize = {{ counterSize }};
  static const unsigned int numRegisters = {{ numRegisters }};
  static const unsigned int numCategories = {{ numCategories }};
  
  
  // ===============
  // getters-setters
  // ===============

  const unsigned int geneticStringSize();
  
  {{ counterSize | cppType }} counter();
  void setCounter({{ counterSize | cppType }} counter);{% for propName, prop in props.items %}
  {{ prop.size | cppType }} {{ propName }}();
  void set{{ propName | capFirst}}({{ prop.size | cppType }} {{ propName }});{% endfor %}{% if numRegisters != 0 %}
  
  void registers(unsigned char* outp, int outp_size);
  unsigned char registerAt(unsigned short index);
  void setRegisters(unsigned char* inp, int inp_size);
  void setRegister(unsigned short index, unsigned char value);{% endif %}
  {% for regName, reg in regs.items %}
  {{ reg.size | cppType }} {{ regName }}();
  void set{{ regName | capFirst }}({{ reg.size | cppType }} {{ regName }});{% endfor %}{% for memName, mem in flexibleMems.items %}

  void {{ memName }}({{ mem.unitSize | cppType }}* outp, int outp_size);
  {{ mem.unitSize | cppType }} {{ memName }}At(unsigned short index);
  void set{{ memName | capFirst }}({{ mem.unitSize | cppType }}* inp, int inp_size);
  void set{{ memName | capFirst }}At(unsigned short index, {{ mem.unitSize | cppType }} value);{% endfor %}{% for memName, mem in fixedMems.items %}

  void {{ memName }}({{ mem.unitSize | cppType }}* outp, int outp_size);
  {{ mem.unitSize | cppType }} {{ memName }}At(unsigned short index);
  void set{{ memName | capFirst }}({{ mem.unitSize | cppType }}* inp, int inp_size);
  void set{{ memName | capFirst }}At(unsigned short index, {{ mem.unitSize | cppType }} value);{% endfor %}

  void output(unsigned char* outp, int outp_size);
  unsigned char outputAt(unsigned short index);
  void setOutput(unsigned char* inp, int inp_size);
  void setOutputAt(unsigned short index, unsigned char value);
  unsigned int outputPtr();
  void setOutputPtr(unsigned int outputPtr);
  
  bool debug();
  void setDebug(bool debug);
  bool countOccurrences();
  void setCountOccurrences(bool countOccurrences);
  bool countTouched();
  void setCountTouched(bool countTouched);
  int numTouched();
  void setNumTouched(int numTouched);
  
  void occurrences(unsigned int* outp, int outp_size);
  unsigned int occurrencesAt(unsigned short index);
  void setOccurrences(unsigned int* inp, int inp_size);
  void setOccurrencesAt(unsigned short index, unsigned int value);
  
  void touched(unsigned short* outp, int outp_size);
  unsigned short touchedAt(unsigned short index);
  void setTouched(unsigned short* inp, int inp_size);
  void setTouchedAt(unsigned short index, unsigned short value);

  
  // ===================================
  // assigned states - main instructions
  // ===================================
  {% for state in assignedNodes %}{% if state.instructionName != "doNothing" %}
  void {{ state.instructionName }}({% for paramName in state.parameters.keys %}{% ifnotequal forloop.counter0 0 %}, {% endifnotequal %}unsigned char {{ paramName }}{% endfor %}); // {{ state.opCodePrefix }} - {{ state.description | clean }}{% endif %}{% endfor %}


private:

  // =======
  // members
  // =======

  unsigned int _geneticStringSize;
    
  bool _haltFlag;
  unsigned int _maxCommands;
  unsigned int _maxOutputs;
  bool _haltAllowed;

  {{ counterSize | cppType }} _counter; // program counter{% for propName, prop in props.items %}
  {{ prop.size | cppType }} _{{ propName }};{% endfor %}{% if numRegisters != 0 %}
  // registers
  unsigned char* _registers; // general-purpose registers{% endif %}
  // additional registers{% for regName, reg in regs.items %}
  {{ reg.size | cppType }} _{{ regName }};{% endfor %}
  
  // flexible memory segments{% for memName, mem in flexibleMems.items %}
  {{ mem.unitSize | cppType }}* _{{ memName }};
  unsigned int _{{ memName }}Size;
  unsigned int _{{ memName }}Mask;{% endfor %}
  
  // fixed memory segments{% for memName, mem in fixedMems.items %}
  {{ mem.unitSize | cppType }}* _{{ memName }};{% endfor %}
  
  unsigned char* _output; // output memory block
  unsigned int _outputPtr; // output write pointer
  unsigned int _outputAllocated;

  bool _debug;
  bool _countOccurrences;
  unsigned int* _occurrences; // count occurrences of command types in debug mode
  bool _countTouched;
  int _numTouched;
  unsigned short* _touched; // mark touched bytes in the ram


  // ===========================
  // private interpreter methods
  // ===========================

  // =================================================
  // further interpret pending states in state machine
  // =================================================
  {% for state in pendingNodes %}{% ifnotequal state root %}
  void _interpret_{{ state.opCodePrefix }}(unsigned char cmd);{% endifnotequal %}{% endfor %}


  // =====================
  // extra private methods
  // =====================

  /**
   * Sets halt flag to true.
   * Only stops execution if it was called with interpret().
   */
  void _halt();

  /**
   * Outputs byte to output array and increases output write pointer.
   */
  void _out(unsigned char byte);
  void _out(unsigned char* bytes, unsigned short pos, unsigned char num);
  void _out(unsigned char* bytes, unsigned short pos, unsigned char num, unsigned int mask);
  

  /**
   * If allocated output size has been reached, extends and copies output array.
   * TODO: optimize using paging?
   */
  void _checkAndExtendOutput(unsigned char num = 1);
  
  /**
   * "Touch" byte in RAM (increase its heat) and return reference to it.
   */
  {{ mainMem.unitSize | cppType }}& _touch(unsigned int pos);
  
  /**
   * Reset counters for occurrences.
   */
  void _resetOccurrences();
  
  /**
   * Reset counters for how many bytes are touched.
   */
  void _resetTouched();
};
