/**
 * WARNING
 * =======
 * This is a generated file that can be overwritten with regeneration.
 * It is an op-code interpreter for the op code state machine {{ name }}.
 */


#include "{{ shortName }}.h"

#define ALLOCATION_UNIT {{ allocationUnit }}


{{ name }}Oci::{{ name }}Oci({% for memName in flexibleMems.keys %}unsigned int {{ memName }}Size, {% endfor %}{% for propName, prop in props.items %}{{ prop.size | cppType }} {{ propName }}, {% endfor %}unsigned int maxCommands, unsigned int maxOutputs, bool haltAllowed) :
	{% for memName in flexibleMems.keys %}_{{ memName }}Size({{ memName }}Size), _{{ memName }}Mask({{ memName }}Size-1), {% endfor %}{% for propName in props.keys %}_{{ propName }}({{ propName }}), {% endfor %}
    _haltFlag(false), _maxCommands(maxCommands), _maxOutputs(maxOutputs), _haltAllowed(haltAllowed),
    _counter(0), _outputPtr(0), _outputAllocated(ALLOCATION_UNIT), _debug(false),
    _countOccurrences(false), _countTouched(false), _numTouched(0)
{{% if numRegisters != 0 %}
  _registers = new unsigned char[numRegisters];
  {% endif %}{% for memName, mem in flexibleMems.items %}
  _{{ memName }} = new {{ mem.unitSize | cppType }}[_{{ memName }}Size];{% endfor %}
  {% for memName, mem in fixedMems.items %}
  _{{ memName }} = new {{ mem.unitSize | cppType }}[{{ mem.size }}];{% endfor %}
  _touched = new unsigned short[_{{ mainMem.name }}Size];
  
  _output = new unsigned char[_outputAllocated];

  _occurrences = new unsigned int[numCategories];
  
  _geneticStringSize = {{ rawGeneticStringSize }}{% for memName, mem in flexibleMems.items %} + _{{ memName }}Size{% if mem.unitSize != 1 %} * {{ mem.unitSize }}{% endif %}{% endfor %};
}


{{ name }}Oci::~{{ name }}Oci()
{{% if numRegisters != 0 %}
  delete [] _registers;{% endif %}{% for memName in flexibleMems.keys %}
  delete [] _{{ memName }};{% endfor %}{% for memName in fixedMems.keys %}
  delete [] _{{ memName }};{% endfor %}
  delete [] _touched;
  delete [] _output;
  delete [] _occurrences;
}


void {{ name }}Oci::_checkAndExtendOutput(unsigned char num)
{
  if (_outputPtr + num > _outputAllocated)
  {
    // TODO implement paging for efficiency
    _outputAllocated += ALLOCATION_UNIT;
    unsigned char* newOutput = new unsigned char[_outputAllocated];

    output(newOutput, _outputPtr);

    delete [] _output;
    _output = newOutput;
  }
}


void {{ name }}Oci::setFromGeneticString(unsigned char* geneticString, int geneticString_size)
{
  _haltFlag = false;
  _outputPtr = 0;
  if (_countOccurrences)
  {
    _resetOccurrences();
  }
  if (_countTouched)
  {
    _resetTouched();
  }

  unsigned int genStrReadPtr = 0;
  unsigned int i;

  // set program counter
  _counter = (({{ counterSize | cppType }} *) &(geneticString[genStrReadPtr]))[0];
  _counter &= _{{ mainMem.name }}Mask;
  genStrReadPtr += {{ counterSize }};{% if numRegisters != 0 %}

  // set main registers
  for (i = 0; i < numRegisters; ++i)
  {
    _registers[i] = geneticString[genStrReadPtr++];
  }{% endif %}{% for regName, reg in regs.items %}

  // set register {{ regName }}
  _{{ regName }} = (({{ reg.size | cppType }} *) &(geneticString[genStrReadPtr]))[0];
  genStrReadPtr += {{ reg.size }};{% endfor %}{% for memName, mem in flexibleMems.items %}

  // set memory {{ memName }}
  for (i = 0; i < _{{ memName }}Size; ++i)
  {
    _{{ memName }}[i] = (({{ mem.unitSize | cppType }} *) &(geneticString[genStrReadPtr]))[i];
  }
  genStrReadPtr += _{{ memName }}Size{% if mem.unitSize != 1 %} * {{ mem.unitSize }}{% endif %};{% endfor %}{% for memName, mem in fixedMems.items %}

  // set memory {{ memName }}
  for (i = 0; i < {{ mem.size }}; ++i)
  {
    _{{ memName }}[i] = (({{ mem.unitSize | cppType }} *) &(geneticString[genStrReadPtr]))[i];
  }
  genStrReadPtr += {{ mem.size }}{% if mem.unitSize != 1 %} * {{ mem.unitSize }}{% endif %};{% endfor %}
}


void {{ name }}Oci::interpret()
{
  _haltFlag = false;
  unsigned int numCommands = 0;
  //_counter = 0;
  _outputPtr = 0;

  while ((!_haltFlag || !_haltAllowed)
       && numCommands < _maxCommands
       && _outputPtr < _maxOutputs)
  {{% if not single %}
    interpretNext();{% else %}
    {{root.instructionName}}({{ root.parameters.keys | join:", " }});{% endif %}
    numCommands++;
  }
}


void {{ name }}Oci::interpretN(unsigned int n)
{
  for (unsigned int i = 0; i < n; ++i)
  {{% if not single %}
    interpretNext();{% else %}
    {{root.instructionName}}({{ root.parameters.keys | join:", " }});{% endif %}
  }
}{% for state in pendingNodes %}


void {{ name }}Oci::{% ifequal state root %}interpretNext(){% else %}_interpret_{{ state.opCodePrefix }}(unsigned char cmd){% endifequal %}
{{% ifequal state root %}
  _touch(_counter);
  unsigned char cmd = _{{ mainMem.name }}[_counter & _{{ mainMem.name }}Mask];
  if (_debug) printf("_{{ mainMem.name }}[%d]=%d => ", _counter & _{{ mainMem.name }}Mask, cmd);
  _counter = (_counter + 1) & _{{ mainMem.name }}Mask;
  {% endifequal %}
  unsigned char i = cmd >> {{ state.bitDifferenceToChildren | compl }};
  cmd = cmd << {{ state.bitDifferenceToChildren }};
  
  switch (i)
  {{% for child in state.children %}
  case {{ forloop.counter0 }}:
    {{% if child.pending %}
      _interpret_{{ child.opCodePrefix }}(cmd);
      break;
    {% else %}{% for paramName, paramBits in child.parameters.items %}
      unsigned char {{ paramName }} = cmd >> {{ paramBits | compl }}; cmd = cmd << {{ paramBits }};{% endfor %}{% if child.instructionName != "doNothing" %}
      if (_debug) printf("{{child.instructionName}}({{ child.parameters.keys | printfTimes }})\n"{% for paramName in child.parameters.keys %}, {{ paramName }}{% endfor %});
      if (_countOccurrences) _occurrences[{{ child.categoryIdx }}]++;
      {{child.instructionName}}({{ child.parameters.keys | join:", " }});{% else %}
      // do nothing...{% endif %}
      break;
    {% endif %}}{% endfor %}
  }
}{% endfor %}


void {{ name }}Oci::_halt()
{
  _haltFlag = true;
}


void {{ name }}Oci::_out(unsigned char byte)
{
  _checkAndExtendOutput();
  _output[_outputPtr++] = byte;
}


void {{ name }}Oci::_out(unsigned char* bytes, unsigned short pos, unsigned char num)
{
  _out(bytes, pos, num, _{{ mainMem.name }}Mask);
}


void {{ name }}Oci::_out(unsigned char* bytes, unsigned short pos, unsigned char num, unsigned int mask)
{
  _checkAndExtendOutput(num);
  for (int i = pos; i < pos + num; ++i)
  {
    _output[_outputPtr++] = bytes[i & mask];
  }
}


{{ mainMem.unitSize | cppType }}& {{ name }}Oci::_touch(unsigned int pos)
{
  if (_countTouched)
  {
  	if (_debug) printf("touch(%d)\n", pos);
    if (!_touched[pos & _{{ mainMem.name }}Mask]) 
    {
      _numTouched++;
    }
    _touched[pos & _{{ mainMem.name }}Mask]++;
  }
  return _{{ mainMem.name }}[pos & _{{ mainMem.name }}Mask];
}


void {{ name }}Oci::_resetOccurrences()
{
  memset(_occurrences, 0, numCategories * sizeof(unsigned int));
}

void {{ name }}Oci::_resetTouched()
{
  _numTouched = 0;
  memset(_touched, 0, _{{ mainMem.name }}Size * sizeof(unsigned short));
}

// ===============
// getters-setters
// ===============

const unsigned int {{ name }}Oci::geneticStringSize() { return _geneticStringSize; }

{{ counterSize | cppType }} {{ name }}Oci::counter() { return _counter; }
void {{ name }}Oci::setCounter({{ counterSize | cppType }} counter) { _counter = counter; }
unsigned int {{ name }}Oci::outputPtr() { return _outputPtr; }
void {{ name }}Oci::setOutputPtr(unsigned int outputPtr) { _outputPtr = outputPtr; }{% for propName, prop in props.items %}
{{ prop.size | cppType }} {{ name }}Oci::{{ propName }}() { return _{{ propName }}; }
void {{ name }}Oci::set{{ propName | capFirst }}({{ prop.size | cppType }} {{ propName }}) { _{{ propName }} = {{ propName }}; }{% endfor %}{% for regName, reg in regs.items %}
{{ reg.size | cppType }} {{ name }}Oci::{{ regName }}() { return _{{ regName }}; }
void {{ name }}Oci::set{{ regName | capFirst }}({{ reg.size | cppType }} {{ regName }}) { _{{ regName }} = {{ regName }}; }{% endfor %}

void {{ name }}Oci::output(unsigned char* out_output, int out_output_size) { memcpy(out_output, _output, out_output_size); }
unsigned char {{ name }}Oci::outputAt(unsigned short index) { return _output[index]; }
void {{ name }}Oci::setOutput(unsigned char* in_output, int in_output_size) { memcpy(_output, in_output, in_output_size); }
void {{ name }}Oci::setOutputAt(unsigned short index, unsigned char value) { _output[index] = value; }{% if numRegisters != 0 %}

void {{ name }}Oci::registers(unsigned char* out_registers, int out_registers_size) { memcpy(out_registers, _registers, out_registers_size); }
unsigned char {{ name }}Oci::registerAt(unsigned short index) { return _registers[index]; }
void {{ name }}Oci::setRegisters(unsigned char* in_registers, int in_registers_size) { memcpy(_registers, in_registers, in_registers_size); }
void {{ name }}Oci::setRegister(unsigned short index, unsigned char value) { _registers[index] = value; }{% endif %}{% for memName, mem in flexibleMems.items %}

void {{ name }}Oci::{{ memName }}({{ mem.unitSize | cppType }}* out_{{ memName }}, int out_{{ memName }}_size) { memcpy(out_{{ memName }}, _{{ memName }}, out_{{ memName }}_size * sizeof({{ mem.unitSize | cppType }})); }
{{ mem.unitSize | cppType }} {{ name }}Oci::{{ memName }}At(unsigned short index) { return _{{ memName }}[index & _{{ memName }}Mask]; }
void {{ name }}Oci::set{{ memName | capFirst }}({{ mem.unitSize | cppType }}* in_{{ memName }}, int in_{{ memName }}_size) { memcpy(_{{ memName }}, in_{{ memName }}, in_{{ memName }}_size * sizeof({{ mem.unitSize | cppType }})); }
void {{ name }}Oci::set{{ memName | capFirst }}At(unsigned short index, {{ mem.unitSize | cppType }} value) { _{{ memName }}[index & _{{ memName }}Mask] = value; }{% endfor %}{% for memName, mem in fixedMems.items %}

void {{ name }}Oci::{{ memName }}({{ mem.unitSize | cppType }}* out_{{ memName }}, int out_{{ memName }}_size) { memcpy(out_{{ memName }}, _{{ memName }}, out_{{ memName }}_size); }
{{ mem.unitSize | cppType }} {{ name }}Oci::{{ memName }}At(unsigned short index) { return _{{ memName }}[index]; }
void {{ name }}Oci::set{{ memName | capFirst }}({{ mem.unitSize | cppType }}* in_{{ memName }}, int in_{{ memName }}_size) { memcpy(_{{ memName }}, in_{{ memName }}, in_{{ memName }}_size); }
void {{ name }}Oci::set{{ memName | capFirst }}At(unsigned short index, {{ mem.unitSize | cppType }} value) { _{{ memName }}[index] = value; }{% endfor %}

bool {{ name }}Oci::debug() { return _debug; }
void {{ name }}Oci::setDebug(bool debug) { _debug = debug; }
bool {{ name }}Oci::countOccurrences() { return _countOccurrences; }
void {{ name }}Oci::setCountOccurrences(bool countOccurrences) { _countOccurrences = countOccurrences; }
bool {{ name }}Oci::countTouched() { return _countTouched; }
void {{ name }}Oci::setCountTouched(bool countTouched) { _countTouched = countTouched; }
int  {{ name }}Oci::numTouched() { return _numTouched; }
void {{ name }}Oci::setNumTouched(int numTouched) { _numTouched = numTouched; }

void {{ name }}Oci::occurrences(unsigned int* out_occurrences, int out_occurrences_size) { memcpy(out_occurrences, _occurrences, out_occurrences_size * sizeof(unsigned int)); }
unsigned int {{ name }}Oci::occurrencesAt(unsigned short index) { return _occurrences[index]; }
void {{ name }}Oci::setOccurrences(unsigned int* in_occurrences, int in_occurrences_size) { memcpy(_occurrences, in_occurrences, in_occurrences_size * sizeof(unsigned int)); }
void {{ name }}Oci::setOccurrencesAt(unsigned short index, unsigned int value) { _occurrences[index] = value; }

void {{ name }}Oci::touched(unsigned short* out_touched, int out_touched_size) { memcpy(out_touched, _touched, out_touched_size * sizeof(unsigned short)); }
unsigned short {{ name }}Oci::touchedAt(unsigned short index) { return _touched[index]; }
void {{ name }}Oci::setTouched(unsigned short* in_touched, int in_touched_size) { memcpy(_touched, in_touched, in_touched_size * sizeof(unsigned short)); }
void {{ name }}Oci::setTouchedAt(unsigned short index, unsigned short value) { _touched[index] = value; }
