'''
Unit tests for C++ EMC OCI
'''
import unittest

from emcpp.wrapper.oci.{{ shortName }} import {{ name }}OciWrapper


class Test{{ name }}Oci(unittest.TestCase):
    
    def setUp(self):
        self.oci = {{ name }}OciWrapper(2 ** 8, 2 ** 8, {% for propName, prop in props.items %}{{ prop.size | pyDefault }}, {% endfor %}True)
    
{% for state in assignedNodes %}{% if state.instructionName != "doNothing" %}{{ state | pyTestMethod }}
    
{% endif %}{% endfor %}
    
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()