'''
WARNING
=======
This is a generated file that can be overwritten with regeneration.
It is a wrapper for the C++ op-code interpreter for the state machine {{ name }}.
'''

from collections import OrderedDict
from numpy import float32

from emcpp.swig.emcpp import {{ name }}Oci


class {{ name }}OciWrapper(object):
    '''
    Wrapper for C++ op-code interpreter {{ name }}
    Description: {{ description | clean }}
    '''
    
    categoryCommands = OrderedDict([ {% for category, numCommands in categoryCommands.items %}{% if not forloop.first %},{% endif %}
      ('{{ category }}', {{ numCommands }}){% endfor %}
    ])
    
    
    def __init__(self, memSizeInBytes, expectedOutputs{% for propName, prop in props.items %}, {{ propName }} = {{ prop.size | pyDefault }}{% endfor %},
                 haltAllowed = False, maxCommandsRatio = 2.0, **kwargs):
        self.memSizeInBytes = memSizeInBytes
        {% for memName, mem in flexibleMems.items %}
        self.{{ memName }}Size = self.memSizeInBytes / ({{ mem.unitSize }} * {{ flexibleMems|length }}) # unitsize={{ mem.unitSize }},numMems={{ flexibleMems|length }}{% endfor %}
        
        self.expectedOutputs = expectedOutputs
        self.haltAllowed = haltAllowed
        self.maxCommandsRatio = maxCommandsRatio
        
        inversePOutput = {% if not pOutput %}256.0 / float32(self.categoryCommands['output']){% else %}1.0 / {{ pOutput }}{% endif %}
        self.maxCommands = int(float32(expectedOutputs) * maxCommandsRatio * inversePOutput)
        
        self.native = {{ name }}Oci({% for memName in flexibleMems.keys %}self.{{ memName }}Size, {% endfor %}{% for propName in props.keys %}{{ propName }}, {% endfor %}self.maxCommands, self.expectedOutputs, self.haltAllowed)
        
    
    def geneticStringToOutput(self, data):
        self.native.setFromGeneticString(data)
        self.native.interpret()
        return self.output()
    
    
    def geneticStringToOccurrences(self, data):
        self.setCountOccurrences(True)
        self.native.setFromGeneticString(data)
        self.native.interpret()
        return self.occurrences()
    
    
    def geneticStringToTouched(self, data):
        self.setCountTouched(True)
        self.native.setFromGeneticString(data)
        self.native.interpret()
        return (self.numTouched(), self.touched())
    
    
    @property
    def geneticStringSize(self): return self.native.geneticStringSize()
    
    def counter(self): return self.native.counter()
    def setCounter(self, counter): self.native.setCounter(counter)
    def outputPtr(self): return self.native.outputPtr()
    def setOutputPtr(self, outputPtr): self.native.setOutputPtr(outputPtr){% for propName in props.keys %}
    def {{ propName }}(self): return self.native.{{ propName }}()
    def set{{ propName | capFirst }}(self, {{ propName }}): self.native.set{{ propName | capFirst }}({{ propName }}){% endfor %}{% for regName in regs.keys %}
    def {{ regName }}(self): return self.native.{{ regName }}()
    def set{{ regName | capFirst }}(self, {{ regName }}): self.native.set{{ regName | capFirst }}({{ regName }}){% endfor %}
    

    def output(self): return self.native.output(self.outputPtr())
    def outputAt(self, index): return self.native.outputAt(index)
    def setOutput(self, output): self.native.setOutput(output)
    def setOutputAt(self, index, value): self.native.setOutputAt(index, value){% if numRegisters != 0 %}

    def registers(self): return self.native.registers({{ numRegisters }})
    def register(self, index): return self.native.registerAt(index)
    def setRegisters(self, registers): self.native.setRegisters(registers)
    def setRegister(self, index, value): self.native.setRegister(index, value){% endif %}{% for memName in flexibleMems.keys %}
    
    def {{ memName }}(self): return self.native.{{ memName }}(self.{{ memName }}Size)
    def {{ memName }}At(self, index): return self.native.{{ memName }}At(index)
    def set{{ memName | capFirst }}(self, {{ memName }}): self.native.set{{ memName | capFirst }}({{ memName }})
    def set{{ memName | capFirst }}At(self, index, value): self.native.set{{ memName | capFirst }}At(index, value){% endfor %}{% for memName, mem in fixedMems.items %}
    
    def {{ memName }}(self): return self.native.{{ memName }}({{ mem.size }})
    def {{ memName }}At(self, index): return self.native.{{ memName }}At(index)
    def set{{ memName | capFirst }}(self, {{ memName }}): self.native.set{{ memName | capFirst }}({{ memName }})
    def set{{ memName | capFirst }}At(self, index, value): self.native.set{{ memName | capFirst }}At(index, value){% endfor %}
    
    def debug(self): return self.native.debug()
    def setDebug(self, debug): self.native.setDebug(debug)
    def countOccurrences(self): return self.native.countOccurrences()
    def setCountOccurrences(self, countOccurrences): self.native.setCountOccurrences(countOccurrences)
    def countTouched(self): return self.native.countTouched()
    def setCountTouched(self, countTouched): self.native.setCountTouched(countTouched)
    def numTouched(self): return self.native.numTouched()
    def setNumTouched(self, numTouched): self.native.setNumTouched(numTouched)
    
    def occurrences(self): return self.native.occurrences({{ numCategories }})
    def occurrencesAt(self, index): return self.native.occurrencesAt(index)
    def setOccurrences(self, occurrences): self.native.setOccurrences(occurrences)
    def setOccurrencesAt(self, index, value): self.native.setOccurrencesAt(index, value)
    
    def touched(self): return self.native.touched(self.{{ mainMem.name }}Size)
    def touchedAt(self, index): return self.native.touchedAt(index)
    def setTouched(self, occurrences): self.native.setTouched(occurrences)
    def setTouchedAt(self, index, value): self.native.setTouchedAt(index, value)
    
    
    def __getstate__(self):
        return (self.memSizeInBytes, {% for memName in flexibleMems.keys %}self.{{ memName }}Size, {% endfor %}{% for propName in props.keys %}self.{{ propName }}(), {% endfor %}self.expectedOutputs, self.haltAllowed, self.maxCommandsRatio, self.maxCommands)
    
    
    def __setstate__(self, state):
        self.memSizeInBytes, {% for memName in flexibleMems.keys %}self.{{ memName }}Size, {% endfor %}{% for propName in props.keys %}{{ propName }}, {% endfor %}self.expectedOutputs, self.haltAllowed, self.maxCommandsRatio, self.maxCommands = state
        self.native = {{ name }}Oci({% for memName in flexibleMems.keys %}self.{{ memName }}Size, {% endfor %}{% for propName in props.keys %}{{ propName }}, {% endfor %}self.maxCommands, self.expectedOutputs, self.haltAllowed)
        