from os.path import abspath, dirname

from ocigen.generator import OpCodeInterpreterModel


def buildTestOci():
    oci = OpCodeInterpreterModel("Test", 
                                 counterSize = 1,
                                 description = 'OCI for testing and demonstration purposes')
    
    oci.assign('00',    'a',                   category = 'first',   description = 'test method a')
    oci.assign('01',    'b', {'r1':3,},        category = 'first',   description = 'test method b')
    oci.assign('1000',  'c',                   category = 'first',   description = 'test method c')
    oci.assign('1001',  'd',                   category = 'first',   description = 'test method d')
    oci.assign('1010',  'e',                   category = 'output',  description = 'test method e')
    oci.assign('10110', 'f', {'r1':3},         category = 'output',  description = 'test method f')
    oci.assign('10111', 'g',                   category = 'output',  description = 'test method g')
    oci.assign('11',    'h', {'r1':3},         category = 'output',  description = 'test method h')
    
    oci.reg('dptr', size = 2)
    oci.reg('acc')
    
    oci.mem('ram', flexible = True, hasInstructions = True)
    oci.mem('stack', size = 16)
    
    oci.compress()
    return oci
    
    
def renderTestOci():
    oci = buildTestOci()
    oci.renderDefault()
    

if __name__ == "__main__":
    renderTestOci()
