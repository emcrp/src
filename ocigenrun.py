#!/usr/bin/env python

'''
Batch for generating the different OCIs
'''
from ocigen.testoci import renderTestOci
from ocigen.immediateoci import renderImmediateOci
from ocigen.indirectoci import renderIndirectOci
from ocigen.musicdsloci import renderDslOci
from ocigen.loadstoreoci import renderLoadStoreOci
from ocigen.stackoci import renderStackOci
from ocigen.sbnzoiscoci import renderSbnzOiscOci


if __name__ == '__main__':
    renderTestOci()
    renderImmediateOci()
    renderIndirectOci()
    renderDslOci()
    renderLoadStoreOci()
    renderStackOci()
    renderSbnzOiscOci()