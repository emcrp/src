#!/usr/bin/env python

'''
Parsing results, and saving mean/max grades.
'''

from numpy import array, float32, zeros, arange, mean, max, sum, std
from os import listdir, makedirs
from os.path import join, exists
from re import search
from time import time

from helper.numpyba import NumpyArrayBA
from helper.serializeutils import fromFile, toFile
import os
from subprocess import call
from results.maxmean import showResults


remoteResultsDir = '/home/emcrp/src/emc/output'
remoteFitnessDir = join(remoteResultsDir, 'fitness')
localResultsDir = '/home/csaba/src/emc/output'
localFitnessDir = join(localResultsDir, 'fitness')

numGens = 5001
numRuns = 20
numUnits = 500


def parseGrades():
    if not exists(remoteFitnessDir):
        makedirs(remoteFitnessDir)
    
    forceRewrite = False
    
    print "Assuming %d runs and %d generations" %(numRuns, numGens)
    
    
    allArchs = ['immediate', 'musicdsl']
    allPitchReprs = ['chromatic', 'shiftedchromatic', 'diatonic']
    
    grades = zeros((numRuns, numGens, 3), dtype=float32)
    lastGenGrades = zeros((numRuns, numUnits), dtype=float32)
    avgGrades = zeros((numGens, 3), dtype=float32)
        
    for archIdx, arch in enumerate(allArchs):
        for pitchReprIdx, pitchRepr in enumerate(allPitchReprs):
                
            testDir = join(remoteResultsDir, 'emc-gecco2019-%s-%s' %(arch, pitchRepr))
            gradesFile = join(remoteFitnessDir, 'emc-gecco2019-%s-%s-grades.bin' %(arch, pitchRepr))

            if not exists(testDir):
                print "Test run doesn't exist yet: arch=%s, pitchRepr=%s" %(arch, pitchRepr)
                print ''
                continue

            if exists(gradesFile) and not forceRewrite:
                print "Test run already completed: arch=%s, pitchRepr=%s" %(arch, pitchRepr)
                print ''
                continue
    
            timestamps = []
            runDirs = []
            
            print "Searching for test runs in %s (arch=%s, pitchRepr=%s)" %(testDir, arch, pitchRepr)
            st = time()
            
            for subDir in sorted(listdir(testDir)):
                match = search('emc_time([0-9\-]*)_rev.*', subDir)
                if match:
                    timestamps.append(match.group(1))
                    runDirs.append('%s/%s' %(testDir, subDir))
            
            
            for run, runDir in enumerate(runDirs):
                if run >= numRuns:
                    print 'WARNING: Number of runs exceeded. Omitting folder %s' %runDir
                    break
                
                sst = time()
                
                for fn in sorted(listdir(runDir)):
                    match = search('emc_grades([0-9]*)\-([0-9]*)\.bin', fn)
                    if match:
                        startGen = int(match.group(1))
                        endGen = int(match.group(2)) + 1
                        f = '%s/%s' %(runDir, fn)
                        #print fn, "=>", startGen, endGen
                        grades[run, startGen:endGen, :] = fromFile(f).data
                    
                print "  Read fitness values for run %s in %.3f seconds" %(timestamps[run], time() - sst)
                sst = time()
                
                
                lastGenAlg = fromFile("%s/emc_gen%05d.emc" %(runDir, numGens-1))
                lastGenGrades[run, :] = lastGenAlg.population.grades
                
                print "  Calculated mean/avg fitness values for run %s in %.3f seconds" %(timestamps[run], time() - sst)
                print ''
                
            
            print "Read all fitness values in %.3f seconds" %(time() - st)
            
            avgGrades[:, :] = mean(grades, axis = 0)
            
            print "Writing average values to files"
            st = time()
            
            toFile(NumpyArrayBA(data = grades),        '%s/emc-gecco2019-%s-%s-grades.bin'        %(remoteFitnessDir, arch, pitchRepr))
            toFile(NumpyArrayBA(data = lastGenGrades), '%s/emc-gecco2019-%s-%s-lastGenGrades.bin' %(remoteFitnessDir, arch, pitchRepr))
            toFile(NumpyArrayBA(data = avgGrades),     '%s/emc-gecco2019-%s-%s-avgGrades.bin'     %(remoteFitnessDir, arch, pitchRepr))
            
            print "Wrote all values in %.3f seconds" %(time() - st)
            

def downloadResults():
    '''
    If on local machine, download results.
    '''
    command = ["rsync", "-tvur", "emcrp@csabasulyok.gq:%s" %(remoteFitnessDir), localResultsDir]
    print "Calling command", command
    call(command)
    
    showResults(resultsDir=localFitnessDir,
                numGens=numGens,
                numRuns=numRuns,
                numUnits=numUnits)



if __name__ == '__main__':
    
    if 'DISPLAY' in os.environ:
        print 'Local machine detected'
        downloadResults()
    else:
        print 'Remote machine detected'
        parseGrades()
    
    