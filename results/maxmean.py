'''
Parsing mean/max results, plotting diagrams
'''

# from django.conf import settings
# from django.template.base import Template
# from django.template.context import Context
from matplotlib.pyplot import *
from numpy import *

from helper.serializeutils import fromFile

from statistical_significance import stat_test


def showResults(resultsDir = '/home/csaba/src/emc/output/fitness',
                numGens = 5001,
                numRuns = 20,
                numUnits = 500):
    

    rcParams['pdf.fonttype'] = 42
    
    allArchs = ['immediate', 'musicdsl']
    allPitchReprs = ['chromatic', 'shiftedchromatic', 'diatonic']
    
    markers = ['o', '*', 'v', 'd', '>', 'v', '<', '^', '+']
    colors = ['#a07060', '#7060a0', '#60a070']
    stdColors = ['#f0e0d0', '#e0d0f0', '#d0f0e0']
    linestyles = ['-', ':', '--', '-.']
    linewidths = [2,   4.0, 2.5,  2.5 ]
    
    archsSize = size(allArchs)
    pitchReprsSize = size(allPitchReprs)
    
    allGrades = zeros((archsSize, pitchReprsSize, numRuns, numGens, 3), dtype=float32)
    allAvgGrades = zeros((archsSize, pitchReprsSize, numGens, 3), dtype=float32)
    allLastGenGrades = zeros((archsSize, pitchReprsSize, numRuns, 3), dtype=float32)
    allAvgLastGenGrades = zeros((archsSize, pitchReprsSize, 3), dtype=float32)
    
    # READ RESULTS FILES
    
    for archIdx, arch in enumerate(allArchs):
        for pitchReprIdx, pitchRepr in enumerate(allPitchReprs):
                    
            prefix = 'emc-gecco2019-%s-%s' %(arch, pitchRepr)
            
            print 'Reading', prefix
            
            allGrades[archIdx, pitchReprIdx, :, :, :] = fromFile('%s/%s-grades.bin' %(resultsDir, prefix)).data
            allAvgGrades[archIdx, pitchReprIdx, :, :] = fromFile('%s/%s-avgGrades.bin' %(resultsDir, prefix)).data
    
     
    allLastGenGrades = allGrades[:, :, :, -1, :]
    allAvgLastGenGrades = allAvgGrades[:, :, -1, :]
    

    # print info
        
    for archIdx, arch in enumerate(allArchs):
        print ''
        for pitchReprIdx, pitchRepr in enumerate(allPitchReprs):
            print arch, pitchRepr, allAvgLastGenGrades[archIdx, pitchReprIdx, :]


    # Figure 1: mean/max by instruction set
    
    # interpolate values
    numTicks = 50
    set_printoptions(precision=3, suppress=True)
    origX = arange(numGens)
    interpX = ((numGens-1)**(1.0/(numTicks-1))) ** arange(numTicks)
    #print interpX
    
    allAvgGradesByArchIdx = mean(allAvgGrades, axis=1)

    semilogx(interpX, interp(interpX, origX, allAvgGradesByArchIdx[1, :, 0]), label='DSL Max',  basex=4, linestyle=linestyles[0], linewidth=linewidths[0])
    semilogx(interpX, interp(interpX, origX, allAvgGradesByArchIdx[1, :, 1]), label='DSL Mean', basex=4, linestyle=linestyles[1], linewidth=linewidths[1])
    semilogx(interpX, interp(interpX, origX, allAvgGradesByArchIdx[0, :, 0]), label='GP Max',   basex=4, linestyle=linestyles[2], linewidth=linewidths[2])
    semilogx(interpX, interp(interpX, origX, allAvgGradesByArchIdx[0, :, 1]), label='GP Mean',  basex=4, linestyle=linestyles[3], linewidth=linewidths[3])
     
    # t-test:
    print('='*50)
    print('DSL/GP Max t-test:')
    stat_test(allAvgGradesByArchIdx[1, :, 0], allAvgGradesByArchIdx[0, :, 0])
    print('DSL/GP Mean t-test:')
    stat_test(allAvgGradesByArchIdx[1, :, 1], allAvgGradesByArchIdx[0, :, 1])
    print('='*50)
     
    grid(True)
    for tick in gca().xaxis.get_major_ticks():
        tick.label.set_fontsize(16)
    for tick in gca().yaxis.get_major_ticks():
        tick.label.set_fontsize(16)
    ylim([0.0, 1.0])
    xlabel('Generation (log)', fontsize=20)
    ylabel('Fitness values', fontsize=20)
    legend(fontsize=16)
    tight_layout()
    #savefig('resultsdsl.pdf')
    #show()
    
    
    # Figure 2: bars for each last generation
    
    figure(figsize=(8, 4))
    y = array([1.05, 0.65, 0.25, -0.25, -0.65, -1.05])
    
    
    # t-tests:
    print('='*50)
    print('DSL/GP Chromatic t-test:')
    print('max:')
    stat_test(allLastGenGrades[1,0,:,0], allLastGenGrades[0,0,:,0])
    print('mean:')
    stat_test(allLastGenGrades[1,0,:,1], allLastGenGrades[0,0,:,1])
    print('DSL/GP Shifted Chromatic t-test:')
    print('max:')
    stat_test(allLastGenGrades[1,1,:,0], allLastGenGrades[0,1,:,0])
    print('mean:')
    stat_test(allLastGenGrades[1,1,:,1], allLastGenGrades[0,1,:,1])
    print('DSL/GP Diatonic t-test:')
    print('max:')
    stat_test(allLastGenGrades[1,2,:,0], allLastGenGrades[0,2,:,0])
    print('mean:')
    stat_test(allLastGenGrades[1,2,:,1], allLastGenGrades[0,2,:,1])
    print('='*50)
        
    barh(y, allAvgLastGenGrades[:, :, 0].flatten(),
         align='center', height=0.2, edgecolor='#505080', color='#c0c0f0', linewidth=2, linestyle='--')
    
    barh(y, allAvgLastGenGrades[:, :, 1].flatten(),
         align='center', height=0.3, edgecolor='#305030', color='#80a080', linewidth=2)
    
    
    yticks(y, ['GP Chromatic', 'GP Shifted Chromatic', 'GP Diatonic',
               'DSL Chromatic', 'DSL Shifted Chromatic', 'DSL Diatonic'])
    ylim([-1.3,1.3])
    xlim([0,1])
    
    xlabel('Grades', fontsize=20)
    
    for tick in gca().xaxis.get_major_ticks():
        tick.label.set_fontsize(18)
    for tick in gca().yaxis.get_major_ticks():
        tick.label.set_fontsize(18)
    
    grid(True)
    tight_layout()
    #savefig('resultslastgenbars.pdf')
    show()

    

if __name__ == '__main__':
    showResults()
#     showResults(resultsDir='/home/miafranc/zzz/2018-2019/munka/sulyok csaba/t-test/fitness')
    