'''
Randomly choose 10 MIDIs above 0.7 for display in report.
'''

from re import search

from numpy import *
from numpy.random.mtrand import randint, choice

from emc.integ.midi import readFromMidi
from emc.scripts.midicrop import cropModel
from os import listdir
from os.path import join, basename
from emc.scripts.lilypond import midi2pdf
from json import dumps, dump

if __name__ == '__main__':
    inputDir = '/home/csaba/src/emc/emcrp.gitlab.io/public/midi/gecco2019/'
    outputDir = '/home/csaba/src/emc/phd-preliminary/midi/random/'
    
    # number of MIDIs to choose
    N = 20
    # minimum grade of chosen MIDIs
    cutOffGrade = 0.0
    # length to crop the MIDIs randomly
    L = 96

    # parse MIDI folder - find MIDIs with good enough overall grade
    goodMidis = []
    
    archs = {'musicdsl':'Music DSL',
             'immediate':'Immediate' }
    pitchs = {'diatonic':'Diatonic',
              'chromatic':'Chromatic',
              'shiftedchromatic':'Shifted chromatic' }
    
    for a in archs.keys():
        for p in pitchs.keys():
            testDir = join(inputDir, 'emc-gecco2019-%s-%s' %(a, p))
            
            timestamps = []
            runDirs = []
            
            print "Searching for test runs in %s" %(testDir)
            
            for subDir in sorted(listdir(testDir)):
                match = search('emc_time([0-9\-]*)_rev.*', subDir)
                if match:
                    timestamps.append(match.group(1))
                    runDirs.append(subDir)
            
            for run, runDir in enumerate(runDirs):
                for mf in listdir(join(testDir, runDir)):
                    match = search('emc_score([0-9\.]+)_gen05000_ind.*\.mid', mf)
                    if match:
                        grade = match.group(1)
                        if grade >= cutOffGrade:
                            goodMidis.append({
                                'arch': a,
                                'pitch': p,
                                'grade': grade,
                                'name': mf,
                                'grade': grade,
                                'inputPath': 'emc-gecco2019-%s-%s/%s/%s' %(a, p, runDir, mf)
                            })
                        
    
    # choose 10 randomly from these MIDIs
    numGoodMidis = len(goodMidis)
    print 'Found %s MIDIs' %(numGoodMidis)
    chosenMidiIdxs = choice(numGoodMidis, N, replace=False)
    print 'Choices:', chosenMidiIdxs
    
    # save model on chosen MIDIs
    chosenMidis = []
    for chosenMidiIdx in chosenMidiIdxs:
        chosenMidi = goodMidis[chosenMidiIdx]
        
        # read model
        model = readFromMidi('%s/%s' %(inputDir, chosenMidi['inputPath']))
        # generate crop start and end point 
        chosenMidi['startTicks'] = randint(model.length - L)
        chosenMidi['endTicks'] = chosenMidi['startTicks'] + L
        chosenMidi['outputName'] = join(outputDir, 'emc-%s-%s-%s-%d-%d.mid' %(
            chosenMidi['arch'], chosenMidi['pitch'], chosenMidi['grade'], chosenMidi['startTicks'], chosenMidi['endTicks']))
        chosenMidi['texInclude'] = '\midifigure{%s}{%s}{%s}{%d}{%d}{20.0}' %(
            chosenMidi['arch'], chosenMidi['pitch'], chosenMidi['grade'], chosenMidi['startTicks'], chosenMidi['endTicks'])
        
        chosenMidis.append(chosenMidi)
    
    dump(chosenMidis, open(join(outputDir, 'model.json'), 'w'), indent=4, sort_keys=True)
    
    for chosenMidi in chosenMidis:
        # read model
        model = readFromMidi('%s/%s' %(inputDir, chosenMidi['inputPath']))
        # save model further
        outputFilename = join(outputDir, chosenMidi['outputName'])
        cropModel(model = model, outputFilename = outputFilename,
                  startTicks = chosenMidi['startTicks'], endTicks = chosenMidi['endTicks'])
        # convert to PDF
        midi2pdf(outputFilename)
        
    for chosenMidi in chosenMidis:
        print chosenMidi['texInclude']