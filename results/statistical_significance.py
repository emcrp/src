from scipy.stats import ttest_rel, ttest_ind
import numpy as np


def stat_test(x, y, alpha=0.05):
    """
    One-tailed t-test.
    x - values expected to be better
    y - values expected to be worse
    alpha - significance level
    """
    tval, pval = ttest_ind(x, y, equal_var=True)
     
    print("t-value={:.6f}, p-value={:.6f}".format(tval, pval))
    
    if tval > 0:
        pval_ok = 0.5 * pval  # diff > 0
    else:
        pval_ok = 1 - 0.5 * pval  # diff > 0

    print("p-value-OK (float)={:.6f}".format(pval_ok))
    print("p-value-OK (exp)  ={:.6e}".format(pval_ok))

    if pval_ok < alpha:
        print("x is significantly better than y.")
    else:
        print("Cannot say whether x is better than y.")

    return pval_ok
    

if __name__ == "__main__":
    x = 2*np.random.rand(100)
    y = np.random.rand(100)
    
    
    pval_ok = stat_test(x, y)
