'''
Unit test of importing/exporting EMC model object to/from MIDI.

'''

from numpy import array, uint16
import unittest

from emc.model.model import Model
from emc.integ.midi import transformToMidi, transformFromMidi
from emcpp.swig.emcpp import randomByteArray
from emcpp.wrapper.modelbuilder import ModelBuilderWrapper


class TestMidiWriter(unittest.TestCase):
    '''
    MIDI export unit tests.
    '''

    def assertEventHas(self, event, type = None, time = None, pitch = None, velocity = None):
        if type:     self.assertEqual(event.type, type)
        if time:     self.assertEqual(event.time, time)
        if pitch:    self.assertEqual(event.pitch, pitch)
        if velocity: self.assertEqual(event.velocity, velocity)
        
        
    def testOneEmptyTrack(self):
        # given
        model = Model(numTracks = 1)
        # when
        mf = transformToMidi(model)
        # then
        self.assertEqual(len(mf.tracks), 2)
        self.assertEqual(len(mf.tracks[1].events), 4)
        self.assertEventHas(mf.tracks[1].events[1], type = 'SEQUENCE_TRACK_NAME')
        self.assertEventHas(mf.tracks[1].events[3], type = 'END_OF_TRACK')
    
        
    def testOneTrackOneNote(self):
        # given
        model = Model(numTracks = 1)
        model.tracks[0].notes = array([[1, 2, 1, 3, 3, 4]], dtype=uint16)
        # when
        mf = transformToMidi(model)
        # then
        self.assertEqual(len(mf.tracks), 2)
        self.assertEqual(len(mf.tracks[1].events), 8)
        self.assertEventHas(mf.tracks[1].events[3], type='NOTE_ON', time=1, pitch=3, velocity=4)
        self.assertEventHas(mf.tracks[1].events[5], type='NOTE_OFF', time=1+2, pitch=3, velocity=80)
    
    
    def testOneTrackSuccesiveMultipleNotes(self):
        # given
        model = Model(numTracks = 1)
        model.tracks[0].notes = array([[1, 2, 1, 3, 2, 4],
                                       [4, 2, 5, 7, 3, 5]], dtype=uint16)
        # when
        mf = transformToMidi(model)
        # then
        self.assertEqual(len(mf.tracks[1].events), 12)
        self.assertEventHas(mf.tracks[1].events[3], type='NOTE_ON', time=1, pitch=2, velocity=4)
        self.assertEventHas(mf.tracks[1].events[5], type='NOTE_OFF', time=1+2, pitch=2, velocity=80)
        self.assertEventHas(mf.tracks[1].events[7], type='NOTE_ON', time=1+4, pitch=3, velocity=5)
        self.assertEventHas(mf.tracks[1].events[9], type='NOTE_OFF', time=1+4+2, pitch=3, velocity=80)
        
        
    def testOneTrackOverlappingNotes(self):
        # given
        model = Model(numTracks = 1)
        model.tracks[0].notes = array([[1, 3, 1, 4, 2, 4],
                                       [1, 3, 2, 5, 3, 5]], dtype=uint16)
        # when
        mf = transformToMidi(model)
        # then
        self.assertEqual(len(mf.tracks[1].events), 12)
        self.assertEventHas(mf.tracks[1].events[3], type='NOTE_ON', time=1, pitch=2, velocity=4)
        self.assertEventHas(mf.tracks[1].events[5], type='NOTE_ON', time=1+1, pitch=3, velocity=5)
        self.assertEventHas(mf.tracks[1].events[7], type='NOTE_OFF', time=1+3, pitch=2, velocity=80)
        self.assertEventHas(mf.tracks[1].events[9], type='NOTE_OFF', time=1+1+3, pitch=3, velocity=80)

    
    def testMultipleTracks(self):
        # given
        model = Model(numTracks = 2)
        model.tracks[0].notes = array([[1, 2, 1, 3, 3, 4]], dtype=uint16)
        model.tracks[1].notes = array([[4, 3, 5, 8, 3, 5]], dtype=uint16)
        # when
        mf = transformToMidi(model)
        # then
        self.assertEqual(len(mf.tracks), 3)
        self.assertEqual(len(mf.tracks[1].events), 8)
        self.assertEqual(len(mf.tracks[2].events), 8)
        self.assertEventHas(mf.tracks[1].events[3], type='NOTE_ON', time=1, pitch=3, velocity=4)
        self.assertEventHas(mf.tracks[1].events[5], type='NOTE_OFF', time=1+2, pitch=3, velocity=80)
        self.assertEventHas(mf.tracks[2].events[3], type='NOTE_ON', time=4, pitch=3, velocity=5)
        self.assertEventHas(mf.tracks[2].events[5], type='NOTE_OFF', time=4+3, pitch=3, velocity=80)




class TestMidiReader(unittest.TestCase):
    '''
    MIDI import unit tests.
    '''
    
    def testBackAndForth(self):
        # given
        data = randomByteArray(100)
        model = ModelBuilderWrapper(velocityEnabled=True).bytesToModel(data)
        # when
        midi = transformToMidi(model)
        newModel = transformFromMidi(midi)
        # then
        self.assertEqual(model, newModel)



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()