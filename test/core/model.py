'''
Unit test of EMC model object.

'''

import os
import unittest
from os.path import dirname, abspath

from numpy import array, uint16, arange

from emc.model.model import Model, Track


class TestModel(unittest.TestCase):
    '''
    Model unit tests.
    '''

    
    
    def testBuildEmptyModel(self):
        # when
        model = Model()
        # then
        self.assertEqual(model.ticksPerQuarterNote, 4)
        self.assertEqual(model.numTracks, 1)
        self.assertEqual(model.tracks.size, 1)
        self.assertEqual(model.tracks.dtype, Track)
    
    
    
    def testAccessProps(self):
        # given
        model = Model(numTracks = 1)
        model.tracks[0].notes = array([[3,  4,  5,  6,  7,  8],
                                       [5,  6,  7,  8,  9,  10],
                                       [7,  8,  9,  10, 11, 12],
                                       [9,  10, 11, 12, 13, 14],
                                       [11, 12, 13, 14, 15, 16]], dtype=uint16)
        
        # when
        iois = model.tracks[0].interonsets
        durations = model.tracks[0].durations
        onsets = model.tracks[0].onsets
        offsets = model.tracks[0].offsets
        pitches = model.tracks[0].pitches
        velocities = model.tracks[0].velocities
        
        # then
        self.assertEqual(iois.tolist(),       [3, 5,  7,  9,  11])
        self.assertEqual(durations.tolist(),  [4, 6,  8,  10, 12])
        self.assertEqual(onsets.tolist(),     [5, 7,  9,  11, 13])
        self.assertEqual(offsets.tolist(),    [6, 8,  10, 12, 14])
        self.assertEqual(pitches.tolist(),    [7, 9,  11, 13, 15])
        self.assertEqual(velocities.tolist(), [8, 10, 12, 14, 16])
        
    
    

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()