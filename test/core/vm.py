'''
Unit test of virtual machine.
'''

import unittest

from emc.vm import VirtualMachine
from emcpp.swig.emcpp import randomByteArray


class TestVirtualMachine(unittest.TestCase):
    '''
    Virtual machine unit tests.
    '''

    def setUp(self):
        self.vm = VirtualMachine()
        
        
    def testVmGivesSameOutputForSameInput(self):
        # given
        geneticStringSize = 2048
        gen = randomByteArray(geneticStringSize)
        # when
        m1 = self.vm.run(gen)
        m2 = self.vm.run(gen)
        m3 = self.vm.run(gen)
        # then
        self.assertEqual(m1, m2)
        self.assertEqual(m1, m3)
    
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
