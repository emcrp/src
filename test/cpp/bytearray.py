'''
C++ byte stream unit tests.

'''

import unittest

from numpy import ndarray, uint8, mean, array, zeros, bitwise_xor, sum as customSum, arange,\
    float32

from emcpp.swig.emcpp import randomByteArray, combineByteArray,\
    mutateBytesInByteArray, sum, contains


class ByteStreamCppTest(unittest.TestCase):
    '''
    C++ byte stream unit tests.
    '''
    
    def testRandomByteArray(self):
        # given
        size = 5000
        
        # when
        b = randomByteArray(size)
        mb = mean(b)
        
        # then
        self.assertIsInstance(b, ndarray)
        self.assertEquals(b.dtype, uint8)
        self.assertEquals(len(b), size)
        self.assertGreaterEqual(mb, 124)
        self.assertLessEqual(mb, 131)



    def testCombineByteArray(self):
        # given
        size = 256
        maxCutPoints = 10
        b1 = randomByteArray(size)
        b2 = randomByteArray(size)
        
        # when
        c1 = zeros(size, dtype=uint8)
        c2 = zeros(size, dtype=uint8)
        combineByteArray(b1, b2, c1, c2, maxCutPoints)
        
        # then
        eq_b1c1 = sum(bitwise_xor(b1, c1) == 0)
        eq_b2c1 = sum(bitwise_xor(b2, c1) == 0)
        eq_b1c2 = sum(bitwise_xor(b1, c2) == 0)
        eq_b2c2 = sum(bitwise_xor(b2, c2) == 0)
        
        # mutual inheritance
        self.assertEqual(eq_b1c1, eq_b2c2)
        self.assertEqual(eq_b1c2, eq_b2c1)
        # every byte taken from one of the parents
        self.assertGreaterEqual(eq_b1c1 + eq_b2c1, size)
    
    
    def testCombineByteArray_copiesValue(self):
        # given
        size = 256
        maxCutPoints = 10
        checkIdx = 42
        
        b1 = randomByteArray(size)
        b2 = randomByteArray(size)
        c1 = zeros(size, dtype=uint8)
        c2 = zeros(size, dtype=uint8)
        combineByteArray(b1, b2, c1, c2, maxCutPoints)
        
        # when
        self.assertTrue(c1[checkIdx] == b1[checkIdx] or c1[checkIdx] == b2[checkIdx])
        b1[checkIdx] -= 10
        b2[checkIdx] -= 10
        
        # then
        self.assertFalse(c1[checkIdx] == b1[checkIdx] or c1[checkIdx] == b2[checkIdx])
    
    
    
    def _testMutateBytesInByteArray(self):
        # given
        size = 1000
        maxm = 50
        iters = 100
        
        # when
        numm = zeros(iters)
        for i in range(iters):
            b = randomByteArray(size)
            oldb = array(b)
            mutateBytesInByteArray(b, maxm)
            numm[i] = sum(oldb != b)
        
        # then
        mean_numm = mean(numm)
        self.assertGreaterEqual(mean_numm, 20)
        self.assertLessEqual(mean_numm, 30)
    
    
    def testSum(self):
        a = arange(0.0, 20.0, 0.1, dtype=float32)
        suma1 = customSum(a)
        suma2 = sum(a)
        self.assertEqual(suma1, suma2)



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()