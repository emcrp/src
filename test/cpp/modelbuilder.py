'''
Unit test of EMC model builder.

'''

import unittest

from numpy import array, uint8

from emcpp.swig.emcpp import ModelBuilder


class TestModelBuilder(unittest.TestCase):
    '''
    Model builder (C++ extension) unit tests.
    '''

    def testBuildEmptyModel(self):
        # given
        mb = ModelBuilder(velocityEnabled = True,
                          restsEnabled = True,
                          debug = True)
        # then
        for i in range(10):
            self.assertEqual(mb.numNotes(i), 0)

    
    
    def testAddNotes_lengthAndNumNotesAreCorrect(self):
        # given
        mb = ModelBuilder(velocityEnabled = True,
                          restsEnabled = True,
                          debug = True)
        
        data = array([0, 1, 2, 3, 4,
                      0, 5, 6, 7, 8,
                      0, 9, 10, 11, 12,
                      1, 2, 3, 4, 5,
                      1, 6, 7, 8, 9,
                      2, 3, 4, 5, 6], dtype=uint8)
        
        # when
        mb.addNotesFromBytes(data)
        
        #then
        self.assertEqual(mb.numNotes(0), 3)
        self.assertEqual(mb.numNotes(1), 2)
        self.assertEqual(mb.numNotes(2), 1)
        self.assertEqual(mb.numNotes(3), 0)
        
    
    
    def testAddNotes_velocityEnabled_notesAreCorrect(self):
        # given
        mb = ModelBuilder(velocityEnabled = True,
                          restsEnabled = True,
                          debug = True)
        
        data = array([0, 1, 2, 3, 4,
                      0, 5, 6, 7, 8,
                      0, 9, 10, 11, 12,
                      1, 2, 3, 4, 5,
                      1, 6, 7, 8, 9,
                      2, 3, 4, 5, 6], dtype=uint8)
        
        # when
        mb.addNotesFromBytes(data)
        notes0 = mb.notes(6 * 3, 0)
        notes1 = mb.notes(6 * 2, 1)
        notes2 = mb.notes(6 * 1, 2)
        notes3 = mb.notes(6 * 0, 3)
        
        #then
        self.assertEqual(notes0.tolist(), [1, 2, 1, 3, 3, 4,
                                           5, 6, 6, 12, 7, 8,
                                           9, 10, 15, 25, 11, 12])
        self.assertEqual(notes1.tolist(), [2, 3, 2, 5, 4, 5,
                                           6, 7, 8, 15, 8, 9])
        self.assertEqual(notes2.tolist(), [3, 4, 3, 7, 5, 6])
        self.assertEqual(notes3.tolist(), [])
    
    
    
    def testAddNotes_velocityNotEnabled_notesAreCorrect(self):
        # given
        mb = ModelBuilder(velocityEnabled = False,
                          restsEnabled = True,
                          debug = True)
        
        data = array([0, 1, 2, 3,
                      0, 5, 6, 7,
                      0, 9, 10, 11,
                      1, 2, 3, 4,
                      1, 6, 7, 8,
                      2, 3, 4, 5], dtype=uint8)
        
        # when
        mb.addNotesFromBytes(data)
        notes0 = mb.notes(6 * 3, 0)
        notes1 = mb.notes(6 * 2, 1)
        notes2 = mb.notes(6 * 1, 2)
        notes3 = mb.notes(6 * 0, 3)
        
        #then
        self.assertEqual(notes0.tolist(), [1, 2, 1, 3, 3, 127,
                                           5, 6, 6, 12, 7, 127,
                                           9, 10, 15, 25, 11, 127])
        self.assertEqual(notes1.tolist(), [2, 3, 2, 5, 4, 127,
                                           6, 7, 8, 15, 8, 127])
        self.assertEqual(notes2.tolist(), [3, 4, 3, 7, 5, 127])
        self.assertEqual(notes3.tolist(), [])
    
    
    
    def testAddNotes_zerosOmmitted_notesAreCorrect(self):
        # given
        mb = ModelBuilder(velocityEnabled = True,
                          restsEnabled = True,
                          omitZeroDurations = True,
                          omitZeroPitches = True,
                          omitZeroVelocities = True,
                          debug = True)
        
        data = array([0, 1, 2, 3, 4,
                      0, 5, 0, 7, 8,
                      0, 9, 10, 11, 12,
                      0, 2, 3, 0, 5,
                      0, 6, 7, 8, 9,
                      0, 3, 4, 5, 0], dtype=uint8)
        
        # when
        mb.addNotesFromBytes(data)
        notes = mb.notes(6 * 3, 0)
        
        #then
        self.assertEqual(mb.numNotes(0), 3)
        self.assertEqual(notes.tolist(), [1, 2, 1, 3, 3, 4,
                                          9, 10, 10, 20, 11, 12,
                                          6, 7, 16, 23, 8, 9])
    
    
    
    def testAddNotes_zerosOmmitted_velocitiesDisabled_notesAreCorrect(self):
        # given
        mb = ModelBuilder(velocityEnabled = False,
                          restsEnabled = True,
                          omitZeroDurations = True,
                          omitZeroPitches = True,
                          omitZeroVelocities = True,
                          debug = True)
        
        data = array([0, 1, 2, 3,
                      0, 5, 0, 7,
                      0, 9, 10, 11,
                      0, 2, 3, 0,
                      0, 6, 7, 8,
                      0, 3, 4, 5], dtype=uint8)
        
        # when
        mb.addNotesFromBytes(data)
        notes = mb.notes(6 * 4, 0)
        
        #then
        self.assertEqual(mb.numNotes(0), 4)
        self.assertEqual(notes.tolist(), [1, 2, 1, 3, 3, 127,
                                          9, 10, 10, 20, 11, 127,
                                          6, 7, 16, 23, 8, 127,
                                          3, 4, 19, 23, 5, 127])
        
        
    
    def testAddNotes_zerosNotOmmitted_notesAreCorrect(self):
        # given
        mb = ModelBuilder(velocityEnabled = True,
                          restsEnabled = True,
                          omitZeroDurations = False,
                          omitZeroPitches = False,
                          omitZeroVelocities = False,
                          debug = True)
        
        data = array([0, 1, 2, 3, 4,
                      0, 5, 0, 7, 8,
                      0, 9, 10, 11, 12,
                      0, 2, 3, 0, 5,
                      0, 6, 7, 8, 9,
                      0, 3, 4, 5, 0], dtype=uint8)
        
        # when
        mb.addNotesFromBytes(data)
        notes = mb.notes(6 * 6, 0)
        
        #then
        self.assertEqual(mb.numNotes(0), 6)
        self.assertEqual(notes.tolist(), [1, 2, 1, 3, 3, 4,
                                          5, 0, 6, 6, 7, 8,
                                          9, 10, 15, 25, 11, 12,
                                          2, 3, 17, 20, 0, 5,
                                          6, 7, 23, 30, 8, 9,
                                          3, 4, 26, 30, 5, 0])
    
    
    
    def testAddNotes_modsTrackNum(self):
        # given
        mb = ModelBuilder(velocityEnabled = True,
                          restsEnabled = True,
                          debug = True)
        
        data = array([18, 1, 2, 3, 4], dtype=uint8)
        
        # when
        mb.addNotesFromBytes(data)
        
        #then
        self.assertEqual(mb.numNotes(0), 0)
        self.assertEqual(mb.numNotes(1), 0)
        self.assertEqual(mb.numNotes(2), 1)
        self.assertEqual(mb.numNotes(3), 0)


        
    def testAddNotes_withRests(self):
        # given
        mb = ModelBuilder(velocityEnabled = True,
                          restsEnabled = True,
                          debug = True)
        
        data = array([0x00, 0x01, 0x02, 0x83, 0x84,               # rest 1
                      0x00, 0x01, 0x02, 0x03, 0x04,               # note
                      0x00, 0x01, 0x02, 0xF3, 0xF4,               # rest 2
                      0x00, 0x01, 0x02, 0x83, 0x04,               # note
                      0x00, 0x01, 0x02, 0x03, 0x84], dtype=uint8) # note
        
        # when
        mb.addNotesFromBytes(data)
        notes0 = mb.notes(6 * 3, 0)
        notes0.shape = (3, 6)
        
        # then
        self.assertEqual(mb.numNotes(0), 3)
        self.assertEqual(notes0[0, 0], 2)  # rest 1 + note
        self.assertEqual(notes0[1, 0], 2)  # rest 2 + note 
        self.assertEqual(notes0[2, 0], 1)  # note
    
    
    
    def testAddNotes_longRest(self):
        # given
        mb = ModelBuilder(velocityEnabled = True,
                          restsEnabled = True,
                          debug = True)
        
        data = array([0x00, 0xFF, 0x02, 0x80, 0x80,               # rest 255
                      0x00, 0xFF, 0x02, 0x80, 0x80,               # rest 255
                      0x00, 0xFF, 0x02, 0x80, 0x80,               # rest 255
                      0x00, 0xFF, 0x02, 0x80, 0x80,               # rest 255
                      0x00, 0x01, 0x02, 0x03, 0x04], dtype=uint8) # note
        
        # when
        mb.addNotesFromBytes(data)
        notes0 = mb.notes(4 * 1, 0)
        notes0.shape = (1, 4)
        
        # then
        self.assertEqual(mb.numNotes(0), 1)
        self.assertEqual(notes0[0, 0], 1021)



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
