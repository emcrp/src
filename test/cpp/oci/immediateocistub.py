'''
WARNING
=======
This is a generated file that can be overwritten with regeneration.
It is a unit test suite of the C++ op-code interpreter for the op code state machine Emc.
'''
import unittest

from numpy import uint8, concatenate, uint16, arange
from numpy.random import randint

from emcpp.wrapper.oci.immediateoci import ImmediateOciWrapper


class TestOciStubEmc(unittest.TestCase):
    
    
    def setUp(self):
        self.oci = ImmediateOciWrapper(2 ** 16, 2 ** 8, True)
        self.geneticStringSize = self.oci.geneticStringSize
    
    
    def testSetFromGeneticString(self):
        # given
        counter = randint(256 ** 2, size = 1).astype(uint16)
        registers = randint(256, size=8).astype(uint8)
        
        acc = randint(256 ** 1, size = 1).astype(uint8)
        dataPtr = randint(256 ** 2, size = 1).astype(uint16)
        flags = randint(256 ** 1, size = 1).astype(uint8)
        stackPtr = randint(256 ** 1, size = 1).astype(uint8)
        
        ram = randint(256, size = 2 ** 16).astype(uint8)
        stack = randint(256, size=256).astype(uint8)
        
        geneticString = concatenate((counter.view(uint8),
                                     registers.view(uint8),
                                     acc.view(uint8),
                                     dataPtr.view(uint8),
                                     flags.view(uint8),
                                     stackPtr.view(uint8),
                                     ram.view(uint8),
                                     stack.view(uint8)))
        
        # when
        self.oci.native.setFromGeneticString(geneticString)
        
        # then
        self.assertEqual(geneticString.size, self.geneticStringSize)
        
        self.assertEqual(self.oci.counter(), counter)
        self.assertTrue((self.oci.registers() == registers).all())
        
        self.assertEqual(self.oci.acc(), acc)
        self.assertEqual(self.oci.dataPtr(), dataPtr)
        self.assertEqual(self.oci.flags(), flags)
        self.assertEqual(self.oci.stackPtr(), stackPtr)
        
        self.assertTrue((self.oci.ram() == ram).all())
        self.assertTrue((self.oci.stack() == stack).all())
    
    
    
    def testInterpret(self):
        # given
        self.oci.setDebug(True)
        
        for i in arange(256):
            self.oci.setRamAt(self.oci.counter(), i)
            self.oci.native.interpretNext()
        
        
        



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()