'''
Unit tests for C++ EMC OCI
'''
import unittest

from emcpp.wrapper.oci.indirectoci import IndirectOciWrapper


class TestIndirectOci(unittest.TestCase):
    
    def setUp(self):
        self.oci = IndirectOciWrapper(2 ** 8, 2 ** 8, True)

    
    '''
    MOVs
    '''
        
    def test_movR_DH(self):
        # given 
        self.oci.setRegister(3, 0x42)
        self.oci.setDataPtr(0x579a)
        # when
        self.oci.native.movR_DH(3)
        # then
        self.assertEqual(self.oci.register(3), 0x57)
        
        
    def test_movR_DL(self):
        # given 
        self.oci.setRegister(3, 0x42)
        self.oci.setDataPtr(0x579a)
        # when
        self.oci.native.movR_DL(3)
        # then
        self.assertEqual(self.oci.register(3), 0x9a)
        
        
    def test_movDH_R(self):
        # given 
        self.oci.setRegister(3, 0x9a)
        self.oci.setDataPtr(0x4243)
        # when
        self.oci.native.movDH_R(3)
        # then
        self.assertEqual(self.oci.dataPtr(), 0x9a43)
    
    
    def test_movDL_R(self):
        # given 
        self.oci.setRegister(3, 0x9a)
        self.oci.setDataPtr(0x4243)
        # when
        self.oci.native.movDL_R(3)
        # then
        self.assertEqual(self.oci.dataPtr(), 0x429a)
        
        
    def test_movR_DPtr(self):
        # given
        self.oci.setRamAt(0x579a, 0x42)
        self.oci.setDataPtr(0x579a)
        # when
        self.oci.native.movR_DPtr(3)
        # then
        self.assertEqual(self.oci.register(3), 0x42)
        self.assertEqual(self.oci.dataPtr(), 0x579a)
        
        
    def test_movDPtr_R(self):
        # given 
        self.oci.setRegister(3, 0x42)
        self.oci.setDataPtr(0x579a)
        # when
        self.oci.native.movDPtr_R(3)
        # then
        self.assertEqual(self.oci.ramAt(0x579a), 0x42)
        self.assertEqual(self.oci.dataPtr(), 0x579a)
    
        
    def test_movR_A(self):
        # given
        self.oci.setAcc(0x42)
        # when
        self.oci.native.movR_A(3)
        # then
        self.assertEqual(self.oci.register(3), 0x42)
        
        
    def test_movA_R(self):
        # given
        self.oci.setRegister(3, 0x42)
        # when
        self.oci.native.movA_R(3)
        # then
        self.assertEqual(self.oci.acc(), 0x42)
        
        
    def test_xchA_R(self):
        # given
        self.oci.setAcc(0x42)
        self.oci.setRegister(3, 0x57)
        # when
        self.oci.native.xchA_R(3)
        # then
        self.assertEqual(self.oci.acc(), 0x57)
        self.assertEqual(self.oci.register(3), 0x42)
        

    def test_movA_DH(self):
        # given 
        self.oci.setAcc(0x42)
        self.oci.setDataPtr(0x579a)
        # when
        self.oci.native.movA_DH()
        # then
        self.assertEqual(self.oci.acc(), 0x57)
        
        
    def test_movA_DL(self):
        # given 
        self.oci.setAcc(0x42)
        self.oci.setDataPtr(0x579a)
        # when
        self.oci.native.movA_DL()
        # then
        self.assertEqual(self.oci.acc(), 0x9a)
        
        
    def test_movDH_A(self):
        # given 
        self.oci.setAcc(0x9a)
        self.oci.setDataPtr(0x4243)
        # when
        self.oci.native.movDH_A()
        # then
        self.assertEqual(self.oci.dataPtr(), 0x9a43)
        
        
    def test_movDL_A(self):
        # given 
        self.oci.setAcc(0x9a)
        self.oci.setDataPtr(0x4243)
        # when
        self.oci.native.movDL_A()
        # then
        self.assertEqual(self.oci.dataPtr(), 0x429a)
        
    
    def test_movA_DPtr(self):
        # given 
        self.oci.setRamAt(0x579a, 0x42)
        self.oci.setDataPtr(0x579a)
        # when
        self.oci.native.movA_DPtr()
        # then
        self.assertEqual(self.oci.acc(), 0x42)
        self.assertEqual(self.oci.dataPtr(), 0x579a)
        
        
    def test_movDPtr_A(self):
        # given 
        self.oci.setAcc(0x42)
        self.oci.setDataPtr(0x579a)
        # when
        self.oci.native.movDPtr_A()
        # then
        self.assertEqual(self.oci.ramAt(0x579a), 0x42)
        self.assertEqual(self.oci.dataPtr(), 0x579a)
        
        
    def test_cplC(self):
        # given
        self.oci.setFlags(0)
        # when
        self.oci.native.cplC()
        # then
        self.assertEqual(self.oci.flags(), 1)
        
        # given
        self.oci.setFlags(1)
        # when
        self.oci.native.cplC()
        # then
        self.assertEqual(self.oci.flags(), 0)
    
        
    '''
    Arithmetic & Logic
    '''
        
    def test_inc_A(self):
        # given
        self.oci.setAcc(0x42)
        # when
        self.oci.native.inc_A()
        # then
        self.assertEqual(self.oci.acc(), 0x43)
        
        
    def test_dec_A(self):
        # given
        self.oci.setAcc(0x42)
        # when
        self.oci.native.dec_A()
        # then
        self.assertEqual(self.oci.acc(), 0x41)
        
        
    def test_inc_D(self):
        # given
        self.oci.setDataPtr(0x579a)
        # when
        self.oci.native.inc_D()
        # then
        self.assertEqual(self.oci.dataPtr(), 0x579b)
        
        
    def test_dec_D(self):
        # given
        self.oci.setDataPtr(0x579a)
        # when
        self.oci.native.dec_D()
        # then
        self.assertEqual(self.oci.dataPtr(), 0x5799)
        
    
    def test_anlA_R(self):
        # given
        self.oci.setAcc(0x49)                  # 0100 1001
        self.oci.setRegister(3, 0x9c)          # 1001 1100
        # when
        self.oci.native.anlA_R(3)
        # then
        self.assertEqual(self.oci.acc(), 0x08) # 0000 10000
        
        
    def test_orlA_R(self):
        # given
        self.oci.setAcc(0x49)                  # 0100 1001
        self.oci.setRegister(3, 0x9c)          # 1001 1100
        # when
        self.oci.native.orlA_R(3)
        # then
        self.assertEqual(self.oci.acc(), 0xdd) # 1101 1101
        
        
    def test_addA_R(self):
        # given
        self.oci.setAcc(0x42)
        self.oci.setRegister(3, 0x57)
        self.oci.setFlags(0)
        # when
        self.oci.native.addA_R(3)
        # then
        self.assertEqual(self.oci.acc(), 0x99)
        self.assertEqual(self.oci.flags(), 0)
    
        # given
        self.oci.setAcc(0xf2)
        self.oci.setRegister(3, 0x57)
        self.oci.setFlags(0)
        # when
        self.oci.native.addA_R(3)
        # then
        self.assertEqual(self.oci.acc(), 0x49)
        self.assertEqual(self.oci.flags(), 0)
        
        
    def test_addcA_R(self):
        # given
        self.oci.setAcc(0x42)
        self.oci.setRegister(3, 0x57)
        self.oci.setFlags(0)
        # when
        self.oci.native.addcA_R(3)
        # then
        self.assertEqual(self.oci.acc(), 0x99)
        self.assertEqual(self.oci.flags(), 0)
    
        # given
        self.oci.setAcc(0xf2)
        self.oci.setRegister(3, 0x57)
        self.oci.setFlags(0)
        # when
        self.oci.native.addcA_R(3)
        # then
        self.assertEqual(self.oci.acc(), 0x49)
        self.assertEqual(self.oci.flags(), 1)
        
        
    def test_subA_R(self):
        # given
        self.oci.setAcc(0x92)
        self.oci.setRegister(3, 0x57)
        self.oci.setFlags(0)
        # when
        self.oci.native.subA_R(3)
        # then
        self.assertEqual(self.oci.acc(), 0x3B)
        self.assertEqual(self.oci.flags(), 0)
        
        # given
        self.oci.setAcc(0x42)
        self.oci.setRegister(3, 0x57)
        self.oci.setFlags(0)
        # when
        self.oci.native.subA_R(3)
        # then
        self.assertEqual(self.oci.acc(), 0xEB)
        self.assertEqual(self.oci.flags(), 0)
        
        
    def test_subbA_R(self):
        # given
        self.oci.setAcc(0x92)
        self.oci.setRegister(3, 0x57)
        self.oci.setFlags(0)
        # when
        self.oci.native.subbA_R(3)
        # then
        self.assertEqual(self.oci.acc(), 0x3B)
        self.assertEqual(self.oci.flags(), 0)
        
        # given
        self.oci.setAcc(0x42)
        self.oci.setRegister(3, 0x57)
        self.oci.setFlags(0)
        # when
        self.oci.native.subbA_R(3)
        # then
        self.assertEqual(self.oci.acc(), 0xEB)
        self.assertEqual(self.oci.flags(), 1)
        
    
    def test_cplA(self):
        # given
        self.oci.setAcc(0x49)                  # 0100 1001
        # when
        self.oci.native.cplA()
        # then
        self.assertEqual(self.oci.acc(), 0xB6) # 1011 0110
        
    
    def test_anlA_RPtr(self):
        # given
        self.oci.setAcc(0x49)                  # 0100 1001
        self.oci.setRegister(2, 0x9a)
        self.oci.setRegister(3, 0x57)
        self.oci.setRamAt(0x579a, 0x9c)        # 1001 1100
        # when
        self.oci.native.anlA_RPtr(1)
        # then
        self.assertEqual(self.oci.acc(), 0x08) # 0000 1000
        
        
    def test_orlA_RPtr(self):
        # given
        self.oci.setAcc(0x49)                  # 0100 1001
        self.oci.setRegister(2, 0x9a)
        self.oci.setRegister(3, 0x57)
        self.oci.setRamAt(0x579a, 0x9c)        # 1001 1100
        # when
        self.oci.native.orlA_RPtr(1)
        # then
        self.assertEqual(self.oci.acc(), 0xdd) # 1101 1101
        
          
    def test_anlA_DPtr(self):
        # given
        self.oci.setAcc(0x49)                  # 0100 1001
        self.oci.setDataPtr(0x579a)
        self.oci.setRamAt(0x579a, 0x9c)        # 1001 1100
        # when
        self.oci.native.anlA_DPtr()
        # then
        self.assertEqual(self.oci.acc(), 0x08) # 0000 10000
        self.assertEqual(self.oci.dataPtr(), 0x579a)
        
        
    def test_orlA_DPtr(self):
        # given
        self.oci.setAcc(0x49)                  # 0100 1001
        self.oci.setDataPtr(0x579a)
        self.oci.setRamAt(0x579a, 0x9c)        # 1001 1100
        # when
        self.oci.native.orlA_DPtr()
        # then
        self.assertEqual(self.oci.acc(), 0xdd) # 1101 1101
        self.assertEqual(self.oci.dataPtr(), 0x579a)
        
        
    def test_addA_RPtr(self):
        # given
        self.oci.setAcc(0x42)
        self.oci.setRegister(2, 0x9a)
        self.oci.setRegister(3, 0x57)
        self.oci.setRamAt(0x579a, 0x57)
        self.oci.setFlags(0)
        # when
        self.oci.native.addA_RPtr(1)
        # then
        self.assertEqual(self.oci.acc(), 0x99)
        self.assertEqual(self.oci.flags(), 0)
    
        # given
        self.oci.setAcc(0xf2)
        self.oci.setRegister(2, 0x9a)
        self.oci.setRegister(3, 0x57)
        self.oci.setRamAt(0x579a, 0x57)
        self.oci.setFlags(0)
        # when
        self.oci.native.addA_RPtr(1)
        # then
        self.assertEqual(self.oci.acc(), 0x49)
        self.assertEqual(self.oci.flags(), 0)
        
        
    def test_addcA_RPtr(self):
        # given
        self.oci.setAcc(0x42)
        self.oci.setRegister(2, 0x9a)
        self.oci.setRegister(3, 0x57)
        self.oci.setRamAt(0x579a, 0x57)
        self.oci.setFlags(0)
        # when
        self.oci.native.addcA_RPtr(1)
        # then
        self.assertEqual(self.oci.acc(), 0x99)
        self.assertEqual(self.oci.flags(), 0)
    
        # given
        self.oci.setAcc(0xf2)
        self.oci.setRegister(2, 0x9a)
        self.oci.setRegister(3, 0x57)
        self.oci.setRamAt(0x579a, 0x57)
        self.oci.setFlags(0)
        # when
        self.oci.native.addcA_RPtr(1)
        # then
        self.assertEqual(self.oci.acc(), 0x49)
        self.assertEqual(self.oci.flags(), 1)
        
        
    def test_subA_RPtr(self):
        # given
        self.oci.setAcc(0x92)
        self.oci.setRegister(2, 0x9a)
        self.oci.setRegister(3, 0x57)
        self.oci.setRamAt(0x579a, 0x57)
        self.oci.setFlags(0)
        # when
        self.oci.native.subA_RPtr(1)
        # then
        self.assertEqual(self.oci.acc(), 0x3B)
        self.assertEqual(self.oci.flags(), 0)
        
        # given
        self.oci.setAcc(0x42)
        self.oci.setRegister(2, 0x9a)
        self.oci.setRegister(3, 0x57)
        self.oci.setRamAt(0x579a, 0x57)
        self.oci.setFlags(0)
        # when
        self.oci.native.subA_RPtr(1)
        # then
        self.assertEqual(self.oci.acc(), 0xEB)
        self.assertEqual(self.oci.flags(), 0)
        
        
    def test_subbA_RPtr(self):
        # given
        self.oci.setAcc(0x92)
        self.oci.setRegister(2, 0x9a)
        self.oci.setRegister(3, 0x57)
        self.oci.setRamAt(0x579a, 0x57)
        self.oci.setFlags(0)
        # when
        self.oci.native.subbA_RPtr(1)
        # then
        self.assertEqual(self.oci.acc(), 0x3B)
        self.assertEqual(self.oci.flags(), 0)
        
        # given
        self.oci.setAcc(0x42)
        self.oci.setRegister(2, 0x9a)
        self.oci.setRegister(3, 0x57)
        self.oci.setRamAt(0x579a, 0x57)
        self.oci.setFlags(0)
        # when
        self.oci.native.subbA_RPtr(1)
        # then
        self.assertEqual(self.oci.acc(), 0xEB)
        self.assertEqual(self.oci.flags(), 1)
        
        
    def test_addA_DPtr(self):
        # given
        self.oci.setAcc(0x42)
        self.oci.setDataPtr(0x579a)
        self.oci.setRamAt(0x579a, 0x57)
        self.oci.setFlags(0)
        # when
        self.oci.native.addA_DPtr()
        # then
        self.assertEqual(self.oci.acc(), 0x99)
        self.assertEqual(self.oci.flags(), 0)
        self.assertEqual(self.oci.dataPtr(), 0x579a)
    
        # given
        self.oci.setAcc(0xf2)
        self.oci.setDataPtr(0x579a)
        self.oci.setRamAt(0x579a, 0x57)
        self.oci.setFlags(0)
        # when
        self.oci.native.addA_DPtr()
        # then
        self.assertEqual(self.oci.acc(), 0x49)
        self.assertEqual(self.oci.flags(), 0)
        self.assertEqual(self.oci.dataPtr(), 0x579a)
        
        
    def test_addcA_DPtr(self):
        # given
        self.oci.setAcc(0x42)
        self.oci.setDataPtr(0x579a)
        self.oci.setRamAt(0x579a, 0x57)
        self.oci.setFlags(0)
        # when
        self.oci.native.addcA_DPtr()
        # then
        self.assertEqual(self.oci.acc(), 0x99)
        self.assertEqual(self.oci.flags(), 0)
        self.assertEqual(self.oci.dataPtr(), 0x579a)
    
        # given
        self.oci.setAcc(0xf2)
        self.oci.setDataPtr(0x579a)
        self.oci.setRamAt(0x579a, 0x57)
        self.oci.setFlags(0)
        # when
        self.oci.native.addcA_DPtr()
        # then
        self.assertEqual(self.oci.acc(), 0x49)
        self.assertEqual(self.oci.flags(), 1)
        self.assertEqual(self.oci.dataPtr(), 0x579a)
        
        
    def test_subA_DPtr(self):
        # given
        self.oci.setAcc(0x92)
        self.oci.setDataPtr(0x579a)
        self.oci.setRamAt(0x579a, 0x57)
        self.oci.setFlags(0)
        # when
        self.oci.native.subA_DPtr()
        # then
        self.assertEqual(self.oci.acc(), 0x3B)
        self.assertEqual(self.oci.flags(), 0)
        self.assertEqual(self.oci.dataPtr(), 0x579a)
        
        # given
        self.oci.setAcc(0x42)
        self.oci.setDataPtr(0x579a)
        self.oci.setRamAt(0x579a, 0x57)
        self.oci.setFlags(0)
        # when
        self.oci.native.subA_DPtr()
        # then
        self.assertEqual(self.oci.acc(), 0xEB)
        self.assertEqual(self.oci.flags(), 0)
        self.assertEqual(self.oci.dataPtr(), 0x579a)
        
        
    def test_subbA_DPtr(self):
        # given
        self.oci.setAcc(0x92)
        self.oci.setDataPtr(0x579a)
        self.oci.setRamAt(0x579a, 0x57)
        self.oci.setFlags(0)
        # when
        self.oci.native.subbA_DPtr()
        # then
        self.assertEqual(self.oci.acc(), 0x3B)
        self.assertEqual(self.oci.flags(), 0)
        self.assertEqual(self.oci.dataPtr(), 0x579a)
        
        # given
        self.oci.setAcc(0x42)
        self.oci.setDataPtr(0x579a)
        self.oci.setRamAt(0x579a, 0x57)
        self.oci.setFlags(0)
        # when
        self.oci.native.subbA_DPtr()
        # then
        self.assertEqual(self.oci.acc(), 0xEB)
        self.assertEqual(self.oci.flags(), 1)
        self.assertEqual(self.oci.dataPtr(), 0x579a)
        
        
    def test_rlA(self):
        # given
        self.oci.setAcc(0x42)                  # 0100 0010
        self.oci.setFlags(0)
        # when
        self.oci.native.rlA()
        self.oci.native.rlA()
        # then
        self.assertEqual(self.oci.acc(), 0x09) # 0000 1001
        self.assertEqual(self.oci.flags(), 0)
        
        
    def test_rrA(self):
        # given
        self.oci.setAcc(0x42)                  # 0100 0010
        self.oci.setFlags(0)
        # when
        self.oci.native.rrA()
        self.oci.native.rrA()
        # then
        self.assertEqual(self.oci.acc(), 0x90) # 1001 0000
        self.assertEqual(self.oci.flags(), 0)
        
        
    def test_rlcA(self):
        # given
        self.oci.setAcc(0x42)
        self.oci.setFlags(0)                   # 0100 0010 0
        # when
        self.oci.native.rlcA()
        self.oci.native.rlcA()
        # then
        self.assertEqual(self.oci.acc(), 0x08) # 0000 1000 1 
        self.assertEqual(self.oci.flags(), 1)
        
        
    def test_rrcA(self):
        # given
        self.oci.setAcc(0x42)
        self.oci.setFlags(0)                   # 0100 0010 0
        # when
        self.oci.native.rrcA()
        self.oci.native.rrcA()
        # then
        self.assertEqual(self.oci.acc(), 0x10) # 0001 0000 1
        self.assertEqual(self.oci.flags(), 1)
        
    
    '''
    Stack operations
    '''
    
    def test_pushDH(self):
        # given
        self.oci.setDataPtr(0x579a)
        self.oci.setStackPtr(0x16)
        # when
        self.oci.native.pushDH()
        # then
        self.assertEqual(self.oci.stackPtr(), 0x17)
        self.assertEqual(self.oci.stackAt(0x17), 0x57)
        
        
    def test_popDH(self):
        # given
        self.oci.setDataPtr(0x1234)
        self.oci.setStackAt(0x17, 0x57)
        self.oci.setStackPtr(0x17)
        # when
        self.oci.native.popDH()
        # then
        self.assertEqual(self.oci.stackPtr(), 0x16)
        self.assertEqual(self.oci.dataPtr(), 0x5734)
        
        
    def test_pushDL(self):
        # given
        self.oci.setDataPtr(0x579a)
        self.oci.setStackPtr(0x16)
        # when
        self.oci.native.pushDL()
        # then
        self.assertEqual(self.oci.stackPtr(), 0x17)
        self.assertEqual(self.oci.stackAt(0x17), 0x9a)
        
        
    def test_popDL(self):
        # given
        self.oci.setDataPtr(0x1234)
        self.oci.setStackAt(0x17, 0x57)
        self.oci.setStackPtr(0x17)
        # when
        self.oci.native.popDL()
        # then
        self.assertEqual(self.oci.stackPtr(), 0x16)
        self.assertEqual(self.oci.dataPtr(), 0x1257)
        
    
    def test_pushA(self):
        # given
        self.oci.setAcc(0x42)
        self.oci.setStackPtr(0x16)
        # when
        self.oci.native.pushA()
        # then
        self.assertEqual(self.oci.stackPtr(), 0x17)
        self.assertEqual(self.oci.stackAt(0x17), 0x42)
        
    
    def test_popA(self):
        # given
        self.oci.setStackAt(0x17, 0x42)
        self.oci.setStackPtr(0x17)
        # when
        self.oci.native.popA()
        # then
        self.assertEqual(self.oci.stackPtr(), 0x16)
        self.assertEqual(self.oci.acc(), 0x42)
    
    
    def test_pushR(self):
        # given
        self.oci.setRegister(2, 0x42)
        self.oci.setStackPtr(0x16)
        # when
        self.oci.native.pushR(2)
        # then
        self.assertEqual(self.oci.stackPtr(), 0x17)
        self.assertEqual(self.oci.stackAt(0x17), 0x42)
        
    
    def test_popR(self):
        # given
        self.oci.setStackAt(0x17, 0x42)
        self.oci.setStackPtr(0x17)
        # when
        self.oci.native.popR(2)
        # then
        self.assertEqual(self.oci.stackPtr(), 0x16)
        self.assertEqual(self.oci.register(2), 0x42)
        
    
    def test_sjmpR(self):
        # given
        self.oci.setCounter(0x579a)
        self.oci.setRegister(2, 0x11)
        # when
        self.oci.native.sjmpR(2)
        # then
        self.assertEqual(self.oci.counter(), 0x57ab)
        
        # given
        self.oci.setCounter(0x579a)
        self.oci.setRegister(2, 0xFC)
        # when
        self.oci.native.sjmpR(2)
        # then
        self.assertEqual(self.oci.counter(), 0x5796)
        
        
    def test_jmpR(self):
        # given
        self.oci.setCounter(0x579a)
        self.oci.setRegister(2, 0x23)
        self.oci.setRegister(3, 0x11)
        # when
        self.oci.native.jmpR(1)
        # then
        self.assertEqual(self.oci.counter(), 0x1123)
        
    
    def test_jmpD(self):
        # given
        self.oci.setCounter(0x579a)
        self.oci.setDataPtr(0x1234)
        # when
        self.oci.native.jmpD()
        # then
        self.assertEqual(self.oci.counter(), 0x1234)
        
        
    def test_scallR(self):
        # given
        self.oci.setCounter(0x579a)
        self.oci.setRegister(2, 0x11)
        self.oci.setStackPtr(0x16)
        # when
        self.oci.native.scallR(2)
        # then
        self.assertEqual(self.oci.counter(), 0x57ab)
        self.assertEqual(self.oci.stackPtr(), 0x18)
        self.assertEqual(self.oci.stackAt(0x18), 0x57)
        self.assertEqual(self.oci.stackAt(0x17), 0x9a)
        
        # given
        self.oci.setCounter(0x579a)
        self.oci.setRegister(2, 0xFC)
        self.oci.setStackPtr(0x16)
        # when
        self.oci.native.scallR(2)
        # then
        self.assertEqual(self.oci.counter(), 0x5796)
        self.assertEqual(self.oci.stackPtr(), 0x18)
        self.assertEqual(self.oci.stackAt(0x18), 0x57)
        self.assertEqual(self.oci.stackAt(0x17), 0x9a)
        
        
    def test_callR(self):
        # given
        self.oci.setRegister(2, 0x23)
        self.oci.setRegister(3, 0x11)
        self.oci.setCounter(0x49ca)
        self.oci.setStackPtr(0x16)
        # when
        self.oci.native.callR(1)
        # then
        self.assertEqual(self.oci.counter(), 0x1123)
        self.assertEqual(self.oci.stackPtr(), 0x18)
        self.assertEqual(self.oci.stackAt(0x18), 0x49)
        self.assertEqual(self.oci.stackAt(0x17), 0xca)
        
        
    def test_callD(self):
        # given
        self.oci.setDataPtr(0x1234)
        self.oci.setCounter(0x49ca)
        self.oci.setStackPtr(0x16)
        # when
        self.oci.native.callD()
        # then
        self.assertEqual(self.oci.counter(), 0x1234)
        self.assertEqual(self.oci.stackPtr(), 0x18)
        self.assertEqual(self.oci.stackAt(0x18), 0x49)
        self.assertEqual(self.oci.stackAt(0x17), 0xca)
        
        
    def test_ret(self):
        # given
        self.oci.setCounter(0x1234)
        self.oci.setStackPtr(0x18)
        self.oci.setStackAt(0x18, 0x49)
        self.oci.setStackAt(0x17, 0xca)
        # when
        self.oci.native.ret()
        # then
        self.assertEqual(self.oci.counter(), 0x49ca)
        self.assertEqual(self.oci.stackPtr(), 0x16)
        

    '''
    Conditional jumps
    '''
           
    # added by smart render
    def test_jnc_D(self):
        #given
        #when
        self.oci.native.jnc_D()
        #then
        self.fail("test not implemented yet")
    
    # added by smart render
    def test_jc_D(self):
        #given
        #when
        self.oci.native.jc_D()
        #then
        self.fail("test not implemented yet")
    
    # added by smart render
    def test_jnzA_D(self):
        #given
        #when
        self.oci.native.jnzA_D()
        #then
        self.fail("test not implemented yet")
    
    # added by smart render
    def test_jzA_D(self):
        #given
        #when
        self.oci.native.jzA_D()
        #then
        self.fail("test not implemented yet")
    
    # added by smart render
    def test_sjnc_R(self):
        #given
        #when
        self.oci.native.sjnc_R(0)
        #then
        self.fail("test not implemented yet")
    
    # added by smart render
    def test_sjc_R(self):
        #given
        #when
        self.oci.native.sjc_R(0)
        #then
        self.fail("test not implemented yet")
    
    # added by smart render
    def test_jnc_R(self):
        #given
        #when
        self.oci.native.jnc_R(0)
        #then
        self.fail("test not implemented yet")
    
    # added by smart render
    def test_jc_R(self):
        #given
        #when
        self.oci.native.jc_R(0)
        #then
        self.fail("test not implemented yet")
    
    # added by smart render
    def test_sjnzA_R(self):
        #given
        #when
        self.oci.native.sjnzA_R(0)
        #then
        self.fail("test not implemented yet")
    
    # added by smart render
    def test_sjzA_R(self):
        #given
        #when
        self.oci.native.sjzA_R(0)
        #then
        self.fail("test not implemented yet")
    
    # added by smart render
    def test_jnzA_R(self):
        #given
        #when
        self.oci.native.jnzA_R(0)
        #then
        self.fail("test not implemented yet")
    
    # added by smart render
    def test_jzA_R(self):
        #given
        #when
        self.oci.native.jzA_R(0)
        #then
        self.fail("test not implemented yet")
    
        
    
    '''
    Output
    '''
        
    def test_outR(self):
        # given
        self.oci.setRegister(3, 0x42)
        # when
        self.oci.native.outR(3)
        # then
        self.assertEqual(self.oci.outputPtr(), 1)
        self.assertEqual(self.oci.outputAt(0), 0x42)
        
        
    def test_outRPtr(self):
        # given
        self.oci.setRegister(2, 0x96)
        self.oci.setRegister(3, 0x42)
        self.oci.setRamAt(0x4296, 0x66)
        # when
        self.oci.native.outRPtr(1)
        # then
        self.assertEqual(self.oci.outputPtr(), 1)
        self.assertEqual(self.oci.outputAt(0), 0x66)
        
        
    def test_outDPtr(self):
        # given
        self.oci.setDataPtr(0x579a)
        self.oci.setRamAt(0x579a, 0x42)
        self.oci.setRamAt(0x579b, 0x96)
        self.oci.setRamAt(0x579c, 0xaa)
        # when
        self.oci.native.outDPtr(3)
        # then
        self.assertEqual(self.oci.outputPtr(), 3)
        self.assertEqual(self.oci.outputAt(0), 0x42)
        self.assertEqual(self.oci.outputAt(1), 0x96)
        self.assertEqual(self.oci.outputAt(2), 0xaa)
        self.assertEqual(self.oci.dataPtr(), 0x579a)
    
    
    # added by smart render
    def test_halt(self):
        #given
        #when
        self.oci.native.halt()
        #then
        self.fail("test not implemented yet")
    
        
     
    
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()