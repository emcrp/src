'''
Unit tests for C++ EMC OCI
'''
import unittest

from emcpp.wrapper.oci.loadstoreoci import LoadStoreOciWrapper


class TestLoadStoreOci(unittest.TestCase):
    
    def setUp(self):
        self.oci = LoadStoreOciWrapper(2 ** 8, 2 ** 8, True)
    
    def test_mov_a_addr(self):
        #given
        self.oci.setA(0x00)
        self.oci.setAddr(0x1234)
        self.oci.setRamAt(0x1234, 0x42)
        #when
        self.oci.native.mov_a_addr()
        #then
        self.assertEqual(self.oci.a(), 0x42)
    
    
    def test_mov_b_addr(self):
        ##given
        self.oci.setB(0x00)
        self.oci.setAddr(0x1234)
        self.oci.setRamAt(0x1234, 0x42)
        #when
        self.oci.native.mov_b_addr()
        #then
        self.assertEqual(self.oci.b(), 0x42)
    
    
    def test_mov_addr_a(self):
        #given
        self.oci.setA(0x42)
        self.oci.setAddr(0x1234)
        self.oci.setRamAt(0x1234, 0x00)
        #when
        self.oci.native.mov_addr_a()
        #then
        self.assertEqual(self.oci.ramAt(0x1234), 0x42)
    
    
    def test_mov_addr_b(self):
        #given
        self.oci.setB(0x42)
        self.oci.setAddr(0x1234)
        self.oci.setRamAt(0x1234, 0x00)
        #when
        self.oci.native.mov_addr_b()
        #then
        self.assertEqual(self.oci.ramAt(0x1234), 0x42)
    
    
    def test_mov_addrl_a(self):
        #given
        self.oci.setA(0x42)
        self.oci.setAddr(0x1234)
        #when
        self.oci.native.mov_addrl_a()
        #then
        self.assertEqual(self.oci.addr(), 0x1242)
    
    
    def test_mov_addrh_a(self):
        #given
        self.oci.setA(0x42)
        self.oci.setAddr(0x1234)
        #when
        self.oci.native.mov_addrh_a()
        #then
        self.assertEqual(self.oci.addr(), 0x4234)
    
    
    def test_jnc_addr(self):
        #given
        self.oci.setCounter(0x4321)
        self.oci.setAddr(0x1234)
        self.oci.setFlags(0x00)
        #when
        self.oci.native.jnc_addr()
        #then
        self.assertEqual(self.oci.counter(), 0x1234)
        
        #given
        self.oci.setCounter(0x4321)
        self.oci.setAddr(0x1234)
        self.oci.setFlags(0x01)
        #when
        self.oci.native.jnc_addr()
        #then
        self.assertEqual(self.oci.counter(), 0x4321)
    
    
    def test_subb(self):
        # given
        self.oci.setA(0x92)
        self.oci.setB(0x57)
        self.oci.setFlags(0x00)
        # when
        self.oci.native.subb()
        # then
        self.assertEqual(self.oci.a(), 0x3B)
        self.assertEqual(self.oci.flags(), 0x00)
        
        # given
        self.oci.setA(0x92)
        self.oci.setB(0x57)
        self.oci.setFlags(0x01)
        # when
        self.oci.native.subb()
        # then
        self.assertEqual(self.oci.a(), 0x3A)
        self.assertEqual(self.oci.flags(), 0x01)
    
    
    def test_cpl_a(self):
        # given
        self.oci.setA(0x49)  # 0100 1001
        # when
        self.oci.native.cpl_a()
        # then
        self.assertEqual(self.oci.a(), 0xB6)  # 1011 0110
    
    
    def test_clr_c(self):
        #given
        self.oci.setFlags(0x01);
        #when
        self.oci.native.clr_c()
        #then
        self.assertEqual(self.oci.flags(), 0x00)
        #given
        self.oci.setFlags(0x00);
        #when
        self.oci.native.clr_c()
        #then
        self.assertEqual(self.oci.flags(), 0x00)
    
    
    def test_cpl_c(self):
        #given
        self.oci.setFlags(0x01);
        #when
        self.oci.native.cpl_c()
        #then
        self.assertEqual(self.oci.flags(), 0x00)
        #given
        self.oci.setFlags(0x00);
        #when
        self.oci.native.cpl_c()
        #then
        self.assertEqual(self.oci.flags(), 0x01)
    
    
    def test_andl(self):
        # given
        self.oci.setA(0x49)  # 0100 1001
        self.oci.setB(0x9c)  # 1001 1100
        # when
        self.oci.native.andl()
        # then
        self.assertEqual(self.oci.a(), 0x08)  # 0000 1000
        
    
    def test_norl(self):
        # given
        self.oci.setA(0x49)  # 0100 1001
        self.oci.setB(0x9c)  # 1001 1100
        # when
        self.oci.native.norl()
        # then
        self.assertEqual(self.oci.a(), 0x22)  # 0010 0010
    
    
    def test_out_a(self):
        #given
        self.oci.setA(0x42)
        #when
        self.oci.native.out_a()
        #then
        self.assertEqual(self.oci.outputPtr(), 1)
        self.assertEqual(self.oci.outputAt(0), 0x42)
    
    

    
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()