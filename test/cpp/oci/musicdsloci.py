'''
Unit tests for C++ EMC OCI
'''
import unittest

from emcpp.wrapper.oci.musicdsloci import MusicDslOciWrapper


class TestMusicDslOci(unittest.TestCase):
    
    def setUp(self):
        self.oci = MusicDslOciWrapper(memSizeInBytes=256, expectedOutputs=256, key=64)
    
    def test_movNote_Next(self):
        #given
        self.oci.setCounter(0x4242)
        self.oci.setRamAt(0x4242, 0x35)
        self.oci.setRamAt(0x4243, 0x36)
        self.oci.setRamAt(0x4244, 0x37)
        #when
        self.oci.native.movNote_Next(2)
        #then
        self.assertEqual(self.oci.register(6), 0x35)
        self.assertEqual(self.oci.register(2), 0x36)
        self.assertEqual(self.oci.register(2), 0x37)
    
    
    def test_movNoteDur_Next(self):
        #given
        self.oci.setCounter(0x4242)
        self.oci.setRamAt(0x4242, 0x35)
        #when
        self.oci.native.movNoteDur_Next(2)
        #then
        self.assertEqual(self.oci.register(2), 0x35)
    
    
    def test_movNotePth_Next(self):
        #given
        self.oci.setCounter(0x4242)
        self.oci.setRamAt(0x4242, 0x35)
        #when
        self.oci.native.movNotePth_Next(2)
        #then
        self.assertEqual(self.oci.register(2), 0x35)
    
    
    def test_movNote_DPtr(self):
        #given
        self.oci.setDataPtr(0x4242)
        self.oci.setRamAt(0x4242, 0x35)
        self.oci.setRamAt(0x4243, 0x36)
        self.oci.setRamAt(0x4244, 0x37)
        #when
        self.oci.native.movNote_DPtr(2)
        #then
        self.assertEqual(self.oci.register(6), 0x35)
        self.assertEqual(self.oci.register(2), 0x36)
        self.assertEqual(self.oci.register(2), 0x37)
    
    
    def test_movNoteDur_DPtr(self):
        #given
        self.oci.setDataPtr(0x4242)
        self.oci.setRamAt(0x4242, 0x35)
        #when
        self.oci.native.movNoteDur_DPtr(2)
        #then
        self.assertEqual(self.oci.register(2), 0x35)
    
    
    def test_movNotePth_DPtr(self):
        #given
        self.oci.setDataPtr(0x4242)
        self.oci.setRamAt(0x4242, 0x35)
        #when
        self.oci.native.movNotePth_DPtr(2)
        #then
        self.assertEqual(self.oci.register(2), 0x35)
    
    
    def test_movDPtr_Note(self):
        #given
        self.oci.setDataPtr(0x4242)
        self.oci.setRegister(6, 0x35)
        self.oci.setRegister(2, 0x36)
        self.oci.setRegister(2, 0x37)
        #when
        self.oci.native.movNote_DPtr(2)
        #then
        self.assertEqual(self.oci.ramAt(0x4242), 0x35)
        self.assertEqual(self.oci.ramAt(0x4243), 0x36)
        self.assertEqual(self.oci.ramAt(0x4244), 0x37)
    
    
    def test_movDPtr_NoteDur(self):
        #given
        self.oci.setDataPtr(0x4242)
        self.oci.setRegister(2, 0x35)
        #when
        self.oci.native.movDPtr_NoteDur(2)
        #then
        self.assertEqual(self.oci.ramAt(0x4242), 0x35)
    
    
    def test_movDPtr_NotePth(self):
        #given
        self.oci.setDataPtr(0x4242)
        self.oci.setRegister(2, 0x35)
        #when
        self.oci.native.movDPtr_NotePth(2)
        #then
        self.assertEqual(self.oci.ramAt(0x4242), 0x35)
    
    
    def test_movD_Next(self):
        #given
        self.oci.setCounter(0x4242)
        self.oci.setRamAt(0x4243, 0x35)
        self.oci.setRamAt(0x4242, 0x36)
        #when
        self.oci.native.movD_Next()
        #then
        self.assertEqual(self.oci.dataPtr(), 0x3536)
    
    
    def test_movDH_Next(self):
        #given
        self.oci.setCounter(0x4242)
        self.oci.setRamAt(0x4242, 0x35)
        self.oci.setDataPtr(0xaaaa)
        #when
        self.oci.native.movDH_Next()
        #then
        self.assertEqual(self.oci.dataPtr(), 0x35aa)
    
    
    def test_movDL_Next(self):
        #given
        self.oci.setCounter(0x4242)
        self.oci.setRamAt(0x4242, 0x35)
        self.oci.setDataPtr(0xaaaa)
        #when
        self.oci.native.movDL_Next()
        #then
        self.assertEqual(self.oci.dataPtr(), 0xaa35)
    
    
    def test_movDPtr_Next(self):
        #given
        self.oci.setCounter(0x4242)
        self.oci.setRamAt(0x4242, 0x35)
        self.oci.setDataPtr(0xaaaa)
        #when
        self.oci.native.movDPtr_Next()
        #then
        self.assertEqual(self.oci.ramAt(0xaaaa), 0x35)
    
    
    def test_movFlag_Next(self):
        #given
        self.oci.setCounter(0x4242)
        self.oci.setRamAt(0x4242, 0xff)
        self.oci.setFlag(0x00)
        #when
        self.oci.native.movFlag_Next()
        #then
        self.assertEqual(self.oci.flag(), 0x01)
    
    
    def test_movFlag_DPtr(self):
        #given
        self.oci.setDataPtr(0x4242)
        self.oci.setRamAt(0x4242, 0xff)
        self.oci.setFlag(0x00)
        #when
        self.oci.native.movFlag_DPtr()
        #then
        self.assertEqual(self.oci.flag(), 0x01)
    
    
    def test_setFlag(self):
        #given
        self.oci.setFlag(0x00)
        #when
        self.oci.native.setFlag()
        #then
        self.assertEqual(self.oci.flag(), 0x01)
    
    
    def test_unsetFlag(self):
        #given
        self.oci.setFlag(0x01)
        #when
        self.oci.native.unsetFlag()
        #then
        self.assertEqual(self.oci.flag(), 0x00)
    
    
    def test_invertFlag(self):
        self.oci.setFlag(0x00)
        self.oci.native.invertFlag()
        self.assertEqual(self.oci.flag(), 0x01)
        self.oci.setFlag(0x01)
        self.oci.native.invertFlag()
        self.assertEqual(self.oci.flag(), 0x00)
    
    
    def test_reset_Dur(self):
        #given
        self.oci.setRegister(2, 0x08)
        #when
        self.oci.native.reset_Dur(2)
        #then
        self.assertEqual(self.oci.register(2), 0x02)
    
    
    def test_reset_Pitch(self):
        #given
        self.oci.setRegister(2, 0x08)
        #when
        self.oci.native.reset_Pitch(2)
        #then
        self.assertEqual(self.oci.register(2), 0x40)
    
    
    def test_inc_DurDbl(self):
        #given
        self.oci.setRegister(1, 0x02)
        self.oci.setRegister(2, 0x04)
        self.oci.setRegister(3, 0x08)
        #when
        self.oci.native.inc_DurDbl(0, 1)
        self.oci.native.inc_DurDbl(0, 2)
        self.oci.native.inc_DurDbl(0, 3)
        #then
        self.assertEqual(self.oci.register(1), 0x04)
        self.assertEqual(self.oci.register(2), 0x08)
        self.assertEqual(self.oci.register(3), 0x08)
    
    
    def test_inc_DurDot(self):
        #given
        self.oci.setRegister(1, 0x02)
        self.oci.setRegister(2, 0x03)
        self.oci.setRegister(3, 0x08)
        #when
        self.oci.native.inc_DurDot(0, 1)
        self.oci.native.inc_DurDot(0, 2)
        self.oci.native.inc_DurDot(0, 3)
        #then
        self.assertEqual(self.oci.register(1), 0x03)
        self.assertEqual(self.oci.register(2), 0x02)
        self.assertEqual(self.oci.register(3), 0x0C)
    
    
    def test_dec_DurHalve(self):
        #given
        self.oci.setRegister(1, 0x2a) # *001*01010
        self.oci.setRegister(2, 0x6a) # *011*01010
        self.oci.setRegister(3, 0xca) # *110*01010
        #when
        self.oci.native.dec_DurHalve(0, 1)
        self.oci.native.dec_DurHalve(0, 2)
        self.oci.native.dec_DurHalve(0, 3)
        #then
        self.assertEqual(self.oci.register(1), 0x0a) # *000*01010
        self.assertEqual(self.oci.register(2), 0x2a) # *001*01010
        self.assertEqual(self.oci.register(3), 0x8a) # *100*01010
    
    
    def test_inc_Pitch2(self):
        #given
        self.oci.setDiatonic(False)
        self.oci.setKey(11)
        self.oci.setIsMajor(True)
        self.oci.setRegister(1, 64)
        self.oci.setRegister(2, 66)
        self.oci.setRegister(3, 68)
        #when
        self.oci.native.inc_Pitch2(0, 1)
        self.oci.native.inc_Pitch2(0, 2)
        self.oci.native.inc_Pitch2(0, 3)
        #then
        self.assertEqual(self.oci.register(1), 66)
        self.assertEqual(self.oci.register(2), 68)
        self.assertEqual(self.oci.register(3), 69)
        
    
    def test_inc_Pitch2_diatonic(self):
        #given
        self.oci.setDiatonic(True)
        self.oci.setKey(11)
        self.oci.setIsMajor(True)
        self.oci.setRegister(1, 64)
        self.oci.setRegister(2, 66)
        self.oci.setRegister(3, 68)
        #when
        self.oci.native.inc_Pitch2(0, 1)
        self.oci.native.inc_Pitch2(0, 2)
        self.oci.native.inc_Pitch2(0, 3)
        #then
        self.assertEqual(self.oci.register(1), 65)
        self.assertEqual(self.oci.register(2), 67)
        self.assertEqual(self.oci.register(3), 69)
    
    
    def test_inc_Pitch3(self):
        #given
        self.oci.setDiatonic(False)
        self.oci.setKey(11)
        self.oci.setIsMajor(True)
        self.oci.setRegister(1, 64)
        self.oci.setRegister(2, 66)
        self.oci.setRegister(3, 68)
        #when
        self.oci.native.inc_Pitch3(0, 1)
        self.oci.native.inc_Pitch3(0, 2)
        self.oci.native.inc_Pitch3(0, 3)
        #then
        self.assertEqual(self.oci.register(1), 68)
        self.assertEqual(self.oci.register(2), 69)
        self.assertEqual(self.oci.register(3), 71)
        
    
    def test_inc_Pitch3_diatonic(self):
        #given
        self.oci.setDiatonic(True)
        self.oci.setKey(11)
        self.oci.setIsMajor(True)
        self.oci.setRegister(1, 64)
        self.oci.setRegister(2, 66)
        self.oci.setRegister(3, 68)
        #when
        self.oci.native.inc_Pitch3(0, 1)
        self.oci.native.inc_Pitch3(0, 2)
        self.oci.native.inc_Pitch3(0, 3)
        #then
        self.assertEqual(self.oci.register(1), 66)
        self.assertEqual(self.oci.register(2), 68)
        self.assertEqual(self.oci.register(3), 70)
        
    
    def test_inc_Pitch4(self):
        #given
        self.oci.setDiatonic(False)
        self.oci.setKey(11)
        self.oci.setIsMajor(True)
        self.oci.setRegister(1, 64)
        self.oci.setRegister(2, 66)
        self.oci.setRegister(3, 68)
        #when
        self.oci.native.inc_Pitch4(0, 1)
        self.oci.native.inc_Pitch4(0, 2)
        self.oci.native.inc_Pitch4(0, 3)
        #then
        self.assertEqual(self.oci.register(1), 69)
        self.assertEqual(self.oci.register(2), 71)
        self.assertEqual(self.oci.register(3), 73)
        
    
    def test_inc_Pitch4_diatonic(self):
        #given
        self.oci.setDiatonic(True)
        self.oci.setKey(11)
        self.oci.setIsMajor(True)
        self.oci.setRegister(1, 64)
        self.oci.setRegister(2, 66)
        self.oci.setRegister(3, 68)
        #when
        self.oci.native.inc_Pitch4(0, 1)
        self.oci.native.inc_Pitch4(0, 2)
        self.oci.native.inc_Pitch4(0, 3)
        #then
        self.assertEqual(self.oci.register(1), 67)
        self.assertEqual(self.oci.register(2), 69)
        self.assertEqual(self.oci.register(3), 71)
        
    
    def test_inc_Pitch5(self):
        #given
        self.oci.setDiatonic(False)
        self.oci.setKey(11)
        self.oci.setIsMajor(True)
        self.oci.setRegister(1, 64)
        self.oci.setRegister(2, 66)
        self.oci.setRegister(3, 68)
        #when
        self.oci.native.inc_Pitch5(0, 1)
        self.oci.native.inc_Pitch5(0, 2)
        self.oci.native.inc_Pitch5(0, 3)
        #then
        self.assertEqual(self.oci.register(1), 71)
        self.assertEqual(self.oci.register(2), 73)
        self.assertEqual(self.oci.register(3), 75)
        
    
    def test_inc_Pitch5_diatonic(self):
        #given
        self.oci.setDiatonic(True)
        self.oci.setKey(11)
        self.oci.setIsMajor(True)
        self.oci.setRegister(1, 64)
        self.oci.setRegister(2, 66)
        self.oci.setRegister(3, 68)
        #when
        self.oci.native.inc_Pitch5(0, 1)
        self.oci.native.inc_Pitch5(0, 2)
        self.oci.native.inc_Pitch5(0, 3)
        #then
        self.assertEqual(self.oci.register(1), 68)
        self.assertEqual(self.oci.register(2), 70)
        self.assertEqual(self.oci.register(3), 72)
        
    
    def test_inc_Pitch8(self):
        #given
        self.oci.setDiatonic(False)
        self.oci.setKey(11)
        self.oci.setIsMajor(True)
        self.oci.setRegister(1, 64)
        self.oci.setRegister(2, 66)
        self.oci.setRegister(3, 68)
        #when
        self.oci.native.inc_Pitch8(0, 1)
        self.oci.native.inc_Pitch8(0, 2)
        self.oci.native.inc_Pitch8(0, 3)
        #then
        self.assertEqual(self.oci.register(1), 76)
        self.assertEqual(self.oci.register(2), 78)
        self.assertEqual(self.oci.register(3), 80)
        
    
    def test_inc_Pitch8_diatonic(self):
        #given
        self.oci.setDiatonic(True)
        self.oci.setKey(11)
        self.oci.setIsMajor(True)
        self.oci.setRegister(1, 64)
        self.oci.setRegister(2, 66)
        self.oci.setRegister(3, 68)
        #when
        self.oci.native.inc_Pitch8(0, 1)
        self.oci.native.inc_Pitch8(0, 2)
        self.oci.native.inc_Pitch8(0, 3)
        #then
        self.assertEqual(self.oci.register(1), 71)
        self.assertEqual(self.oci.register(2), 73)
        self.assertEqual(self.oci.register(3), 75)
    
    
    def test_dec_Pitch2(self):
        #given
        self.oci.setDiatonic(False)
        self.oci.setKey(11)
        self.oci.setIsMajor(True)
        self.oci.setRegister(1, 64)
        self.oci.setRegister(2, 66)
        self.oci.setRegister(3, 68)
        #when
        self.oci.native.dec_Pitch2(0, 1)
        self.oci.native.dec_Pitch2(0, 2)
        self.oci.native.dec_Pitch2(0, 3)
        #then
        self.assertEqual(self.oci.register(1), 63)
        self.assertEqual(self.oci.register(2), 64)
        self.assertEqual(self.oci.register(3), 66)
        
    
    def test_dec_Pitch2_diatonic(self):
        #given
        self.oci.setDiatonic(True)
        self.oci.setKey(11)
        self.oci.setIsMajor(True)
        self.oci.setRegister(1, 64)
        self.oci.setRegister(2, 66)
        self.oci.setRegister(3, 68)
        #when
        self.oci.native.dec_Pitch2(0, 1)
        self.oci.native.dec_Pitch2(0, 2)
        self.oci.native.dec_Pitch2(0, 3)
        #then
        self.assertEqual(self.oci.register(1), 63)
        self.assertEqual(self.oci.register(2), 65)
        self.assertEqual(self.oci.register(3), 67)
    
    
    def test_dec_Pitch3(self):
        #given
        self.oci.setDiatonic(False)
        self.oci.setKey(11)
        self.oci.setIsMajor(True)
        self.oci.setRegister(1, 64)
        self.oci.setRegister(2, 66)
        self.oci.setRegister(3, 68)
        #when
        self.oci.native.dec_Pitch3(0, 1)
        self.oci.native.dec_Pitch3(0, 2)
        self.oci.native.dec_Pitch3(0, 3)
        #then
        self.assertEqual(self.oci.register(1), 61)
        self.assertEqual(self.oci.register(2), 63)
        self.assertEqual(self.oci.register(3), 64)
        
    
    def test_dec_Pitch3_diatonic(self):
        #given
        self.oci.setDiatonic(True)
        self.oci.setKey(11)
        self.oci.setIsMajor(True)
        self.oci.setRegister(1, 64)
        self.oci.setRegister(2, 66)
        self.oci.setRegister(3, 68)
        #when
        self.oci.native.dec_Pitch3(0, 1)
        self.oci.native.dec_Pitch3(0, 2)
        self.oci.native.dec_Pitch3(0, 3)
        #then
        self.assertEqual(self.oci.register(1), 62)
        self.assertEqual(self.oci.register(2), 64)
        self.assertEqual(self.oci.register(3), 66)
        
    
    def test_dec_Pitch4(self):
        #given
        self.oci.setDiatonic(False)
        self.oci.setKey(11)
        self.oci.setIsMajor(True)
        self.oci.setRegister(1, 64)
        self.oci.setRegister(2, 66)
        self.oci.setRegister(3, 68)
        #when
        self.oci.native.dec_Pitch4(0, 1)
        self.oci.native.dec_Pitch4(0, 2)
        self.oci.native.dec_Pitch4(0, 3)
        #then
        self.assertEqual(self.oci.register(1), 59)
        self.assertEqual(self.oci.register(2), 61)
        self.assertEqual(self.oci.register(3), 63)
        
    
    def test_dec_Pitch4_diatonic(self):
        #given
        self.oci.setDiatonic(True)
        self.oci.setKey(11)
        self.oci.setIsMajor(True)
        self.oci.setRegister(1, 64)
        self.oci.setRegister(2, 66)
        self.oci.setRegister(3, 68)
        #when
        self.oci.native.dec_Pitch4(0, 1)
        self.oci.native.dec_Pitch4(0, 2)
        self.oci.native.dec_Pitch4(0, 3)
        #then
        self.assertEqual(self.oci.register(1), 61)
        self.assertEqual(self.oci.register(2), 63)
        self.assertEqual(self.oci.register(3), 65)
        
    
    def test_dec_Pitch5(self):
        #given
        self.oci.setDiatonic(False)
        self.oci.setKey(11)
        self.oci.setIsMajor(True)
        self.oci.setRegister(1, 64)
        self.oci.setRegister(2, 66)
        self.oci.setRegister(3, 68)
        #when
        self.oci.native.dec_Pitch5(0, 1)
        self.oci.native.dec_Pitch5(0, 2)
        self.oci.native.dec_Pitch5(0, 3)
        #then
        self.assertEqual(self.oci.register(1), 57)
        self.assertEqual(self.oci.register(2), 59)
        self.assertEqual(self.oci.register(3), 61)
        
    
    def test_dec_Pitch5_diatonic(self):
        #given
        self.oci.setDiatonic(True)
        self.oci.setKey(11)
        self.oci.setIsMajor(True)
        self.oci.setRegister(1, 64)
        self.oci.setRegister(2, 66)
        self.oci.setRegister(3, 68)
        #when
        self.oci.native.dec_Pitch5(0, 1)
        self.oci.native.dec_Pitch5(0, 2)
        self.oci.native.dec_Pitch5(0, 3)
        #then
        self.assertEqual(self.oci.register(1), 60)
        self.assertEqual(self.oci.register(2), 62)
        self.assertEqual(self.oci.register(3), 64)
        
    
    def test_dec_Pitch8(self):
        #given
        self.oci.setDiatonic(False)
        self.oci.setKey(11)
        self.oci.setIsMajor(True)
        self.oci.setRegister(1, 64)
        self.oci.setRegister(2, 66)
        self.oci.setRegister(3, 68)
        #when
        self.oci.native.dec_Pitch8(0, 1)
        self.oci.native.dec_Pitch8(0, 2)
        self.oci.native.dec_Pitch8(0, 3)
        #then
        self.assertEqual(self.oci.register(1), 52)
        self.assertEqual(self.oci.register(2), 54)
        self.assertEqual(self.oci.register(3), 56)
        
    
    def test_dec_Pitch8_diatonic(self):
        #given
        self.oci.setDiatonic(True)
        self.oci.setKey(11)
        self.oci.setIsMajor(True)
        self.oci.setRegister(1, 64)
        self.oci.setRegister(2, 66)
        self.oci.setRegister(3, 68)
        #when
        self.oci.native.dec_Pitch8(0, 1)
        self.oci.native.dec_Pitch8(0, 2)
        self.oci.native.dec_Pitch8(0, 3)
        #then
        self.assertEqual(self.oci.register(1), 57)
        self.assertEqual(self.oci.register(2), 59)
        self.assertEqual(self.oci.register(3), 61)
    
   
    def test_inc_D(self):
        #given
        #when
        self.oci.native.inc_D()
        #then
        self.fail("test not implemented yet")
    
    
    def test_dec_D(self):
        #given
        #when
        self.oci.native.dec_D()
        #then
        self.fail("test not implemented yet")
    
    
    def test_jmpD(self):
        #given
        #when
        self.oci.native.jmpD()
        #then
        self.fail("test not implemented yet")
    
    
    def test_sjmpNext(self):
        #given
        #when
        self.oci.native.sjmpNext()
        #then
        self.fail("test not implemented yet")
    
    
    def test_jmpNext(self):
        #given
        #when
        self.oci.native.jmpNext()
        #then
        self.fail("test not implemented yet")
    
    
    def test_sjmpDH(self):
        #given
        #when
        self.oci.native.sjmpDH()
        #then
        self.fail("test not implemented yet")
    
    
    def test_sjmpDL(self):
        #given
        #when
        self.oci.native.sjmpDL()
        #then
        self.fail("test not implemented yet")
    
    
    def test_jf_D(self):
        #given
        #when
        self.oci.native.jf_D()
        #then
        self.fail("test not implemented yet")
    
    
    def test_sjf_Next(self):
        #given
        #when
        self.oci.native.sjf_Next()
        #then
        self.fail("test not implemented yet")
    
    
    def test_jf_Next(self):
        #given
        #when
        self.oci.native.jf_Next()
        #then
        self.fail("test not implemented yet")
    
    
    def test_sjf_DH(self):
        #given
        #when
        self.oci.native.sjf_DH()
        #then
        self.fail("test not implemented yet")
    
    
    def test_sjf_DL(self):
        #given
        #when
        self.oci.native.sjf_DL()
        #then
        self.fail("test not implemented yet")
    
    
    def test_outNote(self):
        #given
        #when
        self.oci.native.outNote(0)
        #then
        self.fail("test not implemented yet")
    
    

    
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()