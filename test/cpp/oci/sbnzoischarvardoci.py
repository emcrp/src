'''
Unit tests for C++ EMC OCI
'''
import unittest

from emcpp.wrapper.oci.sbnzoischarvardoci import SbnzOiscHarvardOciWrapper


class TestSbnzOiscHarvardOci(unittest.TestCase):
    
    def test_sbnz(self):
        # 1. no branch, no out
        self.oci = SbnzOiscHarvardOciWrapper(2 ** 16, 2 ** 8, True)
        
        #given
        self.oci.setCounter(0x1545)
        self.oci.setRomAt(0x1545, 0x1234)
        self.oci.setRomAt(0x1546, 0x5678)
        self.oci.setRomAt(0x1547, 0x90ab)
        self.oci.setRomAt(0x1548, 0xcdef)
        
        self.oci.setRamAt(0x1234, 0x1111)
        self.oci.setRamAt(0x5678, 0x1111)
        self.oci.setRamAt(0x90ab, 0x8000)
        #when
        self.oci.native.sbnz()
        #then
        self.assertEqual(self.oci.ramAt(0x90ab), 0x0000)
        self.assertEqual(self.oci.counter(), 0x1549)
        self.assertEqual(self.oci.outputPtr(), 0)
        
        # 2. branch, no out
        self.oci = SbnzOiscHarvardOciWrapper(2 ** 16, 2 ** 8, True)
        
        #given
        self.oci.setCounter(0x1545)
        self.oci.setRomAt(0x1545, 0x1234)
        self.oci.setRomAt(0x1546, 0x5678)
        self.oci.setRomAt(0x1547, 0x90ab)
        self.oci.setRomAt(0x1548, 0xcdef)
        
        self.oci.setRamAt(0x1234, 0x1111)
        self.oci.setRamAt(0x5678, 0x1110)
        self.oci.setRamAt(0x90ab, 0x8000)
        #when
        self.oci.native.sbnz()
        #then
        self.assertEqual(self.oci.ramAt(0x90ab), 0x0001)
        self.assertEqual(self.oci.counter(), 0xcdef)
        self.assertEqual(self.oci.outputPtr(), 0)
        
        # 3. branch, out
        self.oci = SbnzOiscHarvardOciWrapper(2 ** 16, 2 ** 8, True)
        
        #given
        self.oci.setCounter(0x1545)
        self.oci.setRomAt(0x1545, 0x1234)
        self.oci.setRomAt(0x1546, 0x5678)
        self.oci.setRomAt(0x1547, 0x90ab)
        self.oci.setRomAt(0x1548, 0xcdef)
        
        self.oci.setRamAt(0x1234, 0x1110)
        self.oci.setRamAt(0x5678, 0x1111)
        self.oci.setRamAt(0x90ab, 0x8000)
        #when
        self.oci.native.sbnz()
        #then
        self.assertEqual(self.oci.ramAt(0x90ab), 0xffff)
        self.assertEqual(self.oci.counter(), 0xcdef)
        self.assertEqual(self.oci.outputPtr(), 2)
        self.assertEqual(self.oci.outputAt(1), 0x22)
        self.assertEqual(self.oci.outputAt(0), 0x21)
        
        # 4. branch, out + overflow
        self.oci = SbnzOiscHarvardOciWrapper(2 ** 16, 2 ** 8, True)
        
        #given
        self.oci.setCounter(0x1545)
        self.oci.setRomAt(0x1545, 0x1234)
        self.oci.setRomAt(0x1546, 0x5678)
        self.oci.setRomAt(0x1547, 0x90ab)
        self.oci.setRomAt(0x1548, 0xcdef)
        
        self.oci.setRamAt(0x1234, 0x8000)
        self.oci.setRamAt(0x5678, 0x8001)
        self.oci.setRamAt(0x90ab, 0x8000)
        #when
        self.oci.native.sbnz()
        #then
        self.assertEqual(self.oci.ramAt(0x90ab), 0xffff)
        self.assertEqual(self.oci.counter(), 0xcdef)
        self.assertEqual(self.oci.outputPtr(), 2)
        self.assertEqual(self.oci.outputAt(1), 0x00)
        self.assertEqual(self.oci.outputAt(0), 0x01)
    
    
    
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()