'''
Unit tests for C++ EMC OCI
'''
import unittest

from emcpp.wrapper.oci.stackharvardoci import StackHarvardOciWrapper


class TestStackHarvardOci(unittest.TestCase):
    
    def setUp(self):
        self.oci = StackHarvardOciWrapper(2 ** 8, 2 ** 8, True)
    
    def test_dup(self):
        #given
        self.oci.setSp(0x42)
        self.oci.setStackAt(0x42, 0x4259)
        self.oci.setStackAt(0x43, 0x9612)
        #when
        self.oci.native.dup()
        #then
        self.assertEqual(self.oci.stackAt(0x43), 0x4259)
        self.assertEqual(self.oci.sp(), 0x43)
    
    
    def test_one(self):
        #given
        self.oci.setSp(0x42)
        self.oci.setStackAt(0x42, 0x5B85) # 0101 1011 1000 0101
        #when
        self.oci.native.one()
        #then
        self.assertEqual(self.oci.stackAt(0x42), 0xB70B) # 1011 0111 0000 1011
    
    
    def test_zero(self):
        #given
        self.oci.setSp(0x42)
        self.oci.setStackAt(0x42, 0x5B85) # 0101 1011 1000 0101
        #when
        self.oci.native.zero()
        #then
        self.assertEqual(self.oci.stackAt(0x42), 0xB70A) # 1011 0110 0000 1010
    
    
    def test_load(self):
        #given
        self.oci.setSp(0x4259)
        self.oci.setStackAt(0x4259, 0x9642)
        self.oci.setStackAt(0x9642, 0xBB00)
        #when
        self.oci.native.load()
        #then
        self.assertEqual(self.oci.stackAt(0x4259), 0xBB00)
    
    
    def test_pop(self):
        #given
        self.oci.setSp(0x4259)
        self.oci.setStackAt(0x4259, 0x9642)
        self.oci.setStackAt(0x4258, 0xBB00)
        self.oci.setStackAt(0xBB00, 0x1234)
        #when
        self.oci.native.pop()
        #then
        self.assertEqual(self.oci.stackAt(0xBB00), 0x9642)
        self.assertEqual(self.oci.sp(), 0x4257)
    
    
    def test_sub(self):
        #given
        self.oci.setSp(0x42)
        self.oci.setStackAt(0x42, 0x1265)
        self.oci.setStackAt(0x41, 0x1296)
        #when
        self.oci.native.sub()
        #then
        self.assertEqual(self.oci.stackAt(0x41), 0x0031)
        self.assertEqual(self.oci.sp(), 0x41)
    
    
    def test_jpos(self):
        #given
        self.oci.setSp(0x42)
        self.oci.setStackAt(0x42, 0x65)
        self.oci.setStackAt(0x41, 0x01) # 0000 0000 0000 0001
        self.oci.setCounter(0xAA)
        #when
        self.oci.native.jpos()
        #then
        self.assertEqual(self.oci.counter(), 0x65)
        self.assertEqual(self.oci.sp(), 0x40)
        
        #given
        self.oci.setSp(0x42)
        self.oci.setStackAt(0x42, 0x65)
        self.oci.setStackAt(0x41, 0x8001) # 1000 0000 0000 0001
        self.oci.setCounter(0xAA)
        #when
        self.oci.native.jpos()
        #then
        self.assertEqual(self.oci.counter(), 0xAA)
        self.assertEqual(self.oci.sp(), 0x40)
    
    
    def test_out(self):
        # given
        self.oci.setSp(0x4267)
        self.oci.setStackAt(0x4267, 0xAA34)
        # when
        self.oci.native.out()
        # then
        self.assertEqual(self.oci.outputPtr(), 2)
        self.assertEqual(self.oci.outputAt(1), 0xAA)
        self.assertEqual(self.oci.outputAt(0), 0x34)
    
    

    
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()