'''
Unit tests for C++ EMC OCI
'''
import unittest

from emcpp.wrapper.oci.testoci import TestOciWrapper


class TestTestOci(unittest.TestCase):
    
    def setUp(self):
        self.oci = TestOciWrapper(2 ** 8, 2 ** 8, True)
    
    
    def test_a(self):
        #given
        #when
        self.oci.native.a()
        #then
        self.fail("test not implemented yet")
    
    
    def test_b(self):
        #given
        #when
        self.oci.native.b(0)
        #then
        self.fail("test not implemented yet")
    
    
    def test_c(self):
        #given
        #when
        self.oci.native.c()
        #then
        self.fail("test not implemented yet")
    
    
    def test_d(self):
        #given
        #when
        self.oci.native.d()
        #then
        self.fail("test not implemented yet")
    
    
    def test_e(self):
        #given
        #when
        self.oci.native.e()
        #then
        self.fail("test not implemented yet")
    
    
    def test_f(self):
        #given
        #when
        self.oci.native.f(0)
        #then
        self.fail("test not implemented yet")
    
    
    def test_g(self):
        #given
        #when
        self.oci.native.g()
        #then
        self.fail("test not implemented yet")
    
    
    def test_h(self):
        #given
        #when
        self.oci.native.h(0)
        #then
        self.fail("test not implemented yet")
    
    
    
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()