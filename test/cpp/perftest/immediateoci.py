'''
Performance test for test op-code interpreter in C++.
Shows that interpreter can process up to 200000 commands a second.

'''

import time
import unittest

from numpy import uint8, array
from numpy.random.mtrand import rand

from emcpp.wrapper.oci.immediateoci import ImmediateOciWrapper


class EmcOpCodeInterpreterPerfTest(unittest.TestCase):


    def testOciPerformance(self):
        startTime = time.time()

        oci = ImmediateOciWrapper(100000, 2500, True)
        s = array(rand(oci.geneticStringSize)*256, dtype=uint8)
        oci.native.setFromGeneticString(s)
        
        numBytes = 1000000
        
        oci.native.interpretN(numBytes)
        os = oci.outputPtr()
            
        timeMs = (time.time() - startTime) * 1000.0
        print "Time needed %.6f ms" %(timeMs)
        print "Average time %.6f ms" %(timeMs / numBytes)
        print "Bytes per second: %.3f" %(numBytes / timeMs * 1000)
        print "Output size: %d" %(os)



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()