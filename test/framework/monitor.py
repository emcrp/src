import unittest


from monitoring.monitor import request, wire, expose
from test.framework.monitortestdata import ComplexObj
from time import sleep


@unittest.skip
class TestSerializeUtils(unittest.TestCase):

	@classmethod
	def setUpClass(self):
		self.testObj = {}
		wire('testObj', self.testObj)
		expose()
		sleep(1)
		
	def setUp(self):
		self.complexDict = {
					  'key1': 'value1',
					  'key2': {
						  'subkey1': 'subvalue1',
						  'subkey2': 'subvalue2'
					  },
					  'key3': [{
						  'listsubkey1': 'listsubvalue1',
					  }, {
						  'listsubkey2': 'listsubvalue2',
						  'listsubkey3': 'listsubvalue3'
					  }]
				  }
		self.complexObj = ComplexObj()
		self.testObj['complexDict'] = self.complexDict
		self.testObj['complexObj'] = self.complexObj
	
		
	
	def testRead(self):
		urlsToResults = {
					'testObj.complexDict'							: self.complexDict,
					'testObj.complexDict.key1'						: self.complexDict['key1'],
					'testObj.complexDict.key2'						: self.complexDict['key2'],
					'testObj.complexDict.key2.subkey1'				: self.complexDict['key2']['subkey1'],
					'testObj.complexDict.key2.subkey2'				: self.complexDict['key2']['subkey2'],
					'testObj.complexDict.key3'						: self.complexDict['key3'],
					'testObj.complexDict.key3.0'					: self.complexDict['key3'][0],
					'testObj.complexDict.key3.0.listsubkey1'		: self.complexDict['key3'][0]['listsubkey1'],
					'testObj.complexDict.key3.1'					: self.complexDict['key3'][1],
					'testObj.complexDict.key3.1.listsubkey2'		: self.complexDict['key3'][1]['listsubkey2'],
					'testObj.complexDict.key3.1.listsubkey3'		: self.complexDict['key3'][1]['listsubkey3'],
					'testObj.complexObj'							: self.complexObj,
					'testObj.complexObj.intMember'					: self.complexObj.intMember,
					'testObj.complexObj.floatMember'				: self.complexObj.floatMember,
					'testObj.complexObj.innerMember'				: self.complexObj.innerMember,
					'testObj.complexObj.innerMember.innerStrMember'	: self.complexObj.innerMember.innerStrMember
					}
		for url, expResult in urlsToResults.iteritems():
			result = request(url)
			self.assertEquals(str(result), str(expResult))
			
	
	def testCall(self):
		self.assertEquals(84.42,			request('testObj.complexObj.method1', method = 'POST'))
		self.assertEquals('method235hello', request('testObj.complexObj.method2', method = 'POST', intParam = 35, strParam = 'hello'))
		self.assertEquals(142,			  request('testObj.complexObj.method3', method = 'POST', toAdd = 100))
		self.assertEquals(None,			 request('testObj.complexObj.method4', method = 'POST'))
		
		
		
	def testCreate(self):
		request('testObj.complexDict.key3', method = 'POST', newlistsubkey = 'yomamma')
		self.assertEquals({'newlistsubkey': 'yomamma'}, request('testObj.complexDict.key3.2'))
		self.assertEquals('yomamma',					request('testObj.complexDict.key3.2.newlistsubkey'))
	
	
	def testDelete(self):
		request('testObj.complexDict.key2.subkey1', method = 'DELETE')
		request('testObj.complexDict.key2.subkey2', method = 'DELETE')
		self.assertEquals({}, request('testObj.complexDict.key2'))
		request('testObj.complexDict.key3.0', method = 'DELETE')
		request('testObj.complexDict.key3.0', method = 'DELETE')
		self.assertEquals([], request('testObj.complexDict.key3'))
		request('testObj.complexDict.key1', method = 'DELETE')
		request('testObj.complexDict.key2', method = 'DELETE')
		request('testObj.complexDict.key3', method = 'DELETE')
		self.assertEquals({}, request('testObj.complexDict'))
		
		
	def testUpdate(self):
		request('testObj.complexDict.key2', method = 'PUT', subkey1 = 'newvalue1', newsubkey = 'newvalue2')
		self.assertEquals('newvalue1', request('testObj.complexDict.key2.subkey1'))
		self.assertEquals('newvalue2', request('testObj.complexDict.key2.newsubkey'))
		request('testObj.complexObj', method = 'PUT', intMember = 666)
		self.assertEquals(666, request('testObj.complexObj.intMember'))
		
		
		
if __name__ == "__main__":
	#import sys;sys.argv = ['', 'Test.testName']
	unittest.main()
	
