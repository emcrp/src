class ComplexInnerObj(object):
    def __init__(self, innerStrMember = 'abc'):
        self.innerStrMember = 'abc'
    def __repr__(self):
        return 'ComplexInnerObj[innerStrMember=' + self.innerStrMember + ']'

class ComplexObj(object):
    def __init__(self, intMember = 42, floatMember = 42.42, innerMember = ComplexInnerObj()):
        self.intMember = intMember
        self.floatMember = floatMember
        self.innerMember = innerMember
    def method1(self):
        return self.intMember + self.floatMember
    def method2(self, intParam = 10, strParam = 'abc'):
        return 'method2' + str(intParam) + strParam
    def method3(self, toAdd):
        return self.intMember + toAdd
    def method4(self):
        pass
    def __repr__(self):
        return 'ComplexObj[intMember=' + str(self.intMember) + \
              ',floatMember=' + str(self.floatMember) + ',innerMember=' + str(self.innerMember) + ']'