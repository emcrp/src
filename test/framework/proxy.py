import unittest


from monitoring.monitor import wire, expose
from monitoring.proxy import Proxy
from test.framework.monitortestdata import ComplexObj
from time import sleep


class TestProxy(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.testObj = {}
        wire('testObj', self.testObj)
        expose()
        sleep(1)
        
    def setUp(self):
        self.complexDict = {
                      'key1': 'value1',
                      'key2': {
                          'subkey1': 'subvalue1',
                          'subkey2': 'subvalue2'
                      },
                      'key3': [{
                          'listsubkey1': 'listsubvalue1',
                      }, {
                          'listsubkey2': 'listsubvalue2',
                          'listsubkey3': 'listsubvalue3'
                      }]
                  }
        self.complexObj = ComplexObj()
        self.testObj['complexDict'] = self.complexDict
        self.testObj['complexObj'] = self.complexObj
        self.complexObjProxy = Proxy(namespace = 'testObj.complexObj')
        self.complexDictProxy = Proxy(namespace = 'testObj.complexDict')
        
    
    def testRead(self):
        self.assertEquals(self.complexDict['key1'], self.complexDictProxy['key1'])
        self.assertEquals(self.complexDict['key2'], self.complexDictProxy['key2'])
        self.assertEquals(self.complexDict['key3'][0], self.complexDictProxy['key3'][0])
        self.assertEquals(self.complexObj.intMember, self.complexObjProxy.intMember)
        self.assertEquals(self.complexObj.innerMember.innerStrMember, self.complexObjProxy.innerMember.innerStrMember)
        
        
    def testCall(self):
        self.assertEquals(84.42,            self.complexObjProxy.method1())
        self.assertEquals('method235hello', self.complexObjProxy.method2(intParam = 35, strParam = 'hello'))
        self.assertEquals(142,              self.complexObjProxy.method3(toAdd = 100))
        self.assertEquals(None,             self.complexObjProxy.method4())
    
    
    def testDelete(self):
        del self.complexDictProxy['key2']['subkey1']
        del self.complexDictProxy['key2']['subkey2']
        self.assertEquals({}, self.complexDict['key2'])
        del self.complexDictProxy['key3'][0]
        del self.complexDictProxy['key3'][0]
        self.assertEquals([], self.complexDict['key3'])
        del self.complexDictProxy['key1']
        del self.complexDictProxy['key2']
        del self.complexDictProxy['key3']
        self.assertEquals({}, self.complexDict)
    
    
    def testUpdate(self):
        self.complexDictProxy['key2']['subkey1'] = 'newvalue1'
        self.complexDictProxy['key2']['newsubkey'] = 'newvalue2'
        self.assertEquals('newvalue1', self.complexDict['key2']['subkey1'])
        self.assertEquals('newvalue2', self.complexDict['key2']['newsubkey'])
        self.complexObjProxy.intMember = 666
        self.assertEquals(666, self.complexObj.intMember)
        
    
        
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
    
