'''
Unit test of cPickle utility wrapper.
'''

import unittest

from helper.serializeutils import toString, fromString, fromFile
from test.framework.serializeutilsdata import C, A, B


class TestSerializeUtils(unittest.TestCase):
    '''
    Serialize unit tests.
    '''

        
    def testToStringStandard(self):
        # given
        cOrig = C()
        # when
        cStr = toString(cOrig)
        c = fromString(cStr)
        # then
        self._assertTypes(c)
        self._assertStandardValues(c)
    
        
    def testToStringNonStandard(self):
        # given
        aOrig = A(name = 'my,name,with,commas,and"apostrophes"')
        cOrig = C(simpleProp = 'si{mpleP}rop', myA = aOrig)
        # when
        cStr = toString(cOrig)
        c = fromString(cStr)
        # then
        self._assertTypes(c)
        self._assertNonStandardValues(c)


    def _assertTypes(self, obj):
        self.assertIsInstance(obj, C)
        self.assertIsInstance(obj.simpleProp, str)
        self.assertIsInstance(obj.myA, A)
        self.assertIsInstance(obj.myA.name, str)
        self.assertIsInstance(obj.myA.number, int)
        self.assertIsInstance(obj.myA.flag, bool)
        self.assertIsInstance(obj.myB, B)
        self.assertIsInstance(obj.myB.name, str)
        self.assertIsInstance(obj.myB.number, int)
        self.assertIsInstance(obj.myB.flag, bool)
        self.assertIsInstance(obj.myB.subProp, str)
        
        
    def _assertStandardValues(self, obj):
        self.assertEquals(obj.simpleProp, 'mySimpleProp')
        self.assertEquals(obj.myA.name, 'myName')
        self.assertEquals(obj.myA.number, 42)
        self.assertEquals(obj.myA.flag, True)
        self.assertEquals(obj.myB.name, 'myName')
        self.assertEquals(obj.myB.number, 42)
        self.assertEquals(obj.myB.flag, True)
        self.assertEquals(obj.myB.subProp, 'mySubProp')
    
    
    def _assertNonStandardValues(self, obj):
        self.assertEquals(obj.simpleProp, 'si{mpleP}rop')
        self.assertEquals(obj.myA.name, 'my,name,with,commas,and"apostrophes"')
        self.assertEquals(obj.myA.number, 42)
        self.assertEquals(obj.myA.flag, True)
        self.assertEquals(obj.myB.name, 'myName')
        self.assertEquals(obj.myB.number, 42)
        self.assertEquals(obj.myB.flag, True)
        self.assertEquals(obj.myB.subProp, 'mySubProp')



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
