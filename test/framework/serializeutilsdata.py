class A(object):
    '''
    Test class for serialization.
    Contains simple properties.
    '''
    def __init__(self, name = 'myName', number = 42, flag = True):
        self.name = name
        self.number = number
        self.flag = flag
        
        
    
class B(A):
    '''
    Test class for serialization.
    Is a subclass of A to test that parent properties get written.
    '''
    def __init__(self, name='myName', number=42, flag=True, subProp = 'mySubProp'):
        A.__init__(self, name=name, number=number, flag=flag)
        self.subProp = subProp


        
class C(object):
    '''
    Test class for serialization.
    Contains instances of A and B.
    '''
    def __init__(self, simpleProp = 'mySimpleProp', myA = A(), myB = B()):
        self.simpleProp = simpleProp
        self.myA = myA
        self.myB = myB